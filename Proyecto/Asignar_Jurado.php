
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
require_once ("./Modelo/DAO_Proyecto.php");
require_once ("./Modelo/DAO_Profesor.php");
require_once ("./Modelo/DAO_Rol.php");

?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
       <script>
            function confirmar()
            {
                document.getElementById("fechap").value = document.getElementById("fechao").value;
                if(confirm("Se guardarán los datos ¿Desea continuar?"|| document.getElementById("fechao").value)){
                    return true;
               }else{ return false;
	           }
	        }
           
        </script>
    </head>
    <body>
        <?php
        $bandera = htmlentities(addslashes(filter_input(INPUT_GET, "bandera")));
        $info1 = new DAO_Proyecto();
        $info = new DAO_Profesor();
        if($comite!=null || $coordinacion!=null){
            $numero = $info1->Consultar_Proyectos_Faltantes_Jurado();
            if($numero['numero'] >0){ ?>
                <div class="container well" >
                    <p><b><center><h4>Asignar Jurados</h4></center></b></p><hr>
                     <div class="col-sm-4">
                       <table class="table table-sm">
                            <thead>
                             <tr>
                                <th> <center> Fecha para concepto </center></th>
                                <th>
                                    <center> <input type="date" class="form-control-sm" name="fechao" id="fechao" >
                                    <div class="invalid-feedback">        
                                       Por favor, introduzca una fecha valida.
                                    </div>
                                    </center>
                                </th>
                            </tr>
                            </thead>
                            </table>
                    </div>
                    <table class="table table-bordered" >
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"><center>Título Proyecto</th>
                            <th scope="col"><center>Primer Jurado</th>
                            <th scope="col"><center>Segundo Jurado</th>
                            <th scope="col"><center>Guardar</th>
                        </tr>
                    </thead>
                <?php
                    //$lis = $info1->verPDF();
                    $lis = $info1->Listar_Proyectos_Faltantes_Jurado();
                    $temporal = '0';
                        foreach ($lis as $lis) { ?>
                            <form action="Control/Asignar_Jurado.php" method="POST" name="asignar_jurado" >
                                <?php 
                                //$id = $lis["Id_Proyecto"];
                                
                                //$listP_P = $info1->buscarP_P($id);
                                if($temporal != $lis["Id_Proyecto"]) { $temporal = $lis["Id_Proyecto"]?>
                                    <tr>
                                        <td style="display: none">
                                            <input type="text"  id="id_proyecto" name="id_proyecto" placeholder="" value="<?php echo ($lis["Id_Proyecto"]);?>" style="display: none">
                                        </td>
                                        <td><center>
                                            <?php echo $lis["Nombre_Proyecto"];?>
                                        </td>
                                <?php   if($lis['Evaluado_Pcb']!='01'  ) {?>
                                        <!-- Jurado 1-->
                                            <td> <center>
                                                <select id="jurado1" class="form-control-sm" name="jurado1"  onchange="ShowSelected()">
                                                    <option value="00"> Profesor </option>
                                                    <?php
                                                        $lis = $info->listarProfesorJurado();
                                                        foreach ($lis as $lis) {?>
                                                            <option value="<?php echo($lis['Cedula']); ?>" ><?php echo($lis['Nombre']); ?></option>
                                                    <?php }?>
                                                </select>
                                            </td>
                                        <!-- Fin Jurado 1-->
                                <?php }else{ ?>
                                        <!-- Jurado CB-->
                                        <td> <center>
                                            <select id="jurado1" class="form-control-sm" name="jurado1"  onchange="ShowSelected()">
                                                <option value="00"> Profesor Ciencias Básicas </option>
                                                <?php
                                                $lis = $info->listarProfesorJuradoPCB();
                                                    foreach ($lis as $lis) {?>
                                                        <option value="<?php echo($lis['Cedula']); ?>" ><?php echo($lis['Nombre']); ?></option>
                                                <?php }?>
                                            </select>
                                        </td>
                                        <!-- Fin Jurado CB-->
                                <?php } ?>
                                        <!-- Jurado 2-->
                                        <td><center>
                                            <select id="selectTipo" class="form-control-sm" name="jurado2" id="jurado2" required>
                                                <option value="00"> Profesor </option>
                                                <?php
                                                $lis = $info->listarProfesorJurado();
                                                foreach ($lis as $lis) {?>
                                                    <option value="<?php echo($lis['Cedula']); ?>" ><?php echo($lis['Nombre']); ?></option>
                                                <?php }?>
                                            </select>
                                        </td>   
                                        <!-- Fin Jurado 2-->
                                        </td>
                                        <td style="display: none">
                                            <input type="text" id="fechap" name="fechap" class="form-control">
                                        </td>
                                        <td><center>
                                            <input type="submit" id="btn" class="btn btn-outline-danger" value="Guardar" onclick="return confirmar();"> 
                                        </td>
                                        
                                    </tr>
                                    
                                </form>
                    <?php }}?>
                    </table>
                     <?php if($bandera == "0"){ ?>
                        <label>Hubo un problema con la asignación, por favor elija una opción y una fecha superior a 15 días.</label>
                        <?php }elseif($bandera=="1"){ ?>
                        <label> Jurados asignados </label>
                        <?php }elseif($bandera=="2") {?>
                        <label>Los jurados no pueden ser el mismo, por favor elija una opción</label>
                        <?php }elseif($bandera =='3') {?>
                        <label>El proyecto no puede tener como jurado y tutor al mismo profesor</label>
                        <?php }else{?>
                        <Label Por favor elija una opcion </label>
                        <?php }?>
                    
                        
                
        <!--<label for="carrera" style="color: black"></label>-->
          <?php }else{ ?>
          </div>
            <br>
            <div class ="container">
                <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
                    <div class="card-body ">
                        <p><center><h6>NO TIENE PROYECTOS ASIGNADOS COMO JURADO</h6></center></p><hr>
                    </div>
                </div>
            </div>
          
<?php }}else{
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
?>
    </body>
</html>
