<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './Modelo/DAO_Anteproyecto.php';
require_once './home.php';
ob_start();
$pdf = new DAO_Anteproyecto();
$nombre_proyecto = "";

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title></title>
       
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css" >
         <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
        <br>
        <div class="row" id="container-elementosR">

            <?php
            if ((filter_input(INPUT_POST, "buscar"))) {
                $nombre_proyecto = filter_input(INPUT_POST, "nombre");
            }
            ?>
            <?php
            if (!($_GET)) {
                echo "<script>location.href='Buscar_Proyectos.php?pagina=1';</script>";
            }
            ?>
            <form class="form-inline" method="post">
                <div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">Email</label>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Titulo del proyecto">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="nombre" class="sr-only">Password</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Titulo">
                </div>
                <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR"  name="buscar">
            </form>
        </div>
        <br>     
         <?php if ($coordinacion != null || $comite != null) { //aca solo muestra los que el comite avalo y el estado que tiene de coordinacion?> 
        <table class="table" id="table">
            <?php if ($coordinacion != null) { //aca solo muestra los que el comite avalo y el estado que tiene de coordinacion?> 
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Título del proyecto</th>
                        <th scope="col">Estado Coordinación</th>
                        <th scope="col">Fecha de Registro</th>
                        <th scope="col">Ver</th>
                    </tr>
                </thead>
                <?php
                if ($nombre_proyecto == NULL || $nombre_proyecto == ("")) {
                    
                } else {
                    $proy = new DAO_Anteproyecto();
                    $lista1 = $proy->Consultar_Anteproyecto_Titulo($nombre_proyecto);
                    $numero_de_registros = count($lista1);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Buscar_Proyectos.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $proy->Consultar_Anteproyecto_Titulo_paginacion($nombre_proyecto,$iniciar, $registros_x_pagina);
                    foreach ($lista2 as $lista) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lista["Id_Proyecto"]); ?> </th>
                            <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista["Estado"]); ?> </th>
                            <th scope="col"><?php echo($lista["Fecha_Registro"]); ?> </th>
                            <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>" target="_blank">Ver</a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            <?php } else if ($comite != null) { //Aqui muestra independientemente si el comite avalo o no
                ?>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Título del proyecto</th>
                        <th scope="col">Estado Comité</th>
                        <th scope="col">Fecha de Registro</th>
                        <th scope="col">Ver</th>
                    </tr>
                </thead>
                
                <?php
                if ($nombre_proyecto == NULL || $nombre_proyecto == ("")) {
                    
                } else {
                    $proy1 = new DAO_Anteproyecto();
                    $lista_ant = $proy1->Consultar_Anteproyecto_Titulo_Comite($nombre_proyecto);
                    $numero_de_registros = count($lista_ant);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Buscar_Proyectos.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $proy1->Consultar_Anteproyecto_Titulo_Comite_paginacion($nombre_proyecto,$iniciar, $registros_x_pagina);
                    foreach ($lista2 as $lista) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lista["Id_Proyecto"]); ?> </th>
                            <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista["Estado"]); ?> </th>
                            <th scope="col"><?php echo($lista["Fecha_Registro"]); ?> </th>
                            
                            <?php if($lista["Estado"]=="Avalado"){ 
                            ?>
                                    <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></td>
                            <?php }else if($lista["Estado"]=="Devuelto"){
                            ?>
                                    <th class="bot"><a href="Informacion_anteproyectos_devueltos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></th>
                            <?php } ?>
                        </tr>
                <?php
                    }
                }
                ?>
            <?php
            } ?>
        </table>
         <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Buscar_Proyectos.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Buscar_Proyectos.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                    <?php
                    endfor;
                    ?>
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Buscar_Proyectos.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
       <?php }else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>
    </body>
</html>


