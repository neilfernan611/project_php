<?php
require_once './home.php';
ob_start();
require_once ('./Modelo/DAO_Comite.php');

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">

        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar este usuario?'))
                    return false;
            }

        </script>
    </head>
    <body>
        <br>
        <?php
        if ($administrador != null) {
            ?>
            <?php
            if (!($_GET)) {

                echo "<script>location.href='Info_Comite.php?pagina=1';</script>";
            }
            ?>

            <form action="<?php echo filter_input(INPUT_SERVER, 'PHP_SELF') ?>" method="POST">
                <table class="table table-responsive-xl" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">

                        <tr >
                            <th scope="col">Código</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Teléfono</th>
                            <th scope="col">Borrar</th>
                            <th scope="col">Actualizar</th>
                        </tr>
                    </thead>
                    <?php
                    $info = new DAO_Comite;
                    $lista = $info->listarTablaComite("SELECT COMITE.Codigo_C,COMITE.Correo, PERSONA.Telefono FROM COMITE, PERSONA WHERE PERSONA.Correo=COMITE.Correo;");
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 15;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        header('Location:Info_Comite.php?pagina=1');
                    }


                    $lista2 = $info->listarTablaComite("SELECT COMITE.Codigo_C,COMITE.Correo, PERSONA.Telefono FROM COMITE, PERSONA WHERE PERSONA.Correo=COMITE.Correo LIMIT " . $iniciar . "," . $registros_x_pagina . ";");
                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Codigo_C"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Correo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Telefono"]); ?> </th>
                            <td class="bot"><a href="Control/Eliminar_Usuario.php?idco=<?php echo("comite");?>&correo=<?php echo ($lis["Correo"]); ?> "onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Comitee.php?idco=<?php echo ($lis["Codigo_C"]); ?>&correo=<?php echo ($lis["Correo"]); ?>&telefono=<?php echo($lis["Telefono"]); ?>"><img src="Img/update.png"></a></td>
                        </tr>
                        <?php
                    }
                    ?>

                </table>
                <!--Fin tabla-->
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Info_Comite.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                        <?php for ($i = 0; $i < $paginas; $i++): ?>
                            <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Info_Comite.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                            <?php
                        endfor;
                        ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Info_Comite.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                    </ul>
                </nav>
            </form>   

            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>
    </body>
</html>




