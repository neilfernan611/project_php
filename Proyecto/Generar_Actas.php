<?php
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Notas.php';
require_once './home.php';
$pdf = new DAO_Proyecto();
$datos=$_SESSION["informacion2"];
$nombre_proyecto = "";
?>
<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    
    
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
$id_profesor = $datos['Cedula']
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos.css">

        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css" >


    </head>
    <body>

        <br>
        <div class="row" id="container-elementosR">

            <?php  
            
            if ($COMITE =! NULL) {
                ?>
                           </div>
            <br>        

            <table class="table" id="table">
                <thead class="thead-dark">
                    <!--<tr  align="center" valign="middle">
                        <th colspan="7"><h6><p style="color:white"><center>Información Coordinación</center></p></h6></th></tr>-->
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Titulo del proyecto</th>
                        <th scope="col">Ver datos del acta</th>

                    </tr>
                </thead>
                <?php
                    $proy = new DAO_Notas();
                    $lista_ant = $proy->tabla_acta();
                      foreach ($lista_ant as $lista) {
                         $aux=$proy->verificar_calificado($lista["Id_Proyecto"]);
                         if ($aux==0){
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lista["Id_Proyecto"]); ?> </th>
                            <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                            <td class="bot"><a href="Informacion_Acta.php?id=<?php echo($lista["Id_Proyecto"]);?>  ">ver</a></td>
                        </tr>
                        <?php
                                      }       
                                                     }
                        ?>
            </table>
            <?php
        } else {
            session_destroy();
            //header("Location: index.php");
        }
        ?>
    </body>
</html>

