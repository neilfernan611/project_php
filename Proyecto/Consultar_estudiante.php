<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Estudiante.php';

$codigo = "";

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <?php
        if ((filter_input(INPUT_POST, "buscar"))) {
            $codigo = filter_input(INPUT_POST, "codigo");
        }
        ?>

        <br>
        <form class="form-inline" method="post">
            <div class="form-group mb-2">
                <label for="staticEmail2" class="sr-only">Código</label>
                <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Código del estudiante">
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <label for="nombre" class="sr-only"> </label>
                <input type="number" class="form-control" name="codigo" id="nombre" placeholder="Codigo" min="1" max="99999999999" title="Un código estudiantil cuenta con 11 digitos numéricos sin espacios">
            </div>
            <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR"  name="buscar">
        </form>
        <br>     
        <table class="table" id="table">
            <?php
            if ($comite != null) {
                ?>
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Título del proyecto</th>
                        <th scope="col">Fecha de registro</th>
                        <th scope="col">Estado comité</th>
                        <th scope="col">Ver</th>

                    </tr>
                </thead>
                <?php
                if ($codigo == null || $codigo == ("")) {
                    
                } else {
                    $proy = new DAO_Estudiante();
                    $lista_proy = $proy->BuscarEstudianteComite($codigo);
                    foreach ($lista_proy as $lista_p) {
                        ?>

                        <tr>
                            <th scope="col"><?php echo($lista_p["Id_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista_p["Nombre_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista_p["Fecha_Registro"]); ?> </th>
                            <th scope="col"><?php echo($lista_p["Estado_Comite"]); ?> </th>
                            
                            <?php if($lista_p["Estado_Comite"]== 'Avalado'){ ?>
                                <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista_p["Id_Proyecto"]); ?>">Ver</a></td>
                            <?php } else if($lista_p["Estado_Comite"]== 'Devuelto'){ ?>
                                <th class="bot"><a href="Informacion_anteproyectos_devueltos.php?id=<?php echo($lista_p["Id_Proyecto"]); ?>">Ver</a></th>
                            <?php   }
                            ?>
                        </tr>
                        <?php
                    }
                }
            } else if ($coordinacion != null) {
                ?>
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Título del proyecto</th>
                        <th scope="col">Fecha de registro</th>
                        <th scope="col">Ver</th>

                    </tr>
                </thead>
                <?php
                if ($codigo == null || $codigo == ("")) {
                    
                } else {
                    $proy = new DAO_Estudiante();
                    $lista_proy = $proy->BuscarEstudiante($codigo);
                    foreach ($lista_proy as $lista_p) {
                        ?>

                        <tr>
                            <th scope="col"><?php echo($lista_p["Id_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista_p["Nombre_Proyecto"]); ?> </th>
                            <th scope="col"><?php echo($lista_p["Fecha_Registro"]); ?> </th>

                            <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista_p["Id_Proyecto"]); ?>">Ver</a></td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>

    </body>
</html>
