<?php
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Notas.php';
require_once './home.php';
$pdf = new DAO_Proyecto();
$datos=$_SESSION["informacion2"];
$nombre_proyecto = "";
?>
<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    
    
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
$id_profesor = $datos['Cedula']
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
<?php   if ($profesor != NULL) {?>
            <br><div class="container">
                <table class="table table-bordered" >
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"><center>Titulo del proyecto</th>
                            <th scope="col"><center>Radicado</th>
                            <th scope="col"><center>Calificar</th>
                        </tr>
                    </thead>
                <?php
                    $proy = new DAO_Notas();
                    $lista_ant = $proy->tabla_calificar_jurado($id_profesor);
                    $aux = "";
                    foreach ($lista_ant as $lista) {
                    $verificar = $proy->verificar_calificacion_jurado($lista["Id_Proyecto"],$id_profesor);
                       if ($aux != $lista["Id_Proyecto"] && $verificar['conteo'] == '0'){?>
                       <tbody>
                            <tr>
                                <td><center> <?php echo($lista["Nombre_Proyecto"]); ?> </td>
                                <td><center><?php echo($lista["Id_Proyecto"]); ?> </td>
                                <td class="bot"><center><a href="Nota_Jurado.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Calificar</a></td>
                            </tr>
                        </tbody>
                <?php
                $aux = $lista["Id_Proyecto"];
                    }}?>
                </table>
            </div>
<?php   }else{
            session_destroy();
            //header("Location: index.php");
            }?>
    </body>
</html>

