<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Profesor.php';
$info=$_SESSION['informacion2'];
$id_proyecto=$info['Id_Proyecto'];
?>

<?php

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        
    </head>
    <body>
        
       
<?php
if($estudiante!=null){
$proy = new DAO_Proyecto();
$prof = new DAO_Profesor();

$cantidad = $prof -> cantidad_tut($id_proyecto);
    if($cantidad["cantidad"]==2)
    { $lista = $proy->Consultar_Proyectos($id_proyecto);
?>
        <div class="container well">
            <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
                <div class="card-body ">
                    <b><h2><?php echo ($lista["Nombre_Proyecto"]); ?> </h2></b>
                    <p><b>Número de registro: </b> 
                    <?php echo ($lista["Radicado"]); ?> <br></p>
                    <p><b> Directores del proyecto </b></p>
                    <p>&nbsp; <?php echo ($lista["n_tutor1"])?> </p>
                    <p>&nbsp; <?php echo ($lista["n_tutor2"])?> <br>
                    <!--
                    <?php echo "Tutor: " .  ($lista["n_tutor1"])?>  <br>
                    <?php echo "Estado del proyecto: " . ($lista["estadot1"]); ?> <br>
                    <?php echo "Tutor: " .  ($lista["n_tutor2"]) ?>  <br>
                    <?php echo "Estado del proyecto: " . ($lista["estadot2"]);?><br>-->
                    <a href="Ver_Todo.php">Ver Información <img src="Img/ver.png"></a></p>
                </div>   
            </div>
        </div>
<?php }else{ $lista = $proy->Consultar_ProyectoT($id_proyecto);?>
         <div class="container">
            <div class="card">
                <div>
                    <b><h2><?php echo ($lista["Nombre_Proyecto"]); ?> </h2></b>
                    <p><b>Número de registro: </b> 
                    <?php echo ($lista["Radicado"]); ?> <br></p>
                    <p><b> Director del proyecto </b></p>
                    <p>&nbsp; <?php echo ($lista["Nombre"])?> </p>
                    <a href="Ver_Todo.php"><img src="Img/ver.png"></a></td>
                </div>   
            </div>
        </div>      
<?php }}else{
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
?>
    </body>
</html>
