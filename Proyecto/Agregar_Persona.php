<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
    </head>
    <body>
 <?php
        if ($administrador != null) {
            ?>

        <div class="container well" id="container-registro">

            <div class="py-5 text-center" id="centrar">

                <p class="lead" >Ingrese la información en todos los campos</p>
            </div>

            <div class="row" id="container-elementosR">

                <div class="col-md-8 order-md-1">

                    <!-- class="needs-validation" novalidate-->
                    <form action="Control/Agregar_Persona.php" method="POST" id="form-registro" >
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName" style="color: black">Nombre Completo</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                    El nombre es obligatorio.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="email" style="color: black">Email <span class="text-muted"></span></label>
                                <input type="email" class="form-control" name="email" required placeholder="you@example.com">
                                <div class="invalid-feedback">
                                    Por favor, introduzca una dirección de correo electrónico válida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="docuemnto" style="color: black">Cédula</label>
                                <input type="text" class="form-control" name="cedula" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                    El número de documento es necesario
                                </div>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="telefono" style="color: black">Número celular</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">

                                    </div>
                                    <input type="number" class="form-control" name="telefono" id="telefono" ng-model="number" onKeyPress="if(this.value.length==10) return false;" min="0" placeholder="222 222 22 22" required>
                                    <div class="invalid-feedback" style="width: 100%;">
                                        El celular es obligatorio.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="col-md-8 mb-1">
                        <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Información</button>
                    </form>
                </div>
            </div>
        </div>
    <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>
