<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
require_once './Modelo/DAO_Modalidad.php';
require_once './Modelo/DAO_LineaInvestigacion.php';
require_once './Modelo/DAO_Estudiante.php';
require_once './Modelo/DAO_Grupo_Investigacion.php';
require_once './Modelo/DAO_Proyecto.php';
$informacion=$_SESSION['informacion2'];
$carrera=$informacion['Id_Carrera'];
?>


<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css">

       

  <script>
              function confirmar(){
            if(!confirm('¿Está seguro de enviar el Proyecto? Verifique si su archivo está completo de lo contrario no podra realizar modificaciones en la inscripción'))
                  e.preventDefault();
        return false;  
              }  
                   
        </script>

    </head>
    <body>
        <br>
<?php
        if($estudiante!=null){
            
        
            ?>
        <!-- formulario subir anteproyecto -->
        <!--<div class="container well" id="container-registro">-->
        <div class="py-4 text-center">
            <p><center>Subir Documento</center></p>
    </div>
    <div class="col-md-10 order-md-1 container"  >
        <form action="Control/Subir_Proyecto_Monografia.php" method="POST" id="form-registro" enctype="multipart/form-data" >
             <div class="col-md-8 order-1 container"  >
                 <div class="row">
                    <div class="col-auto">
                        
                        <input type="file" name="archivo" aria-describedby="inputGroupFileAddon01" required  > &nbsp;
                        <button class="btn1" id="btn-aceptar" type="submit" onclick="confirmar();" >Subir </button>
                        
                    </div>
                </div>
            </div>
            
        </form>
    </div>
<?php
        }else{
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
?>
</body>
</html>
