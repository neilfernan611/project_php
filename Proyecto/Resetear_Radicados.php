<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?PHP
require_once './home.php';
require_once './Modelo/DAO_Radicado.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title></title>
         <link rel="stylesheet" href="css/estilos.css">
        
    </head>
    <body>

        <?php
         if($administrador!=null)
        {

        if (filter_input(INPUT_POST, "Resetear")) {
            $radicado = new DAO_Radicado();
            $radicado->Reset_consecutivo_Radicado();
            ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Éxito!</strong> Reinicio Aceptado.
            </div>
            <?php
        }
        ?>
        <br>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading"><center>RESETEO DE CONSECUTIVO DE LOS RADICADOS</center></div><br>
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-sm-12 mb-12">
                                    <center><div class="col-sm-9 mb-9">
                                        <p style="text-align: justify; font-size:18px">Bienvenido a la opción de reinicio de consecutivo de los radicados. <br><strong>Recuerde</strong> que esta opción solo debe 
                                            ser utilizada una vez cada año, cuando se requiere reiniciar los consecutivos utilizados para la generación de radicados,
                                        <i>¡una vez reiniciado no se puede devolver esta accion!</i>; el primer radicado utilizara el consecutivo 001</p>

                                    </div>
                                    <div class="col-sm-6 mb-6">
                                        <form name="reset" method="POST">
                                            <input type="submit" class="btn btn-primary mx-sm-6 mb-6" id="btn-aceptarR" name="Resetear" value="Resetear">
                                        </form>
                                    </div></center>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
} else {
  session_destroy();
  echo "<script>location.href='index.php';</script>";
  } 
?>
    </body>
</html>
