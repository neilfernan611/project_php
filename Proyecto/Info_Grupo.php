<?php
require_once './home.php';
require_once ('./Modelo/DAO_Grupo.php');
require_once ("./Modelo/Codigos.php");

?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar este grupo de investigación?'))
                    return false;
            }

        </script>

    </head>
    <body>

        <br>
        <?php
      
        if ($administrador != null) {
                
                 if (!($_GET)) {
                  
                   echo "<script>location.href='Info_Grupo.php?pagina=1';</script>";
                  } 
                 
            if (filter_input(INPUT_POST, "btn")) {

                $grupo = htmlentities(addslashes(filter_input(INPUT_POST, "grupo")));
                $esta = htmlentities(addslashes(filter_input(INPUT_POST, "esta")));
                $g = new Codigos();
                $id_grupo = $g->getPINSoli(3);

                $agregargrupo = new DAO_Grupo();
                $agregar = 0;
                $verifi = $agregargrupo->verificar_grupo($id_grupo, $grupo, $agregar, $esta);

                if ($verifi == true) {
                    $agega = $agregargrupo->insertarGrupo($id_grupo, $grupo, $esta);
                    ?>

                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Éxito!</strong> Grupo de investigación agregado.
                    </div>
                <?php } else {
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Fallo!</strong> Verifique la información ingresada
                    </div>   

                    <?php
                }
            }
            ?>
            <!--Inicio tabla-->
            <form action="<?php echo filter_input(INPUT_SERVER, 'PHP_SELF'); ?>" method="POST">
               
                <table class="table" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">
                        <tr >
                            <th scope="col">Código</th>
                            <th scope="col">Grupo</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Borrar</th>
                            <th scope="col">Actualizar</th>
                        </tr>
                    </thead>
                    <?php
                    $info = new DAO_Grupo();
                    $lista = $info->listarTablaGrupo();
            
                      $numero_de_registros = count($lista);
                      
                      $registros_x_pagina = 5;
                      $paginas = ceil($numero_de_registros / $registros_x_pagina);
                      $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                      if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                      header('Location:Info_Grupo.php?pagina=1');
                      } 


                     $lista2 = $info->listarTablaGrupo_paginacion($iniciar, $registros_x_pagina);
                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Id_Grupo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Grupo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Estado"]); ?> </th>

                            <td class="bot"><a href="Control/Eliminar_Grupo.php?idg=<?php echo ($lis["Id_Grupo"]); ?>"onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Grupoo.php?idg=<?php echo ($lis["Id_Grupo"]); ?>&grupo=<?php echo ($lis["Grupo"]); ?>&estado=<?php echo ($lis["Estado"]); ?>"><img src="Img/update.png"></a></td>
                        </tr>
                        <?php
                    }
                    ?> 
                    <tr>

                        <td><input type='hidden' name='id' size='10' class='centrado'></td>

                        <td>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <input type="text" class="form-control" id="linea" name="grupo"  size='10' class='centrado' required value=""> 
                                    <div class="invalid-feedback">
                                        Completa campo
                                    </div>

                                </div>

                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-md-6 mb-3">


                                    <select id="selectTipo" class="custom-select" required name="esta" size='1' class='centrado' required>
                                        <option required value="04"><?php echo('Habilitado'); ?></option>
                                        <option required value="05"><?php echo('Deshabilitado'); ?></option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Campo obligatorio.
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class='bot'><input type='submit' class="btn btn-primary mb-2" id="btn-aceptarR" name='btn' value='Insertar'></td>
                        <td></td>
                    </tr>

                </table>
                <!--Fin tabla-->
            </form>   
             <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Info_Grupo.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
            <?php for ($i = 0; $i < $paginas; $i++): ?>
                                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Info_Grupo.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                <?php
            endfor;
            ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Info_Grupo.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>


