<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './Modelo/DAO_Anteproyecto.php';
require_once './Modelo/DAO_Profesor.php';
require_once './home.php';
ob_start();
$pdf = new DAO_Anteproyecto();
$objprofesor = new DAO_Profesor();
?>

<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    $datos = $_SESSION["informacion2"];
    $cedula = $datos['Cedula'];
    
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <?php
        if ($profesor != null) {
            
            ?>
            <?php
            if (!($_GET)) {

                echo "<script>location.href='Anteproyecto_Evaluador.php?pagina=1';</script>";
            }
            ?>


            <br>
            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                       <!-- <th colspan="9"><h2><p style="color:white"><center>Información Coordinación</center></p></h2></th></tr>-->
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Título del Anteproyecto</th>
                        <th scope="col">Fecha de registro</th>
                        <th scope="col">Estado evaluador</th>
                        <th scope="col">Ver</th>


                    </tr>
                </thead>
                <?php
                $proy = new DAO_Anteproyecto();
                     $lista = $proy->Consultar_Anteproyectos_Evaluador($cedula);
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 10;
                    
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if($paginas==0){
                        $paginas=1;
                        ?>
                   <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong></strong>No hay Anteproyectos nuevos.
                    </div>
                   <?php    
                    }
                    if ((filter_input(INPUT_GET, 'pagina') > $paginas) || (filter_input(INPUT_GET, 'pagina') <= 0)) {
                       echo "<script>location.href='Anteproyecto_Evaluador.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                
                
                $lista_ant = $proy->Consultar_Anteproyectos_Evaluador_paginacion($cedula,$iniciar, $registros_x_pagina);
                $id_2 = "calificar";
                foreach ($lista_ant as $lista) {
                    ?>

                    <tr>
                        <th scope="col"> <?php echo($lista["Id_Proyecto"]); ?></th>
                        <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                         <th scope="col"> <?php echo($lista["Fecha_Registro"]); ?> </th>
                         <th scope="col"> <?php echo($lista["Estado"]); ?> </th>
                        <th class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>&estado=<?php echo($id_2); ?>">Ver</a></th>
                    </tr>

                    <?php
                }
                ?>
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Anteproyecto_Evaluador.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Anteproyecto_Evaluador.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Anteproyecto_Evaluador.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
            
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>
    </body>
</html>


