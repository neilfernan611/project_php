<?php

require_once "../Modelo/DAO_Grupo.php";
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');

if (!filter_input(INPUT_POST, "bot_grupo")) {
    header("Location:../Editar_Grupoo.php");
} else {
    $id = htmlentities(addslashes(filter_input(INPUT_POST, "id")));
    $grupo = htmlentities(addslashes(filter_input(INPUT_POST, "gru")));
    $estado = htmlentities(addslashes(filter_input(INPUT_POST, "estado")));
    $objG = new DAO_Grupo();
    $editar= 1 ;
    $verifi = $objG->verificar_grupo($id, $grupo,$editar,$estado);
     if ($verifi == true) {
        $actualizar = $objG->actualizarGrupo($id, $grupo, $estado);
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Bien hecho!</h4>
            <p>¡Información Actualizada.</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../Info_Grupo.php'>");
     }else{
          echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Advertencia!</h4>
            <p>¡¡Verifique la información ingresada, dos grupos no pueden tener el mismo nombre o codigo.</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../Info_Grupo.php'>");
         
     }
    
    header("Location:../Info_Grupo.php");
    
}
