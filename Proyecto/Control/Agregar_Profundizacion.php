<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
session_start();
require_once '../Modelo/DAO_Anteproyecto.php';
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/Email.php';

$proyecto = new DAO_Anteproyecto();
$proyectoc = new DAO_Proyecto();
$estudiante = new DAO_Estudiante();

$info = $_SESSION['informacion2'];
$id_proyecto = $info['Id_Proyecto'];
$carrera = $info['Id_Carrera'];
$nombre_archivo = $_FILES['archivo']['name'] = $id_proyecto.".pdf";
//$carta_c = $_FILES['carta_c']['name'] = $id_proyecto.".pdf";
$ruta = $_FILES['archivo']['tmp_name'];
//$rutac = $_FILES['carta_c']['tmp_name'];
$tam = $_FILES['archivo']['size'];
//$tamc = $_FILES['carta_c']['size'];
$codigoe1 = $info['Codigo'];
$error = $_FILES['archivo']['error'];
$numero= 3145728;

if ($tam > 0 && $tam <= $numero /*&& $tamc > 0 && $tamc <= $numero*/) {

    if ($nombre_archivo != "" /*&& $carta_c != ""*/) {
        $ObjEmail = new Email();
        $tutor ='0';
        $evaluador = '0';
        $duracion = '00';
        $fecha = date('Y-m-d');
        $linea = '00';
        $grupo = '00';
        $modalidad = htmlentities(addslashes(filter_input(INPUT_POST, "modalidad")));
        $nombre_ante = htmlentities(addslashes(filter_input(INPUT_POST, "nombred")));
        $resumen = 'No Aplica';
        $objetivo_general = 'No Aplica';
        $objetivos = 'No Aplica';
        $trabajo1 = htmlentities(addslashes(filter_input(INPUT_POST, "Trabajo1")));
        $trabajo2 = htmlentities(addslashes(filter_input(INPUT_POST, "Trabajo2")));
        $rol = "05";
        $rolev = "03";
        
        $destino_pro = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/archivos/" . $nombre_archivo;
        /*$destino_carta_c = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/Carta_Consejo/" . $carta_c;*/
        
        if ((copy($ruta, $destino_pro)) /*&& (copy($rutac, $destino_carta_c))*/) {
   
            $proyecto->Estudiante_Agrega_AnteProyecto($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $grupo, $duracion, $objetivo_general, $objetivos, $resumen);
            /*$proyecto->Documentos_modalidades($id_proyecto, $carta_c, $id_proyecto);*/
            $proyecto->Agregar_tutor($tutor, $rol, $id_proyecto);
            $proyecto->Agregar_tutor($evaluador, $rolev, $id_proyecto); 
            
            if ($trabajo1 == "02" && $trabajo2 == "03") {
                $espacio_academico = "02";
                $espacio_academico2 = "03";
                $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico2);
                                            
            }else if($trabajo1 == "" && $trabajo2 == "" && $carrera == "4828"){
                $espacio_academico = "01";
                $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                            
            } else if ($trabajo2 == "") {
                $espacio_academico = "02";
                $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                            
            } else if ($trabajo1 == "") {
                $espacio_academico = "03";
                $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
            }else if($trabajo1 == "" && $trabajo2 == "" && $carrera != "4828"){
                 $espacio_academico = "02";
                 $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
            }
            
            $estadoe = "02";
            $proyecto->estados_proyecto($id_proyecto);
            $proyecto->Actualizar_Estado_Evaluador($id_proyecto, $estadoe, $fecha);
            $nomodalidad = $proyecto->nombre_modalidad($modalidad);
            
            $mensaje=utf8_decode("Buen día se ha registrado a la modalidad de ".$nomodalidad['Modalidad']);
            $asunto="Registro de modalidad de anteproyecto";
            $destino=$info['Correo']; 
        
            $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje); 
                       
             echo('<br><br><br><br>
                   <div class="alert alert-success" role="alert">
                   <h4 class="alert-heading">Registro Exitoso!</h4>
                   <p>Por favor dirijase a la opción Proyecto/Ver estado.</p>
                   <hr>
                   <p class="mb-0"></p>
                   </div>');           
            echo("<meta http-equiv='refresh' content='6;URL=../home.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
                  <div class="alert alert-danger" role="alert">
                  <h4 class="alert-heading">Algo ha salido mal!</h4>
                  <p>Ruta vacía.</p>
                  <hr>
                  <p class="mb-0"></p>
                  </div>');
            echo("<meta http-equiv='refresh' content='6;URL=../home.php'>"); //redirects after 3 seconds
        }
    }else{
        echo('<br><br><br><br>
              <div class="alert alert-danger" role="alert">
              <h4 class="alert-heading">Algo ha salido mal!</h4>
              <p>Algún documento vacío</p>
              <hr>
              <p class="mb-0"></p>
              </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
        
    }
} else {
    echo('<br><br><br><br>
          <div class="alert alert-danger" role="alert">
          <h4 class="alert-heading">Algo ha salido mal!</h4>
          <p>El documento excedió el tamaño máximo de 3mb</p>
          <hr>
          <p class="mb-0"></p>
          </div>');
    echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
}
