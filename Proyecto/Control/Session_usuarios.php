<?php

require_once '../Modelo/DAO_Administrador.php';
require_once '../Modelo/DAO_Comite.php';
require_once '../Modelo/DAO_Coordinacion.php';
require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Profesor.php';


try {

    session_start();

    $objAdminDAO = new DAO_Administrador();
    $objComitDAO = new DAO_Comite();
    $objCoorDAO = new DAO_Coordinacion();
    $objEstDAO = new DAO_Estudiante();
    $objPerDAO = new DAO_Persona();
    $objProDAO = new DAO_Profesor();
    $usu;
    $pass;
    $usu = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
    $pass = htmlentities(addslashes(filter_input(INPUT_POST, "pass")));
    $pass_encriptado= password_hash($pass,PASSWORD_DEFAULT,['cost'=>5]);
    
    $Admin = $objAdminDAO->SessionAdminin($usu);
    $Comit = $objComitDAO->SessionComite($usu);
    $Coordi = $objCoorDAO->SessionCoordinacion($usu);
    $estudiante = $objEstDAO->SessionEstudiante($usu);
    $Profe = $objProDAO->SessionProfesor($usu);

    if (strcasecmp($usu, $Admin['Correo']) === 0 && (!isset($_SESSION['administrador']))) {
        if ( password_verify($pass, $Admin['Pass_A']) ||  $Admin['Pass_A'] == $pass) {
            //si coincide usuario y password y además no hay sesión iniciada
            $persona = $objPerDAO->infoUser($Admin['Correo']);
            $_SESSION["administrador"] = $persona['Nombre'];
            $_SESSION["nombre"] = $persona['Nombre'];
            $_SESSION["correo"] = $Admin['Correo'];
            $_SESSION["cedula"] = $persona['Cedula'];
            $_SESSION["informacion"] = $persona;

            header("Location: ../home.php");
        } else {
            echo "<script>location.href='../index.php?id=error';</script>";
        session_destroy();
        }
    } else if (strcasecmp($usu ,$Comit['Correo']) === 0 && password_verify($pass, $Comit['Pass']) && (!isset($_SESSION["comite"]))) {

        $persona = $objPerDAO->infoUser($Comit['Correo']);
        $_SESSION["comite"] = $persona['Nombre'];
        $_SESSION["nombre"] = $persona['Nombre'];
        $_SESSION["correo"] = $Comit['Correo'];
        $_SESSION["cedula"] = $persona['Cedula'];
        $_SESSION["informacion"] = $persona;

        header("Location: ../home.php");
    } else if (strcasecmp($usu,$estudiante['Correo']) === 0 && password_verify($pass, $estudiante['Pass_E']) && (!isset($_SESSION["estudiante"]))) {

        $persona = $objPerDAO->infoUser($estudiante['Correo']);
        $estu = $objEstDAO->InformacionEstudiante($usu);
        $_SESSION["estudiante"] = $persona['Nombre'];
        $_SESSION["nombre"] = $persona['Nombre'];
        $_SESSION["correo"] = $estudiante['Correo'];
        $_SESSION["informacion"] = $persona;
        $_SESSION['informacion2'] = $estu;

        header("Location: ../home.php");
    } else if (strcasecmp($usu,$Coordi['Correo']) === 0 && password_verify($pass, $Coordi['Pass_C']) && (!isset($_SESSION["coordinacion"]))) {

        $persona = $objPerDAO->infoUser($Coordi['Correo']);

        $_SESSION["coordinacion"] = $persona['Nombre'];
        $_SESSION["nombre"] = $persona['Nombre'];
        $_SESSION["correo"] = $Comit['Correo'];
        $_SESSION["cedula"] = $persona['Cedula'];
        $_SESSION["informacion"] = $persona;

        header("Location: ../home.php");
    } else if (strcasecmp($usu,$Profe['Correo']) === 0 && password_verify($pass, $Profe['Pass_P']) && (!isset($_SESSION['profesor']))) {
        //si coincide usuario y password y además no hay sesión iniciada
        $persona = $objPerDAO->infoUser($Profe['Correo']);
        
        $_SESSION["profesor"] = $persona['Nombre'];
        $_SESSION["nombre"] = $persona['Nombre'];
        $_SESSION["correo"] = $Profe['Correo'];
        $_SESSION["informacion2"] = $Profe;
        $_SESSION["cedula"] = $persona['Cedula'];
        $_SESSION["informacion"] = $persona;
       	
       	header("Location: ../home.php");
    }
     else {
        
        echo "<script>location.href='../index.php?id=error';</script>";
        session_destroy();
    }
} catch (Exception $ex) {

    echo $ex->getLine() . "<br>";
    //echo $ex->getMessage();
}



