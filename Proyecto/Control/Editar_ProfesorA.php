<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}

require_once "../Modelo/DAO_Persona.php";
require_once "../Modelo/DAO_Profesor.php";

if ($administrador != null) {

    $ced = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
    $nom = htmlentities(addslashes(filter_input(INPUT_POST, "nombre")));
    $co_n = htmlentities(addslashes(filter_input(INPUT_POST, "correo")));
    $tele = htmlentities(addslashes(filter_input(INPUT_POST, "tel")));
    $pertenecer = htmlentities(addslashes(filter_input(INPUT_POST, "pertene")));

    $personaobj = new DAO_Persona();
    $verificar= $personaobj->Existe_correo($co_n);
    $telef =  $personaobj->infoUser($co_n);
        
    if($verificar && ($telef['Correo'] != $co_n)){
        echo('<br><br><br><br>
             <div class="alert alert-danger" role="alert">
             <h4 class="alert-heading">Algo ha salido mal!</h4>
             <p>Lo sentimos! Ha ocurido un error, por favor verifique los datos ingresados</p>
             <hr>
             <p class="mb-0"></p>
             </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../Consultar_Profesor.php'>"); //redirects after 3 seconds
    }else{
        $obPro = new DAO_Profesor();
        $co_a = $obPro->correoProfesor($ced);
        
        $objp = new DAO_Persona();
        $veri = $objp->editarPersona($co_a['Correo'], $co_n, $tele);
        $obPro->editarCienciasBasicas($ced, $pertenecer);
        
        if ($veri == FALSE) {
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Lo sentimos! Ha ocurido un error, por favor verifique los datos ingresados</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=..Editar_Profesor.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados!</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../Consultar_Profesor.php'>"); //redirects after 3 seconds
        }   
    }
}

