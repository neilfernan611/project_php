<?php

require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/GenerarPass.php';
require_once '../Modelo/Email.php';
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
$contra = new GenerarPass();
$persona = new DAO_Persona();
$email = new Email();
$pass = $contra->randomPass(6); //Genra la contraseña
$correo = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
$cedula = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
$verificar = $persona->Verificar_CorreoyCedula($correo, $cedula);
if ($verificar==1) {
    $pass_encriptada = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 5]);
    $resp = $persona->reset_pass($correo, $pass_encriptada, $cedula);
    $asunto = utf8_decode("Cambio de contraseña");
    $mensaje = "Nueva contraseña: " . $pass;

    if ($resp) {
        $email->Enviar_Mail($correo, $asunto, $mensaje);
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Bien hecho!</h4>
            <p>La contraseña se ha actualizado con exito, y ha sido enviada a su correo. <br> 
            ¡Recuerde cambiar la contraseña la primera vez que ingrese a la plataforma!</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../index.php'>");
    } else {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Ha ocurrido un error verifique la información ingresada! DOS</h4>
            <p>No se ha podido encontrar el usuario.<br>
            </p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../index.php'>");
    }
} else {
    echo('<br><br><br><br>
        <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Ha ocurrido un error verifique la información ingresada! UNO</h4>
        <p>No se ha podido encontrar el usuario.<br>
        </p>
        <hr>
        <p class="mb-0">
        </p>
        </div>');
    echo ("<meta http-equiv='refresh' content='4;URL=../index.php'>");
}
