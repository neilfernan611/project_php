<?php
require_once '../Modelo/DAO_Anteproyecto.php';
$obja = new DAO_Anteproyecto();
$fechad = htmlentities(addslashes(filter_input(INPUT_POST,"fechadesde")));
$fechaa = htmlentities(addslashes(filter_input(INPUT_POST,"fechahasta")));
$tutor = htmlentities(addslashes(filter_input(INPUT_POST,"tutor")));
/** Incluye PHPExcel */
require_once '../PHPExcel/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Universidad Distrital")
							 ->setLastModifiedBy("Universidad Distrital")
							 ->setTitle("Office 2010 XLSX Documento")
							 ->setSubject("Office 2010 XLSX Documento")
							 ->setDescription("Documento con la información del reporte")
							 ->setKeywords("office 2010 openxml php")
							 ->setCategory("Archivo con resultado de los proyectos");



// Combino las celdas desde A1 hasta E1


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'REPORTE DE DOCENTES')
            ->setCellValue('A2', 'Modalidad')
            ->setCellValue('B2', 'TOTAL');

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:B2')->applyFromArray($boldArray);		

	
			
//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);	


 
$lista=$obja->grafico_director($fechad,$fechaa,$tutor);
$cel=3;
//llenar los valores de tecnologia


foreach ($lista as $list){
$a="A".$cel;
$b="B".$cel;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a,$list['Modalidad'] )
            ->setCellValue($b, $list['Total']);
$cel+=1;
}

$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
);
// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Reporte de proyectos');


// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);


// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="reporte_docentes.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
