<?php
require_once '../Modelo/DAO_Notas.php';
require_once '../dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$pdf = new Dompdf();
$datos = new DAO_Notas();
///////////////////////id del proyecto
$id_proy = htmlentities(addslashes(filter_input(INPUT_POST, "id_proyecto")));
/////////////////cantidad de proponentes
$proponente = htmlentities(addslashes(filter_input(INPUT_POST, "numeroestudiante")));
/////////////////// cantidad de directores
$director= htmlentities(addslashes(filter_input(INPUT_POST, "Director")));
///////////////////// cantidad de jurados
$jurado= htmlentities(addslashes(filter_input(INPUT_POST, "Jurado")));
/////////////////// numero de acta
$numero_acta = htmlentities(addslashes(filter_input(INPUT_POST, "Numero_Acta")));
/////////////////////// titulo del proyecto
$titulo_proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "Titulo_Proyecto")));
///////////////////////// modalidad de grado
$modalidad = htmlentities(addslashes(filter_input(INPUT_POST, "Modalidad")));
/////////////////////////// datos estudiante 1
$nombre_estudiante1 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Estudiante1")));
$codigo_estudiante1 = htmlentities(addslashes(filter_input(INPUT_POST, "Codigo_Estudiante1")));
$cedula_estudiante1 = htmlentities(addslashes(filter_input(INPUT_POST, "Documento_Estudiante1")));
////////////////////////////// datos estudiante 2
$nombre_estudiante2 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Estudiante2")));
$codigo_estudiante2 = htmlentities(addslashes(filter_input(INPUT_POST, "Codigo_Estudiante2")));
$cedula_estudiante2 = htmlentities(addslashes(filter_input(INPUT_POST, "Documento_Estudiante2")));    
//////////////////// datos director 1
$nombre_director1 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Director1")));
$cedula_director1 = htmlentities(addslashes(filter_input(INPUT_POST, "Documento_Director1")));
$nota1_1 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_1")));
$nota1_2 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_2")));
$nota1_3 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_3")));
$nota1_4 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_4")));
$nota1_5 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_5")));
$nota1_6 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota1_6")));
$final1 = htmlentities(addslashes(filter_input(INPUT_POST, "Final1")));
//////////////////////// datos director 2
$nombre_director2 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Director2")));
$cedula_director2 = htmlentities(addslashes(filter_input(INPUT_POST, "Documento_Director2")));
$nota2_1 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_1")));
$nota2_2 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_2")));
$nota2_3 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_3")));
$nota2_4 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_4")));
$nota2_5 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_5")));
$nota2_6 = htmlentities(addslashes(filter_input(INPUT_POST, "Nota2_6")));
$final2 = htmlentities(addslashes(filter_input(INPUT_POST, "Final2")));
//////////////////////////// datos jurado 1
$nombre_jurado1 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Jurado1")));
$notajurado1_1 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_1")));
$notajurado1_2 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_2")));
$notajurado1_3 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_3")));
$notajurado1_4 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_4")));
$notajurado1_5 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_5")));
$notajurado1_6 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_6")));
$notajurado1_7 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_7")));
$notajurado1_8 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_8")));
$notajurado1_9 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_9")));
$notajurado1_10 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_10")));
$notajurado1_11 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_11")));
$notajurado1_12 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado1_12")));
$finaljurado1 = htmlentities(addslashes(filter_input(INPUT_POST, "Finaljurado1")));
////////////////////////// datos jurado 2
$nombre_jurado2 = htmlentities(addslashes(filter_input(INPUT_POST, "Nombre_Jurado2")));
$notajurado2_1 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_1")));
$notajurado2_2 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_2")));
$notajurado2_3 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_3")));
$notajurado2_4 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_4")));
$notajurado2_5 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_5")));
$notajurado2_6 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_6")));
$notajurado2_7 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_7")));
$notajurado2_8 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_8")));
$notajurado2_9 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_9")));
$notajurado2_10 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_10")));
$notajurado2_11 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_11")));
$notajurado2_12 = htmlentities(addslashes(filter_input(INPUT_POST, "Notajurado2_12")));
$finaljurado2 = htmlentities(addslashes(filter_input(INPUT_POST, "Finaljurado2")));
////////////////
$puntaje_total = htmlentities(addslashes(filter_input(INPUT_POST, "Puntaje_Total")));
$calificacion_final = htmlentities(addslashes(filter_input(INPUT_POST, "Final")));
$caracter = htmlentities(addslashes(filter_input(INPUT_POST, "Validado")));
////////////////////////////////////////////////////////titulo al que opta
$titulo = htmlentities(addslashes(filter_input(INPUT_POST, "Carrera")));
////////////////////////////alterar tabla sustentacion
$datos->Ingresar_finalizado_calificacion($id_proy,$calificacion_final);
//////////////////////////////////fecha///////////////////////////
$carrera = htmlentities(addslashes(filter_input(INPUT_POST, "carrera")));
$dia2 = htmlentities(addslashes(filter_input(INPUT_POST, "ndia")));
$ano = htmlentities(addslashes(filter_input(INPUT_POST, "ano")));
/////////////////////////////////////////////////////// ACTA 1 JURADO 1 DIRECTOR////////////////////////////////
if ($jurado==1 && $director==1){
    $html =' 
<html lang= "en" > 
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los pedrodia (pedrodianum) días del mes de 
            pedromes de pedroaño, en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=3 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=4 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL JURADO</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;" colspan="2">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 1</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Calidad de los resultados alcanzados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Viabilidad tecnológica o científica de los resultados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="2"> Documentación entregada</th>
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento de normas técnicas</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia en la información entregada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 25 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="8"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:left; border:black 1px solid;"> Dominio del tema</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Nivel científico o tecnológico adquirido</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para motivar a la discusión</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_7.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para resumir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_8.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para concluir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_9.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para contestar</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_10.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Uso de recursos tecnológicos</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_11.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia general de la presentación pública</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_12.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;" colspan="2"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado1.'</td>
            </tr>                      
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será el promedio aritmético de los dos puntajes generados por el director y el jurado.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        El jurado calificador conformado por el docente <b>'.$nombre_jurado1.'</b> luego de evaluar el trabajo de grado asignó el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b><br>
        En constancia de lo anterior firman:
        <br>
 <pre style="line-height:1.2em;">   
    _____________________                              ________________________           
    '.$nombre_jurado1.'                                       '.$nombre_director1.'     
    JURADO 1                                           DIRECTOR DEL PROYECTO
     </pre>
        </font>
    </p>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="58" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));
}
///////////////////////////////////////////ACTA 1 JURADO 2 DIRECTORES//////////////////////////////////////////
if ($jurado==1 && $director==2){
   $html =' 
<html lang= "en" > 
    <head> 
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los pedrodia (pedrodianum) días del mes de 
            pedromes de pedroaño, en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=4 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 1</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 2</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$final2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=4 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL JURADO</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;" colspan="2">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 1</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Calidad de los resultados alcanzados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Viabilidad tecnológica o científica de los resultados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="2"> Documentación entregada</th>
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento de normas técnicas</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia en la información entregada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 25 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="8"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:left; border:black 1px solid;"> Dominio del tema</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Nivel científico o tecnológico adquirido</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para motivar a la discusión</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_7.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para resumir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_8.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para concluir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_9.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para contestar</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_10.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Uso de recursos tecnológicos</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_11.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia general de la presentación pública</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_12.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;" colspan="2"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado1.'</td>
            </tr>                      
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será el promedio aritmético de los tres puntajes generados por los directores y el jurado.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        El jurado calificador conformado por el docente <b>'.$nombre_jurado1.'</b> luego de evaluar el trabajo de grado asignó el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b> 
        En constancia de lo anterior firman:
 <pre style="line-height:1.2em;">   
    _____________________             ________________________         ________________________     
    '.$nombre_jurado1.'                      '.$nombre_director1.'                      '.$nombre_director2.'
    JURADO 1                          DIRECTOR 1 DEL PROYECTO          DIRECTOR 2 DEL PROYECTO
     </pre>
        </font>
    </p>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="58" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));
}
///////////////////////////////////////////acta 2 jurados 1 director
if ($jurado==2 && $director==1){

    $html =' 
<html lang= "en" > 
    <head> 
         
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los '.$dia2.' días del mes de 
            '.$carrera.' del '.$ano.', en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=3 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=5 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL JURADO</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;" colspan="2">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 1</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 2</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Calidad de los resultados alcanzados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Viabilidad tecnológica o científica de los resultados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_2.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="2"> Documentación entregada</th>
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento de normas técnicas</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_3.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia en la información entregada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 25 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_4.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="8"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:left; border:black 1px solid;"> Dominio del tema</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_5.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Nivel científico o tecnológico adquirido</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_6.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para motivar a la discusión</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_7.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_7.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para resumir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_8.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_8.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para concluir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_9.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_9.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para contestar</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_10.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_10.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Uso de recursos tecnológicos</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_11.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_11.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia general de la presentación pública</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_12.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_12.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;" colspan="2"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado2.'</td>
            </tr>                      
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será el promedio aritmético de los tres puntajes generados por el director y los dos jurados.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        El jurado calificador conformado por los docentes <b>'.$nombre_jurado1.'</b> y <b>'.$nombre_jurado2.'</b> luego de evaluar el trabajo de grado asignó el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b><br>
        En constancia de lo anterior firman:
        <br>
 <pre style="line-height:1.2em;">   
    _____________________             _______________________         ________________________           
    '.$nombre_jurado1.'                     '.$nombre_jurado2.'                    '.$nombre_director1.'     
    JURADO 1                          JURADO 2                        DIRECTOR DEL PROYECTO
     </pre>
        </font>
    </p>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="58" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));


}
///////////////////////////////////////////acta 2 jurados dos directores
if ($jurado==2 && $director==2){
    $html =' 
<html lang= "en" > 
    <head> 
         
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los pedrodia (pedrodianum) días del mes de 
            pedromes de pedroaño, en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=4 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 1</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 2</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$final2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=5 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL JURADO</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;" colspan="2">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 1</th>
                <th  style="text-align:center; border:black 1px solid;">JURADO 2</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Calidad de los resultados alcanzados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" colspan="2"> Viabilidad tecnológica o científica de los resultados</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_2.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="2"> Documentación entregada</th>
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento de normas técnicas</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_3.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia en la información entregada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 25 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_4.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;" rowspan="8"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:left; border:black 1px solid;"> Dominio del tema</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 10 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_5.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Nivel científico o tecnológico adquirido</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_6.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para motivar a la discusión</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_7.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_7.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para resumir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_8.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_8.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para concluir</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_9.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_9.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Habilidad para contestar</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_10.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_10.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Uso de recursos tecnológicos</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 4 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_11.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_11.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Coherencia general de la presentación pública</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 5 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado1_12.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$notajurado2_12.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;" colspan="2"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$finaljurado2.'</td>
            </tr>                      
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será el promedio aritmético de los cuatro puntajes generados por los directores y los jurados.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        El jurado calificador conformado por los docentes <b>'.$nombre_jurado1.'</b> y <b>'.$nombre_jurado2.'</b> luego de evaluar el trabajo de grado asignó el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b>
        En constancia de lo anterior firman:
        <br>
 <pre style="line-height:1.2em;">   
_________________       _______________________        ________________________       _______________________           
'.$nombre_jurado1.'            '.$nombre_jurado2.'                '.$nombre_director1.'                   '.$nombre_director2.'  
JURADO 1                JURADO 2                       DIRECTOR 1 DEL PROYECTO        DIRECTOR 2 DEL PROYECTO
     </pre>
        </font>
    </p>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="50" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));
}
///////////////////////////////////////////acta 1 director
if ($jurado==0 && $director==1){
    $html =' 
<html lang= "en" > 
    <head> 
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los pedrodia (pedrodianum) días del mes de 
            pedromes de pedroaño, en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=3 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será generado por el director.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        El director del trabajo de grado luego de evaluar el trabajo de grado asignó el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b><br>
        En constancia de lo anterior firma:
        <br>
 <pre style="line-height:1.2em;">   
                                           ________________________           
                                           '.$nombre_director1.'     
                                           DIRECTOR DEL PROYECTO
     </pre>
        </font>
    </p><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="58" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));
}
///////////////////////////////////////////acta 2 directores
if ($jurado==0 && $director==2){
    $html =' 
<html lang= "en" > 
    <head> 
         
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="google-site-verification" content="Bk4Srug_OEXSifI3igyw7flrbzj1qbHmeo2t70MBgkM" />
        <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title> Acta_proyecto_'.$id_proy.' </title>
    </head> 
    <body style="margin-left: 1px; margin-right: 1px; margin-top: 1px; margin-bottom: 1px;">
        <div align="center">
            <img src="../Img/header.png" id="icon" width="120" height="120" alt="Logo de la ud"/>
        </div> 
        <p align="center" >
            <font style="line-height:1em;font-size:10px;">                       
            <b>PROYECTO CURRICULAR TECNOLOGÍA EN ELECTRÓNICA</b><br>
            <b>ACTA DE CALIFICACIÓN Y SUSTENTACIÓN DEL TRABAJO DE GRADO</b><br>             
            </font>           
            <font style="line-height:1em;font-size:9px;">                       
            <b>Acta No. '.$numero_acta.' </b><br>
            (Documento para uso interno de la Universidad)<br>             
            </font>   
        </p>
        <p align="left" >          
            <font style="line-height:1em;font-size:11px;">                       
            En Bogotá a los pedrodia (pedrodianum) días del mes de 
            pedromes de pedroaño, en las instalaciones de la Facultad
            Tecnológica de la Universidad Distrital Francisco José 
            de caldas, se llevó a cabo la sustentación del trabajo
            de grado, titulado <b>“'.$titulo_proyecto.'”</b>, para optar al 
            título de <b>'.$titulo.'</b> , realizado por los estudiantes:
            <br><br>
            <b>Estudiantes:</b>
            </font>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">CODIGO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE COMPLETO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO DE IDENTIDAD</th>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante1.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante1.'</td>
            </tr>
            <tr>
                <td style="text-align:left;border:black 1px solid;"> '.$codigo_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$nombre_estudiante2.'</td>
                <td style="text-align:left;border:black 1px solid;"> '.$cedula_estudiante2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse;border:black 1px solid;">          		
            <tr>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">NOMBRE DIRECTOR</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">DOCUMENTO</th>
                <th style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">PROYECTO CURRICULAR</th>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director1.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
            <tr>             		
                <td style="text-align:left; border:black 1px solid;"> '.$nombre_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> '.$cedula_director2.'</td>
                <td style="text-align:left; border:black 1px solid;"> TECNOLOGÍA EN ELECTRÓNICA</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">          		
            <tr>
                <th COLSPAN=4 style="text-align:center; background-color: #c8c6c6; border:black 1px solid;">EVALUACIÓN DEL DIRECTOR</th>             
            </tr>
            <tr> 
                <th  style="text-align:center; border:black 1px solid;">CONCEPTO</th>                
                <th  style="text-align:center; border:black 1px solid;">PUNTAJE</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 1</th>
                <th  style="text-align:center; border:black 1px solid;">DIRECTOR 2</th>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del Cronograma</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_1.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Fundamentación teórica y matemática utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_2.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_2.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Alcance de las especificaciones de la solución planteada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_3.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_3.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Metodología utilizada</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_4.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_4.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Eficiencia y/o acabado o viabilidad tecnológica</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_5.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_5.'</td>
            </tr>
            <tr>             		
                <th style="text-align:left; border:black 1px solid;"> Cumplimiento del marco general del proyecto</th>
                <th style="text-align:center; border:black 1px solid;"> Hasta 15 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$nota1_6.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$nota2_6.'</td>
            </tr>
            <tr>             		
                <th style="text-align:right; border:black 1px solid;"> TOTAL</th>
                <th style="text-align:center; border:black 1px solid;"> 100 puntos</th>
                <td style="text-align:center; border:black 1px solid;"> '.$final1.'</td>
                <td style="text-align:center; border:black 1px solid;"> '.$final2.'</td>
            </tr>
        </table><br>
        <table BORDER WIDTH="520" style="font-size:10px;border-collapse: collapse; border:black 1px solid;">
            <td><b>PUNTAJE TOTAL </b>  (El puntaje total del trabajo de grado será el promedio aritmético de los dos puntajes generados por los directores.)</td>
            <td WIDTH="60" style="text-align:center; border:black 1px solid;">'.$puntaje_total.'</td>
        </table><br>
        <font style="line-height:1em;font-size:11px;">
        MODALIDAD DEL TRABAJO DE GRADO (ACUERDO 038 DE 2015): <b>'.$modalidad.'</b><br><br>
        Los directores del proyecto de grado luego de evaluar el trabajo de grado asignaron el carácter de <b>'.$caracter.'</b> con una calificación final de (escala de 1 a 5):<b> '.$calificacion_final.'</b>
        En constancia de lo anterior firman:
        <br>
 <pre style="line-height:1.2em;">   
                       ________________________       _______________________           
                       '.$nombre_director1.'                   '.$nombre_director1.'  
                       DIRECTOR 1 DEL PROYECTO        DIRECTOR 2 DEL PROYECTO
     </pre>
        </font>
    </p><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div align="center">
        <img src="../Img/partebaja.png" id="icon1" width="730" height="50" alt="partebaja"/>
    </div> 
</body> 
</html>';
            $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="../Acta_Proyecto/".$id_proy;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("Acta.pdf",array("Attachment" => 0));
}
///////////////////////////////////////////
?>