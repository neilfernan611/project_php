<?php
//Usuario
require_once '../Modelo/DAO_Proyecto.php';
//require_once '../Modelo/DAO_Profesor.php';
require_once '../Modelo/Email.php';
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}


 if ($coordinacion != null) {
     
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION["informacion"];
    $corre_a = $infop['Correo'];

    $objProy= new DAO_Proyecto();
    $ObjEmail = new Email();
    
    $id_proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "id_proyecto")));
    $jurado1 = htmlentities(addslashes(filter_input(INPUT_POST, "jurado1")));
    $jurado2 = htmlentities(addslashes(filter_input(INPUT_POST, "jurado2")));
    $fecha = htmlentities(addslashes(filter_input(INPUT_POST, "fechap")));
    //echo "fecha -->".$fecha;
    
    $fechaactual = date("y-m-d");
    $dias = abs(strtotime($fecha) - strtotime($fechaactual));
    $dias = $dias/86400;
    $InfproyectoE = $objProy->Informacion_estudiantes_proyecto($id_proyecto);
    $InfproyectoT = $objProy->Informacion_Tutor_Proyecto($id_proyecto);
    $Informacion_Proyecto=$objProy->InformacionProyecto($id_proyecto);
    $titulo=$Informacion_Proyecto['Nombre_Proyecto'];
    $infocomite = $objProy ->consultar_comite();
    $datosj1 = $objProy->buscar_profesor($jurado1);
    $datosj2 = $objProy->buscar_profesor($jurado2);
    $validarj2 = $objProy->validarjurado($id_proyecto,$jurado2);
    $validarj1 = $objProy->validarjurado($id_proyecto,$jurado1);
 //echo $id_proyecto . "-". $jurado1 . "-" . $jurado2 . "-" . $fecha;
 
    if($id_proyecto == '0' || $jurado1 == '0' || $jurado2 == '0' || $dias < 15)
    {
        echo "entro";
        echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Jurado.php?bandera=0'>");
    }elseif($jurado1==$jurado2){
        echo "entro2";
         echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Jurado.php?bandera=2'>");
    }elseif($validarj1["cantidad"] >=1 || $validarj2["cantidad"] >= 1){
        echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Jurado.php?bandera=2'>");
        echo "tutor y jurado iguales";
    }else{
        //echo "entro3";
        $verificar=$objProy->asignarJurado($id_proyecto, $jurado1, $jurado2);
        //echo "entro4 -" . $verificar . '\n';
       // echo $id_proyecto."-".$fecha."-".$jurado1."-".$jurado2;
        try{
            $objProy-> Actualizar_Estado_Sustentacion($fecha, $id_proyecto, $jurado1, $jurado2); // Revisar
            echo "entro4 -" ;
            }
            catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " lineas" . $ex->getLine());
            }
        //
    
    //echo $id_proyecto . "-". $jurado1 . "-" . $jurado2 . "-" . $fecha;
    
    if ($verificar == FALSE) {

       // echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Jurado.php?bandera=0'>");
    }else {
        
       //***************Enviar Correo************************
        $asunto = "Jurados Asignados";
        //Estudiantes
        
        foreach($InfproyectoE as $infoE)
        {
            $mensajeE = "Estudiante " .$infoE['Nombre'] ." se le informa que su proyecto de grado con el título " . $titulo . 
            " le fue asignados jurados. <br/>Datos Jurados <br/>Nombre: ".$datosj1['Nombre'] ."<br/>Correo: ". $datosj1['Correo'].
            "<br/>Nombre: ". $datosj2['Nombre']." <br/>Correo: ".$datosj2['Correo'] ;
            $destino = $infoE['Correo'];
            $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);
            
        }
        
        //Tutor
        $mensajeT = "Profesor " .$InfproyectoT['Nombre'] . " se le informa que el proyecto ". $titulo." al que fue asignado como tutor ya le fue asignado jurados <br/>Datos Jurados <br/>Nombre: ".$datosj1['Nombre'] ."<br/>Correo: ". $datosj1['Correo'].
            "<br/>Nombre: ". $datosj2['Nombre']." <br/>Correo: ".$datosj2['Correo'] ; 
        $destino = $InfproyectoT['Correo'];
        $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeT);
        
        //Comite
        $mensajeCom = "El proyecto con radicado ".$id_proyecto. " y título ".$titulo ." ya le fue asignado jurados. <br/>Datos Jurados <br/>Nombre: ".$datosj1['Nombre'] ."<br/>Correo: ". $datosj1['Correo'].
            "<br/>Nombre: ". $datosj2['Nombre']." <br/>Correo: ".$datosj2['Correo'] ; ;
        $destino = $infocomite['Correo'];
        $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeCom);
        
        //Jurados
        $InfproyectoJ = $objProy->Informacion_Jurados_Proyecto($id_proyecto);
        foreach($InfproyectoJ as $infoJ)
        {
            $mensajeJ = "Profesor " .$infoJ['Nombre'] . " se le informa que fue asignado como jurado del proyecto ". $titulo." con radicado " . $id_proyecto. " por favor ingrese a la plataforma para avalar el proyecto o solicitar correcciones";
            $destino = $infoJ['Correo'];
            $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeJ);    
        }
        //*************Fin Enviar Correo***********************
        echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Jurado.php?bandera=1'>"); //redirects after 3 seconds
        
        
            }
    }
      
    
   
    
} 
    