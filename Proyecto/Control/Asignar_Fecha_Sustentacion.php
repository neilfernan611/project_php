<?php

require_once '../Modelo/DAO_Cronograma.php';
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/Email.php';
$proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "proyecto")));
$fecha = htmlentities(addslashes(filter_input(INPUT_POST, "fecha")));
$lugar = htmlentities(addslashes(filter_input(INPUT_POST, "lugar")));
$hora = htmlentities(addslashes(filter_input(INPUT_POST, "hora")));
$codigo_c = htmlentities(addslashes(filter_input(INPUT_POST, "cronograma")));

$objProyecto = new DAO_Proyecto();
$ObjEmail = new Email();
$crono = new DAO_Cronograma();
$Informacion_Proyecto=$objProyecto->InformacionProyecto($proyecto);
$titulo=$Informacion_Proyecto['Nombre_Proyecto'];
$crono->Asignar_Fecha_Sustentacion($proyecto, $fecha, $hora, $lugar);
$InfproyectoE = $objProyecto->Informacion_estudiantes_proyecto($proyecto);
$InfproyectoJ = $objProyecto->Informacion_Jurados_Proyecto($proyecto);
$InfproyectoT = $objProyecto->Informacion_Tutor_Proyecto($proyecto);
$asunto = "Asignacion de fechas de sustentacion";

foreach($InfproyectoE as $infoE)
{
    $mensajeE = "Estudiante " .$infoE['Nombre'] ." se le informa que su proyecto de grado con el título " . $titulo . 
    " esta listo para ser sustentado <br/>Fecha: ".$fecha . "<br/>Hora: ".$hora ."<br/>Lugar: ". $lugar;
    $destino = $infoE['Correo'];
    $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);
}
foreach($InfproyectoJ as $infoJ)
{
    $mensajeJ = "Profesor " .$infoJ['Nombre'] . " se le informa que el proyecto ". $titulo." al que fue asignado como jurado ya le fue asignado fecha y lugar de sustentación" .
    "<br/>Fecha: " .$fecha . "<br/>Hora: ".$hora ."<br/>Lugar: ". $lugar;
    $destino1 = $infoE['Correo'];
    $ObjEmail->Enviar_Mail($destino1, $asunto, $mensajeJ);    
}

$mensajeT = "Profesor " .$infoJ['Nombre'] . " se le informa que el proyecto ". $titulo." al que fue asignado como tutor ya le fue asignado fecha y lugar de sustentación" .
    "<br/>Fecha: " .$fecha . "<br/>Hora: ".$hora ."<br/>Lugar: ". $lugar;
    $destino2 = $InfproyectoT['Correo'];
    $ObjEmail->Enviar_Mail($destino2, $asunto, $mensajeT);    
echo("<meta http-equiv='refresh' content='0;URL=../Asignar_Fecha_Sustentacion.php'>"); //redirects after 3 seconds
