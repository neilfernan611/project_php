<?php

session_start();
$estado = htmlentities(addslashes(filter_input(INPUT_POST, "estado")));

echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');

if($estado=="calendario"){
$nombre_archivo = $_FILES['archivo']['name'] = "Calendario" . ".pdf";
$ruta = $_FILES['archivo']['tmp_name'];
$tam = $_FILES['archivo']['size'];
$numero= 3145728;
if ($tam > 0 && $tam <= $numero) {

    $destino = filter_input(INPUT_SERVER,'DOCUMENT_ROOT') . "/Calendario/" . $nombre_archivo;
    if ($nombre_archivo != "") {

        if (copy($ruta, $destino)) {
            
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Documento actualizado.</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
                  <div class="alert alert-danger" role="alert">
                  <h4 class="alert-heading">Algo ha salido mal!</h4>
                  <p>Ruta vacía</p>
                  <hr>
                  <p class="mb-0"></p>
                  </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
        }
    }
} else {
    echo('<br><br><br><br>
      <div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Algo ha salido mal!</h4>
      <p>El documento excedió el tamaño máximo de 3mb</p>
      <hr>
      <p class="mb-0"></p>
      </div>');
    echo("<meta http-equiv='refresh' content='4;URL=../Subir_Documentos.php'>"); //redirects after 3 seconds
}
    
}else{
$nombre_archivo = $_FILES['archivo']['name'] = "Formato_Anteproyecto" . ".pdf";
$ruta = $_FILES['archivo']['tmp_name'];
$tam = $_FILES['archivo']['size'];
$numero= 3145728;

if ($tam > 0 && $tam <= $numero) {

    $destino = filter_input(INPUT_SERVER,'DOCUMENT_ROOT') . "/Formato/" . $nombre_archivo;
    if ($nombre_archivo != "") {

        if (copy($ruta, $destino)) {
            
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Documento actualizado.</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
                  <div class="alert alert-danger" role="alert">
                  <h4 class="alert-heading">Algo ha salido mal!</h4>
                  <p>Ruta vacía</p>
                  <hr>
                  <p class="mb-0"></p>
                  </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
        }
    }
} else {
    echo('<br><br><br><br>
      <div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Algo ha salido mal!</h4>
      <p>El documento excedió el tamaño máximo de 3mb</p>
      <hr>
      <p class="mb-0"></p>
      </div>');
    echo("<meta http-equiv='refresh' content='4;URL=../Subir_Documentos.php'>"); //redirects after 3 seconds
}
    
}
