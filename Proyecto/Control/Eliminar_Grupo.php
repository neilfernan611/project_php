<?php

session_start();
require_once "../Modelo/DAO_Grupo.php";

$id = (String) htmlentities(addslashes(filter_input(INPUT_GET, "idg")));

$objG = new DAO_Grupo();

$idg = $objG->eliminarGrupo($id);
header("Location:../Info_Grupo.php");

