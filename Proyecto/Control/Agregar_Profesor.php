<?php

echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Profesor.php';
require_once '../Modelo/GenerarPass.php';
require_once '../Modelo/Email.php';

$nombre = htmlentities(addslashes(filter_input(INPUT_POST, "nombre")));
$cedula = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
$correo = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
$telefono = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
$pertenece = htmlentities(addslashes(filter_input(INPUT_POST, "pertenecer")));
$externo = htmlentities(addslashes(filter_input(INPUT_POST, "externo")));

$contra = new GenerarPass();
$pass = $contra->randomPass(6); //Genra la contraseña
$pass_encriptada = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 5]);
$objDP = new DAO_Persona();
$objpro = new DAO_Profesor();
$ObjEmail = new Email();

$verificarP = $objDP->Verificar_Correo_Cedula($correo, $cedula);

if($pertenece=="" || $externo==""){
    echo('<br><br><br><br>
        <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Algo ha salido mal!</h4>
        <p>Por favor seleccione datos del checkbox.</p>
        <hr>
        <p class="mb-0"></p>
        </div>');
    echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Profesor.php'>"); //redirects after 3 seconds
}else{
    if ($verificarP == false) {

        $id = $objDP->Insertar_Persona($nombre, $correo, $cedula, $telefono);
               
        $objpro->crearUSuarioPro($correo, $pass_encriptada, $cedula, $pertenece, $externo);

 
        if ($id == "") {
            $mensaje =  utf8_decode("Buen día " . $nombre . " se ha registrado en la plataforma sisproyud<br> Usuario: " . $correo . "<br>Contraseña: " . $pass . "<br> sisproyud: https://sisproyud.com/");
            $asunto = "Registro En La Plataforma Sisproyud ";
            $destino = $correo;
    
            $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje);
   
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>El usuario ha sido registrado con exito.<br>
                Toda la información necesaria para acceder a la plataforma llegara en breve a su correo electronico.</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Profesor.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>El usuario no ha sido creado con exito le recordamos verificar la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Profesor.php'>"); //redirects after 3 seconds
        }
    } else {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>El usuario no ha sido creado con exito le recordamos verificar la información proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Profesor.php'>"); //redirects after 3 seconds
    }
       
}


