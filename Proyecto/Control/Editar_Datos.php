<?php

session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}

require_once "../Modelo/DAO_Persona.php";
require_once "../Modelo/DAO_Estudiante.php";
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
  $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
  $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
  $objEs = new DAO_Persona();
  $verificar=$objEs->Existe_correo($correo_n);
  $tele =  $objEs->infoUser($correo_n);
  
if($verificar && ($tele['Correo'] != $correo_n)){
    echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Advertencia</h4>
            <p>Verifique la información que proporciona.!</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
    echo("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
}else{

    if ($estudiante != null) {
    
        $nombre = $_SESSION["nombre"];
        $info = $_SESSION['informacion2'];
        $infop = $_SESSION['informacion'];
        $codigo = $info['Codigo'];
        $corre_a = $info['Correo'];
    
        $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
        $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
      
        $verificar = $objEs->editarPersona($corre_a, $correo_n, $tel);
    
        if ($verificar == FALSE) {
            
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Verifique la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
        } else {
    
            session_destroy();
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
        }
     }else if ($coordinacion != null) {
        $nombre = $_SESSION["nombre"];
        $infop = $_SESSION["informacion"];
        $corre_a = $infop['Correo'];
    
    
        $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
        $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
    
        $objEs = new DAO_Persona();
        $verificar = $objEs->editarPersona($corre_a, $correo_n, $tel);
    
        if ($verificar == FALSE) {
    
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Verifique la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
        } else {
    
            session_destroy();
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
        }
    } else if ($profesor != null) {
        $nombre = $_SESSION["nombre"];
        $infop = $_SESSION["informacion"];
        $corre_a = $infop['Correo'];
    
    
        $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
        $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
    
        $objEs = new DAO_Persona();
        $verificar = $objEs->editarPersona($corre_a, $correo_n, $tel);
    
        if ($verificar == FALSE) {
    
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Verifique la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
        } else {
    
            session_destroy();
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
        }
    } else if ($comite != null) {
        $nombre = $_SESSION["nombre"];
        $infop = $_SESSION["informacion"];
        $corre_a = $infop['Correo'];
    
    
        $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
        $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
    
        $objEs = new DAO_Persona();
        $verificar = $objEs->editarPersona($corre_a, $correo_n, $tel);
    
        if ($verificar == FALSE) {
    
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Verifique la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
        } else {
    
            session_destroy();
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
        }
    } elseif ($administrador != null) {
        $nombre = $_SESSION["nombre"];
        $infop = $_SESSION["informacion"];
        $corre_a = $infop['Correo'];
    
    
        $correo_n = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
        $tel = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
    
        $objAd = new DAO_Persona();
        $verificar = $objAd->editarPersona($corre_a, $correo_n, $tel);
    
        if ($verificar == FALSE) {
    
            echo('<br><br><br><br>
                <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Algo ha salido mal!</h4>
                <p>Verifique la información que proporciona.</p>
                <hr>
                <p class="mb-0"></p>
                </div>');
            echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Datos.php'>"); //redirects after 3 seconds
        } else {
    
            session_destroy();
            echo('<br><br><br><br>
                <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bien hecho!</h4>
                <p>Datos actualizados</p>
                <hr>
                <p class="mb-0">
                </p>
                </div>');
            echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
        }
    }
}




