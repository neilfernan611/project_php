<?php
require_once '../Modelo/DAO_Profesor.php';
session_start();
//Nuevo
$cedula2 = htmlentities(addslashes(filter_input(INPUT_POST, "cedulaProfe")));
$rol = htmlentities(addslashes(filter_input(INPUT_POST, "idRol")));

$objpro = new DAO_Profesor();

//Nuevo
$profe = $objpro->InformacionProfesor($cedula2);
$contra= $profe['Pass_P'];
$correo= $profe['Correo'];
$codi=$objpro->crearUsuarioProfe($rol, $correo, $cedula2, $contra);

if ($codi==$cedula2) {
    echo ("<meta http-equiv='refresh' content='4;URL=../Asignar_Rol.php'>"); //redirects after 3 seconds
    echo ("<p style='color:green; font-size:50px;text-align:center;'>Rol Asignado</p>");
} else {
    echo ("<meta http-equiv='refresh' content='4;URL=../Asignar_Rol.php'>"); //redirects after 3 seconds
    echo ("<p style='color:red; font-size:50px;text-align:center;'>Lo sentimos el usuario ya tiene el rol asignado</p>");
}