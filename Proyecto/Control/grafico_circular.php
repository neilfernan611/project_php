<?php
ob_start();
require_once'../Modelo/DAO_Anteproyecto.php';
$dato=new DAO_Anteproyecto();

$carrera=htmlentities(addslashes(filter_input(INPUT_GET,"carrera")));
$linea=htmlentities(addslashes(filter_input(INPUT_GET,"linea")));
$fechad=htmlentities(addslashes(filter_input(INPUT_GET,"fechadesde")));
$fechaa=htmlentities(addslashes(filter_input(INPUT_GET,"fechahasta")));
$listat;
$listaic;
$listatit;

if($carrera=='Avalados'){
$listat=$dato->prueba_grafico("4828", $linea, $fechad, $fechaa);
$listaic=$dato->prueba_grafico("4829", $linea, $fechad, $fechaa);
$listait=$dato->prueba_grafico("4830", $linea, $fechad, $fechaa);
}else{
$listat=$dato->prueba_grafico_devueltos("4828", $linea, $fechad, $fechaa);
$listaic=$dato->prueba_grafico_devueltos("4829", $linea, $fechad, $fechaa);
$listait=$dato->prueba_grafico_devueltos("4830", $linea, $fechad, $fechaa);  
}
//llenar los valores de tecnologia
$valoresxt=array();
$valoresyt=array();
foreach ($listat as $list){
$valoresyt[]=$list['Total'];
$valoresxt[]=$list['Modalidad'];
}


$datosxt= json_encode($valoresxt);
$datosyt= json_encode($valoresyt);

//llenar los valores de ingenieria en control
$valoresxic=array();
$valoresyic=array();
foreach ($listaic as $lisic){
$valoresyic[]=$lisic['Total'];
$valoresxic[]=$lisic['Modalidad'];
}
$datosxic= json_encode($valoresxic);
$datosyic= json_encode($valoresyic);
//llenar los valores de ingenieria en telematica
$valoresxit=array();
$valoresyit=array();
foreach ($listait as $lisit){
$valoresyit[]=$lisit['Total'];
$valoresxit[]=$lisit['Modalidad'];
}
$datosxit= json_encode($valoresxit);
$datosyit= json_encode($valoresyit);

?>


<div id="circular"></div>
<div id="circular2"></div>
<div id="circular3"></div>
<script type="text/javascript">
    function grafica(json){
        var parsed = JSON.parse(json);
        var arr=[];
        for(var x in parsed){
            arr.push(parsed[x]);
        }
        return arr;
    }
</script>


<script type="text/javascript">
  
    datosxt=grafica('<?php echo $datosxt; ?>');
    datosyt=grafica('<?php echo $datosyt; ?>');
    var data = [{
  values: datosyt,
  labels: datosxt,
  name: 'Tecnología',
  type: 'pie'
}

];
var layout = {
  height: 400,
  width: 500,
  title: 'Tecnología',
};
Plotly.newPlot('circular', data, layout);
    </script>
    
    <script type="text/javascript">
    datosxic=grafica('<?php echo $datosxic; ?>');
    datosyic=grafica('<?php echo $datosyic; ?>');
    var data = [
        {
  values: datosyic,
  labels: datosxic,
  type: 'pie'
}

];
var layout = {
  height: 400,
  width: 500,
  title: 'Ingenieria en Control',
};
Plotly.newPlot('circular2', data, layout);
    </script>
    
    <script type="text/javascript">
    datosxit=grafica('<?php echo $datosxit; ?>');
    datosyit=grafica('<?php echo $datosyit; ?>');
    var data = [{
  values: datosyit,
  labels: datosxit,
  type: 'pie'
}

];
var layout = {
  height: 400,
  width: 500,
  title: 'Ingenieria en Telemática',
};
Plotly.newPlot('circular3', data, layout);
    </script>