<?php
/*
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/Generar_Radicado.php';
$id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));*/
require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/Codigo_Proyecto.php';
$estudiante=new DAO_Estudiante();
$proyecto=new DAO_Proyecto();
$pin=new Codigo_Proyecto();
$carrera=$estudiante->Codigo_Carrera("Proy1");
$radicado=$proyecto->ultimo_radicado();
$anio = date('y');
$mes=date('m');
$semestre;
$cod_carrera=$carrera['Id_Carrera'];
$radi=$radicado['last'];
$tres=substr($radi,-3);

if($mes<=5){
    $semestre=1;
}else if($mes>5 && $mes<7){
    $semestre=2;
}else{
    $semestre=3;
}

$nummero=$pin->generate_numbers($tres,2,3);
$final=count($nummero, COUNT_RECURSIVE);
$radicado="20".$anio.$semestre.$cod_carrera.$nummero[$final-1];

/*
echo ("<meta http-equiv='refresh' content='4;URL=../Consultar_Proyectos.php'>"); //redirects after 3 seconds
echo ("<p style='color:green; font-size:50px;text-align:center;'>Radicado Generado</p>");

*/