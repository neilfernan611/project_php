<?php

require_once '../Modelo/DAO_Anteproyecto.php';


$dato=new DAO_Anteproyecto();
$fechad = htmlentities(addslashes(filter_input(INPUT_GET,"fechadesde")));
$fechaa = htmlentities(addslashes(filter_input(INPUT_GET,"fechahasta")));
$listat;
$listaic;
$listait;
$modalidad=$dato->modalidades();

foreach($modalidad as $mod){
    /*llenado de informacion tecnologia*/
    $resultado=$dato->prueba_grafico("4828", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listat[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}


foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico("4829", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listaic[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}
foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico("4830", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listait[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}

//llenar los valores de tecnologia
$valoresxt=array();
$valoresyt=array();

foreach ($listat as $list){
$valoresyt[]=$list['Cantidad'];
$valoresxt[]=$list['Modalidad'];
}

$datosxt= json_encode($valoresxt);
$datosyt= json_encode($valoresyt);

//llenar los valores de ingenieria en control
$valoresxic=array();
$valoresyic=array();
foreach ($listaic as $lisic){
$valoresyic[]=$lisic['Cantidad'];
$valoresxic[]=$lisic['Modalidad'];
}
$datosxic= json_encode($valoresxic);
$datosyic= json_encode($valoresyic);

//llenar los valores de ingenieria en telematica
$valoresxit=array();
$valoresyit=array();
foreach ($listait as $lisit){
$valoresyit[]=$lisit['Cantidad'];
$valoresxit[]=$lisit['Modalidad'];
}
$datosxit= json_encode($valoresxit);
$datosyit= json_encode($valoresyit);




?>

<div id='barras'></div>
<script type="text/javascript">
    function grafica(json){
        var parsed = JSON.parse(json);
        var arr=[];
        for(var x in parsed){
            arr.push(parsed[x]);
                   }
        return arr;
    }
</script>
<script type="text/javascript">
    datosxt=grafica('<?php echo $datosxt; ?>');
    datosyt=grafica('<?php echo $datosyt; ?>');
    var trace1 = {
    x: datosxt,
    y: datosyt,
    name: 'Tecnologia',
    type: 'bar'
};

    datosxic=grafica('<?php echo $datosxic; ?>');
    datosyic=grafica('<?php echo $datosyic; ?>');
    var trace2 = {
    x: datosxic,
    y: datosyic,
    name: 'Ing Control',
    type: 'bar'
};


   datosxit=grafica('<?php echo $datosxit; ?>');
    datosyit=grafica('<?php echo $datosyit; ?>');
    var trace3 = {
    x: datosxit,
    y: datosyit,
    name: 'Ing Telematica',
    type: 'bar'
};


var data = [trace1, trace2, trace3];
var layout = {barmode: 'group'};

Plotly.newPlot('barras', data, layout);
</script>

