<?php

require_once "../Modelo/DAO_Proyecto.php";
session_start();

if (!filter_input(INPUT_POST, "bot_proponente")) {

    header("Location:../Editar_Proponentes.php");
} else {
    $proy = htmlentities(addslashes(filter_input(INPUT_POST, "codigo")));
    $tutor = htmlentities(addslashes(filter_input(INPUT_POST, "tutor")));
    $es = htmlentities(addslashes(filter_input(INPUT_POST, "nom_es")));
    $estu = htmlentities(addslashes(filter_input(INPUT_POST, "nom_estu")));
    $objProy = new DAO_Proyecto();
    $obj1 = $objProy->ActualizarProponentes($proy, $es, $estu);
    $obj2 = $objProy->ActualizarTutor($proy, $tutor);
    header("Location:../Modificacion_Proponentes.php");
}
