<?php
require_once '../Modelo/DAO_Anteproyecto.php';
$obja = new DAO_Anteproyecto();
$fechad = htmlentities(addslashes(filter_input(INPUT_GET,"fechadesde")));
$fechaa = htmlentities(addslashes(filter_input(INPUT_GET,"fechahasta")));
$tutor = htmlentities(addslashes(filter_input(INPUT_GET,"tutor")));
$lista=$obja->grafico_director($fechad, $fechaa,$tutor);


//llenar los valores de tecnologia
$valoresxt=array();
$valoresyt=array();
foreach($lista as $list){
$valoresyt[]=$list['Total'];
$valoresxt[]=$list['Modalidad'];
}
$datosxt= json_encode($valoresxt);
$datosyt= json_encode($valoresyt);


?>


<div id="circular"></div>
<script type="text/javascript">
    function grafica(json){
        var parsed = JSON.parse(json);
        var arr=[];
        for(var x in parsed){
            arr.push(parsed[x]);
        }
        return arr;
    }
</script>


<script type="text/javascript">
  
    datosxt=grafica('<?php echo $datosxt; ?>');
    datosyt=grafica('<?php echo $datosyt; ?>');
    var data = [{
  values: datosyt,
  labels: datosxt,
  name: 'Proyectos directores de proyecto',
  type: 'pie'
}

];
var layout = {
  height: 400,
  width: 500,
  title: 'Cantidad de proyectos',
};
Plotly.newPlot('circular', data, layout);
    </script>

    