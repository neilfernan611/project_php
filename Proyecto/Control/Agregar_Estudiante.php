<?php

require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/Codigo_Proyecto.php';
require_once '../Modelo/GenerarPass.php';
require_once '../Modelo/Email.php';
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');

$nombre = htmlentities(addslashes(filter_input(INPUT_POST, "nombre")));
$cedula = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
$correo = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
$telefono = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));
$id_carrera = htmlentities(addslashes(filter_input(INPUT_POST, "carrera")));
$codigo = htmlentities(addslashes(filter_input(INPUT_POST, "codigo")));

$contra = new GenerarPass();
$pass = $contra->randomPass(6); //Genra la contraseña
$ObjEmail = new Email();
$objDP = new DAO_Persona();
$objEst = new DAO_Estudiante();
$cod = new Codigo_Proyecto();
$id_proyecto = $cod->getPIN(3); //Genera codigo para el estudiante
$pass_encriptada = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 5]);

$verificarC= $objEst->Estudiante_Codigo($codigo);
$verificarP =$objDP->Verificar_Correo_Cedula($correo, $cedula);

if($verificarP==FALSE){
    if($verificarC == FALSE){
        $id = $objDP->Insertar_Persona($nombre, $correo, $cedula, $telefono);
        $resp_pro = $objEst->Ingresar_Proyecto($id_proyecto);
        $codi = $objEst->crearUSuarioEst($codigo, $id_proyecto, $id_carrera, $correo, $pass_encriptada);

        if ($id == "" && $codi == $codigo && $resp_pro == $id_proyecto) {

            $mensaje = utf8_decode("Buen día se ha registrado en la plataforma Sisproyud " . $nombre . "<br>Usuario: " . $correo . "<br>Contraseña: " . $pass . " <br>¡Recuerde cambiar su contraseña!");
            $asunto = "Registro en la plataforma Sisproyud ";
            $destino = $correo;

            $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje);

            echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Bien hecho!</h4>
            <p>¡El usuario ha sido creado con exito! Le recordamos cambiar la contraseña la primera vez que ingrese a la plataforma.<br>
            Toda la información necesaria para acceder a la plataforma llegara en breve a su correo electrónico.</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../index.php'>"); //redirects after 3 seconds
        } else {
            echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>¡El usuario no ha sido creado con exito! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../index.php'>"); //redirects after 3 seconds
        }   
    }else{
            echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>¡El usuario no ha sido creado con exito! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../index.php'>"); //redirects after 3 seconds
    }
}else{
      echo('<br><br><br><br>
      <div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Algo ha salido mal!</h4>
      <p>¡El usuario no ha sido creado con exito! Le recordamos verificar la información que proporciona.</p>
      <hr>
      <p class="mb-0"></p>
      </div>');
      echo ("<meta http-equiv='refresh' content='3;URL=../index.php'>"); //redirects after 3 seconds
}
