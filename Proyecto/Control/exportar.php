<?php
require_once'../Modelo/DAO_Anteproyecto.php';
$dato=new DAO_Anteproyecto();

$fechad=htmlentities(addslashes(filter_input(INPUT_POST,"fechadesde")));
$fechaa=htmlentities(addslashes(filter_input(INPUT_POST,"fechahasta")));
$listat;
$listaic;
$listait;
$listatd;
$listaicd;
$listaitd;

require_once '../PHPExcel/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Universidad Distrital")
							 ->setLastModifiedBy("Universidad Distrital")
							 ->setTitle("Office 2010 XLSX Documento")
							 ->setSubject("Office 2010 XLSX Documento")
							 ->setDescription("Documento con la información del reporte")
							 ->setKeywords("office 2010 openxml php")
							 ->setCategory("Archivo con resultado de los proyectos");



// Combino las celdas desde A1 hasta E1

//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Tipo de Proyecto')
            ->setCellValue('B1', 'Monografia')
            ->setCellValue('C1', 'Pasantia')
            ->setCellValue('D1', 'Profundización')
            ->setCellValue('E1', 'Postgrado')
            ->setCellValue('F1', 'Investigación Innovación')
            ->setCellValue('G1', 'Proyecto de emprendimiento')
            ->setCellValue('H1', 'Produccion academica')
            ->setCellValue('I1', 'Total')
            ->setCellValue('A2', 'CONTROL')
            ->setCellValue('A3', 'TECNOLOGIA')
            ->setCellValue('A4', 'TELECO')
            ->setCellValue('A5', 'Total general');


// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($boldArray);		
$objPHPExcel->getActiveSheet()->getStyle('A1:A5')->applyFromArray($boldArray);	
	
			
//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);	
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
		

/*llenando arrays de los proyectos revisados*/
$modalidad=$dato->modalidades();

foreach($modalidad as $mod){
    /*llenado de informacion tecnologia*/
    $resultado=$dato->prueba_grafico("4828", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listat[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}

/*llenado de informacion control*/
foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico("4829", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listaic[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}
foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico("4830", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listait[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}


/*llenando arrays de los proyectos devueltos*/

$modalidad=$dato->modalidades();

foreach($modalidad as $mod){
    /*llenado de informacion tecnologia*/
    $resultado=$dato->prueba_grafico_devueltos("4828", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listatd[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}


foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico_devueltos("4829", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listaicd[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}
foreach($modalidad as $mod){
    $resultado=$dato->prueba_grafico_devueltos("4830", $fechad, $fechaa,$mod['Id_Modalidad']);
    $listaitd[]=array("Cantidad"=>$resultado['Cantidad'],"Modalidad"=>$resultado['Modalidad']);
}



//llenar datos control
$cel=2;
$letras=['B','C','D','E','F','G','H','I'];
$i=0;
foreach ($listaic as $lisic){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($letras[$i].$cel, $lisic['Cantidad']);
 $i++;           
}



$cel=3;
$i=0;
//llenar los valores de tecnologia
$valoresxt=array();
$valoresyt=array();
foreach ($listat as $list){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($letras[$i].$cel, $list['Cantidad']);
 $i++; 
}
//llenar los valores de ingenieria en teleco
$cel=4;
$i=0;
foreach ($listait as $lisit){


$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($letras[$i].$cel, $lisit['Cantidad']);
 $i++; 
}
/*CONSTRUYENDO TOTALES DE LAS COLUMNAS*/
 $cel=5;

for ($j=0;$j<sizeof($letras);$j++){
if($letras[$j]=="I"){
   
}else{
$total='=SUM('.$letras[$j]."2".','.$letras[$j]."3".','.$letras[$j]."4".')';
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($letras[$j].$cel, $total);
}

}
/*CONTRUYENDO TOTALES DE LAS FILAS*/
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('I2', '=SUM(B2,C2,D2,E2,F2,G2,H2)');
            
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('I3', '=SUM(B3,C3,D3,E3,F3,G3,H3)');
            
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('I4', '=SUM(B4,C4,D4,E4,F4,G4,H4)');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('I5', '=SUM(B5,C5,D5,E5,F5,G5,H5)');
 
 /*Construyendo tabla de totales de proyectos revisados tecnologia*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B12:C12');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B12', 'Total de proyectos revisados Tecnologìa');
$objPHPExcel->getActiveSheet()->getStyle('B12:C12')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B13', 'TECNOLOGIA')
            ->setCellValue('C13', 'CANTIDAD')
            ->setCellValue('B14', 'Monografia')
            ->setCellValue('B15', 'Pasantia')
            ->setCellValue('B16', 'Profundización')
            ->setCellValue('B17', 'Postgrado')
            ->setCellValue('B18', 'Investigación Innovación')
            ->setCellValue('B19', 'Proyecto de emprendimiento')
            ->setCellValue('B20', 'Produccion academica')
            ->setCellValue('B21', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('B21:C21')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('B13:C13')->applyFromArray($boldArray);	
 
 /*Construyendo tabla de totales de proyectos devueltos tecnologia*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F12:G12');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F12', 'Total de proyectos Devueltos Tecnologìa');
$objPHPExcel->getActiveSheet()->getStyle('F12:G12')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F13', 'TECNOLOGIA')
            ->setCellValue('G13', 'CANTIDAD')
            ->setCellValue('F14', 'Monografia')
            ->setCellValue('F15', 'Pasantia')
            ->setCellValue('F16', 'Profundización')
            ->setCellValue('F17', 'Postgrado')
            ->setCellValue('F18', 'Investigación Innovación')
            ->setCellValue('F19', 'Proyecto de emprendimiento')
            ->setCellValue('F20', 'Produccion academica')
            ->setCellValue('F21', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('F21:G21')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('F13:G13')->applyFromArray($boldArray);	
 
 
 
 
 
 /*Construyendo tabla de totales de proyectos revisados CONTROL*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B24:C24');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B24', 'Total de proyectos revisados Ingenieria en Control');
$objPHPExcel->getActiveSheet()->getStyle('B24:C24')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B25', 'TECNOLOGIA')
            ->setCellValue('C25', 'CANTIDAD')
            ->setCellValue('B26', 'Monografia')
            ->setCellValue('B27', 'Pasantia')
            ->setCellValue('B28', 'Profundización')
            ->setCellValue('B29', 'Postgrado')
            ->setCellValue('B30', 'Investigación Innovación')
            ->setCellValue('B31', 'Proyecto de emprendimiento')
            ->setCellValue('B32', 'Produccion academica')
            ->setCellValue('B33', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('B33:C33')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('B25:C25')->applyFromArray($boldArray);	
 
 /*Construyendo tabla de totales de proyectos devueltos CONTROL*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F24:G24');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F24', 'Total de proyectos Devueltos Ingenieria en Control');
$objPHPExcel->getActiveSheet()->getStyle('F24:G24')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F25', 'TECNOLOGIA')
            ->setCellValue('G25', 'CANTIDAD')
            ->setCellValue('F26', 'Monografia')
            ->setCellValue('F27', 'Pasantia')
            ->setCellValue('F28', 'Profundización')
            ->setCellValue('F29', 'Postgrado')
            ->setCellValue('F30', 'Investigación Innovación')
            ->setCellValue('F31', 'Proyecto de emprendimiento')
            ->setCellValue('F32', 'Produccion academica')
            ->setCellValue('F33', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('F33:G33')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('F25:G25')->applyFromArray($boldArray);
 
 
 
 /*Construyendo tabla de totales de proyectos revisados teleco*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B36:C36');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B36', 'Total de proyectos revisados Ingenieria en Telecomunicaciones');
$objPHPExcel->getActiveSheet()->getStyle('B36:C36')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B37', 'TECNOLOGIA')
            ->setCellValue('C37', 'CANTIDAD')
            ->setCellValue('B38', 'Monografia')
            ->setCellValue('B39', 'Pasantia')
            ->setCellValue('B40', 'Profundización')
            ->setCellValue('B41', 'Postgrado')
            ->setCellValue('B42', 'Investigación Innovación')
            ->setCellValue('B43', 'Proyecto de emprendimiento')
            ->setCellValue('B44', 'Produccion academica')
            ->setCellValue('B45', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('B37:C37')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('B45:C45')->applyFromArray($boldArray);	
 
 /*Construyendo tabla de totales de proyectos devueltos teleco*/
 
 $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F36:G36');
 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F36', 'Total de proyectos Devueltos Ingenieria en Telecomunicaciones');
$objPHPExcel->getActiveSheet()->getStyle('F36:G36')->applyFromArray($boldArray);	
//Ancho de las columnas


 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F37', 'TECNOLOGIA')
            ->setCellValue('G37', 'CANTIDAD')
            ->setCellValue('F38', 'Monografia')
            ->setCellValue('F39', 'Pasantia')
            ->setCellValue('F40', 'Profundización')
            ->setCellValue('F41', 'Postgrado')
            ->setCellValue('F42', 'Investigación Innovación')
            ->setCellValue('F43', 'Proyecto de emprendimiento')
            ->setCellValue('F44', 'Produccion academica')
            ->setCellValue('F45', 'Total general');
 
 $objPHPExcel->getActiveSheet()->getStyle('F37:G37')->applyFromArray($boldArray);	
 $objPHPExcel->getActiveSheet()->getStyle('F45:G45')->applyFromArray($boldArray);
 
 
 
 /*llenando tabla totales revisados tecnologia*/
 
 $cel=14;
//llenar los valores de tecnologia

foreach ($listat as $list){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("C".$cel, $list['Cantidad']);
$cel++; 
}

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C21', '=SUM(C14,C15,C16,C17,C18,C19,C20)');
 /*llenando tabla totales devueltos tecnologia*/

$cel=14;
//llenar los valores de tecnologia

foreach ($listatd as $listd){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("G".$cel, $listd['Cantidad']);
$cel++; 
}

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('G21', '=SUM(G14,G15,G16,G17,G18,G19,G20)');


/*LLENANDO TOTALES REVISADOS CONTROL*/

$cel=26;

foreach ($listaic as $lisic){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("C".$cel, $lisic['Cantidad']);
$cel++;    
}
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C33', '=SUM(C26,C27,C28,C29,C30,C31,C32)');
 /*llenando tabla totales devueltos control*/
$cel=26;

foreach ($listaicd as $lisicd){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("G".$cel, $lisicd['Cantidad']);
$cel++;    
}
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('G33', '=SUM(G26,G27,G28,G29,G30,G31,G32)');

/*LLENANDO TOTALES REVISADOS teleco*/

$cel=38;

foreach ($listait as $lisit){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("C".$cel, $lisit['Cantidad']);
$cel++;    
}
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C45', '=SUM(C38,C39,C40,C41,C42,C43,C44)');
            
/*LLENANDO TOTALES DEVUELTOS TELECO*/
$cel=38;

foreach ($listaitd as $lisitd){

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("G".$cel, $lisitd['Cantidad']);
$cel++;    
}
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('G45', '=SUM(G38,G39,G40,G41,G42,G43,G44)');


function cellColor($cells,$color){
       global $objPHPExcel;

       $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
           'type' => PHPExcel_Style_Fill::FILL_SOLID,
           'startcolor' => array(
                'rgb' => $color
           )
       ));
}

cellColor('A1:I1', 'a9e5ee');
cellColor('A5:I5', 'a9e5ee');

cellColor('B13:C13', 'a9e5ee');
cellColor('B21:C21', 'a9e5ee');
cellColor('F13:G13', 'a9e5ee');
cellColor('F21:G21', 'a9e5ee');

cellColor('B25:C25', 'a9e5ee');
cellColor('F25:C25', 'a9e5ee');
cellColor('B33:C33', 'a9e5ee');
cellColor('F33:G33', 'a9e5ee');

cellColor('B37:C37', 'a9e5ee');
cellColor('F37:G37', 'a9e5ee');
cellColor('B45:C45', 'a9e5ee');
cellColor('F45:G45', 'a9e5ee');

$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
);
// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Reporte de proyectos');


// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);


// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="reporte.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(true);
$objWriter->save('php://output');
exit;
