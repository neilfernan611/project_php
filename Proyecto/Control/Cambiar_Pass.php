<?php

session_start();
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}

require_once "../Modelo/DAO_Estudiante.php";
require_once '../Modelo/DAO_Coordinacion.php';
require_once '../Modelo/DAO_Comite.php';
require_once '../Modelo/DAO_Profesor.php';
require_once '../Modelo/DAO_Administrador.php';
require_once '../Modelo/Email.php';


if ($estudiante != null) {

    $nombre = $_SESSION["nombre"];
    $info = $_SESSION['informacion2'];
    $infop = $_SESSION['informacion'];
    $correo = $info['Correo'];
    

    $contra_a = htmlentities(addslashes(filter_input(INPUT_POST, "pass_a")));
    $contra_n = htmlentities(addslashes(filter_input(INPUT_POST, "pass_n")));
    $ObjEmail=new Email();
    $objEs = new DAO_Estudiante();
    $verificar = $objEs->cambiar_pass($correo, $contra_a, $contra_n);
    
    if (!$verificar) {

        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Contraseña Incorrecta! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Cambiar_Pass.php'>"); //redirects after 3 seconds
    } else {
        $mensaje = "Su contraseña ha sido actualiza<br>Contraseña nueva: *********" . substr($contra_n, -3);
        $asunto=utf8_decode("Cambio de contraseña");
        $destino = $correo;

        $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje);

        session_destroy();
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Contraseña actualizada!</h4>
            <p>Vuelva a iniciar sesión</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
    }
} else if ($coordinacion != null) {
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION['informacion'];
    $correo = $infop['Correo'];


    $contra_a = htmlentities(addslashes(filter_input(INPUT_POST, "pass_a")));
    $contra_n = htmlentities(addslashes(filter_input(INPUT_POST, "pass_n")));

    $objCor = new DAO_Coordinacion();
    $verificar = $objCor->cambiar_pass($correo, $contra_a, $contra_n);


    if (!$verificar) {

        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Contraseña Incorrecta! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Cambiar_Pass.php'>"); //redirects after 3 seconds
    } else {
        $mensaje = "Su contraseña ha sido actualiza<br>Contraseña nueva: *********" . substr($contra_n, -3);
        $asunto=utf8_decode("Cambio de contraseña");
        $destino = $corre;

        session_destroy();
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Contraseña actualizada!</h4>
            <p>Vuelva a iniciar sesión</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
    }
} else if ($profesor != null) {
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION['informacion'];
    $correo = $infop['Correo'];


    $contra_a = htmlentities(addslashes(filter_input(INPUT_POST, "pass_a")));
    $contra_n = htmlentities(addslashes(filter_input(INPUT_POST, "pass_n")));

    $objPro = new DAO_Profesor();
    $verificar = $objPro->cambiar_pass($correo, $contra_a, $contra_n);


    if (!$verificar) {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Contraseña Incorrecta! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Cambiar_Pass.php'>"); //redirects after 3 seconds
    } else {
        $mensaje = "Su contraseña ha sido actualiza<br>Contraseña nueva: *********" . substr($contra_n, -3);
        $asunto=utf8_decode("Cambio de contraseña");
        $destino = $corre;

        session_destroy();
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Contraseña actualizada!</h4>
            <p>Vuelva a iniciar sesión</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
    }
} else if ($comite != null) {
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION['informacion'];
    $correo = $infop['Correo'];


    $contra_a = htmlentities(addslashes(filter_input(INPUT_POST, "pass_a")));
    $contra_n = htmlentities(addslashes(filter_input(INPUT_POST, "pass_n")));

    $objCom = new DAO_Comite();
    $verificar = $objCom->cambiar_pass($correo, $contra_a, $contra_n);


    if (!$verificar) {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Contraseña Incorrecta! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Cambiar_Pass.php'>"); //redirects after 3 seconds
    } else {
        $mensaje = "Su contraseña ha sido actualiza<br>Contraseña nueva: *********" . substr($contra_n, -3);
        $asunto=utf8_decode("Cambio de contraseña");
        $destino = $corre;

        session_destroy();
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Contraseña actualizada!</h4>
            <p>Vuelva a iniciar sesión</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
    }
} else if ($administrador != null) {
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION['informacion'];
    $correo = $infop['Correo'];


    $contra_a = htmlentities(addslashes(filter_input(INPUT_POST, "pass_a")));
    $contra_n = htmlentities(addslashes(filter_input(INPUT_POST, "pass_n")));

    $objAdm = new DAO_Administrador();
    $verificar = $objAdm->cambiar_pass($correo, $contra_a, $contra_n);

    if (!$verificar) {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Contraseña Incorrecta! Le recordamos verificar la información que proporciona.</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Cambiar_Pass.php'>"); //redirects after 3 seconds
    } else {
        $mensaje = "Su contraseña ha sido actualiza<br>Contraseña nueva: *********" . substr($contra_n, -3);
        $asunto="Cambio de contraseña";
        $destino = $corre;

        session_destroy();
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Contraseña actualizada!</h4>
            <p>Vuelva a iniciar sesión</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../index.php'>"); //redirects after 3 seconds
    }
}




