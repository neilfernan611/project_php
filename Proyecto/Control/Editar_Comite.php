<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
require_once "../Modelo/DAO_Comite.php";
require_once "../Modelo/DAO_Persona.php";

$cod = htmlentities(addslashes(filter_input(INPUT_POST, "cod")));
$correo = htmlentities(addslashes(filter_input(INPUT_POST, "correo")));
$telefono = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));


$personaobj = new DAO_Persona();
$veri= $personaobj->Existe_correo($correo);
$tele =  $personaobj->infoUser($correo);
    
if($veri && ($tele['Correo'] != $correo)){
    echo('<br><br><br><br>
         <div class="alert alert-danger" role="alert">
         <h4 class="alert-heading">Algo ha salido mal!</h4>
         <p>Lo sentimos! Ha ocurido un error, por favor verifique los datos ingresados.</p>
         <hr>
         <p class="mb-0"></p>
         </div>');
    echo ("<meta http-equiv='refresh' content='4;URL=../Info_Comite.php'>"); //redirects after 3 seconds
}else{
    $objComi = new DAO_Comite();
    $correo_a = $objComi->correoComite($cod);
    
    $objp = new DAO_Persona();
    $verificar = $objp->editarPersona($correo_a['Correo'], $correo, $telefono);
    
    if ($verificar == FALSE) {
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Lo sentimos! Ha ocurido un error, por favor verifique los datos ingresados</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../Editar_Comitee.php'>"); //redirects after 3 seconds
    } else {
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Bien hecho!</h4>
            <p>Datos actualizados!</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Info_Comite.php'>"); //redirects after 3 seconds
    }
}

