<?php

require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/Email.php';
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');

//$evaluado_pcb = htmlentities(addslashes(filter_input(INPUT_POST, "Evaluado_Pcb")));
$id_proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "Id_Proyecto")));
$estudiante1 = htmlentities(addslashes(filter_input(INPUT_POST, "Estudiante1")));
$estudiante2 = htmlentities(addslashes(filter_input(INPUT_POST, "Estudiante2")));
$estado = htmlentities(addslashes(filter_input(INPUT_POST, "Solicitar_Sustentacion")));
$obj = new DAO_Proyecto();
$ObjEmail = new Email();
$obj->Solictar_S($estado, $id_proyecto);
//$obj->Actualizar_Pcb($evaluado_pcb, $id_proyecto);
$Informacion_Proyecto=$obj->InformacionProyecto($id_proyecto);
$titulo=$Informacion_Proyecto['Nombre_Proyecto'];
$InfproyectoE = $obj->Informacion_estudiantes_proyecto($id_proyecto);
$InfproyectoT = $obj->Informacion_Tutor_Proyecto($id_proyecto);
$infoC = $obj->consultar_coordinacion();
$infocomite = $obj ->consultar_comite();

//**********************************Enviar Correo
$asunto = "Solicitud Sustentación";
//Estudiantes
foreach($InfproyectoE as $infoE)
{
    $mensajeE = "Estudiante " .$infoE['Nombre'] ." se le informa que se ha solicitado sustentación para su proyecto de grado con el título " . $titulo . 
    " por favor estar pendiente de la plataforma y de su correo";
    $destino = $infoE['Correo'];
    $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);
}

//Cordinación
$mensajeC = "El proyecto con radicado ".$id_proyecto. " y título ".$titulo ." ha solicitado sustentación, por favor realice la asignación de jurados para asignarle fecha de sustentación";
$destino = $infoC['Correo'];
$ObjEmail->Enviar_Mail($destino, $asunto, $mensajeC);

//Tutor
$mensajeT = "Profesor " .$InfproyectoT['Nombre'] . " se le informa que el proyecto ". $titulo." al que fue asignado como tutor ha realizado la solicitud de sustentación del proyecto";
$destino = $InfproyectoT['Correo'];
$ObjEmail->Enviar_Mail($destino, $asunto, $mensajeT);

//Comite
$mensajeCom = "El proyecto con radicado ".$id_proyecto. " y título ".$titulo ." ha solicitado sustentación, por favor verifique si coordinación ya realizo la asignacion de jurados para que asigne fecha de sustentación";
$destino = $infocomite['Correo'];
$ObjEmail->Enviar_Mail($destino, $asunto, $mensajeCom);
//****************************Fin Correo

echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo.php'>"); //redirects after 3 seconds
