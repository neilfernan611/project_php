<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
require_once '../Modelo/DAO_Coordinacion.php';
require_once '../Modelo/Email_archivos_adjuntos.php';
$coordinacion=new DAO_Coordinacion();
$ObjEmail = new Email2();
$para= htmlentities(addslashes(filter_input(INPUT_POST, "destinatarios")));
$mensaje= htmlentities(addslashes(filter_input(INPUT_POST, "mensaje")));
$asunto= htmlentities(addslashes(filter_input(INPUT_POST, "asunto")));

$archivo[0]=$_FILES['archivo']['name'];
$archivo[1]=$_FILES['archivo']['tmp_name'];
if($para=="Todos los Estudiantes"){
    $respuesta=$coordinacion->Correo_Masivo_Estudiantes();
  
    foreach ($respuesta as $resp){
        $ObjEmail->Enviar_Mail($resp['Correo'], $asunto, $mensaje,$archivo);
    }
     echo('<br><br><br><br>
      <div class="alert alert-success" role="alert">
      <h4 class="alert-heading">Envio de correo Exitoso!</h4>
      <hr>
      <p class="mb-0">
      </p>
      </div>');
  echo ("<meta http-equiv='refresh' content='3;URL=../Correo_Masivo.php'>"); //redirects after 3 seconds*/
 }else if($para=="Todos los Docentes"){
    $respuesta=$coordinacion->Correo_Masivo_Profesor();
      foreach ($respuesta as $resp){
        $ObjEmail->Enviar_Mail($resp['Correo'], $asunto, $mensaje,$archivo);
    }
    echo('<br><br><br><br>
      <div class="alert alert-success" role="alert">
      <h4 class="alert-heading">Envio de correo Exitoso!<br> </h4>
      <hr>
      <p class="mb-0">
      </p>
      </div>');
      echo ("<meta http-equiv='refresh' content='3;URL=../Correo_Masivo.php'>"); //redirects after 3 seconds*/
}



