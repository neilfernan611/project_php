<?php
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
}

$info=$_SESSION['informacion2'];
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/Email.php';
$id_proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "id_proyecto")));
$estado = htmlentities(addslashes(filter_input(INPUT_POST, "Estado_Sustentacion")));
$rol = htmlentities(addslashes(filter_input(INPUT_POST, "rol")));
$codigo = htmlentities(addslashes(filter_input(INPUT_POST, "codigo")));
$cedula = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
$pcb =  htmlentities(addslashes(filter_input(INPUT_POST, "pcb")));
$comentario =  htmlentities(addslashes(filter_input(INPUT_POST, "comentario")));
$objp = new DAO_Proyecto();
$ObjEmail = new Email();
$InfproyectoE = $objp->Informacion_estudiantes_proyecto($id_proyecto);
$InfproyectoT = $objp->Informacion_Tutor_Proyecto($id_proyecto,$cedula);
$Infprof = $objp->buscar_profesor($cedula);
$concepto = $objp-> consulta_concepto ($estado);
$estudiante2 = $objp -> consultarEstudiante2($id_proyecto, $codigo);
$estudiante = $objp ->Buscar_Codigo_Estudiante($id_proyecto, $codigo);
//echo $rol."-".$id_proyecto."-".$estado."-".$codigo;
//echo $id_proyecto;
//echo $cedula;

if ($estudiante != NULL)
{
    if($estado == 01){
        $objp->Actualizar_Estado_Sustentacion_E($id_proyecto,$estado,$codigo);      
        
    }else if($estado == 02){
        if($comentario == NULL)
        {
          echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo.php?bandera=2'>"); //redirects after 3 seconds
        }else{
        //$objp->Actualizar_Estado_Sustentacion_E($id_proyecto,$estado,$codigo);      
            header('Location:https://sisproyud.com/Ver_Todo.php?bandera=2');
        }
    }
     $asunto = "Concepto Proyecto de grado";
     $mensajeE = "Se le informa que su proyecto de grado con radicado ".$id_proyecto." ha recibido el concepto de ". $concepto["EstadoS"] . 
     " por parte de su compañero ". $estudiante["Nombre"]."<br>Ingrese a la plataforma para más información https://sisproyud.com/ <br><br>";
     $destino = $estudiante2["Correo"];
     if($comentario != NULL)
     {
        $mensajeE = $mensajeE ."Motivo del aplazamiento <br> ".$comentario;    
     }
      $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);

    echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo.php?bandera=1'>"); //redirects after 3 seconds

}
else if($profesor != NULL)
{
    
  
    
   if ($rol == "05") 
   {
    //
    if($estado == 01 ){if($pcb != 01){ $pcb='00'; }
        $objp->Actualizar_Estado_T($id_proyecto,$estado,$cedula);
        $objp->Actualizar_Pcb($id_proyecto,$pcb);
        //echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo_T.php?bandera=1&id=" . $id_proyecto . "'>"); //redirects after 3 seconds    
    }else if($estado == 02)
    {
        if($comentario == NULL)
        {
          //echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo_T.php?bandera=2&id=" . $id_proyecto . "'>"); //redirects after 3 seconds
              header('Location:https://sisproyud.com/Ver_Todo_T.php?bandera=2&id='. $id_proyecto );
        }else{
        $objp->Actualizar_Estado_T($id_proyecto,$estado,$cedula);
        $objp-> proyecto_aplazado ($id_proyecto, '00');
        }
            
    }
     foreach($InfproyectoE as $infoE)
        {
            $asunto = "Concepto Proyecto de grado";
            $mensajeE ="Se le informa que su proyecto con radicado " .$id_proyecto . " ha recibido el concepto de ". $concepto["EstadoS"] . 
            ' por parte del director de proyecto ' . $Infprof["Nombre"]."<br>Ingrese a la plataforma para más información https://sisproyud.com/ <br><br>";
            $destino = $infoE['Correo'];
            if ($comentario != null)
            {
                $mensajeE = $mensajeE ."Motivo del aplazamiento <br> ".$comentario;    
            }
            $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);
          
            //echo $mensajeE ."--".  $destino ."-- <br>";
        }
        echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo_T.php?bandera=1&id=" . $id_proyecto . "'>"); //redirects after 3 seconds    
        
         
    
   }elseif($rol == "01")
   {    
       if($estado == 01 ){
        $objp->Actualizar_Estado_J($id_proyecto,$estado,$cedula); 
        }else if($estado == 02){
            if($comentario == NULL){
                //echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo_J.php?bandera=2&id=" . $id_proyecto . "'>"); //redirects after 3 seconds
                header('Location:https://sisproyud.com/Ver_Todo_J.php?bandera=2&id='. $id_proyecto );
                //echo "entro";
            }else{
                $objp->Actualizar_Estado_J($id_proyecto,$estado,$cedula); 
                $objp-> proyecto_aplazado ($id_proyecto, '00');
            }
        }
            foreach($InfproyectoE as $infoE){
                $asunto = "Concepto Proyecto de grado";
                $mensajeE ="Se le informa que su proyecto con radicado " .$id_proyecto . " ha recibido el concepto de ". $concepto["EstadoS"] . 
                ' por parte del jurado ' . $Infprof["Nombre"]."<br>Ingrese a la plataforma para más información https://sisproyud.com/ <br><br>";
                $destino = $infoE['Correo'];
                if ($comentario != null){
                    $mensajeE = $mensajeE ."Motivo del aplazamiento <br> ".$comentario;    
                }
          //      $ObjEmail->Enviar_Mail($destino, $asunto, $mensajeE);
            }
        echo ("<meta http-equiv='refresh' content='0;URL=../Ver_Todo_J.php?bandera=1&id=" . $id_proyecto . "'>"); //redirects after 3 seconds
    }
}



    