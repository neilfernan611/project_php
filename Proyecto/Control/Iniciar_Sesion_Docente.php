<?php

require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Profesor.php';
try {

    session_start();

    $objProDAO = new DAO_Profesor();

    $objPerDAO = new DAO_Persona();
    $usu;
    $pass;
    $rol = "3";
    $usu = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
    $pass = htmlentities(addslashes(filter_input(INPUT_POST, "pass")));
    $roll = htmlentities(addslashes(filter_input(INPUT_POST, "id_roll")));
    $Profe = $objProDAO->SessionProfesor($usu, $pass, $roll);



    if ($usu == $Profe['Correo'] /*&& password_verify($pass,$Profe['Pass_P'])*/&& $Profe['Id_Rol'] == $roll && (!isset($_SESSION['profesor']))) {
//si coincide usuario y password y además no hay sesión iniciada

        $persona = $objPerDAO->infoUser($Profe['Correo']);
        $_SESSION["profesor"] = $persona['Nombre'];
        $_SESSION["nombre"] = $persona['Nombre'];
        $_SESSION["correo"] = $Profe['Correo'];
        $_SESSION["informacion2"] = $Profe;
        $_SESSION["cedula"] = $persona['Cedula'];
        $_SESSION["informacion"] = $persona;


        header("Location: ../home.php");
        //echo ("<meta http-equiv='refresh' content='1;URL=../index.php'>"); //redirects after 3 seconds
    } else {

        header("Location: ../Login_Docente.php");
        session_destroy();
    }
} catch (Exception $ex) {
    echo $ex->getLine() . "<br>";
    echo $ex->getMessage();
} 

