<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
echo '<style> p{text-aling:justyfi;} </style>';
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    $datos = $_SESSION["informacion2"];
    $cedula = $datos['Cedula'];
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}

require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/DAO_Anteproyecto.php';
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/DAO_Radicado.php';
require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Comite.php';
require_once '../Modelo/Codigo_Proyecto.php';
require_once '../Modelo/Email.php';

$id_proyecto = htmlentities(addslashes(filter_input(INPUT_POST, "id_proyecto")));
$estado = htmlentities(addslashes(filter_input(INPUT_POST, "estado")));
$fech = htmlentities(addslashes(filter_input(INPUT_POST, "fech")));
$comentario = htmlentities(addslashes(filter_input(INPUT_POST, "comentario")));
$rol = htmlentities(addslashes(filter_input(INPUT_POST, "rol")));
$fecha = date('Y-m-d');

$comite1 = new DAO_Comite();
$cantidad_d = $comite1->cantidad_directores($id_proyecto);
$cant_d = $cantidad_d['cantidad'];
$director = "";
$fecha_aux = explode("-", $fech);
setlocale(LC_ALL, "es_CO");
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$dia = $dias[date('N', strtotime($fech))];
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha2 = $dia . " " . ($fecha_aux[2]) . " de " . $meses[(int) $fecha_aux[1] - 1] . " del " . $fecha_aux[0];

if ($cant_d > 1) {
    $tutor = $comite1->director2($id_proyecto);
    foreach ($tutor as $tu) {
        $director .= "" . $tu['Nombre'] . "<br>";
    }
} else {
    $tutor = $comite1->director($id_proyecto);
    $director .= "" . $tutor['Nombre'] . "";
}

if ($director == "") {
    $director = "No Aplica";
}

//Nuevo para que coordinacion actualice
$id_proy = htmlentities(addslashes(filter_input(INPUT_GET, "id_proyecto")));
$estac = htmlentities(addslashes(filter_input(INPUT_GET, "estado")));

$objp = new DAO_Anteproyecto();
$objproyectoc = new DAO_Proyecto();
$ObjEmail = new Email();

//Comentario Evaluador
$id_comentarioe = $objp->Ultimo_Comentario_Evaluador();
$id_comenc = intval($id_comentarioe['LAST']);
$ide = $id_comenc + 1;

//Comentario Comite
$id_comentario = $objp->ultimo_comentario();
$id_comen = intval($id_comentario['last']);
$id = $id_comen + 1;



if ($profesor != null) {

      $objp->Actualizar_Estado_Evaluador($id_proyecto, $estado, $fecha);
      $objp->Guardar_Comentario_Evaluador($ide, $comentario, $fecha, $id_proyecto, $cedula);

    if ($estado == "03") {
        $objProyecto = new DAO_Anteproyecto();
        $objestudiante = new DAO_Estudiante();
        $objpersona = new DAO_Persona();
        $cod = new Codigo_Proyecto();
        $cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
        $cant = intval(implode($cantidad));

        $objProyecto->Actualizar_Estado_Comite($id_proyecto, "03", $fecha);
        $objProyecto->Actualizar_Estado_Coordinacion($id_proyecto, "03", $fecha);

        if ($cant > 1) {
            
            $proyecto = $objProyecto->datosficha2($id_proyecto);
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            $estudiante_dos = $objpersona->infoUser($proyecto['correo_dos']);
            
            $existedu = $objProyecto->Consultar_Existencia_Estudiante_Devuelto($proyecto['Codigo']);
            $existedd = $objProyecto->Consultar_Existencia_Estudiante_Devuelto($proyecto['Codigo_Dos']);
              
            $proyanterioru = $objProyecto->Anteproyectodevueltoevaluadorcodigo($proyecto['Codigo']);
            $proyanteriord = $objProyecto->Anteproyectodevueltoevaluadorcodigo($proyecto['Codigo_Dos']);
                
            $cantidade = $objProyecto->estudiante_cantidad_devuelto($proyanterioru['Id_Proyecto']);
            $cantidaded = $objProyecto->estudiante_cantidad_devuelto($proyanteriord['Id_Proyecto']);

            
            if($existedu==1 && $existedd == 1){
                
                $objProyecto->Anteproyectodevueltoevaluadorupdate($proyecto['Codigo'],$id_proyecto,$fecha,$cedula);
                $objProyecto->Anteproyectodevueltoevaluadorupdate($proyecto['Codigo_Dos'],$id_proyecto,$fecha,$cedula);
                
                if($proyanterioru['Id_Proyecto'] ==  $proyanteriord['Id_Proyecto'] ){
                    $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanterioru['Id_Proyecto']);
                    
                }else{
                    
                    if($cantidade['Cantidad'] > 1){
                    }else{
                        $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanterioru['Id_Proyecto']);
                    }
                    
                    if($cantidaded['Cantidad'] > 1){
                    }else{
                        $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanteriord['Id_Proyecto']);
                    }
                }
            }else if($existedu==1 && $existedd == 0){
                $objProyecto->Anteproyectodevueltoevaluadorupdate($proyecto['Codigo'],$id_proyecto,$fecha,$cedula);
                $objProyecto->Anteproyectodevueltoevaluador($proyecto['Codigo_Dos'], $id_proyecto, $fecha, $cedula);
                
                if($cantidade['Cantidad']> 1){
                }else{
                    $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanterioru['Id_Proyecto']);
                }
                
            }else if($existedu==0 && $existedd == 1){
                $objProyecto->Anteproyectodevueltoevaluadorupdate($proyecto['Codigo_Dos'],$id_proyecto,$fecha,$cedula);
                $objProyecto->Anteproyectodevueltoevaluador($proyecto['Codigo'], $id_proyecto, $fecha, $cedula);
                
                if($cantidaded['Cantidad'] > 1){
                }else{
                    $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanteriord['Id_Proyecto']);
                }
                
            }else if($existedu == 0 && $existedd == 0) {
                $objProyecto->Anteproyectodevueltoevaluador($proyecto['Codigo'], $id_proyecto, $fecha, $cedula);
                $objProyecto->Anteproyectodevueltoevaluador($proyecto['Codigo_Dos'], $id_proyecto, $fecha, $cedula);
            }
            
            $id_proyecto_nuevo = $cod->getPIN(3); //Genera codigo para el estudiante;
            $id_proyecto_nuevo2 = $cod->getPIN(3); //Genera codigo para el estudiante;
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo);
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo2);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo'], $id_proyecto_nuevo);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo_Dos'], $id_proyecto_nuevo2);
        } else {
            $cod = new Codigo_Proyecto();
            $objProyecto = new DAO_Anteproyecto();
            $objestudiante = new DAO_Estudiante();
            $objpersona = new DAO_Persona();

            $proyecto = $objProyecto->datosficha($id_proyecto);
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            
            $existedu = $objProyecto->Consultar_Existencia_Estudiante_Devuelto($proyecto['Codigo']);
            $proyanterioru = $objProyecto->Anteproyectodevueltoevaluadorcodigo($proyecto['Codigo']);
            $cantidade = $objProyecto->estudiante_cantidad_devuelto($proyanterioru['Id_Proyecto']);
            
            if($existedu==1){
                $proyanterioru = $objProyecto->Anteproyectodevueltoevaluadorcodigo($proyecto['Codigo']);
                $objProyecto->Anteproyectodevueltoevaluadorupdate($proyecto['Codigo'],$id_proyecto,$fecha,$cedula);
                
                if($cantidade['Cantidad'] > 1){
                }else{
                    $objProyecto->Anteproyectodevueltoevaluadoreliminar($proyanterioru['Id_Proyecto']);
                }
                
            }else{
                $objProyecto->Anteproyectodevueltoevaluador($proyecto['Codigo'], $id_proyecto, $fecha, $cedula);    
            }
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo']);
            $id_proyecto_nuevo = $cod->getPIN(3); //Genera codigo para el estudiante;
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo'], $id_proyecto_nuevo);
        }
    }
    
    echo('<br><br><br><br>
        <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Modificación Exitosa!</h4>
        <hr>
        <p class="mb-0">
        </p>
        </div>');
    echo ("<meta http-equiv='refresh' content='3;URL=../Anteproyecto_Evaluador.php'>"); //redirects after 3 seconds
} else if ($comite != null) {
    
    $objp->Actualizar_Estado_Comite($id_proyecto, $estado, $fech);
    $objp->Guardar_Comentario_Comite($id, $comentario, $fech, $id_proyecto); 
    
    if ($estado == "03") {
        $objProyecto = new DAO_Anteproyecto();
        $objestudiante = new DAO_Estudiante();
        $objpersona = new DAO_Persona();
        $cod = new Codigo_Proyecto();
        $cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
        $cant = intval(implode($cantidad));
        $objp->Actualizar_Estado_Coordinacion($id_proyecto, "03", $fecha);


        if ($cant > 1) {
            $proyecto = $objProyecto->datosficha2($id_proyecto);
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            $estudiante_dos = $objpersona->infoUser($proyecto['correo_dos']);
            
            $es_uno = $objestudiante->InformacionEstudiante($proyecto['Correo']);
            $es_dos = $objestudiante->InformacionEstudiante($proyecto['correo_dos']);
            
            $objProyecto->Anteproyectodevuelto($proyecto['Codigo'], $id_proyecto, $estudiante_uno['Cedula'], $proyecto['Correo'], $proyecto['Estudiante_Uno'], $proyecto['Telefono'], $es_uno['Id_Carrera'], $fech);
            $objProyecto->Anteproyectodevuelto($proyecto['Codigo_Dos'], $id_proyecto, $estudiante_dos['Cedula'], $proyecto['correo_dos'], $proyecto['Estudiante_Dos'], $proyecto['Telefono_Dos'], $es_dos['Id_Carrera'], $fech);
            $id_proyecto_nuevo = $cod->getPIN(3); //Genera codigo para el estudiante;
            $id_proyecto_nuevo2 = $cod->getPIN(3); //Genera codigo para el estudiante;
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo);
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo2);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo'], $id_proyecto_nuevo);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo_Dos'], $id_proyecto_nuevo2);

            $asunto = "Concepto Propuesta Proyecto de Grado";

            $titulo = utf8_decode($proyecto['Nombre_Proyecto']);
            $estudiante1 = $proyecto['Codigo'] . " " . $proyecto['Estudiante_Uno'];
            $estudiante2 = $proyecto['Codigo_Dos'] . " " . $proyecto['Estudiante_Dos'];
            $concepto = "DEVUELTO";

            $mensaje = utf8_decode("<p>Cordial saludo,</p>"
                    . "<p>En sesión del día " . $fecha2 . " llevado a cabo por los "
                    . "miembros del Comité de Proyecto de Grado, sesión en la "
                    . "cual se evaluó la pertenencia, coherencia y calidad de los "
                    . "proyectos radicados; se reviso la propuesta de proyecto titulada:<br>"
                    . "<br><b>" . $titulo . "</b><br><br>"
                    . "Presentado por los alumnos:<br><br>"
                    . "" . $estudiante1 . "<br>"
                    . "" . $estudiante2 . "<br><br>"
                    . "Director del proyecto: <br> " . $director . "<br><br>"
                    . "Concepto de revisión <b>" . $concepto . "</b><br><br>"
                    . "" . $comentario . "<br><br>"
                    . "Atentamente,<br>Comité de Proyectos de Grado </p>");

            $ObjEmail->Enviar_Mail($estudiante_uno['Correo'], $asunto, $mensaje);
            $ObjEmail->Enviar_Mail($estudiante_dos['Correo'], $asunto, $mensaje);
            if ($cant_d > 1) {
                foreach ($tutor as $tu) {
                    $ObjEmail->Enviar_Mail($tu['Correo'], $asunto, $mensaje);
                }
            } else {
                $ObjEmail->Enviar_Mail($tutor['Correo'], $asunto, $mensaje);
            }
        } else {
            $cod = new Codigo_Proyecto();
            $objProyecto = new DAO_Anteproyecto();
            $objestudiante = new DAO_Estudiante();
            $objpersona = new DAO_Persona();

            $proyecto = $objProyecto->datosficha($id_proyecto);
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            
            $es_uno = $objestudiante->InformacionEstudiante($proyecto['Correo']);
            
            $objProyecto->Anteproyectodevuelto($proyecto['Codigo'], $id_proyecto, $estudiante_uno['Cedula'], $proyecto['Correo'], $proyecto['Estudiante_Uno'], $proyecto['Telefono'],  $es_uno['Id_Carrera'], $fech);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo']);
            $id_proyecto_nuevo = $cod->getPIN(3); //Genera codigo para el estudiante;
            $objestudiante->Ingresar_Proyecto($id_proyecto_nuevo);
            $objestudiante->anteproyecto_devuelto_estudiante($proyecto['Codigo'], $id_proyecto_nuevo);
            $asunto = "Concepto Propuesta Proyecto de Grado";

            $titulo = utf8_decode($proyecto['Nombre_Proyecto']);
            $estudiante1 = $proyecto['Codigo'] . " " . $proyecto['Estudiante_Uno'];

            $concepto = "DEVUELTO";

            $mensaje = utf8_decode("<p>Cordial saludo,</p>"
                    . "<p>En sesión del día " . $fecha2 . " llevado a cabo por los "
                    . "miembros del Comité de Proyecto de Grado, sesión en la "
                    . "cual se evaluó la pertenencia, coherencia y calidad de los "
                    . "proyectos radicados; se reviso la propuesta de proyecto titulada:<br>"
                    . "<br><b>" . $titulo . "</b><br><br>"
                    . "Presentado por los alumno:<br><br>"
                    . "" . $estudiante1 . "<br><br>"
                    . "Director del proyecto:<br> " . $director . "<br><br>"
                    . "Concepto de revisión <b>" . $concepto . "</b><br><br>"
                    . "" . $comentario . "<br><br>"
                    . "Atentamente,<br>Comité de Proyectos de Grado </p>");

            $ObjEmail->Enviar_Mail($proyecto['Correo'], $asunto, $mensaje);

            if ($cant_d > 1) {
                foreach ($tutor as $tu) {
                    $ObjEmail->Enviar_Mail($tu['Correo'], $asunto, $mensaje);
                }
            } else {
                $ObjEmail->Enviar_Mail($tutor['Correo'], $asunto, $mensaje);
            }
        }
        $objp->Actualizar_Estado_Coordinacion($id_proyecto, "03", $fech);
    } else {

        $objProyecto = new DAO_Anteproyecto();
        $objestudiante = new DAO_Estudiante();
        $objpersona = new DAO_Persona();
        $cod = new Codigo_Proyecto();

        $cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
        $cant = intval(implode($cantidad));

        if ($cant > 1) {
            $proyecto = $objProyecto->datosficha2($id_proyecto);
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            $estudiante_dos = $objpersona->infoUser($proyecto['correo_dos']);


            $asunto = "Concepto Propuesta Proyecto de Grado";

            $titulo = utf8_decode($proyecto['Nombre_Proyecto']);
            $estudiante1 = $proyecto['Codigo'] . " " . $proyecto['Estudiante_Uno'];
            $estudiante2 = $proyecto['Codigo_Dos'] . " " . $proyecto['Estudiante_Dos'];
            $concepto = "APROBADO";

            $mensaje = utf8_decode("<p>Cordial saludo,</p>"
                    . "<p>En sesión del día " . $fecha2 . " llevado a cabo por los "
                    . "miembros del Comité de Proyecto de Grado, sesión en la "
                    . "cual se evaluó la pertenencia, coherencia y calidad de los "
                    . " proyectos radicados; se reviso la propuesta de proyecto titulada:<br>"
                    . "<br><b>" . $titulo . "</b><br><br>"
                    . "Presentado por los alumnos:<br><br>"
                    . "" . $estudiante1 . "<br>"
                    . "" . $estudiante2 . "<br><br>"
                    . "Director del proyecto: <br>" . $director . "<br><br>"
                    . "Concepto de revisión <b>" . $concepto . "</b><br><br>"
                    . "" . $comentario . "<br><br>"
                    . "Atentamente,<br>Comité de Proyectos de Grado </p>");

            $ObjEmail->Enviar_Mail($estudiante_uno['Correo'], $asunto, $mensaje);
            $ObjEmail->Enviar_Mail($estudiante_dos['Correo'], $asunto, $mensaje);

            if ($cant_d > 1) {
                foreach ($tutor as $tu) {

                    $ObjEmail->Enviar_Mail($tu['Correo'], $asunto, $mensaje);
                }
            } else {

                $ObjEmail->Enviar_Mail($tutor['Correo'], $asunto, $mensaje);
            }
        } else {
            $proyecto = $objProyecto->datosficha($id_proyecto);
            $asunto = "Concepto Propuesta Proyecto de Grado";
            $titulo = utf8_decode($proyecto['Nombre_Proyecto']);
            $objpersona = new DAO_Persona();
            $estudiante_uno = $objpersona->infoUser($proyecto['Correo']);
            $estudiante1 = $proyecto['Codigo'] . " " . $proyecto['Estudiante_Uno'];
            $concepto = "APROBADO";
            $mensaje = utf8_decode("<p>Cordial saludo,</p>"
                    . "<p>En sesión del día " . $fecha2 . " llevado a cabo por los "
                    . "miembros del Comité de Proyecto de Grado, sesión en la "
                    . "cual se evaluó la pertenencia, coherencia y calidad de los "
                    . " proyectos radicados; se reviso la propuesta de proyecto titulada:<br>"
                    . "<br><b>" . $titulo . "</b><br><br>"
                    . "Presentado por los alumnos:<br><br>"
                    . "" . $estudiante1 . "<br><br>"
                    . "Director del proyecto:<br> " . $director . "<br><br>"
                    . "Concepto de revisión <b>" . $concepto . "</b><br><br>"
                    . "" . $comentario . "<br><br>"
                    . "Atentamente,<br>Comité de Proyectos de Grado </p>");

            $ObjEmail->Enviar_Mail($proyecto['Correo'], $asunto, $mensaje);
            if ($cant_d > 1) {
                foreach ($tutor as $tu) {
                    $tu['Correo'];
                    $ObjEmail->Enviar_Mail($tu['Correo'], $asunto, $mensaje);
                }
            } else {
                $ObjEmail->Enviar_Mail($tutor['Correo'], $asunto, $mensaje);
            }
        }
    }
    echo('<br><br><br><br>
          <div class="alert alert-success" role="alert">
          <h4 class="alert-heading">Modificación Exitosa y Correo Enviado!</h4>
          <hr>
          <p class="mb-0">
          </p>
          </div>');
    echo ("<meta http-equiv='refresh' content='3;URL=../tabla_comite.php'>"); //redirects after 3 seconds
} else if ($coordinacion != null) {

    $objp->Actualizar_Estado_Coordinacion($id_proy, $estac, $fecha);
    if ($estac == "01") {

        $estudiante = new DAO_Estudiante();
        $proyecto = new DAO_Radicado();
        $reseteo= $proyecto->Consultar_consecutivo_Radicado();
        $reset=$reseteo['Valor'];
        $pin = new Codigo_Proyecto();
        $carrera = $estudiante->Codigo_Carrera($id_proy);
        $radicado = $proyecto->ultimo_radicado();
        $anio = date('y');
        $mes = date('m');
        $semestre;
        $cod_carrera = $carrera['Id_Carrera'];
        $radi = $radicado['last'];
        

        if ($mes <= 5) {
            $semestre = 1;
        } else if ($mes > 5 && $mes < 7) {
            $semestre = 2;
        } else {
            $semestre = 3;
        }
    
        if($radi==NULL || $radi=="" || $reset==1){
            $nummero="001";
          
            $radicado = "20" . $anio . $semestre . $cod_carrera . $nummero;
         
            $proyecto->Activar_consecutivo_Radicado();
     
            
        }else{
            $tres = substr($radi, -3);
            $nummero = $pin->generate_numbers($tres, 2, 3);
            $final = count($nummero, COUNT_RECURSIVE);
            $radicado = "20" . $anio . $semestre . $cod_carrera . $nummero[$final - 1];
        }
        
  
        $proyecto->Actualizar_Radicado($id_proy, $radicado);
    
        $proyecto->Actualizar_Duracion_Proyecto($id_proy);
       
        $proyecto->Actualizar_Id_Anteproyecto_A_Radicado($id_proy, $radicado); //FUNCION NUEVA*/
     
        $objproyectoc->Actualizar_Estado_Sustentacion_Id_Radicado($id_proy, $radicado);

        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Radicado Asignado!</h4>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo ("<meta http-equiv='refresh' content='0;URL=../generar_carta.php?id=$radicado'>");
    } else {
        echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">No se pudo realizar la actualización !</h4>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
        echo ("<meta http-equiv='refresh' content='2;URL=../Generar_Radicados.php>");
    }
}