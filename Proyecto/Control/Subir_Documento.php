<?php

session_start();
$documento = htmlentities(addslashes(filter_input(INPUT_POST, "documento")));
$nombre_archivo = $_FILES['archivo']['name'] = $documento . ".pdf";
$ruta = $_FILES['archivo']['tmp_name'];
$tam = $_FILES['archivo']['size'];

if ($tam <= ((1048576) * 3)) {
//$destino = "archivos/" . $nombre;

    $destino = filter_input(INPUT_SERVER,'DOCUMENT_ROOT') . "/Documentacion/" . $nombre_archivo;
    if ($nombre_archivo != "") {

        if (copy($ruta, $destino)) {

            echo("<meta http-equiv='refresh' content='6;URL=../home.php'>"); //redirects after 3 seconds
            echo("<p style='color:green; font-size:40px;text-align:center;'>Documento Actualizado</p>");
        } else {
            echo("<meta http-equiv='refresh' content='6;URL=../Subir_Documentos.php'>"); //redirects after 3 seconds
            echo("<p style='color:green; font-size:40px;text-align:center;'>Error</p>");
        }
    }
} else {
    echo("<meta http-equiv='refresh' content='4;URL=../Subir_Documentos.php'>"); //redirects after 3 seconds
    echo("<p style='color:green; font-size:40px;text-align:center;'>Excedio el tamaño maximo de 3mb</p>");
}
