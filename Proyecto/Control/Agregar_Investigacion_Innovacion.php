<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
session_start();
require_once '../Modelo/DAO_Anteproyecto.php';
require_once '../Modelo/DAO_Proyecto.php';
require_once '../Modelo/DAO_Estudiante.php';
require_once '../Modelo/Email.php';

$proyecto = new DAO_Anteproyecto();
$proyectoc = new DAO_Proyecto();
$estudiante = new DAO_Estudiante();

$info = $_SESSION['informacion2'];
$id_proyecto = $info['Id_Proyecto'];
$codigoe1 = $info['Codigo'];
$nombre_archivo = $_FILES['archivo']['name'] = $id_proyecto. ".pdf";
$carta_c = $_FILES['carta_c']['name'] = $id_proyecto. ".pdf";
$ruta_pro = $_FILES['archivo']['tmp_name'];
$ruta_carta_c = $_FILES['carta_c']['tmp_name'];
$tam = $_FILES['archivo']['size'];
$tamc = $_FILES['carta_c']['size'];
$error = $_FILES['archivo']['error'];
$numero= 3145728;

if ($tam > 0 && $tam <= $numero && $tamc > 0 && $tamc <= $numero) {
    if ($nombre_archivo != "" && $carta_c != "") {
            $ObjEmail = new Email();
            $tutor = htmlentities(addslashes(filter_input(INPUT_POST, "tutor")));
            $tutor2 = htmlentities(addslashes(filter_input(INPUT_POST, "tutor2")));
            $duracion = htmlentities(addslashes(filter_input(INPUT_POST, "duracion")));
            $evaluador = htmlentities(addslashes(filter_input(INPUT_POST, "evaluador")));
            $fecha = date('Y-m-d');
            $linea = htmlentities(addslashes(filter_input(INPUT_POST, "linea")));
            $grupo = htmlentities(addslashes(filter_input(INPUT_POST, "grupo")));
            $estudiante2 = htmlentities(addslashes(filter_input(INPUT_POST, "codigo2")));
            $modalidad = "206";
            $nombre_ante = htmlentities(addslashes(filter_input(INPUT_POST, "nombre")));
            $resumen = htmlentities(addslashes(filter_input(INPUT_POST, "resumen")));
            $objetivo_general = htmlentities(addslashes(filter_input(INPUT_POST, "objetivo_general")));
            $objetivos = htmlentities(addslashes(filter_input(INPUT_POST, "objetivos")));
            $trabajo1 = htmlentities(addslashes(filter_input(INPUT_POST, "Trabajo1")));
            $trabajo2 = htmlentities(addslashes(filter_input(INPUT_POST, "Trabajo2")));
            $radioc =  htmlentities(addslashes(filter_input(INPUT_POST, "radioc")));
            $rol = "05";
            $rolev = "03";

            if($estudiante2 == $codigoe1 || $codigoe1 == $estudiante2){
                echo('<br><br><br><br>
                      <div class="alert alert-danger" role="alert">
                      <h4 class="alert-heading">Algo ha salido mal!</h4>
                      <p>Su código ya se encuentra relacionado con el proyecto, por favor ingrese otro código.</p>
                      <hr>
                      <p class="mb-0"></p>
                      </div>');
                echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 4 seconds
            }else{
                
                if($evaluador==$tutor || $evaluador==$tutor2){
                    echo('<br><br><br><br>
                          <div class="alert alert-danger" role="alert">
                          <h4 class="alert-heading">Algo ha salido mal!</h4>
                          <p>El director de proyecto y evaluador no pueden ser el mismo docente.</p>
                          <hr>
                          <p class="mb-0"></p>
                          </div>');
                    echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 4 seconds
                }else{
                    if($tutor == $tutor2 || $tutor2==$tutor){
                        echo('<br><br><br><br>
                              <div class="alert alert-danger" role="alert">
                              <h4 class="alert-heading">Algo ha salido mal!</h4>
                              <p>El director de proyecto no puede ser el segundo director de proyecto.</p>
                              <hr>
                              <p class="mb-0"></p>
                              </div>');
                        echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                        
                    }else{
                    
                        if ($radioc == "no" || strcmp($radioc, "no") === 0) {
                            $destino_pro = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/archivos/" . $nombre_archivo;
                            $destino_carta_c = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/Carta_Grupo_Inves/" . $carta_c;
                
                            if ((copy($ruta_pro, $destino_pro)) && (copy($ruta_carta_c, $destino_carta_c))) {
                
                                $proyecto->Estudiante_Agrega_AnteProyecto($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $grupo, $duracion, $objetivo_general, $objetivos, $resumen);
                                $proyecto->Agregar_tutor($tutor, $rol, $id_proyecto);
                                $proyecto->Agregar_tutor($evaluador, $rolev, $id_proyecto); 
                                $proyecto->Documentos_modalidades($id_proyecto, $carta_c, $id_proyecto);
                                $proyecto->estados_proyecto($id_proyecto);
                                
                                if ($trabajo1 == "02" && $trabajo2 == "03") {
                                    $espacio_academico = "02";
                                    $espacio_academico2 = "03";
                                    $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                    $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico2);
                                            
                                }else if($trabajo1 == "" && $trabajo2 == ""){
                                    $espacio_academico = "01";
                                    $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                            
                                } else if ($trabajo2 == "") {
                                    $espacio_academico = "02";
                                    $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                            
                                } else if ($trabajo1 == "") {
                                    $espacio_academico = "03";
                                    $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                }
                                            
                                if ($tutor2 == "0") {
                                } else {
                                    $proyecto->Agregar_tutor($tutor2, $rol, $id_proyecto);
                                }
                                $proyectoc->Ingresar_Datos_ES($id_proyecto,$codigoe1);
                                
                                $mensaje=utf8_decode("Buen día ha registrado un anteproyecto con el nombre ".$nombre_ante);
                                $asunto="Registro de anteproyecto";
                                $destino=$info['Correo']; 

                                $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje); 
                                
                                echo('<br><br><br><br>
                                      <div class="alert alert-success" role="alert">
                                      <h4 class="alert-heading">Registro Exitoso!</h4>
                                      <p>Por favor dirijase a la opción Proyecto/Ver estado.</p>
                                      <hr>
                                      <p class="mb-0"></p>
                                     </div>');
                                echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
                                
                            } else {
                                echo('<br><br><br><br>
                                      <div class="alert alert-danger" role="alert">
                                      <h4 class="alert-heading">Algo ha salido mal!</h4>
                                      <p>Ruta vacía.</p>
                                      <hr>
                                      <p class="mb-0"></p>
                                      </div>');
                                echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                            }
                        } else { 
                            
                            if($estudiante2 ==""){
                                echo('<br><br><br><br>
                                      <div class="alert alert-danger" role="alert">
                                      <h4 class="alert-heading">Algo ha salido mal!</h4>
                                      <p>El código del segundo estudiante debe ser ingresado.</p>
                                      <hr>
                                      <p class="mb-0"></p>
                                      </div>');
                                echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                            }else{
                                $verificarcod1=$estudiante->Verificacion_de_carrera($codigoe1);
                                $verificarcod2=$estudiante->Verificacion_de_carrera($estudiante2);
                                $verifi= $estudiante->estudianteExiste($estudiante2);
                                $relacionado = $estudiante->proyectoRelacionado($estudiante2);
                                $name = $relacionado['Nombre_Proyecto'];
                        
                                if($verifi==TRUE){
                                
                                    if($verificarcod1['Id_Carrera']==$verificarcod2['Id_Carrera']){
                                        
                                        if($name == NULL){
                                            $verificaar=$estudiante->eliminarProyecto($estudiante2); 
                            
                                            if($verificaar==TRUE){
                                                $destino_pro = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/archivos/" . $nombre_archivo;
                                                $destino_carta_c = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . "/Carta_Grupo_Inves/" . $carta_c;
                            
                                                if ((copy($ruta_pro, $destino_pro)) && (copy($ruta_carta_c, $destino_carta_c))) {
                                                    $proyecto->Agregar_estudiante($id_proyecto, $estudiante2);
                                                    $proyecto->Estudiante_Agrega_AnteProyecto($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $grupo, $duracion, $objetivo_general, $objetivos, $resumen);
                                                    $proyecto->Agregar_tutor($tutor, $rol, $id_proyecto);
                                                    $proyecto->Agregar_tutor($evaluador, $rolev, $id_proyecto); 
                                                    $proyecto->Documentos_modalidades($id_proyecto, $carta_c, $id_proyecto);
                                                    $proyecto->estados_proyecto($id_proyecto);
                                                    
                                                    if ($trabajo1 == "02" && $trabajo2 == "03") {
                                                        $espacio_academico = "02";
                                                        $espacio_academico2 = "03";
                                                        $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                                        $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico2);
                                                    
                                                    }else if($trabajo1 == "" && $trabajo2 == ""){
                                                        $espacio_academico = "01";
                                                        $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                                    
                                                    } else if ($trabajo2 == "") {
                                                        $espacio_academico = "02";
                                                        $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                                    
                                                    } else if ($trabajo1 == "") {
                                                        $espacio_academico = "03";
                                                        $proyecto->Agregar_Espacio_Academico($id_proyecto, $espacio_academico);
                                                    }
                                                    
                                                    if ($tutor2 == "0") {
                                                    } else {
                                                        $proyecto->Agregar_tutor($tutor2, $rol, $id_proyecto);
                                                    }
                                                    $proyectoc->Ingresar_Datos_ES($id_proyecto,$codigoe1);
                                                    $proyectoc->Ingresar_Datos_ES($id_proyecto,$estudiante2);
                                                    
                                                    $mensaje=utf8_decode("Buen día ha registrado un anteproyecto con el nombre ".$nombre_ante);
                                                    $asunto="Registro de anteproyecto";
                                                    $destino=$info['Correo']; 
                                
                                                    $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje); 
                                                    
                                                    echo('<br><br><br><br>
                                                          <div class="alert alert-success" role="alert">
                                                          <h4 class="alert-heading">Registro Exitoso!</h4>
                                                          <p>Por favor dirijase a la opción Proyecto/Ver estado.</p>
                                                          <hr>
                                                          <p class="mb-0"></p>
                                                          </div>');
                                                    echo("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
                                                } else {
                                                    echo('<br><br><br><br>
                                                          <div class="alert alert-danger" role="alert">
                                                          <h4 class="alert-heading">Algo ha salido mal!</h4>
                                                          <p>Ruta vacía.</p>
                                                          <hr>
                                                          <p class="mb-0"></p>
                                                          </div>');
                                                    echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                                                }
                                            }else{
                                                echo('<br><br><br><br>
                                                      <div class="alert alert-danger" role="alert">
                                                      <h4 class="alert-heading">Algo ha salido mal!</h4>
                                                      <p>Problema con eliminación del anteproyecto del segundo estudiante.</p>
                                                      <hr>
                                                      <p class="mb-0"></p>
                                                      </div>');
                                                echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>");
                                            }
                                        }else{
                                            echo('<br><br><br><br>
                                                  <div class="alert alert-danger" role="alert">
                                                  <h4 class="alert-heading">Algo ha salido mal!</h4>
                                                  <p>El segundo estudiante ya se encuentra registrado en otro anteproyecto.</p>
                                                  <hr>
                                                  <p class="mb-0"></p>
                                                  </div>');
                                            echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                                        }        
                                    }else{
                                        echo('<br><br><br><br>
                                              <div class="alert alert-danger" role="alert">
                                              <h4 class="alert-heading">Algo ha salido mal!</h4>
                                              <p>¡Los estudiantes deben pertenecer a la misma carrera!</p>
                                              <hr>
                                              <p class="mb-0"></p>
                                              </div>');
                                        echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
                                    }
                                }else{
                                    echo('<br><br><br><br>
                                          <div class="alert alert-danger" role="alert">
                                          <h4 class="alert-heading">Algo ha salido mal!</h4>
                                          <p>Segundo estudiante no encontrado, por favor verifique si ya se registro en la plataforma</p>
                                          <hr>
                                          <p class="mb-0"></p>
                                          </div>');
                                    echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>");
                                }
                            }
                        }
                    }
                }
            }
    }else{
        echo('<br><br><br><br>
              <div class="alert alert-danger" role="alert">
              <h4 class="alert-heading">Algo ha salido mal!</h4>
              <p>Documento vacío</p>
              <hr>
              <p class="mb-0"></p>
              </div>');
        echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
    }
} else {
    echo('<br><br><br><br>
          <div class="alert alert-danger" role="alert">
          <h4 class="alert-heading">Algo ha salido mal!</h4>
          <p>Algún documento excedió el tamaño máximo de 3mb</p>
          <hr>
          <p class="mb-0"></p>
          </div>');
    echo("<meta http-equiv='refresh' content='4;URL=../Agregar_Investigacion_Innovacion.php'>"); //redirects after 3 seconds
}
