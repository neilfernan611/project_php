<?php
echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');
echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');
require_once "../Modelo/DAO_LineaInvestigacion.php";

if (!filter_input(INPUT_POST, "bot_linea")) {

    header("Location:../Editar_Lineaa.php");
} else {
    $id = htmlentities(addslashes(filter_input(INPUT_POST, "il")));
    $li = htmlentities(addslashes(filter_input(INPUT_POST, "lin")));
    $es = htmlentities(addslashes(filter_input(INPUT_POST, "estado")));
    $editar = 1;
    $objLi = new DAO_LineaInvestigacion();
    $verifi = $objLi->verificar_inves($id_linea, $linea,$editar,$es);
      if ($verifi == 0) {

        $Li = $objLi->actualizarLinea($id, $li, $es);
                
         echo('<br><br><br><br>
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Bien hecho!</h4>
            <p>¡Información Actualizada.</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../Info_Linea.php'>");
                }else {
                    echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Advertencia!</h4>
            <p>¡Verifique la información ingresada, dos lineas no pueden tener el mismo nombre o codigo.</p>
            <hr>
            <p class="mb-0">
            </p>
            </div>');
            echo ("<meta http-equiv='refresh' content='3;URL=../Info_Linea.php'>");
                }
   
    header("Location:../Info_Linea.php");
}
