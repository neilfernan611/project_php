<?php

session_start();
require_once "../Modelo/DAO_LineaInvestigacion.php";

$id = (String) htmlentities(addslashes(filter_input(INPUT_GET, "idl")));

$objL = new DAO_LineaInvestigacion();

$idl = $objL->eliminarLinea($id);
header("Location:../Info_Linea.php");
