<?php

echo ('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
');

echo('<link rel="stylesheet" href="../css/estilos_mensaje.css"> ');


session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}

require_once '../Modelo/DAO_Persona.php';
require_once '../Modelo/DAO_Coordinacion.php';
require_once '../Modelo/DAO_Comite.php';
require_once '../Modelo/Codigos.php';
require_once '../Modelo/GenerarPass.php';
require_once '../Modelo/Email.php';

$iden = htmlentities(addslashes(filter_input(INPUT_POST, "iden")));
$ObjEmail = new Email();
if ($administrador != null) {
    $nombre = $_SESSION["nombre"];
    $infop = $_SESSION["informacion"];
    $corre_a = $infop['Correo'];

    $nom = htmlentities(addslashes(filter_input(INPUT_POST, "nombre")));
    $cedula = htmlentities(addslashes(filter_input(INPUT_POST, "cedula")));
    $correo = htmlentities(addslashes(filter_input(INPUT_POST, "email")));
    $telefono = htmlentities(addslashes(filter_input(INPUT_POST, "telefono")));

    $contra = new GenerarPass();
    $pass = $contra->randomPass(6); //Genera la contraseña
    $pass_encriptada = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 5]);
    $objDP = new DAO_Persona();
    $objCo = new DAO_Coordinacion();
    $objCom = new DAO_Comite();
    $verificarp=$objDP->Verificar_Correo_Cedula($correo, $cedula);
    $c = new Codigos();
    
    if($verificarp == FALSE){
        if ($iden == "COOR") {

            $idp = $objDP->Insertar_Persona($nom, $correo, $cedula, $telefono);
            $codc = $c->getPINCOOR(3); //Genera codigo para la coordinacion
            $co = $objCo->crearUsuarioCoordinacion($codc, $correo, $pass_encriptada);
            if ($idp == "") {
                $mensaje = utf8_decode("Buen día " . $nom . " se ha registrado en la plataforma sisproyud<br>Usuario: " . $correo . "<br>Contraseña: " . $pass . "<br>https://sisproyud.com/");
                $asunto = "Registro en la plataforma Sisproyud ";
                $destino = $correo;
    
                $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje);
    
                echo('<br><br><br><br>
                    <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Bien hecho!</h4>
                    <p>¡El usuario ha sido creado con exito!</p>
                    <hr>
                    <p class="mb-0">
                    </p>
                    </div>');
                echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Coordinacion.php'>"); //redirects after 3 seconds
                
            } else {
                echo('<br><br><br><br>
                    <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Algo ha salido mal!</h4>
                    <p>Lo sentimos! El correo o la cédula ya se encuentran registrados.<br>Por favor verifique los datos ingresados</p>
                    <hr>
                    <p class="mb-0"></p>
                    </div>');
                echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Coordinacion.php'>"); //redirects after 3 seconds
            }
        } else if ($iden == "COM") {
            $id = $objDP->Insertar_Persona($nom, $correo, $cedula, $telefono);
            $codc = $c->getPINCOM(3); //Genera codigo para la comite
            $com = $objCom->crearUsuarioComite($codc, $correo, $pass_encriptada);
    
            if ($id == "") {
                $mensaje = utf8_decode("Buen día " . $nom . " se ha registrado en la plataforma Sisproyud<br>Usuario: " . $correo . "<br>Contraseña: " . $pass . "<br>https://sisproyud.com/");
                $asunto = "Registro en la plataforma Sisproyud ";
                $destino = $correo;
    
                $ObjEmail->Enviar_Mail($destino, $asunto, $mensaje);
                
                echo('<br><br><br><br>
                    <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Bien hecho!</h4>
                    <p>¡El usuario ha sido creado con exito!</p>
                    <hr>
                    <p class="mb-0">
                    </p>
                    </div>');
                echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Comite.php'>"); //redirects after 3 seconds
                
            } else {
                    echo('<br><br><br><br>
                        <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Algo ha salido mal!</h4>
                        <p>Lo sentimos! El correo o la cédula ya se encuentran registrados.<br>Por favor verifique los datos ingresados</p>
                        <hr>
                        <p class="mb-0"></p>
                        </div>');
                    echo ("<meta http-equiv='refresh' content='4;URL=../Agregar_Comite.php'>"); //redirects after 3 seconds
            }
        }
    }else{
        echo('<br><br><br><br>
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Algo ha salido mal!</h4>
            <p>Lo sentimos! El correo o la cédula ya se encuentran registrados.<br>Por favor verifique los datos ingresados</p>
            <hr>
            <p class="mb-0"></p>
            </div>');
        echo ("<meta http-equiv='refresh' content='4;URL=../home.php'>"); //redirects after 3 seconds
    }
}








