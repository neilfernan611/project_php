<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
ob_start();
require_once './Modelo/DAO_Profesor.php';

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}


$nombrepro = "";
$externo="";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar este usuario?'))
                    return false;
            }

        </script>
    </head>
    <body>
        <br>
        <?php
        if ((filter_input(INPUT_POST, "buscar"))) {
            $nombrepro = filter_input(INPUT_POST, "nombre");
            $externo = filter_input(INPUT_POST, "externo");
        }
        if ($administrador != null) {
            ?>

            <?php
            if (!($_GET)) {

                echo "<script>location.href='Consultar_Profesor.php?pagina=1';</script>";
            }
            ?>

            <form class="form-inline" method="post">
                <!--<div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">EMai</label>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Nombre del profesor">
                </div>-->
                <div class="form-group mx-sm-3 mb-2">
                    
                    <input type="text" class="form-control" name="nombre" id="nombre" required placeholder="Nombre del profesor">
                </div>
                 <div class="form-group mx-sm-4 mb-3">
                            <label for="staticEmail2" class="sr-only">EMai</label>
                            <select id="selectTipo" class="custom-select" required name="externo" >
                                
                                <option value="" disabled selected>Externo</option>
                                <?php
                                $obj_gr = new DAO_Profesor();
                                $listagr = $obj_gr->estado_externo();
                                foreach ($listagr as $list) {
                                ?>
                                    <option required value="<?php echo($list['Id_Externo']); ?>"><?php echo($list['Externo']); ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR"  name="buscar">
            </form>

            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                    <!--<th colspan="7"><h2><p style="color:white"><center>Información Profesor</center></p></h2></th></tr>-->
                    <tr >
                        <th scope="col">Cédula</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Ciencias Básicas</th>
                        <th scope="col">Borrar</th>
                        <th scope="col">Actualizar</th>
                    </tr>
                </thead>

                <?php
                if ($nombrepro == null || $nombrepro == ("")) {
                    $info = new DAO_Profesor();
                    $lista = $info->listarProfesor();
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Consultar_Profesor.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $info->listarProfesor_paginacion($iniciar, $registros_x_pagina);
                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Cedula"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Nombre"]); ?> </th>
                            <th scope="col"><?php echo($lis["Correo"]); ?> </th>
                            <th scope="col"><?php echo($lis["Telefono"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Pertenecer"]); ?> </th>
                            <td class="bot"><a href="Control/Eliminar_Usuario.php?idco=<?php echo("profesor");?>&correo=<?php echo ($lis["Correo"]);?>"  onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Profesor.php?idp=<?php echo ($lis["Cedula"]);?>&nombre=<?php echo ($lis["Nombre"]); ?>&correo=<?php echo ($lis["Correo"]); ?>&tel=<?php echo ($lis["Telefono"]); ?>&pertenece=<?php echo ($lis["Pertenecer"]); ?> "><img src="Img/update.png"></a></td>
                        </tr>
                        <?php
                    }
                } else {
                    $info = new DAO_Profesor();
                    $lista = $info->listarProfesorNombre($nombrepro,$externo);
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Consultar_Profesor.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $info->listarProfesorNombre_paginacion($nombrepro, $iniciar, $registros_x_pagina);
                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Cedula"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Nombre"]); ?> </th>
                            <th scope="col"><?php echo($lis["Correo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Telefono"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Pertenecer"]); ?> </th>
                            <td class="bot"><a href="Control/Eliminar_Profesor.php?correo=<?php echo ($lis["Correo"]) ?>"  onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Profesor.php?idp=<?php echo ($lis["Cedula"]); ?> & nombre=<?php echo ($lis["Nombre"]); ?> & correo=<?php echo ($lis["Correo"]); ?> & tel=<?php echo ($lis["Telefono"]); ?> & pertenece=<?php echo ($lis["Pertenecer"]); ?> "><img src="Img/update.png"></a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Consultar_Profesor.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Consultar_Profesor.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Consultar_Profesor.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>

            <!--Fin tabla-->
            <?php
        } else {
            session_destroy();
            
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>

    </body>
</html>




