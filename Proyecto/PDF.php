<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './Modelo/DAO_Anteproyecto.php';

$id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
$id2 = htmlentities(addslashes(filter_input(INPUT_GET, "correo")));
$pdf = new DAO_Anteproyecto();
$PDF = $pdf->InformacionProyecto($id_proyecto);
$carta2= $pdf->Archivo_proyecto($id_proyecto);
?>
<?php
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
     <?php 
        if($id2==$id_proyecto){
        if ($carta2['Nombre_Archivo'] == "") { ?>
            <p>No tiene archivos asociados</p>
        <?php } else {
            ?>
            <div class="embed-responsive embed-responsive-16by9">
                <?php
                if($PDF['Id_Modalidad']!='206')
                {
                ?>
                <iframe class="embed-responsive-item" src="Carta_Consejo/<?php echo ($PDF['Nombre_Archivo']); ?>" scrolling="no" allowfullscreen></iframe>
           <?php
                }else{
                    ?>
                <iframe class="embed-responsive-item" src="Carta_Grupo_Inves/<?php echo ($PDF['Nombre_Archivo']); ?>" scrolling="no" allowfullscreen></iframe>
                <?php
                }
           ?>
            </div>     
        <?php }
        }
        else{
        ?>
        <?php if ($PDF['Nombre_Archivo'] == "") { ?>
            <p>No tiene archivos asociados</p>
        <?php } else {
            ?>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="archivos/<?php echo ($PDF['Nombre_Archivo']); ?>" scrolling="no" allowfullscreen></iframe>
            </div>     
        <?php } 
        }?>
    </body>
</html>


