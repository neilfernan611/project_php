<!DOCTYPE html>

<?php
require_once './home.php';
require_once './Modelo/DAO_Anteproyecto.php';
require_once './Modelo/DAO_Estudiante.php';
require_once './Modelo/DAO_Modalidad.php';
require_once './Modelo/DAO_Profesor.php';
require_once './Modelo/DAO_LineaInvestigacion.php';
?>
<html>
    <head><meta charset="utf-8">

        <title></title>
    
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/plotly-latest.min.js"></script>
            <link rel="stylesheet" href="css/estilos.css">
    
        </style>
    </head>
    <body>
        

        <?php
        if ($coordinacion = !null || $comite = !null) {
        
            ?>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary">
                            <div class="panel panel-heading">Ingrese la información</div><br>
                            <div class="panel panel-body">
                               <form method="POST" action="Control/exportar.php">
                              
                                <div class="row">
                                    
                                    <div class="col-sm-4 mb-3">
                                        <label for="selectTipo" >Fecha desde</label>
                                        <input type="date" class="custom-select" id="fechadesde"  name="fechadesde" required="">
                                    </div>
                                    <div class="col-sm-4 mb-3">
                                        <label for="selectTipo" >Fecha hasta</label>
                                        <input type="date" class="custom-select" id="fechahasta"  name="fechahasta" required="">
                                    </div>
                                    <div><input type="submit" class="btn btn-primary mx-sm-4 mb-4" id="btn-aceptarR" name="guardar" onclick="ajax_post();" value="buscar"></div>
                                   
                                    </div>
                                     </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary">
                            <div class="panel panel-heading">Graficas</div>
                            <div class="panel panel-body">
                                <div class="row">
                                    <!-- barras -->

                                    <div class="col-sm-6">
                                        <div id="cargar_barras">

                                        </div>
                                    </div>

                                    <!-- circular -->

                                    <div class="col-sm-6">
                                        <div id="cargar_circular">


                                        </div>
                                    </div>
                                    <!-- circular -->
                                    <div class="col-sm-6">
                                        <div id="cargar_circular2">

                                        </div>
                                    </div>
                                    <!-- circular -->
                                    <div class="col-sm-6">
                                        <div id="cargar_circular3">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

           






            <script type="text/javascript">


                resultado = document.getElementById("cargar_barras");

                function ajax_post() {
                    var ajax;
                    if (window.XMLHttpRequest) {
                        ajax = new XMLHttpRequest();
                    } else {
                        ajax = new ActiveXObject("Microsoft.XMLHTTP");

                    }

                    var fechad = document.getElementById("fechadesde").value;
                    var fechaa = document.getElementById("fechahasta").value;

                    var datos3 = "fechadesde=" + fechad + "&fechahasta=" + fechaa ;

                    ajax.onreadystatechange = function () {
                        if (ajax.readyState === 4 && ajax.status === 200) {
                            
                        }
                    };
                    
                    $('#cargar_barras').load("Control/grafico_barras.php?fechadesde=" + fechad + "&fechahasta=" + fechaa );
                   /* $('#cargar_circular').load("Control/grafico_circular.php?fechadesde=" + fechad + "&fechahasta=" + fechaa );
                    $('#cargar_circular2').load("Control/grafico_circular.php?fechadesde=" + fechad + "&fechahasta=" + fechaa );
                    $('#cargar_circular3').load("Control/grafico_circular.php?fechadesde=" + fechad + "&fechahasta=" + fechaa );*/
                }
                  

            </script>



           
            
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>
