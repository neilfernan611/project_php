<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './Modelo/DAO_Proyecto.php';
require_once './home.php';
$pdf = new DAO_Proyecto();
    
?>

<?php

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        
        
        <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                       <!-- <th colspan="13"><h2><p style="color:white"><center>Información Coordinación</center></p></h2></th></tr>-->
                    <tr>
                        <th scope="col">Id_Proyecto</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Objetivos</th>
                        <th scope="col">Resumen Ejecutivo</th>
                        <th scope="col">Tutor</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Jurado 1</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Jurado 2</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Radicado</th>
                        <th scope="col">Ver</th>
                        <th scope="col">Generar Radicado</th>
                        
                    </tr>
                </thead>
                <?php
                 
                $proy = new DAO_Proyecto();
                $lista_ant = $proy->Consultar_Proyectos();
                foreach ($lista_ant as $lista){
                ?>
              
                    <tr>
                        <th scope="col"><?php echo($lista["id_proyecto"]); ?> </th>
                        <th scope="col"><?php echo($lista["nombre_proyecto"]); ?> </th>
                        <th scope="col"><?php echo($lista["objetivos"]); ?> </th>
                        <th scope="col"><?php echo($lista["resumen_ejecutivo"]); ?> </th>
                        <th scope="col"><?php echo($lista["nombre_tutor"]); ?> </th>
                        <th scope="col"><?php echo($lista["estadot"]); ?>  </th>
                        <th scope="col"><?php echo($lista["nombre_jurado1"]); ?> </th>
                        <th scope="col"><?php echo($lista["estadoj1"]); ?>  </th>
                        <th scope="col"><?php echo($lista["nombre_jurado2"]); ?> </th>
                        <th scope="col"><?php echo($lista["estadoj2"]); ?>  </th>
                        <th scope="col"><?php echo($lista["radicado"]); ?>  </th>
                        <th class="bot"><a href="PDF.php?id=<?php echo($lista["id_proyecto"]); ?>">Ver</a></th>
                        <?php
                        if($lista["radicado"] == NULL && $lista["nombre_jurado1"]!= "0" && $lista["nombre_jurado2"]!= "0" ||  $lista["radicado"] == "0" && $lista["nombre_jurado1"]!= "0" && $lista["nombre_jurado2"]!= "0" || $lista["radicado"] =='' && $lista["nombre_jurado1"]!= "0" && $lista["nombre_jurado2"]!= "0")
                        {
                        ?>
                        <th class="bot"><a href="Control/Generar_Radicado_Control.php?id=<?php echo($lista["id_proyecto"]); ?>">Generar</a></th>
                       <?php
                        }else{                            
                        ?>
                       <th class="bot">Generar</th>
                <?php}?>
                            
                        
                    </tr>
                 <?php
                }}
                    
                 ?>
            </table>
   
        
    </body>
</html>
