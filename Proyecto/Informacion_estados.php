<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
    $datos = $_SESSION["informacion2"];
    $codigo = $datos['Codigo'];
    $proyme = $datos['Id_Proyecto'];
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
    </head>
    <body>
        <?php
        if ($estudiante != null) {
            
            require_once './Modelo/DAO_Anteproyecto.php';
            require_once './Modelo/DAO_Estudiante.php';
            
            $id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
            
            $objProyecto = new DAO_Anteproyecto();
            $objestu = new DAO_Estudiante();
            
            //Cuando ya subio anterpoyecto
            $proyecto = $objProyecto->Consultar_Estado_EvaluadorE($proyme);
            $proy = $objProyecto->Consultar_Estado_ComiteE($proyme);
            $proyc = $objProyecto->Consultar_Estado_CoordinacionE($proyme);
            
            //Cuando tiene ya tiene un anteproyecto devuelto
            $existenciae =  $objProyecto->Consultar_Existencia_Estudiante_Devuelto($codigo);
            $proyectod = $objProyecto->Consultar_Estado_EvaluadorE_Devuelto($codigo);
            $namep = $objestu->proyectoRelacionado($codigo);
            ?>
            <div class="container">
                <?php    
                if($existenciae == 1 && $namep['Nombre_Proyecto']== NULL){
                ?>
                        <div class="card">
                            <div>
                                <h2>EVALUADOR</h2>
                                <p><b><?php echo ($proyectod['Rol'] . ": "); ?></b><?php echo ($proyectod['Nombre']); ?></p><br>
                                <p><b>Fecha: </b><?php echo($proyectod['Fecha_Evaluador']); ?></p><br>
                                <p><b>Concepto: </b><?php echo($proyectod['Estado']); ?></p><br>
                                <p><b>Comentario:</b></p>
                                <p style="font-size: 19px">
                                    <?php
                                    $comentariop= $proyectod['Id_Proyecto'];
                                    $num = $objProyecto->Consultar_Existencia_Comentario_EvaluadorE($comentariop);
                                    if ($num == 0) {                              
                                    } else {
                                        $comentario = $objProyecto->Consultar_Comentario_EvaluadorE($comentariop);
                                        echo($comentario['Comentario']);
                                    ?>
                                </p>
                                    <?php } ?>
                            </div>   
                        </div>
                    </div>
                <?php } else{
                    
                            if($namep['Id_Modalidad']=='204' || $namep['Id_Modalidad']=='205' || $namep['Id_Modalidad']=='208'){
                            
                                if($proyc['Radicado']!= NULL){
                ?>
                                    <div class="card">
                                        <div>
                                            <h2>COORDINACIÓN</h2>
                                            <p><b>Fecha del radicado: </b><?php echo($proyc['Fecha_Radicado']); ?></p><br>
                                            <p><b>Radicado: </b><?php echo($proyc['Radicado']); ?></p><br>
                                        </div>   
                                    </div>
                                <?php }?>
                                <div class="card">
                                    <div>
                                        <h2>COMITÉ</h2>
                                        <p><b>Fecha: </b><?php echo($proy['Fecha_Comite']); ?> </p><br>
                                        <p><b>Concepto: </b><?php echo($proy['Estado']); ?></p><br>
                                        <p><b>Comentario:</b></p>
                                        <p style="font-size: 19px"> 
                                            <?php
                                            $numE = $objProyecto->Consultar_Existencia_Comentario_ComiteE($id_proyecto);
                                            if ($numE == 0) { 
                                            } else {
                                                $comentario = $objProyecto->Consultar_Comentario_ComiteE($id_proyecto);
                                                echo($comentario['Comentario']);
                                            ?>
                                        </p>
                                            <?php } ?>
                                    </div>   
                                </div>
                            <?php }else{
                                    
                                    if($proyc['Radicado']!=NULL){
                            ?>
                                        <div class="card">
                                            <div>
                                                <h2>COORDINACIÓN</h2>
                                                <p><b>Fecha del radicado: </b><?php echo($proyc['Fecha_Radicado']); ?></p><br>
                                                <p><b>Radicado: </b><?php echo($proyc['Radicado']); ?></p><br>
                                            </div>   
                                        </div>
                                    <?php }
                                    if($proy['Estado']!="Por Evaluar"){
                                    ?>
                                        <div class="card">
                                            <div>
                                                <h2>COMITÉ</h2>
                                                <p><b>Fecha de sesión: </b><?php echo($proy['Fecha_Comite']); ?> </p><br>
                                                <p><b>Concepto: </b><?php echo($proy['Estado']); ?></p><br>
                                                <p><b>Comentario:</b></p>
                                                <p style="font-size: 19px"> 
                                                    <?php
                                                    $numE = $objProyecto->Consultar_Existencia_Comentario_ComiteE($id_proyecto);
                                                    if ($numE == 0) { 
                                                    } else {
                                                        $comentario = $objProyecto->Consultar_Comentario_ComiteE($id_proyecto);
                                                        echo($comentario['Comentario']);
                                                    ?>
                                                </p>
                                                    <?php } ?>
                                            </div>   
                                        </div>
                                    <?php } ?>
                                    <div class="card">
                                        <div>
                                            <h2>EVALUADOR</h2>
                                            <p><b><?php echo ($proyecto['Rol'] . ": "); ?></b><?php echo ($proyecto['Nombre']); ?> </p><br>
                                            <p><b>Fecha: </b><?php echo($proyecto['Fecha_Evaluador']); ?> </p><br>
                                            <p><b>Concepto: </b><?php echo($proyecto['Estado']); ?></p><br>
                                            <p><b>Comentario:</b></p>
                                            <p style="font-size: 19px"> 
                                                <?php
                                                $num = $objProyecto->Consultar_Existencia_Comentario_EvaluadorE($id_proyecto);
                                                if ($num == 0) {                              
                                                } else {
                                                    $comentario = $objProyecto->Consultar_Comentario_EvaluadorE($id_proyecto);
                                                    echo($comentario['Comentario']);
                                                ?>
                                            </p>
                                                <?php } ?>
                                        </div>   
                                    </div>
                            <?php }
                            ?>
                <?php }
                ?>
            </div>
        <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        } ?>
    </body>
</html>


