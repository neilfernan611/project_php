<!DOCTYPE html>
<?php
require_once './Modelo/DAO_Estudiante.php';
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
    </head>
    <body>
        <header>
            <section class="cabecera">
                <a href="index.php"><img src="Img/header1.png" width="100px"></a>
                <h2>Tecnología en Electrónica</h2>
            </section>
        </header>
        <div class="container well" id="container-registro">
            <h5><p class="lead" >CREE UNA CUENTA</p></h5>
            <div class="row" id="container-elementosR">
                <div class="col-md-8 order-md-1">
                    <form action="Control/Agregar_Estudiante.php" method="POST" id="form-registro" >
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="nombre" style="color: black">Nombre Completo</label>
                                <input type="text" class="form-control" id="nombre"  pattern="[A-Z a-záéíóú]{8,50}" name="nombre" required>
                                <div class="invalid-feedback">
                                    Los nombres son obligatorios.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="email" style="color: black">Email <span class="text-muted"></span></label>
                                <input type="email" class="form-control" id="email" name="email" required placeholder="you@example.com">
                                <div class="invalid-feedback">
                                    Por favor, introduzca una dirección de correo electrónico válida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="cedula" style="color: black">Cédula</label>
                                <input type="number" class="form-control" id="cedula" name="cedula"  inputmode="numeric" title="un número de cedula correcto No tiene puntos,espacios ni letras" min="1" placeholder="" required>
                                <div class="invalid-feedback">
                                    El número de documento es necesario
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="telefono" style="color: black">Número celular</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    </div>
                                    <input type="text" class="form-control" name="telefono" id="telefono" placeholder="3221112222"   pattern="[0-9]{10,10}" maxlength="10" minlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>  
                                    <div class="invalid-feedback" style="width: 100%;">
                                        El Número de celular es obligatorio.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="codigo" style="color: black">Código</label>
                                <input type="number" class="form-control" id="codigo" name="codigo" placeholder="" min="11111111111" max="99999999999" title="un codigo estudiantil cuenta con 11 digitos numéricos sin espacios"  required>
                                <div class="invalid-feedback">
                                    El código es obligatorio.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="selectTipo" >Carrera</label>
                                <select id="selectTipo"  required class="custom-select" name="carrera">
                                    <option selected></option>

                                    <?php
                                    $obj_ca = new DAO_Estudiante();
                                    $lista = $obj_ca->ListaCarrera();
                                    foreach ($lista as $list) {
                                        ?>
                                        <option value="<?php echo($list['Id_Carrera']); ?>"><?php echo($list['Carrera']); ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                                <div class="invalid-feedback">
                                    Campo obligatorio.
                                </div>
                            </div>
                        </div>
                        <hr class="col-md-8 mb-1">
                        <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Información</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
