<?php
require_once './home.php';
require_once './Modelo/DAO_Notas.php';
//session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
    </head>
    <body>
        <br>
        <?php
         $datos = new DAO_Notas();
         //////////////id del proyecto
         $id_proy = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
         ////////////
         $titulo = $datos->get_titulo($id_proy);
         //////////////////////////////////////
         $modalidad = $datos->get_modalidad($id_proy);
         ////////// cantidad de proponentes-cantidad de directores-cantidad de jurados
         $propo = $datos->get_proponentes($id_proy);
         $proponente = $propo["proponentes"];
         $direc = $datos->get_directores($id_proy);
         $director = $direc["directores"];
         $jura = $datos->get_jurados($id_proy);
         $jurado = $jura["jurados"];
         /////////////////datos proponente 1
         $estu = $datos->get_estudiante1($id_proy);
         $codigo_estudiante1 = $estu["codigo"];
         $nombre_estudiante1 = $estu["nombre"];
         $cedula_estudiante1 = $estu["cedula"];
         $codigo_estudiante2 = "";
         $nombre_estudiante2 = "";
         $cedula_estudiante2 = "";
         /////////////////datos proponente 2
         if ($proponente == 2){
         $estu2 = $datos->get_estudiante2($id_proy,$cedula_estudiante1);
         $codigo_estudiante2 = $estu2["codigo"];
         $nombre_estudiante2 = $estu2["nombre"];
         $cedula_estudiante2 = $estu2["cedula"];
         }     
         //////////////////////datos director 1
         $dir1 = $datos->get_director1($id_proy);
         $nombre_director1 = $dir1["nombre"];
         $cedula_director1 = $dir1["cedula"];
         $notas_dir1 = $datos->get_notas_director($id_proy,$cedula_director1);
         $nombre_director2 = "";
         $cedula_director2 = "";
         $notas_dir2 = ["n1"=>"","n2"=>"","n3"=>"","n4"=>"","n5"=>"","n6"=>"","final"=>0,];
         //////////////////////datos director 2
         if ($director == 2){
            $dir2 = $datos->get_director2($id_proy,$cedula_director1);
            $nombre_director2 = $dir2["nombre"];
            $cedula_director2 = $dir2["cedula"];
            $notas_dir2 = $datos->get_notas_director($id_proy,$cedula_director2);  
         }
         ///////////////////////////////////datos jurado 1
         if ($jurado==1){
         $jur1 = $datos->get_jurado1($id_proy);
         $nombre_jurado1 = $jur1["nombre"];
         $cedula_jurado1 = $jur1["cedula"];
         $nombre_jurado2 ="";
         $notas_jur1 = $datos->get_notas_jurado($id_proy,$cedula_jurado1);
         $notas_jur2 = ["n1"=>"","n2"=>"","n3"=>"","n4"=>"","n5"=>"","n6"=>"","n7"=>"","n8"=>"","n9"=>"","n10"=>"","n11"=>"","n12"=>"","final"=>0,];
         }
         ///////////////////////////////////datos jurado 2
         if ($jurado==2){
         $jur1 = $datos->get_jurado1($id_proy);
         $nombre_jurado1 = $jur1["nombre"];
         $cedula_jurado1 = $jur1["cedula"];
         $notas_jur1 = $datos->get_notas_jurado($id_proy,$cedula_jurado1);
         $jur2 = $datos->get_jurado2($id_proy,$cedula_jurado1);
         $nombre_jurado2 = $jur2["nombre"];
         $cedula_jurado2 = $jur2["cedula"]; 
         $notas_jur2 = $datos->get_notas_jurado($id_proy,$cedula_jurado2);
         }
         //////////// puntaje de directores y puntaje total
         $puntaje1 = $notas_dir1["final"];
         $puntaje2 =$notas_dir2["final"];
         $puntaje_total = $puntaje1 + $puntaje2;
         ///////////puntaje de jueces y puntaje total
         $puntaje_jues1 = $notas_jur1["final"];
         $puntaje_jues2 = $notas_jur2["final"];
         $puntaje_total_jues = $puntaje_jues1 + $puntaje_jues2;
         //////////////
         $auxilio = $proponente;
         //////////////////
         setlocale(LC_ALL, "es_CO");
         $dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
         $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
         $fecha = $dias[date("w")-1] . " " . (date("d")) . " de " . $meses[date("n") - 1] . " del " . date("Y");
         /////////////////
         $carrera = $datos->get_carrera($id_proy);
        if ($comite = !NULL) {
            ?>
            <div class="py-1 text-center" id="centrar">
                <p><center><b>Datos pre-acta </b></center></p>
        </div>
        <div class="col-md-15 order-md-10 container">
            <form action="Control/Informacion_Acta.php" target="_blank" method="POST" id="form-registro" >
                <input type="text"  id="id_proyecto" name="id_proyecto"  value="<?php  echo $id_proy ?>" style="display: none">
                <input type="text"  id="Director" name="Director"  value="<?php  echo $director ?>" style="display: none">
                <input type="text"  id="numeroestudiante" name="numeroestudiante"  value="<?php  echo $auxilio ?>" style="display: none">
                <input type="text"  id="Jurado" name="Jurado"  value="<?php  echo $jurado ?>" style="display: none">
                <input type="text"  id="Carrera" name="Carrera"  value="<?php  echo $carrera["Carrera"] ?>" style="display: none">
                <label> <b>Acta Numero:</b><input type="text" name="Numero_Acta"  class="form-control" placeholder="Ingrese un valor" required/> </label><br>
                
                <label><b>Fecha:</b></label><br>
                <input type="text" placeholder="dia" name="ndia" required> de
                <select name="carrera"  required>
                <option selected></option>
                <?php
                for ($i = 0; $i < 12; $i++) {
                ?>
                <option value="<?php echo $meses[$i] ?>"><?php echo$meses[$i] ?></option>
                <?php
                }
                ?>
                </select> del
                <input name="ano" value =<?php echo date("Y"); ?>><br>
                
                <label> <b>Titulo del proyecto: </b><input type="text" name="Titulo_Proyecto"  class="form-control" placeholder="Titulo del proyecto" required value="<?php echo $titulo["titulo"];?>" /></label><br>
                <label> <b>Modalidad: </b><input type="text" name="Modalidad" readonly="readonly" class="form-control" placeholder="Modalidad" required value="<?php echo $modalidad["modalidad"];?>"  /> </label><br>
                <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=3 ><center> Proponente 1 </center></TH> 
                    </TR>
                    <TR>
                        <TH> Codigo: </Th> <Th> Nombre: </Th> <Th> Documento: </Th> 
                    </TR>  
                    <TR>
                        <Td><input type="text" name="Codigo_Estudiante1"  class="form-control" placeholder="Codigo estudiante 1" required value ="<?php echo $codigo_estudiante1?>"/></Td> 
                        <Td><input type="text" name="Nombre_Estudiante1"  class="form-control" placeholder="Nombre estudiante 1" required value ="<?php echo $nombre_estudiante1?>"/></Td>
                        <Td><input type="text" name="Documento_Estudiante1"  class="form-control" placeholder="Documento estudiante 1" required value ="<?php echo $cedula_estudiante1?>"/></Td>
                    </TR>
                </TABLE>    
                <br>
                <?php
                if ($proponente == 2) {
                ?>  
                  <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=3><center> Proponente 2 </center></TH> 
                    </TR>
                    <TR>
                        <TH> Codigo: </Th> <Th> Nombre: </Th> <Th> Documento: </Th> 
                    </TR>  
                    <TR>
                        <Td><input type="text" name="Codigo_Estudiante2"  class="form-control" placeholder="Codigo estudiante 2" value ="<?php echo $codigo_estudiante2?>"/></Td> 
                        <Td><input type="text" name="Nombre_Estudiante2"  class="form-control" placeholder="Nombre estudiante 2" value ="<?php echo $nombre_estudiante2?>"/></Td>
                        <Td><input type="text" name="Documento_Estudiante2"  class="form-control" placeholder="Documento estudiante 2" value ="<?php echo $cedula_estudiante2?>"/></Td>
                    </TR>
                </TABLE>
                <br>
                 <?php
                   }   
                 ?>
                 <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=7><center> Director 1 </center></TH> 
                    </TR>
                    <TR>
                        <TH COLSPAN=3> Nombre: </Th> <Th COLSPAN=4> Documento: </Th> 
                    </TR>  
                    <TR>
                        <Td COLSPAN=3><input type="text" name="Nombre_Director1"  class="form-control" placeholder="Nombre del Director 1" required value ="<?php echo $nombre_director1?>"/></Td> 
                        <Td COLSPAN=4><input type="text" name="Documento_Director1"  class="form-control" placeholder="Documento del Director 1" required value="<?php echo $cedula_director1?>"/></Td>
                    </TR>
                    <TR>
                        <TH COLSPAN=6> Notas: </TH> <TH COLSPAN=1> Total: </TH>
                    </TR>
                    <TR>
                        <Td><input type="text" style="width : 90px" name="Nota1_1" readonly="readonly" required value="<?php echo $notas_dir1["n1"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota1_2" readonly="readonly" required value="<?php echo $notas_dir1["n2"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota1_3" readonly="readonly" required value="<?php echo $notas_dir1["n3"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota1_4" readonly="readonly" required value="<?php echo $notas_dir1["n4"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota1_5" readonly="readonly" required value="<?php echo $notas_dir1["n5"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota1_6" readonly="readonly" required value="<?php echo $notas_dir1["n6"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Final1" readonly="readonly" required value="<?php echo $notas_dir1["final"];?>" /></Td>
                    </TR>
                </TABLE>
                <br>
                <?php
                if ($director != 1) {
                    $puntaje_total = $puntaje_total/2;
                ?>  
                 <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=7><center> Director 2</center></TH> 
                    </TR>
                    <TR>
                        <TH COLSPAN=3> Nombre: </Th> <Th COLSPAN=4> Documento: </Th>  
                    </TR>  
                    <TR>
                        <Td COLSPAN=3><input type="text" name="Nombre_Director2"  class="form-control" placeholder="Nombre del Director 2" value ="<?php echo $nombre_director2?>"/></Td> 
                        <Td COLSPAN=4><input type="text" name="Documento_Director2"  class="form-control" placeholder="Documento del Director 2" value ="<?php echo $codigo_director2?>"/></Td>
                    </TR>
                    <TR>
                        <TH COLSPAN=6> Notas: </TH> <TH COLSPAN=1> Total: </TH>
                    </TR>
                    <TR>
                        <Td><input type="text" style="width : 90px" name="Nota2_1" readonly="readonly" required value="<?php echo $notas_dir2["n1"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota2_2" readonly="readonly" required value="<?php echo $notas_dir2["n2"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota2_3" readonly="readonly" required value="<?php echo $notas_dir2["n3"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota2_4" readonly="readonly" required value="<?php echo $notas_dir2["n4"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota2_5" readonly="readonly" required value="<?php echo $notas_dir2["n5"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Nota2_6" readonly="readonly" required value="<?php echo $notas_dir2["n6"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Final2" readonly="readonly" required value="<?php echo $notas_dir2["final"];?>" /></Td>
                    </TR>
                </TABLE>
                <br>
                 <?php
                   }   
                 ?>
                 <?php
                if ($jurado == 0) {
                    $puntaje_jues1 = 0;
                    $puntaje_jues2 = 0;
                    $puntaje_total_jues = $puntaje_jues1 + $puntaje_jues2;
                                  } 
                ?> 
                 <?php
                if ($jurado == 1) {
                ?>  
                 <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=13><center> Jurado 1 </center></TH> 
                    </TR>
                    <TR>
                        <TH COLSPAN=13> Nombre: </Th>  
                    </TR>  
                    <TR>
                        <Td COLSPAN=13><input type="text" name="Nombre_Jurado1"  class="form-control" placeholder="Nombre del Jurado 1" value = "<?php echo $nombre_jurado1;?>"/></Td> 
                    </TR>
                    <TR>
                        <TH COLSPAN=12> Notas: </TH> <TH COLSPAN=1> Total: </TH>
                    </TR>
                    <TR>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_1" readonly="readonly" required value="<?php echo $notas_jur1 ["n1"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_2" readonly="readonly" required value="<?php echo $notas_jur1 ["n2"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_3" readonly="readonly" required value="<?php echo $notas_jur1 ["n3"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_4" readonly="readonly" required value="<?php echo $notas_jur1 ["n4"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_5" readonly="readonly" required value="<?php echo $notas_jur1 ["n5"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_6" readonly="readonly" required value="<?php echo $notas_jur1 ["n6"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_7" readonly="readonly" required value="<?php echo $notas_jur1 ["n7"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_8" readonly="readonly" required value="<?php echo $notas_jur1 ["n8"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_9" readonly="readonly" required value="<?php echo $notas_jur1 ["n9"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_10" readonly="readonly" required value="<?php echo $notas_jur1 ["n10"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_11" readonly="readonly" required value="<?php echo $notas_jur1 ["n11"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Notajurado1_12" readonly="readonly" required value="<?php echo $notas_jur1 ["n12"];?>" /></Td>
                        <Td><input type="text" style="width : 90px" name="Finaljurado1" readonly="readonly" required value="<?php echo $notas_jur1 ["final"];?>" /></Td>
                    </TR>
                </TABLE>
                <br>
                 <?php
                   }   
                 ?>
                  <?php
                if ($jurado == 2) {
                    $puntaje_total_jues =  $puntaje_total_jues/2
                ?>  
                 <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=13><center> Jurado 1 </center></TH> 
                    </TR>
                    <TR>
                        <TH COLSPAN=13> Nombre: </Th>  
                    </TR>  
                    <TR>
                        <Td COLSPAN=13><input type="text" name="Nombre_Jurado1"  class="form-control" placeholder="Nombre del Jurado 1" value = "<?php echo $nombre_jurado1;?>"/></Td> 
                    </TR>
                    <TR>
                        <TH COLSPAN=12> Notas: </TH> <TH COLSPAN=1> Total: </TH>
                    </TR>
                    <TR>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_1" readonly="readonly" required value="<?php echo $notas_jur1 ["n1"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_2" readonly="readonly" required value="<?php echo $notas_jur1 ["n2"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_3" readonly="readonly" required value="<?php echo $notas_jur1 ["n3"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_4" readonly="readonly" required value="<?php echo $notas_jur1 ["n4"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_5" readonly="readonly" required value="<?php echo $notas_jur1 ["n5"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_6" readonly="readonly" required value="<?php echo $notas_jur1 ["n6"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_7" readonly="readonly" required value="<?php echo $notas_jur1 ["n7"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_8" readonly="readonly" required value="<?php echo $notas_jur1 ["n8"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_9" readonly="readonly" required value="<?php echo $notas_jur1 ["n9"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_10" readonly="readonly" required value="<?php echo $notas_jur1 ["n10"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_11" readonly="readonly" required value="<?php echo $notas_jur1 ["n11"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado1_12" readonly="readonly" required value="<?php echo $notas_jur1 ["n12"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Finaljurado1" readonly="readonly" required value="<?php echo $notas_jur1 ["final"];?>" /></Td>
                    </TR>
                </TABLE>
                <br>
                <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=13><center> Jurado 2 </center></TH> 
                    </TR>
                    <TR>
                        <TH COLSPAN=13> Nombre: </Th>  
                    </TR>  
                    <TR>
                        <Td COLSPAN=13><input type="text" name="Nombre_Jurado2"  class="form-control" placeholder="Nombre del Jurado 2" value = "<?php echo $nombre_jurado2;?>"/></Td> 
                     
                    </TR>
                    <TR>
                        <TH COLSPAN=12> Notas: </TH> </TH> <TH COLSPAN=1> Total: </TH>
                    </TR>
                    <TR>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_1" readonly="readonly" required value="<?php echo $notas_jur2 ["n1"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_2" readonly="readonly" required value="<?php echo $notas_jur2 ["n2"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_3" readonly="readonly" required value="<?php echo $notas_jur2 ["n3"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_4" readonly="readonly" required value="<?php echo $notas_jur2 ["n4"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_5" readonly="readonly" required value="<?php echo $notas_jur2 ["n5"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_6" readonly="readonly" required value="<?php echo $notas_jur2 ["n6"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_7" readonly="readonly" required value="<?php echo $notas_jur2 ["n7"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_8" readonly="readonly" required value="<?php echo $notas_jur2 ["n8"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_9" readonly="readonly" required value="<?php echo $notas_jur2 ["n9"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_10" readonly="readonly" required value="<?php echo $notas_jur2 ["n10"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_11" readonly="readonly" required value="<?php echo $notas_jur2 ["n11"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Notajurado2_12" readonly="readonly" required value="<?php echo $notas_jur2 ["n12"];?>" /></Td>
                        <Td><input type="text" style="width : 50px" name="Finaljurado2" readonly="readonly" required value="<?php echo $notas_jur2 ["final"];?>" /></Td>
                    </TR>
                </TABLE>
                <br>
                 <?php
                   }   
                 ?>
                <label> <b> Puntaje Total </b><input type="text" name="Puntaje_Total" readonly="readonly" class="form-control" required value="<?php echo (($puntaje_total+$puntaje_total_jues)/2);?>"/> </label><br>
                <label> <b> Calificacion final: </b><input type="text" name="Final" readonly="readonly" class="form-control" required value="<?php 
                if ($jurado != 0){
                    $final = (($puntaje_total + $puntaje_total_jues)/4);
                } else{
                   $final = ($puntaje_total/2); 
                }
                $finalp=$final*0.1;
                $finalp = number_format($finalp,1);
                echo $finalp;?>"/>  </label><br>
                 <?php
                 if ($final >= 35){
                     $validado = "Aprobado";
                 } else 
                     $validado = "No aprobado";
                ?> 
                <label> <b> Se le otorgo el caracter de: </b><input type="text" name="Validado" readonly="readonly" class="form-control" required value="<?php echo $validado;?>"/></label><br>
        <hr class="col-md-8 mb-1">
        <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" onclick=" location.href='Generar_Actas.php'" type="submit">Generar acta</button>
        <div class="modal-body">
            <p></p>
            <hr>
            <p></p>
        </div>
    </form>
    </div>
    <?php
} else {
    session_destroy();
    header("Location: index.php");
}
?>
</body>

