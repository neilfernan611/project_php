<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <br>
        <?php
        if ($coordinacion != null) {
            ?>

            <div class="py-5 text-center">

                <p><center>Ingrese la información en todos los campos</center></p>
        </div>


        <div class="col-md-8 order-md-1 container">

            <!-- class="needs-validation" novalidate-->
            <form action="Control/Subir_Documento.php" enctype="multipart/form-data" method="POST" id="form-registro" >
                <div class="row">
                    <div class="col-md-8 mb-3">
                        <label for="selectTipo" >Tipo Documento</label>
                        <select id="selectTipo"  required class="custom-select" name="documento">

                            <option value="Produccion Academica">Produccion Academica</option>
                            <option value="Jurado y Tutor">Jurado y Tutor</option>
                            <option value="Jurado">Jurado</option>
                            <option value="Dos Jurados">Dos Jurados</option>
                            <option value="Anteproyecto">Anteproyecto</option>

                        </select>
                        <div class="invalid-feedback">
                            Campo obligatorio.
                        </div>
                    </div>

                    <div class="col-md-8 mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Subir</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" required name="archivo" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Elija el archivo</label>
                        </div>
                    </div>  


                </div>
                <hr class="col-md-8 mb-1">
                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Subir</button>

                <div class="modal-body">
                    <p></p>
                    <hr>
                    <p></p>
                </div>
            </form>
        </div>
        <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>

</body>
</html>
