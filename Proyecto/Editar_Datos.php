<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>


<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
    </head>
    <body>
        <?php
        if ($estudiante != null) {
            $nombre = $_SESSION["nombre"];
            $info = $_SESSION['informacion2'];
            $infop = $_SESSION['informacion'];
            $codigo = $info['Codigo'];
            $correo = $info['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($coordinacion != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($profesor != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($comite != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($administrador != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        }
        ?>


        <br><br><br>
        <?php
        if ($administrador != null || $comite != null || $coordinacion != null || $estudiante != null || $profesor != NULL) {
            ?>
            <div class="container">
            <div class="mb-lg-5 text-center modal-title">
                <label><i class="fa fa-user" aria-hidden="true">  <?php echo ($nombre); ?> </i></label>
            </div>
            <div class="col-md-6 order-md-1 container">
                <form action="Control/Editar_Datos.php" method="POST" id="form-registro" >
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="email" style="color: black"><i class="fa fa-envelope" aria-hidden="true"></i>
                                Email <span class="text-muted"></span></label>
                            <input type="email" class="form-control" name="email" required placeholder="<?php echo ($correo); ?>">

                            <div class="invalid-feedback">
                                Por favor, introduzca una dirección de correo electrónico válida.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="telefono" style="color: black"><i class="fa fa-phone" aria-hidden="true"></i>  Número celular</label>
                            <div class="input-group">
                                <div class="input-group-prepend">

                                </div>
                                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="<?php echo ($tele); ?>" pattern="[0-9]{10,10}"  title="un número de telefono correcto tiene 10 digitos y sin espacios" required>
                                <div class="invalid-feedback" style="width: 100%;">
                                    El número de celular es obligatorio.
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="col-md-8 mb-1">
                    <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Información</button>
                </form>
            </div>
            </div>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>


