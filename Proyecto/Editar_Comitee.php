<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
require_once "./Modelo/DAO_Comite.php";
if (!filter_input(INPUT_POST, "bot_comi")) {


    $cod = htmlentities(addslashes(filter_input(INPUT_GET, "idco")));
    $correo = htmlentities(addslashes(filter_input(INPUT_GET, "correo")));
    $telefono = htmlentities(addslashes(filter_input(INPUT_GET, "telefono")));
}

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>

        <br>

        <?php
        if ($administrador != null) {
            ?>
            <form action="Control/Editar_Comite.php" method="POST">
                <table class="table" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">
                           <!-- <th colspan="16"><h2><p style="color:white"><center>Actualizar Correo Comité</center></p></h2></th></tr>-->
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Teléfono</th>
                            <th scope="col"></th>

                        </tr>
                    </thead>

                    <tr>

                        <th ><?php echo $cod ?> </th>
                        <td  hidden><input  type="hidden" name="cod" value="<?php echo $cod ?>"></td>
                        <td>
                            <div class="row">
                                <div class="col-md-5 mb-3">
                                    <input type="email" class="form-control"  id="correo" name="correo" placeholder="" value="<?php echo $correo; ?>" required value="<?php echo $correo ?>"> 
                                    <div class="invalid-feedback">
                                        Por favor, introduzca una dirección de correo electrónico válida.
                                    </div>

                                </div>

                            </div>
                        </td>
                        
                        <td>
                            <div class="row">
                                
                        <div class="col-md-6 mb-3">
                            
                            <input type="text" pattern="[0-9]{10,10}" title="Formato 10 nùmeros" class="form-control" name="telefono" id="telefono" placeholder="3221112222" maxlength="10" minlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" 
                              required value="<?php echo $telefono ?>" >
                            
                        </div>

                            </div>
                        </td>

                        <td>
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR" name="bot_comi" value="Actualizar">
                                </div>
                            </div>
                        </td>
                    </tr>

                </table>
            </form>
            <?php
        } else {
            session_destroy();
            header("Location: index.php");
        }
        ?>
    </body>
</html>

