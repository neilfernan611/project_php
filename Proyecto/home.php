<?php
require_once './Modelo/DAO_Anteproyecto.php';
session_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
    require_once './Modelo/DAO_Estudiante.php';

    $informacion = $_SESSION['informacion2'];
    $id_proyecto = $informacion['Id_Proyecto'];
    $codigo = $informacion['Codigo'];
    
    $proponentes = new DAO_Estudiante();
    $cantidad = $proponentes->cantidad_de_proponentes($id_proyecto);
    $cant = intval(implode($cantidad));

    $obj_proyecto = new DAO_Anteproyecto();
    $informacion_proy = $obj_proyecto->InformacionProyecto($id_proyecto);
    $existe = $obj_proyecto->Consultar_Existencia_Estudiante_Devuelto($codigo);
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $info = $_SESSION['informacion2'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    require_once './Modelo/DAO_Profesor.php';
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
         <meta name="keywords" content="sisproyud, Tecnologia en electronica, sisproyud ud, Sisproyud, SYSPROYUD">
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <title>sisproyud</title>
        <link rel="stylesheet" type="text/css" href="css/estilosMenu.css">
        <script type="text/javascript" src="js/js.js"></script>
         <script type="text/javascript" src="https://sisproyud.com/js/menu.js"></script>
         <style>
             body{
    background: #EEEEEE;
}
         </style>
    </head>

    <body>
        <section class="arriba2">
            <i class="fa fa-phone-square" aria-hidden="true"> 01-8000-914410 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://www.udistrital.edu.co/directorio"><i class="fa fa-book" aria-hidden="true"> Directorio </i> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
        </section>
        <header>
            <?php if ($administrador != null) {
                ?>
                <nav id='cssmenu'>
                    <div class="logo titulo"><!--<img src="Img/logo_universidad_acreditacion.png">-->Tecnologia en Electronica</div>
                    <div id="head-mobile"></div>
                    <div class="button"></div>
                    <ul>
                        <li class=''><a href='home.php' style="color: #ffffff;">HOME</a></li>
                        <li><a href='#'>Usuarios</a>
                            <ul>
                                <li><a href='#'>Coordinación</a>
                                    <ul>
                                        <li><a href="Agregar_Coordinacion.php">Agregar</a></li>
                                        <li><a href="Info_Coordinacion.php">Ver</a></li>

                                    </ul>
                                </li>
                                <li><a href='#'>Comité</a>
                                    <ul>
                                        <li><a href="Agregar_Comite.php">Agregar</a></li>
                                        <li><a href="Info_Comite.php">Ver</a></li>

                                    </ul>
                                </li>
                                <li><a href='#'>Docentes</a>
                                    <ul>
                                        <li><a href="Agregar_Profesor.php">Agregar</a></li>
                                        <li><a href="Consultar_Profesor.php">Ver</a></li>
                                   
                                    </ul>
                                </li>
                                <li><a href='#'>Estudiantes</a>
                                    <ul>
                                        <li><a href="Consultar_Estudiantes.php">Ver</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href='#'>Opciones</a>
                            <ul>
                                <li><a href='#'>Editar</a>
                                    <ul>
                                        <li><a href="Info_Linea.php">Líneas Investigación</a></li>
                                        <li><a href="Info_Grupo.php">Grupos Investigación</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Calendario</a>
                                    <ul>
                                        <li><a href="Agregar_Calendario.php">Agregar</a></li>
                                        <li><a href="<?php filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') ?>
                                        Calendario/Calendario.pdf" target="_blank">Ver</a></li>
                                    </ul>
                                </li>
                                <li><a href='#'>Formato Anteproyecto</a>
                                    <ul>
                                        <li><a href="Agregar_Formato_Anteproyecto.php">Agregar</a></li>
                                        <li><a href="<?php filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') ?>
                                               Formato/Formato_Anteproyecto.pdf" target="_blank">Ver</a></li>
                                    </ul>
                                </li>
                                <li><a href='Resetear_Radicados.php'>Resetear Consecutivo</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Configuraciones</a>
                            <ul>
                                <li><a href="Editar_Datos.php">Editar Datos</a></li>
                                <li><a href="Cambiar_Pass.php">Cambiar Contraseña</a></li>

                            </ul>
                        </li>
                        <li><a href="Control/Cerrar_Sesion.php">Cerrar Sesión</a></li>
                    </ul>
                </nav>
                <?php
            } else if ($estudiante != NULL) {
                ?>
                <nav id='cssmenu'>
                    <div class="logo">Tecnología En Electrónica</div>
                    <div id="head-mobile"></div>
                    <div class="button"></div>
                    <ul>
                        <li class='active'><a href='home.php' style="color: #ffffff;">HOME</a></li>
                        <li><a href="#">Anteproyecto/Proyecto</a>
                            <ul>
                                <?php
                                if ($informacion_proy['Nombre_Proyecto'] == NULL) {
                                    ?>
                                    <li><a href="#">Registrar Anteproyecto</a>
                                        <ul>
                                            <li><a href="Agregar_AnteProyecto.php">Monografía o Emprendimiento</a></li>
                                            <li><a href="Agregar_Pasantia.php">Pasantía</a></li>
                                            <li><a href="Agregar_Produccion_Academica.php">Producción Académica</a></li>
                                            <li><a href="Agregar_Postgrado_Profundizacion.php">Profundización o Postgrado</a></li>
                                            <li><a href="Agregar_Investigacion_Innovacion.php">Investigación e Innovación</a></li>
                                        </ul>
                                        <?php
                                    } else {
                                        
                                    }
                                    ?>
                                </li>
                                <?php
                                if ($informacion_proy['Radicado'] != NULL) {
                                    ?>
                                    <li><a href="Ver_Proyecto.php">Ver Proyecto</a></li>                                                         
                                    <!--<li><a href="Agregar_Proyecto.php">Subir Proyecto</a></li>-->
                                    <?php
                                }
                                if ($informacion_proy['Nombre_Proyecto'] != NULL || $existe === 1) {
                                    ?>       
                                    <li><a href="Ver_AnteProyecto.php">Ver Estado</a></li>
                                <?php
                                } else {
                                    
                                }
                                ?>
                            </ul>
                        </li>
                        <li><a href="#">Ver</a>
                            <ul>
                                <li><a href="<?php filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') ?>
                                        Calendario/Calendario.pdf" target="_blank">Calendario Académico</a></li>
                                <li><a href="<?php filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') ?>
                                Formato/Formato_Anteproyecto.pdf" target="_blank">Formato Anteproyecto</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Configuraciones</a>
                            <ul>
                                <li><a href="Editar_Datos.php">Editar Datos</a></li>
                                <li><a href="Cambiar_Pass.php">Cambiar Contraseña</a></li>
                            </ul>
                        </li>
                        <li><a href="Control/Cerrar_Sesion.php">Cerrar Sesión</a></li>
                    </ul>
                </nav>
                <?php
            } else if ($coordinacion != NULL) {
                ?>
                <nav id='cssmenu'>
                    <div class="logo">Tecnología En Electrónica</div>
                    <div id="head-mobile"></div>
                    <div class="button"></div>
                    <ul>
                        <li class='active'><a href='home.php' style="color: #ffffff;">HOME</a></li>
                        <li><a href="#">Proyecto</a>
                            <ul>
                                <li><a href="Generar_Radicados.php">Generar Radicado</a></li>
                                <li><a href="Asignar_Jurado.php">Asignar Jurado</a></li>
                                <li><a href="Cancelar_Proyecto.php">Solicitudes</a></li>
                                <li><a href="Generar_Actas.php">Generar actas</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Buscar</a>
                            <ul>
                                <li><a href="Buscar_Proyectos.php">Buscar anteproyectos por titulo</a></li>
                                <li><a href="Consultar_estudiante.php">Anteproyectos por estudiante</a></li>
                                <li><a href="Consultar_Cantidad_Proyectos.php">Explorar Anteproyectos</a></li>
                                <!--<li><a href="Consultar_Total_Proyectos.php">Consultar Proyectos por profesor</a></li> COMENTARIADO POR CAROL-->
                            </ul>
                        </li>
                        <li><a href="#">Ver</a>
                            <ul>
                                <li><a href="Info_LineaII.php">Líneas Investigación</a></li>
                                <li><a href="Info_GrupoII.php">Grupos Investigación</a></li>
                                <li><a href="Listado_Profesores_Roles.php">Profesores</a></li>
                            </ul>
                        </li>
                        <li><a href="Correo_Masivo.php">Correos</a></li>
                        <li><a href="#">Configuraciones</a>
                            <ul>
                                <li><a href="Editar_Datos.php">Editar Datos</a></li>
                                <li><a href="Cambiar_Pass.php">Cambiar Contraseña</a></li>
                            </ul>
                        </li>
                        <li><a href="Control/Cerrar_Sesion.php">Cerrar Sesión</a></li>
                    </ul>
                </nav>
            <?php } else if ($comite != NULL) {
                ?>
                <nav id='cssmenu'>
                    <div class="logo">Tecnología En Electrónica</div>
                    <div id="head-mobile"></div>
                    <div class="button"></div>
                    <ul>
                        <li class='active'><a href='home.php'>HOME</a></li>
                        <li><a href="#">Proyecto</a>
                            <ul>
                                <li><a href="tabla_comite.php">Generar aval</a></li>
                                <li><a href="Asignar_Fecha_Sustentacion.php">Asignar Fechas de Sustentación</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Buscar</a>
                            <ul>
                                <li><a href="Buscar_Proyectos.php">Buscar Anteproyectos por titulo</a></li>
                                <li><a href="Consultar_estudiante.php">Anteproyectos por estudiante</a></li>
                                <li><a href="Consultar_Cantidad_Proyectos.php">Explorar Anteproyectos</a></li>
                                <!--<li><a href="Consultar_Total_Proyectos.php">Proyectos por profesor</a></li> COMENTARIADO POR CAROL-->
                            </ul>
                        </li>
                        <li><a href="#">Ver</a>
                            <ul>
                                <li><a href="Info_LineaII.php">Líneas Investigación</a></li>
                                <li><a href="Info_GrupoII.php">Grupos Investigación</a></li>
                                <li><a href="Listado_Profesores_Roles.php">Profesores</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Reportes</a>
                            <ul>
                                <li><a href="Grafico.php">Proyectos</a></li>
                                <li><a href="Reportes_de_Director.php">Directores de proyectos</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Configuraciones</a>
                            <ul>
                                <li><a href="Editar_Datos.php">Editar Datos</a></li>
                                <li><a href="Cambiar_Pass.php">Cambiar Contraseña</a></li>
                            </ul>
                        </li>
                        <li><a href="Control/Cerrar_Sesion.php">Cerrar Sesión</a></li>
                    </ul>
                </nav>
            <?php } else if ($profesor != NULL) {
                ?>
                <nav id='cssmenu'>
                    <div class="logo">Tecnología En Electrónica</div>
                    <div id="head-mobile"></div>
                    <div class="button"></div>
                    <ul>
                        <li class='active'><a href='home.php' style="color: #ffffff;">HOME</a></li>
                        <!-- Jurado -->
                        <?php
                        $objprofesor = new DAO_Profesor();
                        $roles = $objprofesor->verificarRoles($info['Cedula']);
                        foreach ($roles as $rol) {
                            ?>
                        <?php if ($rol['Id_Rol'] == "01") { ?>
                                <li><a href="#">Jurado</a>
                                    <ul>
                                        <li><a href="#">Proyectos</a>
                                            <ul>
                                                <li><a href="Calificar_Jurado.php">Calificar</a></li>
                                                <li><a href="Ver_Proyectos.php?idrol=01">Avalar proyecto</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <!--  Evaluador-->
                        <?php } else if ($rol['Id_Rol'] == "03") { ?>
                                <li><a href="#">Evaluador</a>
                                    <ul>
                                        <li><a href="Anteproyecto_Evaluador.php">Evaluar Anteproyecto</a></li>
                                    </ul>
                                </li>
                                <!--  Director de proyecto-->
                        <?php } else if ($rol['Id_Rol'] == "05") { ?>
                                <li><a href="#">Director de Proyecto</a>
                                    <ul>
                                        <li><a href="Calificar_Director.php">Calificar</a></li>
                                        <li><a href="Ver_Proyectos.php?idrol=05">Ver Proyectos</a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        <li><a href="#">Configuraciones</a>
                            <ul>
                                <li><a href="Editar_Datos.php">Editar Datos</a></li>
                                <li><a href="Cambiar_Pass.php">Cambiar Contraseña</a></li>
                            </ul>
                        </li>
                        <li><a href="Control/Cerrar_Sesion.php">Salir</a></li>
                    </ul>
                </nav>
                <?php
            } else {
                session_destroy();
                echo "<script>location.href='index.php';</script>";
            }
            ?>
        </header>
        
        <div class ="alinear_izquierda">
        <?php
        
        if ($comite != null || $coordinacion != null || $profesor != null  || $administrador!=null) { 
           
            $host = filter_input(INPUT_SERVER, "HTTP_HOST");
            $url = filter_input(INPUT_SERVER, "REQUEST_URI");
            $urlreal = "http://" . $host . $url;
            $urlreal2 = "https://" . $host . $url;
           
            if(("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2)){ ?>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
                <br><br>
               
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 mb-8">
                            <div class="panel panel-primary">
                                <div class="panel panel-heading"><center><h2>Recomendaciones</h2> </center></div><br>
                                <div class="panel panel-body">
                                    <div class="row">
                                        <div class="col-sm-12 mb-12">
                                            <div class="col-sm-8 mb-8">
                                                <p style="text-align: justify">
                                                    <ul>
                                                        <li>Para una mejor experiencia utilizar Google Chrome</li>
                                                        <li>Cuando se suben documentos pdf recuerde borrar el cache del navegador, <a href="https://support.google.com/accounts/answer/32050?co=GENIE.Platform%3DDesktop&hl=es-419">Clic Aquí</a></li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
              
           <?php 
              
            }
           
            if (("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2) && $coordinacion != null) {
                $proy = new DAO_Anteproyecto();
                $lista = $proy->Cantidad_Anteproyectos_Coordinacion();
                $numero_de_registros = count($lista);
                if ($numero_de_registros >= 0) {
                    ?>

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel panel-heading"></div><br>
                                        <div class="panel panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="container">
                                                        <h2>Novedades</h2>
                                                        <div class="alert alert-info alert-dismissible fade show">
                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>Usuario coordinación!<br></strong>Tiene <?php echo $numero_de_registros ?> proyectos sin asignar radicado.<br>
                                                            <a href="Generar_Radicados.php">clic aquí</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            } else if (("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2) && $comite != null) {
                $proy = new DAO_Anteproyecto();
                $lista = $proy->Cantidad_Anteproyectos_Comite();
                $numero_de_registros = count($lista);
                if ($numero_de_registros >= 0) {
                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                 
                                    <div class="panel panel-body">
                                        <div class="row">
                                            <div class="col-sm-12 mb-12">
                                                <div class="col-sm-9 mb-9">
                                                <ul style="text-align: justify">
                                                    <li>Para tener una mejor vista de las gráficas en los reportes utilizar Google Chrome</li>
                                                </ul>
                                              </div>
                                            
                                            </div>
            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel panel-heading"></div><br>
                                        <div class="panel panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="container">
                                                        <h2>Novedades</h2>
                                                        <div class="alert alert-info alert-dismissible fade show">
                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                            <strong>Usuario comité!<br></strong>Tiene <?php echo $numero_de_registros ?> proyectos por avalar.<br>
                                                            <a href="tabla_comite.php">clic aquí</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } 
            else if (("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2) && $administrador != null)
            {
                
            }
            else if (("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2)) {

                $proy = new DAO_Anteproyecto();
                $lista = $proy->Consultar_Anteproyectos_Evaluador($info['Cedula']);
                $numero_de_registros = count($lista);
                $objprofesor = new DAO_Profesor();
                $roles = $objprofesor->verificarRoles($info['Cedula']);
                foreach ($roles as $rol) {
                    if ($rol['Id_Rol'] == "03") {
                        $rold = $rol['Id_Rol'];
                    }
                }

                if ($numero_de_registros >= 0 && $rold == "03") {
                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel panel-heading"></div><br>
                                    <div class="panel panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="container">
                                                    <h2>Novedades</h2>
                                                    <div class="alert alert-info alert-dismissible fade show">
                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                        <strong>Rol evaluador!<br></strong>Tiene <?php echo $numero_de_registros ?> proyectos por Evaluar.<br>
                                                        <a href="Anteproyecto_Evaluador.php">clic aquí</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            
        }else if($estudiante != NULL){
            $host = filter_input(INPUT_SERVER, "HTTP_HOST");
            $url = filter_input(INPUT_SERVER, "REQUEST_URI");
            $urlreal = "http://" . $host . $url;
            $urlreal2 = "https://" . $host . $url;
            
            if("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2){
                
                $objProyecto = new DAO_Anteproyecto();
                $objestu = new DAO_Estudiante();
                $namep = $objestu->proyectoRelacionado($codigo);
                
                $existenciae =  $objProyecto->Consultar_Existencia_Estudiante_Devuelto($codigo);
                $proyectod = $objProyecto->Consultar_Estado_EvaluadorE_Devuelto($codigo);
                $namep = $objestu->proyectoRelacionado($codigo);
                
                $proyecto = $objProyecto->Consultar_Estado_EvaluadorE($id_proyecto);
                $proy = $objProyecto->Consultar_Estado_ComiteE($id_proyecto);
                $proyc = $objProyecto->Consultar_Estado_CoordinacionE($id_proyecto);
                
            ?>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
                <br><br>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 mb-8">
                            <div class="panel panel-primary">
                                <div class="panel panel-heading"><center><h1>Recomendaciones</h1> </center></div><br>
                                <div class="panel panel-body">
                                    <div class="row">
                                        <div class="col-sm-12 mb-12">
                                            <div class="col-sm-10 mb-10">
                                                <p style="text-align: justify">
                                                    <ul>
                                                        <li>Para una mejor experiencia utilizar Google Chrome</li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                if($existenciae == 1 && $namep['Nombre_Proyecto']== NULL){
                ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel panel-heading"></div><br>
                                        <div class="panel panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="container">
                                                        <h2>Novedades</h2>
                                                        <div class="alert alert-info alert-dismissible fade show">
                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                El evaluador devolvío su anteproyecto por favor vuelva a subir su anteproyecto.<br>
                                                                Para ver el comentario del evaluador
                                                            <a href="Informacion_estados.php">clic aquí</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                 } else{
                        if($existenciae == 0 && $namep['Nombre_Proyecto']== NULL){ ?>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading"></div><br>
                                                <div class="panel panel-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="container">
                                                                <h2>Novedades</h2>
                                                                <div class="alert alert-info alert-dismissible fade show">
                                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                    Aún no ha subido su anteproyecto por favor dirijase a la opción <strong>Anteproyecto/Proyecto.<br></strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php    
                        }
                        else if($namep['Id_Modalidad']=='204' || $namep['Id_Modalidad']=='205' || $namep['Id_Modalidad']=='208'){
                            
                            if($namep['Nombre_Proyecto'] != NULL && $proy['Estado']=="Por Evaluar" && $proyc['Radicado']==NULL){ ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                    <div class="alert alert-info alert-dismissible fade show">
                                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                         Aún no han evaluado su anteproyecto.<br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php    
                            }else if($proy['Estado']!="Por Evaluar" && $proyc['Radicado']==NULL){ ?>                  
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                    <div class="alert alert-info alert-dismissible fade show">
                                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                         Concepto de comité sobre su anteproyecto.<br>
                                                                         <a href="Informacion_estados.php">clic aquí</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php                
                            }else if($proy['Estado']!="Por Evaluar" && $proyc['Radicado']!=NULL){ ?>              
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                    <div class="alert alert-info alert-dismissible fade show">
                                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                          Radicado generado por parte de coordinación.<br>
                                                                         <a href="Informacion_estados.php">clic aquí</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                      <?php }
                      }else{
                           if($namep['Nombre_Proyecto'] != NULL && $proyecto['Estado']=="Por Evaluar" && $proy['Estado']=="Por Evaluar" && $proyc['Radicado']==NULL){ ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                    <div class="alert alert-info alert-dismissible fade show">
                                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                         Aún no han evaluado su anteproyecto.<br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php    
                            } 
                            else if($proyecto['Estado']!="Por Evaluar" && $proy['Estado']=="Por Evaluar" && $proyc['Radicado']==NULL){ ?>      
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                        <div class="alert alert-info alert-dismissible fade show">
                                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                             Concepto del evaluador sobre su anteproyecto.<br>
                                                                             <a href="Informacion_estados.php">clic aquí</a>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                          <?php    
                          }else if($proyecto['Estado']!="Por Evaluar" && $proy['Estado']!="Por Evaluar" && $proyc['Radicado']==NULL){ ?>  
                               <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel panel-heading"></div><br>
                                                        <div class="panel panel-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="container">
                                                                        <h2>Novedades</h2>
                                                                        <div class="alert alert-info alert-dismissible fade show">
                                                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                             Concepto de comité sobre su anteproyecto.<br>
                                                                             <a href="Informacion_estados.php">clic aquí</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                          <?php    
                          }else if($proyecto['Estado']!="Por Evaluar" && $proy['Estado']!="Por Evaluar" && $proyc['Radicado']!=NULL){ ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-primary">
                                                <div class="panel panel-heading"></div><br>
                                                    <div class="panel panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="container">
                                                                    <h2>Novedades</h2>
                                                                    <div class="alert alert-info alert-dismissible fade show">
                                                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                         Radicado generado por parte de coordinación.<br>
                                                                         <a href="Informacion_estados.php">clic aquí</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                        <?php
                          }
                      }
                 }
                ?>
            <?php    
            }
        }
        ?>
          </div> 
         
          <?php
          
          if($administrador!= NULL || $estudiante!=null || $comite!=null || $coordinacion!=null || $profesor!=null)
         if ("http://sisproyud.com/home.php" == $urlreal || "https://www.sisproyud.com/home.php" == $urlreal2)  
         {
          {
              $randon='./Img/universidad'.mt_rand(1,2).'.jpg';
           
          ?>
           <div class="alinear_derecha">
              <br><br><br><br><br><br>
              <img src="<?php  echo ($randon); ?>" alt="universidad" class="responsive">
            
          </div>
         <?php
          }
         }
         ?> 
        
        <!--desabilitar clic derecho-->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    

      
    </body>
</html>


