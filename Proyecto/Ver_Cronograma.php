<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './Modelo/DAO_Cronograma.php';
require_once './home.php';
?>

<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <br>
        <?php
        if ($administrador != null || $estudiante != null || $coordinacion != null || $comite != null) {
            ?>


            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                      <!--  <th colspan="7"><h2><p style="color:white"><center>Información Coordinación</center></p></h2></th></tr>-->
                    <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Fecha de Inicio</th>
                        <th scope="col">Fecha Fin</th>

                        <th scope="col">Borar</th>

                    </tr>
                </thead>
                <?php
                $cro = new DAO_Cronograma;
                $lista_ant = $cro->Ver_Cronograma();
                foreach ($lista_ant as $lista) {
                    ?>

                    <tr>
                        <th scope="col"><?php echo($lista["Codigo_Cronograma"]); ?> </th>
                        <th scope="col"> <?php echo($lista["Fecha_Inicio"]); ?> </th>
                        <th scope="col"><?php echo($lista["Fecha_Fin"]); ?> </th>
                        <td class="bot"><a href="Control/Eliminar_Cronogrma.php?id=<?php echo ($lista["Codigo_Cronograma"]) ?>"  onclick="return confirmar()"><img src="Img/recycle.png"></a></td>


                    </tr>
                    <?php
                }
                ?>
            </table>

            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>

    </body>
</html>
