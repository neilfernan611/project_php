<?php
ob_start();
require_once'home.php';
require_once ('./Modelo/DAO_LineaInvestigacion.php');
require_once ("./Modelo/Codigos.php");
?>
<!DOCTYPE html> 
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 


        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar esta línea de investigación?'))
                    return false;
            }

        </script>

    </head>
    <body>

        <br>
        <!--Fin Navbar-->
        <?php if ($administrador != NULL) {
            ?>
            <?php
            if (filter_input(INPUT_POST, "bt")) {
                $linea = htmlentities(addslashes(filter_input(INPUT_POST, "linea")));
                $est = htmlentities(addslashes(filter_input(INPUT_POST, "esta")));
                $insertar = 0;
                $l = new Codigos();
                $id_linea = $l->getPINSoli(3);

                $objlinea = new DAO_LineaInvestigacion();

                $verifi = $objlinea->verificar_inves($id_linea, $linea,$insertar,$est);
                if ($verifi == 0) {

                    $resultado = $objlinea->insertarLinea($id_linea, $linea, $est);
                    ?>

                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Éxito!</strong> Línea Agregada.
                    </div>

                    <?php
                } else {
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Fallo!</strong> Verifique la Información ingresada
                    </div>   

                    <?php
                }
            }
            ?>
            <form action="<?php echo filter_input(INPUT_SERVER, 'PHP_SELF'); ?>" method="POST">
                <?php
                 if (!($_GET)) {
                  
                   echo "<script>location.href='Info_Linea.php?pagina=1';</script>";
                  } 
                ?>
                <table class="table" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">
                            <!--<th colspan="6"><h2><p style="color:white">Información Línea De Investigación</p></h2></th></tr>-->
                        <tr >
                            <th scope="col">Código</th>
                            <th scope="col">Línea</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Borrar</th>
                            <th scope="col">Actualizar</th>
                        </tr>
                    </thead>
                    <?php
                    $info = new DAO_LineaInvestigacion();
                     $lista = $info->listarTablaLinea();
                      $numero_de_registros = count($lista);
                      $registros_x_pagina = 5;
                      $paginas = ceil($numero_de_registros / $registros_x_pagina);
                      $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                      if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                      header('Location:Info_Linea.php?pagina=1');
                      } 


                     $lista2 = $info->listarTablaLinea2($iniciar, $registros_x_pagina);
                    
                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Id_Linea"]); ?></th>
                            <th scope="col"><?php echo($lis["Linea"]); ?></th>
                            <th scope="col"><?php echo($lis["Estado"]); ?></th>

                            <td class="bot"><a href="Control/Eliminar_Linea.php?idl=<?php echo ($lis["Id_Linea"]); ?>" onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Lineaa.php?idl=<?php echo ($lis["Id_Linea"]); ?>&linea=<?php echo ($lis["Linea"]); ?>&estado=<?php echo ($lis["Estado"]); ?>"><img src="Img/update.png"></a></td>


                        </tr>
                        <?php
                    }
                    ?> 
                    <tr>

                        <td><input type='hidden' name='id' size='10' class='centrado'></td>

                        <td>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <input type="text" class="form-control" id="linea" name="linea"  size='10' class='centrado' required value=""> 
                                    <div class="invalid-feedback">
                                        Completa campo
                                    </div>

                                </div>

                            </div>
                        </td>

                        <td>
                            <div class="row">


                                <div class="col-md-6 mb-3">

                                    <select id="selectTipo" class="custom-select" required name="esta" size='1' class='centrado' required>
                                        <option required value="04"><?php echo('Habilitado'); ?></option>
                                        <option required value="05"><?php echo('Deshabilitado'); ?></option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Campo obligatorio.
                                    </div>
                                </div>

                            </div>
                        </td>

                        <td class='bot'><input type='submit' class="btn btn-primary mb-2" id="btn-aceptarR" name='bt' value='Insertar'></td>
                        <td></td>

                    </tr>

                </table>
                <!--Fin tabla-->
            </form>   
            
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Info_Linea.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
            <?php for ($i = 0; $i < $paginas; $i++): ?>
                                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Info_Linea.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                <?php
            endfor;
            ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Info_Linea.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
            
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ob_end_flush();
        ?>

    </body>
</html>


