<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './Modelo/DAO_Anteproyecto.php';
require_once './home.php';
ob_start();
$pdf = new DAO_Anteproyecto();

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta charset="gb18030">
        
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
<link rel="stylesheet" href="css/estilos.css">
<style>
    body {
    background: #EEEEEE;
}
</style>
    </head>
    <body>
        <?php
        if ($coordinacion != null || $comite != null) {
            ?>
<?php
            if (!($_GET)) {

                echo "<script>location.href='Generar_Radicados.php?pagina=1';</script>";
            }
            ?>

            <br>
            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                       <!-- <th colspan="9"><h2><p style="color:white"><center>Información Coordinación</center></p></h2></th></tr>-->
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Título del Anteproyecto</th>
                        <th scope="col">Fecha de Registro</th>
                        <th scope="col">Estado del Comité</th>
                        <th scope="col">Estado Radicado</th>
                        <th scope="col">Ver</th>
                        <th scope="col">Generar Radicado</th>


                    </tr>
                </thead>
                <?php
                $proy = new DAO_Anteproyecto();
                $estado = $proy->Consultar_Estados();
                $lista = $proy->Consultar_Anteproyectos_Coordinacion();
                $id_2 = "calificar";
                $est = "01";
                $numero_de_registros = count($lista);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Generar_Radicados.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $proy->Consultar_Anteproyectos_Coordinacion_paginacion($iniciar, $registros_x_pagina);
                foreach ($lista2 as $lista) {
                    
                     if ($lista['Estado_Coordinacion'] == "Por Evaluar") {
                                
                    ?>
                     <tr style="background-color: #cccccc">
                        <th scope="col"> <?php echo($lista["Id_Proyecto"]); ?></th>
                        <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                        <th scope="col"> <?php echo($lista["Fecha_Registro"]); ?> </th>
                        <th scope="col"><?php echo($lista["Estado_Comite"]); ?>  </th>
                        <th scope="col"><?php echo($lista["Estado_Coordinacion"]); ?>  </th>
                        <th class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>&estado=<?php echo($id_2); ?>"target="_blank">Ver</a></th>
                        <td class="bot">
                            <a href="<?php echo($lista['Radicado'] != NULL ?'#':'Control/Actualizar_Anteproyecto.php?id_proyecto='.$lista["Id_Proyecto"].'& estado='.$est);?> " target="_blank">
                            <img src="Img/chulo.png"></a></td>
                    </tr>
                    <?php
                            } else {
                                ?>

                                <tr style="background-color: #FBFBE6">
                                    <th scope="col"> <?php echo($lista["Id_Proyecto"]); ?></th>
                                    <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                     <th scope="col"> <?php echo($lista["Fecha_Registro"]); ?> </th>
                                    <th scope="col"><?php echo($lista["Estado_Comite"]); ?>  </th>
                                     <th scope="col"><?php echo($lista["Estado_Coordinacion"]); ?>  </th>
                        <th class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>&estado=<?php echo($id_2); ?>"target="_blank">Ver</a></th>
                        <td class="bot">
                            <a href="<?php echo($lista['Radicado'] != NULL ?'#':'Control/Actualizar_Anteproyecto.php?id_proyecto='.$lista["Id_Proyecto"].'&estado='.$est);?> " target="_blank">
                            <img src="Img/chulo.png"></a></td>
                                    
                                </tr>

                                <?php
                            }
                        }
                ?>    
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Generar_Radicados.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Generar_Radicados.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Generar_Radicados.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>


            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>
    </body>
</html>
