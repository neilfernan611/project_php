<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
        <form action="Control/Correo_Masivo.php" method="POST" enctype="multipart/form-data">
            <div class="container custom-control">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10 mb-3">
                            <label for="selectTipo" >Destinatario</label>
                            <select id="selectTipo"  required class="custom-select" name="destinatarios">
                                <option selected></option>
                                <option value="Todos los Estudiantes">Todos los Estudiantes</option>
                                <option value="Todos los Docentes">Todos los Docentes</option>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-10 mb-10">
                            <label for="firstName" style="color: black">Asunto</label>
                            <input type="text" class="form-control" id="nombre" name="asunto" pattern="[A-Z a-z]{8,50}" required>
                            <div class="invalid-feedback">
                                Los nombres son obligatorios.
                            </div>
                        </div>

                        <div class="col-md-10 mb-10">
                            <label for="firstName" style="color: black">Mensaje</label>
                            <textarea type="text" class="form-control" rows="10" name="mensaje" pattern="[A-Z a-z]{8,50}" required></textarea>
                            <div class="invalid-feedback">
                                Los nombres son obligatorios.
                            </div>
                        </div>
                        
                        <div class="col-md-6 mb-3">
                        <div class="input-group-prepend">
                            <label for="custom-button" style="color: black">Archivo adjunto</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="archivo" id="real-file" hidden="hidden" />
                            <button type="button" id="custom-button">Subir Documento</button>
                            <span id="custom-text">Ningún archivo elegido, todavía.</span>
                        </div>
                    </div>   
                        <hr class="col-md-8 mb-1">
                        
                            <div class="col-md-6 mb-6">
                                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar</button>
                            </div>
                               

                       
                    </div>
                </div>
            </div>
        </form>
        <script>
            const realFileBtn = document.getElementById("real-file");
            const customBtn = document.getElementById("custom-button");
            const customTxt = document.getElementById("custom-text");

            customBtn.addEventListener("click", function () {
                realFileBtn.click();
            });

            realFileBtn.addEventListener("change", function () {
                if (realFileBtn.value) {
                    customTxt.innerHTML = realFileBtn.value.match(
                            /[\/\\]([\w\d\s\.\-\(\)]+)$/
                            )[1];
                } else {
                    customTxt.innerHTML = "No file chosen, yet.";
                }
            });

        </script>
    </body>
</html>
