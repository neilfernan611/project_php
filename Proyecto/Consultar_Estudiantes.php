<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
ob_start();
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}


require_once './Modelo/DAO_Estudiante.php';
require_once './home.php';
$codigo = "";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
        
        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar este usuario?'))
                    return false;
            }

        </script>
    </head>
    <body>

        <?php
        if ((filter_input(INPUT_POST, "buscar"))) {
            $codigo = filter_input(INPUT_POST, "codigo");
        }
        if ($administrador != null) {
            ?>
            <?php
            if (!($_GET)) {

                echo "<script>location.href='Consultar_Estudiantes.php?pagina=1';</script>";
            }
            ?>
            <br>
            <form class="form-inline" method="post">
                <div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">Código</label>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Código del estudiante">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="nombre" class="sr-only"> </label>
                    <input type="number" class="form-control" name="codigo" id="nombre" placeholder="" min="1" max="99999999999" title="Un código estudiantil cuenta con 11 digitos numéricos sin espacios">
                </div>
                <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR"  name="buscar">
            </form>

            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr  align="center" valign="middle">
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Borrar</th>
                        <th scope="col">Actualizar</th>
                    </tr>
                </thead>
                <?php
                if ($codigo == null || $codigo == ("")) {

                    $info = new DAO_Estudiante();
                    $lista = $info->ListarEstudiantes();
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 15;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        header('Location:Consultar_Estudiantes.php?pagina=1');
                    }

                    $lista2 = $info->ListarEstudiantes_paginacion($iniciar, $registros_x_pagina);

                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Codigo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Nombre"]); ?> </th>
                            <th scope="col"><?php echo($lis["Correo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Telefono"]); ?> </th>
                            <td class="bot"><a href="Control/Eliminar_Usuario.php?idco=<?php echo("estudiante");?> & correo=<?php echo ($lis["Correo"]);?>"  onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Estudiante.php?codigo=<?php echo($lis["Codigo"]);?> & nombre=<?php echo ($lis["Nombre"]);?> & correo=<?php echo ($lis["Correo"]); ?> & tel=<?php echo ($lis["Telefono"]); ?>  "><img src="Img/update.png"></a></td>

                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <?php
                    $info = new DAO_Estudiante();
                    $lista = $info->ListarEstudiantesCodigo($codigo);
                    $numero_de_registros = count($lista);
                    $registros_x_pagina = 15;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        header('Location:Consultar_Estudiantes.php?pagina=1');
                    }

                    $lista2 = $info->ListarEstudiantesCodigo_paginaciono($codigo, $iniciar, $registros_x_pagina);

                    foreach ($lista2 as $lis) {
                        ?>
                        <tr>
                            <th scope="col"><?php echo($lis["Codigo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Nombre"]); ?> </th>
                            <th scope="col"><?php echo($lis["Correo"]); ?> </th>
                            <th scope="col"> <?php echo($lis["Telefono"]); ?> </th>
                            <td class="bot"><a href="Control/Eliminar_Estudiante.php?correo=<?php echo ($lis["Correo"]) ?>"  onclick="return confirmar()"><img src="Img/recycle.png"></a></td>
                            <td class="bot"><a href="Editar_Estudiante.php?codigo=<?php echo ($lis["Codigo"]); ?>&nombre=<?php echo ($lis["Nombre"]); ?>&correo=<?php echo ($lis["Correo"]); ?>&tel=<?php echo ($lis["Telefono"]); ?> "><img src="Img/update.png"></a></td>

                        </tr>
                        <?php
                    }
                    ?>  
                    <?php
                }
                ?>

            </table>
            <!--Fin tabla-->
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Consultar_Estudiantes.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Consultar_Estudiantes.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Consultar_Estudiantes.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ob_end_flush();
        ?>

    </body>
</html>




