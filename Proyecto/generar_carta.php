<?php
ob_start();
require_once './home.php';
require_once './Modelo/DAO_Comite.php';
require_once './Modelo/DAO_Estudiante.php';
require_once 'dompdf/autoload.inc.php';
require_once "./Modelo/DAO_Anteproyecto.php";
//require_once 'Modelo/Email_archivos_adjuntos.php';
require_once 'PhpMailer/PHPMailerAutoload.php';
use Dompdf\Dompdf;
$pdf = new Dompdf();
$anteproyecto = new DAO_Anteproyecto();
$objestudiante = new DAO_Estudiante();
$id_proyecto=htmlentities(addslashes(filter_input(INPUT_GET,"id")));
$comite1 = new DAO_Comite();
setlocale(LC_ALL, "es_CO");
$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha = $dias[date("w")-1] . " " . (date("d")) . " de " . $meses[date("n") - 1] . " del " . date("Y");
$cantidad_d = $comite1->cantidad_directores($id_proyecto);
$cant_d = $cantidad_d['cantidad'];
$cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
$cant = intval(implode($cantidad));
$proyecto;
$destino[0]="";
$destino[1]="";
if($cant>1){
  $proyecto = $anteproyecto->datosficha2($id_proyecto);  
}else{
    $proyecto = $anteproyecto->datosficha($id_proyecto);

}

$tutor="";
if($cant_d>1){
     $tutor = $comite1->director2($id_proyecto);
    foreach ($tutor as $tu) {
        if($cant_d==2){
        $director .= "" . $tu['Nombre'] . " y ";
        $cant_d=3;
        }else{
        $director .= "" . $tu['Nombre'] . " ";    
        }
    }
 $cant_d=2;
}else{
     $tutor = $comite1->director($id_proyecto);
    $director .= "" . $tutor['Nombre'] . "";
}


?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
       
        <style>
            p{
                line-height: 100%;  
            }
            .justificar{
                text-align: justify;
            }
            input[type=number]::-webkit-inner-spin-button,
            input[type=number]::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] { -moz-appearance:textfield; }

#btn-aceptarR{
    background-color:#9a0a18;
    border-radius:2px;
    width:120px;
}

#btn-aceptarR :hover{
    background-color: #fdb400; 
    color:#252525;
}

        </style>
    </head>
    <body>
    <?php
    if($coordinacion=!null){
    ?>
        <form method="POST"> <!--action="Control/Carta"-->
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-12 col-md-7 align-self-center">
                        <img src="Img/header.png" width="25%">
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-12 col-md-7">
                        <p>Bogota D.C <?php echo $fecha ?></p>
                        <p><input type="text" placeholder="Numero carta" name="carta"></p>
                        <p>Señores <?php
                            if ($cant == 2) {
                                $destino[0]=$proyecto['Correo'];
                                $destino[1]=$proyecto['correo_dos'];
                                echo "<br><b>" . $proyecto["Estudiante_Uno"] . "</b><br>";
                                echo "<b>" . $proyecto["Codigo"] . "</b><br>";
                                echo "<b>" . $proyecto["Estudiante_Dos"] . "</b><br>";
                                echo "<b>" . $proyecto["Codigo_Dos"] . "</b><br>";
                                echo "Estudiantes ".$proyecto["Carrera"] ."<br>";
                                echo "Facultad Tecnológica<br>";
                                echo "Universidad Distrital Francisco José de Caldas<br>";
                                echo "La ciudad";
                            } else {
                                $destino[0]=$proyecto['Correo'];
                                echo "<b><br>" . $proyecto["Estudiante_Uno"] . "</b><br>";
                                echo "<b>" . $proyecto["Codigo"] . "</b><br>";
                                echo "" . $proyecto["Carrera"] . "<br>";
                                echo "Facultad Tecnológica<br>";
                                echo "Universidad Distrital Francisco José de Caldas<br";
                                echo "La ciudad";
                            }
                            ?> </p>
                    </div>
                </div>
                <div class="row justify-content-end justificar">
                    <div class="izquierda">
                        <p><b>Ref:</b> Respuesta Anteproyecto de Grado</p>
                    </div>
                    <div class="col-12 col-md-12">
                        <p><b>Respetados Estudiantes:</b><br> Reciban un cordial saludo desde
                            el Proyecto Curricular de Tecnología en Electrónica, la presente
                            es con el fin de remitir la decisión tomada por el Consejo Curricular de Tecnología
                            en Electrónica, en sesión NO. <input type="number" name="sesion" placeholder="sesion"> del dia 
                            <select name="dia">
                                <option selected ></option>
                                <?php
                                for ($i = 0; $i < 7; $i++) {
                                    ?>
                                    <option value="<?php echo$dias[$i] ?> "><?php echo$dias[$i] ?></option>
                                    <?php
                                }
                                ?>
                            </select> 
                            <input type="text" placeholder="dia" name="ndia" >
                            de <select name="carrera" >
                                <option selected></option>
                                <?php
                                for ($i = 0; $i < 12; $i++) {
                                    ?>
                                    <option value="<?php echo $meses[$i] ?>"><?php echo$meses[$i] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            de <?php echo date("Y"); ?> , respecto al asunto en referencia:</p>
                        <p>Al respecto el Consejo Curricular informa que en concordancia del Acuero 015 de 2010 por el
                            cual se establece y reglamenta las opciones de grado para los proyectos curriculares
                            de la Universidad Distrital Francisco José de Caldas, el Consejo Curricular
                            designo al Docente <?php echo"<b>".$director."</b>" ?> como tutor del proyecto y al docente:
                            <?php echo"<b>".$proyecto['Evaluador']."</b>" ?> como revisor
                            del anteproyecto titulado: <?php echo" ".$proyecto["Nombre_Proyecto"]. " ". $proyecto["Carrera"] . "</b>" ?>
                            , con código <?php echo"<b>" . $proyecto["Id_Proyecto"] . "</b>" ?>, bajo la modalidad
                            de Monografia como el trabajo de grado, el cual tiene concepto
                            de <b>APROBADO</b> asi mismo se informa que dicho proyecto cuenta con
                            un (1) año para su desarrollo a partir de la fecha de aprobación.</p>
                        <p>Agradezco de antemano su colaboración y quedando a su disposición para cualquier
                            aclaración me suscribo</p>
                        <p>Cordialmente,</p>

                    </div>

                </div>
                <br><br>
                <div class="row">
                    <div class="col-12 col-md-9">
                        <p><b>Copia de Original</b></p>
                    </div>

                </div>
                <br><br>
                <div class="row justify-content-end">
                    <div class="col-12 col-md-12 justify-content-end">
                        <b>
                            <center><p>PROYECTO CURRICULAR DE TECNOLOGIA EN ELECTRONICA,</p></center>
                            <center><P>INGENIERIA EN CONTROL E INGENIERIA EN TELECOMUNICACIONES</p></center>
                            <center><P>FACULTAD TECNOLOGICA</p></center>
                            <center><P>PBX. 3238400 Ext. 5014/5015</P></center>
                        </b>
                    </div>

                </div>

            </div>
          <center>  <input type="submit" name="enviar" id="btn-aceptarR" value="Generar Carta"></center>
        </form>
<br>
        <?php
         
        if (filter_input(INPUT_POST, "enviar")) {
            
            $cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
            $cant = intval(implode($cantidad));
            $sesion = filter_input(INPUT_POST, "sesion");
            $carta = filter_input(INPUT_POST, "carta");
            $carrera = filter_input(INPUT_POST, "carrera");
            $dia2 = filter_input(INPUT_POST, "ndia");
            $dia = filter_input(INPUT_POST, "dia");
          
            $html.='<html>
               <head>
         <meta name= "viewport" content= "width=device-width, initial-scale=1, shrink-to-fit=no" >
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            p{
            line-height: 100%;  
            }
            .justificar{
            text-align: justify;
            }
            input[type=number]::-webkit-inner-spin-button,
            input[type=number]::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] { -moz-appearance:textfield; }

        .izquierda{
            
        }
        </style>
    </head>
    <body>
       
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-md-7 align-self-center">
                    <center><img src="Img/header.png" width="25%"></center>
                </div>
                <br>
            </div>
            <div class="row justify-content-start">
                <div class="col-12 col-md-7">
                    <p>Bogota D.C '.$fecha.'</p>
                    <p><b>FTEM '.$carta.'</b></p>
                    <br><b>';
                             if($cant>1){
                                $html.='<p>Señores
                             <br><b>'.$proyecto["Estudiante_Uno"].'</b><br>
                             <b>'.$proyecto["Codigo"].'</b><br>
                             <b>'.$proyecto["Estudiante_Dos"].'</b><br>
                             <b>'.$proyecto["Codigo_Dos"].'</b><br>
                             '.$proyecto["Carrera"].'<br>
                             Facultad Tecnológica<br>
                             Universidad Distrital Francisco José de Caldas<br>
                             La ciudad
                        
                         </p>
                </div>
            </div>'; 
                             }else{
                                 $html.='<p>Señor
                             <br><b>'.$proyecto["Estudiante_Uno"].'</b><br>
                             <b>'.$proyecto["Codigo"].'</b><br>
                             '.$proyecto["Carrera"].'<br>
                             Facultad Tecnológica<br>
                             Universidad Distrital Francisco José de Caldas<br>
                             La ciudad
                        
                         </p>
                </div>
            </div>';
                }
            $html.='<div class="row justificar">
              <div class="col-12 col-md-12">
                <p align="right"><b>Ref:</b> Respuesta Anteproyecto de Grado</p><br>
                </div>
                <div class="col-12 col-md-12">
                    <br><br><br><p><b>Respetados Estudiantes:</b> Reciban un cordial saludo desde
                        el Proyecto Curricular de Tecnología en Electrónica, la presente
                        es con el fin de remitir la decisión tomada por el Consejo Curricular de Tecnología
                        en Electrónica, en sesión NO. '.$sesion.' del dia '.$dia.'
                        '.$dia2.' de '.$carrera.' de '.date("Y").' , respecto al asunto en referencia:</p>
                    <p>Al respecto el Consejo Curricular informa que en concordancia del Acuero 015 de 2010 por el
                        cual se establece y reglamenta las opciones de grado para los proyectos curriculares
                        de la Universidad Distrital Francisco José de Caldas, el Consejo Curricular
                        designo al Docente <b>'.$director.'</b>  como tutor del proyecto y al Docente:<b>
                        '.$proyecto["Evaluador"].' </b>como revisor
                        del anteproyecto titulado: <b>"'.$proyecto["Nombre_Proyecto"].'"</b>,
                        con código <b>'.$proyecto["Id_Proyecto"].'</b>, bajo la modalidad
                        de Monografia como el trabajo de grado, el cual tiene concepto
                        de <b>APROBADO</b> asi mismo se informa que dicho proyecto cuenta con
                        un (1) año para su desarrollo a partir de la fecha de aprobación.</p>
                    <p>Agradezco de antemano su colaboración y quedando a su disposición para cualquier
                        aclaración me suscribo</p>
                    <p>Cordialmente,</p>

                </div>

            </div>
            
                <div class="row">
                    <div class="col-12 col-md-9">
                        <p><b>Copia de Original</b></p>
                    </div>

                </div>
            <br><br>
        <div class="row justify-content-end">
                <div class="col-12 col-md-12 justify-content-end">
                    <b>
                        <center><p>PROYECTO CURRICULAR DE TECNOLOGIA EN ELECTRONICA,</p></center>
                        <center><P>INGENIERIA EN CONTROL E INGENIERIA EN TELECOMUNICACIONES</p></center>
                        <center><P>FACULTAD TECNOLOGICA</p></center>
                        <center><P>PBX. 3238400 Ext. 5014/5015</P></center>
                    </b>
                </div>

            </div>

        </div>
              
    </body>
</html>';

           $pdf->loadHtml($html);
            $pdf->setPaper("Legal");
            ob_clean();
            $pdf->render();
            $ruta="Carta_Anteproyecto/".$id_proyecto;
            file_put_contents( $ruta.".pdf", $pdf->output());
            $pdf->stream("carta.pdf",array("Attachment" => 0));
            if($cant == 2){
            for($i=0;i<=2;$i++){
            $asunto="Carta de Radicado";
            $mensaje=utf8_decode("Buen día se adjunta la carta del radicado");
            $archivo[0]=$_SERVER["DOCUMENT_ROOT"] ."/" .$ruta.".pdf";
            $archivo[1]=$id_proyecto.'.pdf';
            
            
            $mail = new PHPMailer;
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';

            $mail->Username = "moduloproyectoselectronica@gmail.com";
            $mail->Password = "txqguzrhjukzfkwg";

            $mail->setFrom('moduloproyectoselectronica@gmail.com');
            $mail->addAddress($destino[$i]);
            $mail->addReplyTo("moduloproyectoselectronica@gmail.com");

            $mail->isHTML(TRUE);
            $mail->Subject = $asunto;
            $mail->Body = $mensaje;
            $mail->AddAttachment($archivo[0], $archivo[1]);
            $mail->send();
            if($i==2){
                break;
            }
            }
                
            }else{
                 for($i=0;i<=1;$i++){
            $asunto="Carta de Radicado";
            $mensaje=utf8_decode("Buen día se adjunta la carta del radicado");
            $archivo[0]=$_SERVER["DOCUMENT_ROOT"] ."/" .$ruta.".pdf";
            $archivo[1]=$id_proyecto.'.pdf';
            
            
            $mail = new PHPMailer;
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';

            $mail->Username = "moduloproyectoselectronica@gmail.com";
            $mail->Password = "txqguzrhjukzfkwg";

            $mail->setFrom('moduloproyectoselectronica@gmail.com');
            $mail->addAddress($destino[$i]);
            $mail->addReplyTo("moduloproyectoselectronica@gmail.com");

            $mail->isHTML(TRUE);
            $mail->Subject = $asunto;
            $mail->Body = $mensaje;
            $mail->AddAttachment($archivo[0], $archivo[1]);
            $mail->send();
            if($i==1){
                break;
            }
            }
            exit;
           
        }
        ?>


<?php
}
}else{
    session_destroy();
    echo "<script>location.href='index.php';</script>";
}
?>

    </body>
</html>
