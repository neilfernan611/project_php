<?php
require_once ('./Modelo/DAO_Anteproyecto.php');
require_once ('./Modelo/DAO_Estudiante.php');
require_once ('./home.php');
$nombre_proyecto = "";
$proyecto=new DAO_Anteproyecto();
$estudiante = new DAO_Estudiante();
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <script>
            function confirmar() {
                if (!confirm('¿Seguro que quiere borrar este grupo de investigación?'))
                    return false;
            }

        </script>

        <title></title>
    </head>
    <body>
        <br>
        <div class="row container">

            <?php
            if ((filter_input(INPUT_POST, "buscar"))) {
                $nombre_proyecto = filter_input(INPUT_POST, "nombre");
            }
            ?>

            <div class="col-md-12 order-md-2 container">
                <form class="form-inline" method="post">
                    <div class="form-group mb-2">
                        <label for="staticEmail2" class="sr-only"></label>
                        <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Titulo del proyecto">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="nombre" class="sr-only">Password</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required placeholder="Titulo">
                    </div>
                    <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR" name="buscar">
                </form>
            </div>
        </div>
        <?php
         if (filter_input(INPUT_POST, "Guardar")) {
                $codigo = filter_input(INPUT_POST, "codigo");
                $id = filter_input(INPUT_POST, "id_proyecto");
                $lista=$estudiante->cantidad_de_proponentes($id);
                $verificar=$estudiante->estudianteExiste($codigo);
                
                $verificarcod2=$estudiante->Codigo_Carrera($id);
                $verificarcod1=$estudiante->Verificacion_de_carrera($codigo);
                
                $relacionado = $estudiante->proyectoRelacionado($codigo);
                $name = $relacionado['Nombre_Proyecto'];
                
                if($lista['COUNT(Id_Proyecto)']=='2'){ ?>
                
                   <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>La operación no fue exitosa!</strong> Se excede la cantidad de proponentes.
                    </div>
               
               <?php }else if($verificar==false){ ?>
               
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>La operación no fue exitosa!</strong> Segundo estudiante no encontrado, por favor verifique si ya se registro en la plataforma.
                        </div>
                <?php }else if($name != NULL){ ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>La operación no fue exitosa!</strong> El segundo estudiante ya tiene un proyecto registrado. 
                        </div>
                <?php } else if($verificarcod1['Id_Carrera']!=$verificarcod2['Id_Carrera']){ ?>
                         <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>La operación no fue exitosa!</strong> Los estudiantes no son de la misma carrera.
                        </div>
                <?php    
                }else{
                        $codi = $proyecto->Agregar_estudiante($id, $codigo);
                        
                        if ($codi == $id) {
                            ?>
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Éxito!</strong> Estudiante Agregado.
                            </div>
                        <?php } else {
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>la operación no fue exitosa!</strong> No se pudo Agregar al Estudiante.
                            </div>
                            <?php
                    }
                
                }
                
            }
        ?>
        <br>
        <div class="row">
            <div class="col-md-12 order-md-1 container">
               
                    <table class="table" id="table">
                        <thead class="thead-dark">
                            <tr  align="center" valign="middle">
                            <tr>
                                <th scope="col">Proponentes</th>
                                <th scope="col">Titulo</th>
                                <th scope="col">Cancelar</th>
                                <th scope="col">Agregar Proponente</th>
                                <th scope="col">Aceptar</th>
                            </tr>
                        </thead>
                        <?php
                        $proy = new DAO_Anteproyecto();
                        if ($nombre_proyecto== null || $nombre_proyecto == "") {
                            
                        } else {
                            
                            $lista_ant = $proy->buscar_proyectos_para_cancelar($nombre_proyecto);
                            foreach ($lista_ant as $lista) {
                                ?>
                                <tr>
                                <form method="POST">
                                    <th scope="col"><?php echo($lista["Nombres"]); ?> </th>
                                    <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                    <td class="bot">
                                        <a href="Control/Cancelar_Proyecto.php?id_proyecto=<?php echo($lista["Id_Proyecto"]);?>" onclick="return confirmar()"><img src="Img/recycle.png"></a></td>

                                    <td class="bot"><input type="number" name="codigo" required placeholder="" min="11111111111" max="99999999999" title="un codigo estudiantil cuenta con 11 digitos numéricos sin espacios">
                                        <input type="text" hidden value="<?php echo($lista["Id_Proyecto"]); ?>" name="id_proyecto">
                                    </td>
                                <th scope="col"><input type="submit" name="Guardar" id="btn" class="btn btn-outline-success" value="Guardar"> </th>
                                </form>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                    </table>
                
            </div>
        </div>
    </body>
</html>
