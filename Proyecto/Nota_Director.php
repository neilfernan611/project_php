<?php
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Notas.php';
require_once './home.php';
$pdf = new DAO_Proyecto();
$datos=$_SESSION["informacion2"];
$nombre_proyecto = "";
?>

<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    
    
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
$cedula = $datos['Cedula']
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
    </head>
    <body>
       
        <br>
        <?php
      
        $id_proy = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
        if ($profesor = !NULL ) {
            ?>
            <div class="py-1 text-center" id="centrar">
<script>
            function fncSumar() {
                caja = document.forms["sumar"].elements;
                var numero1 = Number(caja["nota1"].value);
                var numero2 = Number(caja["nota2"].value);
                var numero3 = Number(caja["nota3"].value);
                var numero4 = Number(caja["nota4"].value);
                var numero5 = Number(caja["nota5"].value);
                var numero6 = Number(caja["nota6"].value);
                resultado = numero1 + numero2 + numero3 + numero4 + numero5 + numero6;
                if (!isNaN(resultado)) {
                    caja["resultado"].value = numero1 + numero2 + numero3 + numero4 + numero5 + numero6;
                }
            }
        </script>

        <div class="col-md-8 order-md-1 container">

            <!-- class="needs-validation" novalidate-->
            <form action="Control/Nota_Director.php" method="POST" id="form-registro" name="sumar">
                <input type="text"  id="cedula" name="cedula" placeholder="" value="<?php echo $cedula; ?>" style="display: none">
                <input type="text"  id="id_proy" name="id_proy" placeholder="" value="<?php echo $id_proy; ?>" style="display: none">
                <TABLE BORDER>
                    <TR>
                        <TH COLSPAN=2> EVALUACIÓN DEL DIRECTOR </TH> <TH> NOTA </TH> 
                    </TR>
                    <TR>
                        <TD> Cumplimiento del Cronograma </TD> <TD> Hasta 15 puntos </TD> <TD><input type="number" name="nota1"  min="0" max="15" class="form-control" required onKeyUp="fncSumar()"> </TD>
                    </TR>
                    <TR>
                        <TD> Fundamentación teórica y matemática utilizada </TD> <TD> Hasta 15 puntos </TD> <TD><input type="number" name="nota2"  min="0" max="15" class="form-control" required onKeyUp="fncSumar()"> </TD>
                    </TR>
                    <TR>
                        <TD> Alcance de las especificaciones de la solución planteada </TD> <TD> Hasta 15 puntos </TD> <TD><input type="number" name="nota3"  min="0" max="15" class="form-control" required  onKeyUp="fncSumar()"> </TD>
                    </TR>
                    <TR>
                        <TD> Metodología utilizada </TD> <TD> Hasta 15 puntos </TD> <TD><input type="number" name="nota4"  min="0" max="15" class="form-control" required onKeyUp="fncSumar()"> </TD>
                    </TR>
                    <TR>
                        <TD> Eficiencia y/o acabado o viabilidad tecnológica </TD> <TD> Hasta 15 puntos </TD> <TD><input type="number" name="nota5"  min="0" max="15" class="form-control" required onKeyUp="fncSumar()"> </TD>
                    </TR>
                    <TR>
                        <TD> Cumplimiento del marco general del proyecto </TD> <TD> Hasta 25 puntos </TD> <TD><input type="number" name="nota6"  min="0" max="25" class="form-control" required onKeyUp="fncSumar()" > </TD>
                    </TR>
                    <TR>
                        <TD style="text-align: right" colspan="2"> TOTAL </TD> <TD><input type="number" name="resultado" readonly> </TD>
                    </TR>
                </TABLE>
                <br>
                <div class="col-md-15 mb-1">
                    <label for="letra" style="color: black">¿El trabajo de grado presentado aporta los elementos necesarios para postularlo a mención  Meritoria o Laureada? <span class="text-muted"></span></label>
                
                    <input type="radio"  name="radio" value="Si" required >Si</label><br>
                    <input type="radio"  name="radio" value="No" required >No</label><br>

                    <div class="invalid-feedback">
                        Seleccione al menos 1 por favor.
                    </div>
                </div>
        <hr class="col-md-8 mb-1">
        <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Información</button>

        <div class="modal-body">
            <p></p>
            <hr>
            <p></p>
        </div>
    </form>
    </div>
    <?php
} else {
    session_destroy();
    echo "<script>location.href='index.php';</script>";
}
?>
</body>

