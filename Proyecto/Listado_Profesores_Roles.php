<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
ob_start();
require_once './Modelo/DAO_Profesor.php';
require_once './Modelo/DAO_Rol.php';
require_once './home.php';
$id_rol = "";


if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <br>

        <?php
        if($comite!=null || $coordinacion!=null)
        {
            if (!($_GET)) {

                echo "<script>location.href='Listado_Profesores_Roles.php?pagina=1';</script>";
            }
            
        if ((filter_input(INPUT_POST, "buscar"))) {
            $id_rol = filter_input(INPUT_POST, "id_rol");
        }
            
        ?>


        <form class="form-inline" method="POST">
            <div class="form-group mb-2">

                <label for="selectTipo" >Rol Del Profesor</label>
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <select id="selectTipo"  required class="custom-select" name="id_rol">

                    <?php
                    $obj_r = new DAO_Rol();
                    $lista = $obj_r->listaRol();
                    foreach ($lista as $list) {
                        ?>
                        <option value="<?php echo($list['Id_Rol']); ?>"><?php echo($list['Rol']); ?></option>
                        <?php
                    }
                    ?>

                </select>
                <div class="invalid-feedback">
                    Campo obligatorio.
                </div>
            </div>
            <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR"  name="buscar">
        </form>
        <br>     
        <table class="table" id="table" >
            <thead class="thead-dark">
                <tr  align="center" valign="middle">

                <tr>
                    <th scope="col">Cedula</th>
                    <th scope="col">Profesor</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Correo</th>
                </tr>
            </thead>
            <?php
             if ($id_rol == NULL || $id_rol == ("")) {
                    
                } else {
            $profesor = new DAO_Profesor();
            $lista = $profesor->listarProfesorRoles($id_rol);
              $numero_de_registros = count($lista);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='Listado_Profesores_Roles.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    
                    $lista2 = $profesor->listarProfesorRoles_paginacion($id_rol,$iniciar, $registros_x_pagina);
            foreach ($lista2 as $lista) {
                ?>

                <tr>
                    <th scope="col"><?php echo($lista['Cedula']); ?> </th>
                    <th scope="col"> <?php echo($lista['Nombre']); ?> </th>
                    <th scope="col"> <?php echo($lista['Telefono']); ?> </th>
                    <th scope="col"> <?php echo($lista['Correo']); ?> </th>
                </tr>
                <?php
            }
                }
            ?>
        </table>
        <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Listado_Profesores_Roles.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Listado_Profesores_Roles.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Listado_Profesores_Roles.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>


<?php
}else{
    session_destroy();
    echo "<script>location.href='index.php';</script>";
ob_end_flush();
}
?>
    </body>
</html>
