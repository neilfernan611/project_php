<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
require_once './Modelo/DAO_Profesor.php';
require_once './Modelo/DAO_Rol.php';
$obj_Pro = new DAO_Profesor();
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
    </head>
    <body>

        <br>
        <?php
        if ($administrador != null) {
            if (filter_input(INPUT_POST, "Guardar")) {
                $cedula = filter_input(INPUT_POST, "cedula");
                $rol = filter_input(INPUT_POST, "idRol");
                $profe = $obj_Pro->InformacionProfesor($cedula);
                $contra = $profe['Pass_P'];
                $correo = $profe['Correo'];
                $codi = $obj_Pro->crearUsuarioProfe($rol, $correo, $cedula, $contra);
                if ($codi == $cedula) {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Éxito!</strong> Rol asignado.
                    </div>
                <?php } else {
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Fallo!</strong> El usuario ya tiene ese Rol.
                    </div>
                    <?php
                }
            } 
            ?>

            <table class="table" id="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Cédula</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Rol</th>
                        <th scope="col">Aceptar</th>
                    </tr>
                </thead>
                <?php
                $lista = $obj_Pro->listarProfesor();
                foreach ($lista as $lis) {
                    ?>

                    <tr>
                    <form method="POST">
                        <th scope="col"><?php echo($lis["Cedula"]); ?> <input type="text" hidden name="cedula" value="<?php echo($lis["Cedula"]); ?>"> </th>
                        <th scope="col"><?php echo($lis["Nombre"]); ?> <input type="text" hidden name="nombre" value="<?php echo($lis["Nombre"]); ?>"></th>
                        <th scope="col">
                            <div class="col-md-6 mb-3">

                                <select id="selectTipo" class="custom-select" name="idRol" required>
                                    <?php
                                    $obj_Rol = new DAO_Rol();
                                    $listaR = $obj_Rol->listaRol();

                                    foreach ($listaR as $list) {


                                        if ($veri['Id_Rol'] == $list['Id_Rol']) {
                                            
                                        } else {
                                            ?>

                                            <option value="<?php echo($list['Id_Rol']); ?>" ><?php echo($list['Rol']); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>

                                </select>
                                <div class="invalid-feedback">
                                    Campo obligatorio.
                                </div>

                        </th>
                        <th scope="col"><input type="submit" name="Guardar" id="btn" class="btn btn-outline-success" value="Guardar"> </th>
                    </form>
                </tr>

                <?php
            }
            ?>

        </table>

        <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>
</body>
</html>  
