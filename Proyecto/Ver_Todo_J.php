<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Profesor.php';
$info=$_SESSION['informacion2'];
$correo=$info['Correo'];
$cedula=  $info['Cedula'];
$id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
$pdf = new DAO_Proyecto();
$PDF=$pdf->verPDF2($id_proyecto); 

?>

<?php

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head>
        
        
        <title></title>
           <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
            <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!--<link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css">-->
   <script>
            function confirmar()
            {
                var c1 = document.getElementById('Estado_Sustentacion').checked;
                var c2 = document.getElementById('Estado_Sustentacion1').checked;
                if(c1 && c2 || c1 != true && c2 != true )
                {
                    confirm("Seleccione solo un concepto")
                    return false;
                }else{
	            if(confirm("¿Desea continuar?")){
	                return true;
	            }else{
	                return false;
                }}
            }
            function showContent() {
        element = document.getElementById("content");
        check = document.getElementById("Estado_Sustentacion1");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }
</script>
    </head>
    <body>

            
    <?php
    $bandera = htmlentities(addslashes(filter_input(INPUT_GET, "bandera")));
    $codigoo='';
    $obj_D_P = new DAO_Proyecto();
    $obj_D_Pro = new DAO_Profesor();
    //$codigo2=$obj_D_P->Buscar_E($id_proyecto);
    $codigooo = NULL;
    $listar = $obj_D_P->listarProyectos($id_proyecto,$codigooo);
    $infoJurado1 = $obj_D_P -> Consultar_Jurado1($id_proyecto, $cedula);    
    $infoJurado2 = $obj_D_P -> Consultar_Jurado2($id_proyecto, $cedula);
    $cedulaj = $infoJurado2["Id_Jurado"];
    $infoPJ2 = $obj_D_P -> listarProyectosJ($id_proyecto, $cedulaj);
    $infoPJ1 = $obj_D_P -> listarProyectosJ($id_proyecto, $cedula);    
    $info2E = $obj_D_P -> verificar_2estudiantes($id_proyecto);
    $infoE = $obj_D_P -> consultar_estudiantes($id_proyecto);
    $infoE1 = $obj_D_P -> consultar_1Estudiante($id_proyecto);
    $cantT = $obj_D_Pro -> cantidad_tut($id_proyecto);
    $validarP = $obj_D_P->validar_proyecto($id_proyecto);
   
    if($profesor!=null){
    ?>
    <div class="container well" id="Ver_Todo_J">
        <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
            <div class="card-header color_cabecera">
                <b><b><center>Información Del Proyecto</center></b></b>
            </div>
            <div class="card-body ">
                    <b><label  style="color: black">Titulo del Anteproyecto </label></b>
                    <label style="color: black"><?php echo ($listar["Nombre_Proyecto"]);?></label>
                    
                    <b><label style="color: black">Número de radicado </label></b>
                    <label style="color: black"><?php echo ($listar["Radicado"]);?></label>
        
                    <b><label style="color: black">Fecha de Radicado </label></b>
                    <label style="color: black"><?php echo ($listar["Fecha_Radicado"]);?></label>
        
                    <b><label style="color: black">Fecha de Registro </label></b>
                    <label style="color: black"><?php echo ($listar["Fecha_Registro"]);?></label>
            </div>
            <hr>
            <div class="card-body ">
                <b><label style="color: black">Comite </label></b>
                 <label style="color:#20A300"><?php echo ($listar["Com_Estado"]);?></label>
                
                <b> <label style="color: black">Coordinacion </label></b>
                 <label style="color:#20A300"><?php echo ($listar["Coo_Estado"]);?></label>
            
             <!-- Tutor-->
                    <?php if($cantT["cantidad"] == '1')
                            {
                                $listaTut = $obj_D_Pro -> uno_tutor($id_proyecto);
                                
                    ?>
                              <b><label style="color: black">Concepto Tutor <?php echo $listaTut["Nombre"] ;?> </label></b>
                                <?php if($listaTut["EstadoS"] == 'Listo para sustentar') {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["EstadoS"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["EstadoS"]);?> </label></b></i>
                                 <?php }?>
                    <?php
                            }elseif ($cantT["cantidad"] == '2')
                            {
                                $listaTut = $obj_D_Pro -> dos_tutor($id_proyecto);
                    ?>          
                                <b><label style="color: black">Concepto Tutor <?php echo ($listaTut["Nombret1"]);?> </label></b>
                                <?php if(($listaTut["nestado1"]) == "Listo para sustentar") {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["nestado1"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["nestado1"]);?> </label></b></i>
                                 <?php }?>
                                 
                                 <b><label style="color: black">Concepto Tutor <?php echo ($listaTut["Nombret2"]);?> </label></b>
                                 <?php if(($listaTut["nestado2"]) == "Listo para sustentar") {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["nestado2"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["nestado2"]);?> </label></b></i>
                                 <?php }?>
                    <?php
                            }
                    
                    ?>    
            
                <!-- Estudiantes-->
                <?php if($info2E['numero']!=0) {?>
                    <!-- UNO -->
                    <b><label style="color: black">Concepto Estudiante <?php echo ($infoE["nombre_e1"]);?> </label></b>
                    <?php if(($infoE["estados_e1"]) == 'Listo para sustentar'){?>
                     <b><i><label style="color:#20A300"><?php echo ($infoE["estados_e1"]);?></label></b></i>
                    <?php }else{ ?>
                     
                     <b><i><label style="color:#C10000"><?php echo ($infoE["estados_e1"]);?></label></b></i>
                    <?php }?>
                    
                    <b><label style="color: black">Concepto Estudiante <?php echo ($infoE["nombre_e2"]);?> </label></b>
                    <?php if(($infoE["estados_e2"]) == 'Listo para sustentar'){?>
                     <b><i><label style="color:#20A300"><?php echo ($infoE["estados_e2"]);?></label></b></i>
                    <?php }else{ ?>
                     <b><i><label style="color:#C10000"><?php echo ($infoE["estados_e2"]);?></label></b></i>
                    <?php }?>
                    <?php 
                }else{ ?>
                    <!-- DOS-->
                    <b><label style="color: black">Concepto Estudiante <?php echo ($infoE1["Nombre"]);?> </label></b>
                    <?php if(($infoE1["EstadoS"]) == 'Listo para sustentar'){?>
                   <b><i><label style="color:#20A300"><?php echo ($infoE1["EstadoS"]);?></label></b></i>
                    <?php }else{?>
                   
                   <b><i><label style="color:#C10000"><?php echo ($infoE1["EstadoS"]);?></label></b></i>
                    <?php }?>
                <?php } ?>
                <!-- Fin estudiantes-->
                <!-- Estado jurados-->
                </div>
                <hr>
                <div class="card-body ">
                    <form action="Control/Actualizar_Estado_Sustentacion.php" method="POST" id="form-registro">
                    <?php if($infoPJ1["Estado_Jurado"] != "01" && $validarP["cantidad"] == 0)
                    { ?>
         
                        <div id="content" style="display: none;" > 
                            <b> <label style="color: black" value=NULL > Motivo del aplazamiento</label> </b>
                            <input type ="text" name="comentario">
                        </div>
                    <?php if($bandera == 2 )
                    { ?>
                         <b><i><label style="color: black" > El motivo del aplazamiento es obligatorio </label> </i></b>
                    <?php } ?>
                    <b><label  style="color: black" >Concepto</label></b>
                    <div class="col-md-12 mb-2">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Estado_Sustentacion" name="Estado_Sustentacion" value="01">
                                <label class="custom-control-label" for="Estado_Sustentacion">Listo para sustentar</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Estado_Sustentacion1" name="Estado_Sustentacion"  onchange="javascript:showContent()" value="02">
                                <label class="custom-control-label" for="Estado_Sustentacion1">Aplazado</label>
                            </div>
                    </div>
                    
                    <input type="submit" onclick="return confirmar()" style="background:#A20C0C" name="Guardar"class="btn btn-primary btn-lg"  value="Guardar Información" >
                    <input type="text"  id="id_proyecto" name="id_proyecto" placeholder="" value="<?php echo ($id_proyecto);?>" style="display: none">
                    <input type="text"  id="codigo" name="cedula" placeholder="" value="<?php echo ($infoJurado1['Id_Jurado']);?>" style="display: none">
                    <input type="text"  id="rol" name="rol" placeholder="" value="<?php echo '01';?>" style="display: none">
                    <?php }?>
                    </form>
                    <?php if($bandera == 1 )
                    { ?>
                         <i><label style="color: black" > Información guardada </label> </i>
                    <?php } ?>
                    
               
                <!-- Fin -->
                  <?php $pro = $obj_D_P ->verPDF2($id_proyecto);
                if ($pro{"Nombre_Archivo"} == $id_proyecto."R.pdf"){?>
                    <b><label for="Ver" style="color: black">Ver Documento 
                    <a href="PDF.php?id=<?php echo($id_proyecto); ?>" target="_blank"> <img src="Img/doc.png"></a></label></b>
                    
                <?php 
                }else{?>
                    <b><label for="Ver" style="color: black">El documento no ha sido cargado por los estudiantes </label></b>
                <?php }?>              
                 
                             
            </div>
        </div>
    </div>
    <br>
    <?php 
    }else{
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
?>
    </body>
</html>
