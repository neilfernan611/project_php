<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Anteproyecto.php';
require_once './Modelo/DAO_Estudiante.php';
$info = $_SESSION['informacion2'];
$id_proyecto = $info['Id_Proyecto'];
$codigo = $info['Codigo'];

?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
    </head>
    <body>
        <?php
        if ($estudiante != null) {
            $proy = new DAO_Anteproyecto();
            $objestu = new DAO_Estudiante();
            
            //Cuando tiene ya tiene un anteproyecto devuelto
            $existenciae =  $proy->Consultar_Existencia_Estudiante_Devuelto($codigo);
            $namep = $objestu->proyectoRelacionado($codigo);
            $idpd =  $objestu->proyectoDevuelto($codigo);
            $modalidadde= $objestu->proyectodevueltomodalidad($codigo);
        ?>
            <div class="container">
                <?php 
                if($existenciae == 1 && $namep['Nombre_Proyecto']== NULL){
                    $listad = $proy->verPDF2($idpd['Id_Proyecto']);
                ?>
                    <div class="card">
                        <div>
                            <h2><?php echo($listad["Nombre_Proyecto"]); ?> - <?php echo($listad["Estado"]); ?> </h2>
                            <p><b>Fecha de registro: </b><?php echo($listad["Fecha_Registro"]); ?></p><br>
                            <?php
                                $cantDirector = $proy->cantidad_de_directores($idpd['Id_Proyecto']);
                                $cantD = intval(implode($cantDirector));
                                if ($cantD > 1) {
                                    $directores = $proy->datosfichasDosDirector($idpd['Id_Proyecto']);
                            ?>      <p><b>Directores de proyecto</b></p>           
                                    <p><?php echo($directores['Evaluador_Uno']); ?></p>
                                    <p><?php echo($directores['Evaluador_Dos']); ?></p>
                            <?php 
                                } else {
                                    $undirector = $proy->datosfichasUnDirector($idpd['Id_Proyecto']);
                            ?>      <p><b>Director de proyecto </b></p>             
                                    <p><?php echo($undirector['Nombre']); ?></p>
                            <?php  } 
                            ?>
                            <?php
                            if($modalidadde['Id_Modalidad']=='204' || $modalidadde['Id_Modalidad']=='205'){
                            ?>    
                                <a href="PDF.php?id=<?php echo($listad["Id_Proyecto"]); ?>" target="_blank">Carta a consejo</a>
                            <?php } else{ 
                            ?>          
                                        <a href="PDF.php?id=<?php echo($listad["Id_Proyecto"]); ?>" target="_blank">Documento del anteproyecto</a>
                                         <?php
                                        if($modalidadde['Id_Modalidad']=='208' /*|| $modalidadde['Id_Modalidad']=='205' || $modalidadde['Id_Modalidad']=='204'*/ || $modalidadde['Id_Modalidad']=='206'){ ?>
                                           
                                            <a href="PDF.php?id=<?php echo($listad["Id_Proyecto"]); ?>&correo=<?php echo($listad["Id_Proyecto"]);?>" target="_blank">Anexo de documentos</a> 
                                        <?php } ?>
                            
                            <?php }
                            ?>
                                <a href="Informacion_estados.php?id=<?php echo($listad["Id_Proyecto"]); ?>">Ver estados</a>
                        </div>   
                    </div>
                <?php
                }else{
                    $lista = $proy->verPDF2($id_proyecto);
                ?>
                    <div class="card">
                        <div>
                            <h2><?php echo($lista["Nombre_Proyecto"]); ?></h2>
                            <p><b>Fecha de registro: </b><?php echo($lista["Fecha_Registro"]); ?></p><br>
                            <?php
                                $cantDirector = $proy->cantidad_de_directores($id_proyecto);
                                $cantD = intval(implode($cantDirector));
                                if ($cantD > 1) {
                                    $directores = $proy->datosfichasDosDirector($id_proyecto);
                            ?>      <p><b>Directores de proyecto </b></p>            
                                    <p><?php echo($directores['Evaluador_Uno']); ?></p>
                                    <p><?php echo($directores['Evaluador_Dos']); ?></p>
                            <?php 
                                } else {
                                    $undirector = $proy->datosfichasUnDirector($id_proyecto);
                            ?>      <p><b>Director de proyecto </b></p>             
                                    <p><?php echo($undirector['Nombre']); ?></p>
                            <?php  } 

                            if($namep['Id_Modalidad']=='204' || $namep['Id_Modalidad']=='205'){
                            ?>    
                                <a href="PDF.php?id=<?php echo($lista["Id_Proyecto"]); ?>" target="_blank">Carta a consejo</a>
                            <?php } else{ 
                            ?>
                                        <a href="PDF.php?id=<?php echo($lista["Id_Proyecto"]); ?>" target="_blank">Documento del anteproyecto</a>
                                         <?php
                                        if($namep['Id_Modalidad']=='208' /*|| $namep['Id_Modalidad']=='205' || $proyecto['Id_Modalidad']=='204'*/ || $namep['Id_Modalidad']=='206'){ ?>
                                           
                                            <a href="PDF.php?id=<?php echo($lista["Id_Proyecto"]); ?>&correo=<?php echo($lista["Id_Proyecto"]);?>" target="_blank">Anexo de documentos</a> 
                                        <?php } ?>
                            <?php }
                            ?>
                                <a href="Informacion_estados.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver estados</a>
                        </div>   
                    </div>
                <?php }
                ?>
            </div>
        <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>
