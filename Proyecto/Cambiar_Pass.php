<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
include './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>


<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <?php
        if ($estudiante != null) {
            $nombre = $_SESSION["nombre"];
            $info = $_SESSION['informacion2'];
            $infop = $_SESSION['informacion'];
            $codigo = $info['Codigo'];
            $correo = $info['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($coordinacion != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($profesor != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($comite != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        } elseif ($administrador != null) {
            $nombre = $_SESSION["nombre"];
            $infop = $_SESSION["informacion"];
            $correo = $infop['Correo'];
            $tele = $infop['Telefono'];
        }
        ?>

        <br><br><br><br>
        <?php
        if ($administrador != null || $comite != null || $coordinacion != null || $estudiante != null || $profesor != NULL) {
            ?>
            <div class="container">

            <div class="mb-auto text-center">

                <p><center><h4>CAMBIAR CONTRASEÑA<h4></center></p>
        </div>

        <div class="col-md-8 order-md-1 container">

            <!-- class="needs-validation" novalidate-->
            <form action="Control/Cambiar_Pass.php" method="POST" id="form-registro" >

                <div class="form-group">
                    <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Contraseña actual</label>
                    <input type="password" class="form-control"  name="pass_a"  pattern=".{6,}" placeholder="password" title="Debe tener un minimo de 6 caracteres" required>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-unlock-alt" aria-hidden="true"></i> Contraseña nueva</label>
                    <input type="password" class="form-control"  name="pass_n" pattern=".{6,}" placeholder="password" title="Debe tener un minimo de 6 caracteres" required>
                </div>

                <hr class="col-md-8 mb-1">
                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Aceptar</button>

                <div class="modal-body">
                    <p></p>
                    <hr>
                    <p></p>
                </div>
            </form>
        </div>
        </div>

        <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>
</body>
</html>


