<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <title></title>
    </head>
    <body>

        <br>
        <?php
        if ($administrador != null) {
            ?>

            <div class="py-5 text-center" id="centrar">
                <h3><center>AGREGAR DOCUMENTO DEL CALENDARIO ACADÉMICO</center></h3>
                <p><center>Ingrese la información en todos los campos</center></p>
            </div>
            <div class="col-md-8 order-md-1 container">
                <form action="Control/Subir_Calendario.php" enctype="multipart/form-data" method="POST" id="form-registro" >
                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <div class="input-group-prepend">
                                <label for="firstName" style="color: black">Documento PDF</label>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="archivo" id="real-file" hidden="hidden" />
                                <button type="button" id="custom-button">Subir Documento</button>
                                <span id="custom-text">Ningún archivo elegido, todavía.</span>
                            </div>
                        </div> 
                        <div class="col-md-6 mb-3">
                            <input type="text" name="estado" value="calendario" hidden>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Subir</button>
                    <hr>
                </form>
            </div>
        <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>
    <script>
        const realFileBtn = document.getElementById("real-file");
        const customBtn = document.getElementById("custom-button");
        const customTxt = document.getElementById("custom-text");

        customBtn.addEventListener("click", function () {
            realFileBtn.click();
        });

        realFileBtn.addEventListener("change", function () {
            if (realFileBtn.value) {
                customTxt.innerHTML = realFileBtn.value.match(
                        /[\/\\]([\w\d\s\.\-\(\)]+)$/
                        )[1];
            } else {
                customTxt.innerHTML = "No file chosen, yet.";
            }
        });

    </script>
</body>
</html>
