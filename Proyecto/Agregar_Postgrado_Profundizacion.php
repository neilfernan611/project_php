<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
require_once './Modelo/DAO_Modalidad.php';
require_once './Modelo/DAO_LineaInvestigacion.php';
require_once './Modelo/DAO_Estudiante.php';
require_once './Modelo/DAO_Grupo_Investigacion.php';
require_once './Modelo/DAO_Profesor.php';
$informacion = $_SESSION['informacion2'];
$carrera = $informacion['Id_Carrera'];
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
       
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/estilos.css">
        
       <script>
            function confirmar() {
                if (!confirm('¿Está seguro de enviar los documentos? Verifique si los archivos están completos de lo contrario no podra realizar modificaciones en la solicitud'))
                    return false;
            }
        </script>

    </head>
    <body>
        <?php
        if ($estudiante != null) {
        ?>
         <?php if($carrera == '4828'){ ?>
            <div class="py-5 text-center">
                <h5>SOLICITUD DE MATERIAS EN PROFUNDIZACIÓN</h5> 
            </div>
        <?php }else{ ?>
            <div class="py-5 text-center">
                <h5>SOLICITUD DE MATERIAS EN POSTGRADO</h5> 
            </div>
        <?php }
        ?>
        <div class="col-md-8 order-md-1 container">
            <form action="Control/Agregar_Profundizacion.php" method="POST" id="form-registro" enctype="multipart/form-data" >
                <div class="row">
                    <?php if($carrera == '4828'){
                    ?>
                        <div class="col-md-12 mb-3">
                            <input type="hidden" class="form-control" id="nombre" name="nombred" value="Solicitud de materias en profundización">
                        </div>
                    <?php }else{ ?>
                        <div class="col-md-12 mb-3">
                            <input type="hidden" class="form-control" id="nombre" name="nombred" value="Solicitud de materias en postgrado">
                        </div>
                    <?php }
                    ?>
                    <div class="col-md-6 mb-3">
                        <div class="input-group-prepend">
                            <label for="firstName" style="color: black">Carta de solicitud PDF</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="archivo" id="real-file" hidden="hidden" required />
                            <button type="button" id="custom-button">Subir Documento</button>
                            <span id="custom-text">Ningún archivo elegido, todavía.</span>
                        </div>
                    </div>    
                    <!--<div class="col-md-6 mb-3">
                        <div class="input-group-prepend">
                            <label for="firstName" style="color: black">Carta a Consejo PDF</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="carta_c" id="real-filed" hidden="hidden" required />
                            <button type="button" id="custom-buttond">Subir Documento</button>
                            <span id="custom-textd">Ningún archivo elegido, todavía.</span>
                        </div>
                    </div>-->
                </div>
                <div class="row">
                    <?php if ($carrera == '4828') { ?>
                        <div class="col-md-6 mb-3">
                            <input type="hidden" class="form-control" id="nombre" name="modalidad" value="204" min="1" maxlength="11" pattern="^[0-9]+" required>                       
                        </div>
                    <?php } else {
                    ?>
                                <div class="col-md-6 mb-3">
                                    <label for="firstName" style="color: black">Espacios Académicos</label>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="Trabajo1" value="02" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Trabajo de grado 1</label>

                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="Trabajo2" value="03" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Trabajo de grado 2</label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="hidden" class="form-control" id="nombre" name="modalidad" value="205" min="1" maxlength="11" pattern="^[0-9]+" required>                       
                                </div>
                    <?php }
                    ?>
                </div>
                <hr class="col-md-8 mb-1">
                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit" onclick="return confirmar()">Enviar Información</button>
            </form>
        </div>
    <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>

    <script>
        const realFileBtn = document.getElementById("real-file");
        const customBtn = document.getElementById("custom-button");
        const customTxt = document.getElementById("custom-text");

        customBtn.addEventListener("click", function () {
            realFileBtn.click();
        });

        realFileBtn.addEventListener("change", function () {
            if (realFileBtn.value) {
                customTxt.innerHTML = realFileBtn.value.match(
                        /[\/\\]([\w\d\s\.\-\(\)]+)$/
                        )[1];
            } else {
                customTxt.innerHTML = "No file chosen, yet.";
            }
        });

    </script>
     <script>
        const realFileBtn2 = document.getElementById("real-filed");
        const customBtn2 = document.getElementById("custom-buttond");
        const customTxt2 = document.getElementById("custom-textd");

        customBtn2.addEventListener("click", function () {
            realFileBtn2.click();
        });

        realFileBtn2.addEventListener("change", function () {
            if (realFileBtn2.value) {
                customTxt2.innerHTML = realFileBtn2.value.match(
                        /[\/\\]([\w\d\s\.\-\(\)]+)$/
                        )[1];
            } else {
                customTxt2.innerHTML = "No file chosen, yet.";
            }
        });

    </script>
    </body>
</html>
