<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
ob_start();
require_once './Modelo/DAO_Anteproyecto.php';
require_once './Modelo/DAO_Estudiante.php';
require_once './Modelo/DAO_Modalidad.php';
require_once './Modelo/DAO_Profesor.php';

$codigo = "";
$modalidad = "";
$carrera = "";
$director = "";
?>

<?php
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/estilos.css">
    <scritp src="https://code.jquery.com/jquery-3.4.1.min.js"></scritp>
    <script>

        $(document).ready(function ()
        {
            $(".datepicker-input").datepicker("option", "yearRange", "-99:+0");

            $(".datepicker-input").datepicker("option", "maxDate", "+0m +0d");
        });
    </script>



</head>
<body>
    <br>
    <?php
    if ((filter_input(INPUT_POST, "buscar"))) {
        $codigo = filter_input(INPUT_POST, "codigo");
        $modalidad = filter_input(INPUT_POST, "modalidad");
        $carrera = filter_input(INPUT_POST, "carrera");
        $director = filter_input(INPUT_POST, "director");
        $fechadesde = filter_input(INPUT_POST, "fechadesde");
        $fechahasta = filter_input(INPUT_POST, "fechahasta");
    }
    ?>
    <div class="container-fluid">

        <form  method="post">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="selectTipo" >Fecha desde</label>
                    <input type="date" class="custom-select"   name="fechadesde" required="">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="selectTipo" >Fecha hasta</label>
                    <input type="date" class="custom-select" id="datepicker"  name="fechahasta" required="">
                </div>

                <div class="col-md-4 mb-3">
                    <label for="selectTipo" >Director De Proyecto</label>
                    <select id="selectTipo" class="custom-select"  name="director">
                        <option> </option>
                        <?php
                        $obj_pro = new DAO_Profesor();
                        $listrol = $obj_pro->listartutor();
                        foreach ($listrol as $list) {
                            ?>
                            <option required value="<?php echo($list["Cedula"]); ?>"><?php echo($list['Nombre']); ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                        Campo obligatorio.
                    </div> 
                </div>

                <div class="col-md-4 mb-3">
                    <label for="selectTipo" >Carrera</label>
                    <select id="selectTipo"   class="custom-select" name="carrera">
                        <option  value="00">Todas</option>
                        <?php
                        $obj_ca = new DAO_Estudiante();
                        $listac = $obj_ca->ListaCarrera();
                        foreach ($listac as $listc) {
                            ?>
                            <option value="<?php echo($listc['Id_Carrera']); ?>"><?php echo($listc['Carrera']); ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                        Campo obligatorio.
                    </div>
                </div>

                <div class="col-md-4 mb-3">
                    <label for="selectTipo" >Modalidad</label>
                    <select id="selectTipo" class="custom-select" required name="modalidad">
                        <option required value="00">Todas</option>
                        <?php
                        $obj_m = new DAO_Modalidad();
                        $listam = $obj_m->listaModalidad();
                        foreach ($listam as $list) {
                            ?>
                            <option required value="<?php echo($list['Id_Modalidad']); ?>"><?php echo($list['Modalidad']); ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                        Campo obligatorio.
                    </div> 
                </div>     

                <?php if ($comite != null) { ?>
                    <div class="col-md-4 mb-3">
                        <label for="selectTipo" >Estado Comité </label>
                        <select id="selectTipo"  required class="custom-select" name="codigo">
                            <?php
                            $obj_p = new DAO_Anteproyecto();
                            $lista = $obj_p->Consultar_Estados_comite();
                            foreach ($lista as $list) {
                                ?>
                                <option value="<?php echo($list['Id_Estado']); ?>"><?php echo($list['Estado']); ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">
                            Campo obligatorio.
                        </div>
                    </div>
                <?php } ?>
                <center><input type="submit" class="btn btn-primary mx-sm-4 mb-4" id="btn-aceptarR"  name="buscar"></center>
            </div>
        </form>

        <?php
        if ($comite != null || $coordinacion != NULL) {
            if (!($_GET)) {

                echo "<script>location.href='Consultar_Cantidad_Proyectos.php?pagina=1';</script>";
            }
            ?>

            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="table">
                        <?php if ($comite != null) {
                            ?>
                            <thead class="thead-dark">
                                <tr  align="center" valign="middle">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Título del anteproyecto</th>
                                    <th scope="col">Fecha de registro</th>
                                    <th scope="col">Ver</th>
                                </tr>
                            </thead>
                            <?php
                            if ($codigo == null || $codigo == ("")) {
                                
                            } else {
                                $proy = new DAO_Anteproyecto();

                               // if ($modalidad == '00') {
                                    
                                    $lista = $proy->Filtrado_EstadoDOS($codigo, $carrera, $director, $fechadesde, $fechahasta,$modalidad);
                                    
                                    $numero_de_registros = count($lista);
                                    $registros_x_pagina = 10;
                                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                                   
                                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                                        echo "<script>location.href='Consultar_Cantidad_Proyectos.php?pagina=1';</script>";
                                    }
                                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                                    $lista_p = $proy->Filtrado_EstadoDOS_paginacion($codigo, $carrera, $director, $fechadesde, $fechahasta, $iniciar, $registros_x_pagina,$modalidad);
                               /* } else {
                                    $lista = $proy->Filtrado_Estado($codigo, $modalidad, $carrera, $director, $fechadesde, $fechahasta);
                                    $numero_de_registros = count($lista);
                                    $registros_x_pagina = 10;
                                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                                        echo "<script>location.href='Consultar_Cantidad_Proyectos.php?pagina=1';</script>";
                                    }
                                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                                    $lista_p = $proy->Filtrado_Estado_paginacion($codigo, $modalidad, $carrera, $director, $fechadesde, $fechahasta, $iniciar, $registros_x_pagina);
                                }*/
                                foreach ($lista_p as $lista) {
                                    ?>
                                    <tr>
                                        <th scope="col"><?php echo($lista["Id_Proyecto"]); ?> </th>
                                        <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                        <th scope="col"><?php echo($lista["Fecha_Registro"]); ?> </th>
                                        
                                        <?php if($lista["Estado"]=='Avalado'){ 
                                        ?>
                                            <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></td>
                                        <?php }else if($lista["Estado"] == 'Devuelto'){
                                        ?>
                                            <th class="bot"><a href="Informacion_anteproyectos_devueltos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></th>
                                        <?php }
                                        ?>
                                    </tr>
                                    <?php
                                }
                            }
                        } else if ($coordinacion != null) {
                            ?>
                            <thead class="thead-dark">
                                <tr  align="center" valign="middle">
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Título del anteproyecto</th>
                                    <th scope="col">Fecha de registro</th>
                                    <th scope="col">Ver</th>
                                </tr>
                            </thead>
                            <?php
                            if ($carrera == null || $carrera == (" ")) {
                                
                            } else {
                                $proy = new DAO_Anteproyecto();

                                //if ($modalidad == '00') {
                                    $lista = $proy->Filtrado_EstadoC($carrera, $director, $fechadesde, $fechahasta,$modalidad);
                                    
                                    $numero_de_registros = count($lista);
                                    $registros_x_pagina = 10;
                                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                                        echo "<script>location.href='Consultar_Cantidad_Proyectos.php?pagina=1';</script>";
                                    }
                                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                                    $lista_p = $proy->Filtrado_EstadoC_paginacion($carrera, $director, $fechadesde, $fechahasta, $iniciar, $registros_x_pagina,$modalidad);
                               /* } else {
                                    $lista = $proy->Filtrado_EstadoC($modalidad, $carrera, $director, $fechadesde, $fechahasta);

                                    $numero_de_registros = count($lista);
                                    $registros_x_pagina = 10;
                                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                                        echo "<script>location.href='Consultar_Cantidad_Proyectos.php?pagina=1';</script>";
                                    }
                                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                                    $lista_p = $proy->Filtrado_EstadoC_paginacion($modalidad, $carrera, $director, $fechadesde, $fechahasta, $iniciar, $registros_x_pagina);
                                }*/
                                foreach ($lista_p as $lista) {
                                    ?>
                                    <tr>
                                        <th scope="col"><?php echo($lista["Id_Proyecto"]); ?> </th>
                                        <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                        <th scope="col"><?php echo($lista["Fecha_Registro"]); ?> </th>
                                        <td class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>

                    </div>

                <?php }
                ?>
                </table>
            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="Consultar_Cantidad_Proyectos.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="Consultar_Cantidad_Proyectos.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="Consultar_Cantidad_Proyectos.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>

            <?php
        }else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
            ob_end_flush();
        }
        ?>


</body>
</html>
