<?php

require_once 'Conexion.php';

class subir extends Conexion {

    //put your code here
    private $con;

    public function subir() {
        $this->con = parent::__construct();
    }

    public function subido($titulo, $descripcion, $tam, $tipo, $nombre) {
        try {


            $id = "1";
            $consulta = "INSERT INTO subir values (:id,:titulo,:descripcion,:tamano,:tipo,:nombre);";
            $resultado = $this->con->prepare($consulta);
            $resultado->execute(array(":id" => $id, ":titulo" => $titulo, ":descripcion" => $descripcion, ":tamano" => $tam, ":tipo" => $tipo, ":nombre" => $nombre));
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function verPDF() {
        $sql = $this->con->query("select * from subir");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function verPDF2($id) {
        $sql = $this->con->query("select * from subir where id='$id'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

}
