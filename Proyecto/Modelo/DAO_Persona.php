<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Persona extends Conexion implements DAO_Contraseña {

    private $con;

    public function DAO_Persona() {

        $this->con = parent::__construct();
    }

    public function Insertar_Persona($nombre, $correo, $cedula, $telefono) {
        $id = "";
        $sql = $this->con->query("SELECT * FROM PERSONA WHERE Cedula='" . $cedula . "' OR Correo='" . $correo . "';");
        $consulta = "INSERT INTO PERSONA (Cedula,Correo,Nombre,Telefono) " . "VALUES (:cedula,:correo,:nombre,:telefono);";
        $verificar = $sql->fetch(PDO::FETCH_ASSOC);
        if ($verificar['Cedula'] == $cedula or $verificar['Correo'] == $correo) {
            $id = $verificar['Cedula'];
        } else {
            $resultado = $this->con->prepare($consulta);
            $resultado->execute(array(":cedula" => $cedula, ":correo" => $correo, ":nombre" => $nombre, ":telefono" => $telefono));
        }
        return $id;
    }

    public function verificar($User, $cedula) {
        $id = "";
        $query = $this->con->query("SELECT * 
                                    FROM PERSONA 
                                    WHERE Correo='" . $User . "' and Cedula='" . $cedula . "'  ;");
        $resultado = $query->fetch(PDO::FETCH_ASSOC);

        if ($resultado['correo'] == $User) {
            $id = $resultado['correo'];
            return $id;
        } else {
            return;
        }
    }

    public function editarPersona($correo_a, $correo_n, $tel) {
        try {

            $Query = "UPDATE PERSONA SET Correo=:correo_n, Telefono=:tel WHERE Correo=:correo_a";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":correo_a" => $correo_a, ":tel" => $tel, ":correo_n" => $correo_n));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $correo_a . "<br>" . $correo_n . "<br>" . $tel);
        }
        return $Resp;
    }

    public function eliminarPersona($correo) {
        try {

            $consulta = $this->con->query("SELECT * FROM PERSONA WHERE Correo='" . $correo . "'");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if ($verificarConsulta['Correo'] == $correo) {
                $Query = "DELETE FROM PERSONA WHERE Correo=:cor;";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":cor" => $correo));
                $Respuesta = TRUE;
            } else {
                $Respuesta = false;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }

    public function infoUser($correo) {
        $query = $this->con->query("SELECT * 
                                    FROM PERSONA
                                    WHERE Correo='" . $correo . "';");
        $list_Persona = $query->fetch(PDO::FETCH_ASSOC);
        return $list_Persona;
    }

    public function informacion_persona($cedula) {
        $query = $this->con->query("SELECT * 
                                    FROM PERSONA
                                    WHERE Cedula='" . $cedula . "';");
        $list_Persona = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Persona;
    }

    public function Verificar_Correo_Cedula($correo, $cedula) {
        try {

            $consulta = $this->con->query("SELECT Correo,Cedula FROM PERSONA WHERE lower(Correo) ='" . $correo . "' OR Cedula='" . $cedula . "'; ");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if (strcasecmp($correo, $verificarConsulta['Correo']) === 0  || $verificarConsulta['Cedula'] == $cedula) {
                $Respuesta = TRUE;
            } else {
                $Respuesta = FALSE;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }
    /*Tenemos dos funciones que hacen lo mismo mirar donde las usamos!!!*/
    public function Verificar_CorreoyCedula($correo, $cedula) {
            $consulta = $this->con->query("SELECT Correo,Cedula FROM PERSONA WHERE lower(Correo)='".$correo."' AND Cedula= '".$cedula."'; ");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);
           if ( $verificarConsulta['Cedula'] == $cedula) {
                $Respuesta = 1;
            } else {
                $Respuesta = 0;
            }
             return $Respuesta;
   
       
    }
    
     public function Existe_correo($correo) {
        try {

            $consulta = $this->con->query("SELECT Correo FROM PERSONA WHERE lower(Correo) ='" . $correo . "';");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if (strcasecmp($correo, $verificarConsulta['Correo']) === 0) {
                $Respuesta = TRUE;
            } else {
                $Respuesta = FALSE;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }


    public function reset_pass($correo, $pass) {
        try {

            $sql1 = $this->con->query("SELECT * FROM ESTUDIANTE WHERE lower(Correo) ='" . $correo . "'");
            $resultadoE = $sql1->fetch(PDO::FETCH_ASSOC);

            $sql2 = $this->con->query("SELECT * FROM PROFESOR WHERE lower(Correo) ='".$correo."'");
            $resultadoP = $sql2->fetch(PDO::FETCH_ASSOC);

            $sql3 = $this->con->query("SELECT * FROM COMITE WHERE lower(Correo) ='" . $correo . "'");
            $resultadoC = $sql3->fetch(PDO::FETCH_ASSOC);

            $sql4 = $this->con->query("SELECT * FROM COORDINACION WHERE lower(Correo) ='" . $correo . "'");
            $resultadoCO = $sql4->fetch(PDO::FETCH_ASSOC);

            if (strcasecmp($resultadoE['Correo'], $correo) === 0) {
                $Query = "UPDATE ESTUDIANTE SET Pass_E=:pass WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":correo" => $correo, ":pass" => $pass));
                $Resp = TRUE;
            } else if (strcasecmp($resultadoP['Correo'], $correo) === 0) {
                $Query = "UPDATE PROFESOR SET Pass_P=:pass WHERE Correo= :correo";
                $resultado = $this->con->prepare($Query);
                
                $resultado->execute(array(":correo" => $correo, ":pass" => $pass));
                $Resp = TRUE;
            } else if (strcasecmp($resultadoC['Correo'], $correo) === 0) {
                $Query = "UPDATE COMITE SET Pass=:pass WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":correo" => $correo, ":pass" => $pass));
                $Resp = TRUE;
            } else if (strcasecmp($resultadoCO['Correo'], $correo) === 0) {
                $Query = "UPDATE COORDINACION SET Pass_C=:pass WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":correo" => $correo, ":pass" => $pass));
                $Resp = TRUE;
            } else {
                $Resp = false;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine());
        }
        return $Resp;
    }

    public function cambiar_pass($correo, $paas_ant, $pass_nue) {}
    
    

}
