<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Administrador extends Conexion implements DAO_Contraseña{

    private $con;

    public function DAO_Administrador() {

        $this->con = parent::__construct();
    }

    public function SessionAdminin($usuario) {

        $sql = $this->con->query("SELECT * 
                                  FROM ADMINISTRADOR 
                                  WHERE  lower(Correo)='" . $usuario . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    public function cambiar_pass($correo, $contra_a, $contra_n) {
        try {

            $Sql = $this->con->query("SELECT * FROM ADMINISTRADOR WHERE Correo='" . $correo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ($ver['Correo'] == $correo) {
                if ((hash_equals($ver['Pass_A'], $contra_a)) || (password_verify($contra_a, $ver['Pass_A']))) {
                    $Query = "UPDATE ADMINISTRADOR SET Pass_A=:contra_n WHERE Correo=:correo";
                    $resultado = $this->con->prepare($Query);
                    $encriptado = password_hash($contra_n, PASSWORD_DEFAULT, ['cost' => 5]);
                    $resultado->execute(array(":correo" => $correo, ":contra_n" => $encriptado));
                    $Resp = TRUE;
                }
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }

    
    public function reset_pass($correo, $contraseña) {}

}



