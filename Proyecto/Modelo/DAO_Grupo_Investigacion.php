<?php

require_once 'Conexion.php';

class DAO_Grupo_Investigacion extends Conexion {

    private $con;

    public function DAO_Grupo_Investigacion() {
        $this->con = parent::__construct();
    }

    public function listaGrupo() {
        $sql = $this->con->query("SELECT * 
                                  FROM GRUPOINVESTIGACION;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    //FUNCION NUEVA 20/11/2019
    public function listaGrupohabilitados() {
        $sql = $this->con->query("SELECT * 
                                  FROM GRUPOINVESTIGACION WHERE Id_EstadoI!='05';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }

}
