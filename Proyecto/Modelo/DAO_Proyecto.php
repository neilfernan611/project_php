<?php

require_once 'Conexion.php';

class DAO_Proyecto extends Conexion {

    private $con;

    public function DAO_Proyecto() {
        $this->con = parent::__construct();
    }

   //REVISAR PARA SABER LA ELIMINAMOS
    /*
      public function crearUSuarioPro($id_rol, $correo, $pass_P, $cedula) {

      $id = "";

      $Sql = "INSERT INTO PROFESOR (Id_Rol,contrap,Correo,Cedula,Pass_P) "
      . "VALUES (:id,:correo,:cedula,:pass);";

      $SQLID = $this->con->query("SELECT * FROM profesor WHERE Cedula = '" . $cedula . "' AND Id_Rol='" . $id_rol . "';");
      $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

      if ($verificar['Cedula'] == $cedula and $verificar['Id_Rol'] == $id_rol) {
      return;
      } else {
      $resultado = $this->con->prepare($Sql);
      $resultado->execute(array(":id" => $id_rol, ":correo" => $correo, ":cedula" => $cedula
      , ":pass" => $pass_P));

      $SQLID = $this->con->query("SELECT * FROM profesor WHERE Cedula = '" . $cedula . "' AND Id_Rol='" . $id_rol . "';");
      $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
      $id = $verificar['Id_Rol'];
      }
      return $id;
      }
     */
     
     //Uso Carlos
    public function InformacionProyecto($id_proyecto) {

        $sql = $this->con->query("SELECT * 
                                  FROM PROYECTO 
                                  WHERE Id_Proyecto='" . $id_proyecto . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }
     // Uso Carlos
    public function verPDF() {
        $sql = $this->con->query("SELECT * FROM PROYECTO");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //FUNCION MODIFICADA (CAROL) 17/11/2019 // Uso Carlos
    public function verPDF2($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Nombre_Proyecto,TIPO_ESTADO_PROYECTO.Estado ,PERSONA.Nombre 
                                  FROM PROYECTO, PERSONA, PROFESOR_PROYECTO,ESTADO_PROYECTO, TIPO_ESTADO_PROYECTO,PROFESOR
                                  WHERE
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Comite AND 
                                  PERSONA.Cedula = PROFESOR.Cedula AND
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROYECTO.Id_Proyecto='" . $id . "' AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = ESTADO_PROYECTO.Id_Proyecto
                                  GROUP BY PROYECTO.Id_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Nombre_Proyecto,TIPO_ESTADO_PROYECTO.Estado ,PERSONA.Nombre ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //CONSULTA QUE AUN NO SE SI SIRVA PARA EL PROYECTO //Uso Carlos
    public function Consultar_Proyectos($id_proyecto) {
        $sql = $this->con->query("SELECT
    PROYECTO.Nombre_Proyecto, PROYECTO.Radicado, tutor1.Nombre AS n_tutor1, tutor1.Cedula AS c_tutor1, TIPO_ESTADO_SUSTENTACION.EstadoS AS estadot1, tutor2.Nombre AS n_tutor2, tutor2.Cedula AS c_tutor2,  TIPO_ESTADO_SUSTENTACION.EstadoS AS estadot2
FROM
    PROYECTO,
    ESTADO_SUSTENTACION,
    PROFESOR_PROYECTO AS t1,
    PROFESOR_PROYECTO AS t2,
    PERSONA AS tutor1,
    PERSONA AS tutor2,
    TIPO_ESTADO_SUSTENTACION
WHERE
	PROYECTO.Id_Proyecto = '".$id_proyecto."'
	AND PROYECTO.Id_Proyecto = t1.Id_Proyecto
    AND PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto
    AND t2.Id_Proyecto = t1.Id_Proyecto
    AND t1.Cedula != t2.Cedula
    AND t1.Id_Rol = '05'
    AND t2.Id_Rol = '05'
    AND tutor1.Cedula = t1.Cedula
    AND tutor2.Cedula = t2.Cedula
    AND t1.Estado = TIPO_ESTADO_SUSTENTACION.Id_EstadoS
    AND t2.Estado = TIPO_ESTADO_SUSTENTACION.Id_EstadoS
    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    /*copia
    public function Consultar_Proyectos($id_proyecto) {
        $sql = $this->con->query("SELECT PROYECTO.Nombre_Proyecto, PROYECTO.Radicado, tutor1.Nombre AS Nombre_Tutor1
        FROM PROYECTO, PROFESOR_PROYECTO, PROFESOR, PERSONA AS tutor1, ROL
        WHERE PROYECTO.Id_Proyecto = '" . $id_proyecto ."'
        AND PROFESOR_PROYECTO.Id_Proyecto = '" . $id_proyecto ."'
        AND PROFESOR_PROYECTO.Cedula = PROFESOR.Cedula
        AND PROFESOR.Id_Rol = ROL.Id_Rol
        AND PROFESOR.Cedula = tutor1.Cedula
        AND PROFESOR_PROYECTO.Id_Rol = ROL.Id_Rol LIMIT 1");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    */

    //FUNCION NUEVA 14/11/2019
    public function Consultar_Anteproyectos_Evaluador($cedula) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado FROM  TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO
            WHERE 
            TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
            PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND 
            ESTADO_PROYECTO.Estado_Evaluador != '02' AND 
            PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
            PROFESOR_PROYECTO.Id_Rol='03' AND 
            PROFESOR_PROYECTO.Cedula='" . $cedula . "';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
     public function Consultar_Anteproyectos_Evaluador_paginacion($cedula,$iniciar,$ariculosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado FROM  TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO
            WHERE 
            TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
            PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND 
            ESTADO_PROYECTO.Estado_Evaluador != '02' AND 
            PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
            PROFESOR_PROYECTO.Id_Rol='03' AND 
            PROFESOR_PROYECTO.Cedula='" . $cedula . "' LIMIT " . $iniciar . "," . $ariculosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }



    //FUNCION MODIFICADA 20/11/2019     
    public function Consultar_Anteproyectos_Coordinacion() {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Fecha_Registro ,COMITET.Estado AS Estado_Comite,COMITE.Fecha_Comite,  COORDINACIONT.Estado AS Estado_Coordinacion, COORDINACION.Fecha_Coordinacion, PROYECTO.Radicado
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, ESTADO_PROYECTO COORDINACION , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, TIPO_ESTADO_PROYECTO COORDINACIONT
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITE.Estado_Comite='02' AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COORDINACION.Fecha_Coordinacion ASC ;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

 public function Consultar_Anteproyectos_Coordinacion_paginacion($iniciar, $articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Fecha_Registro ,COMITET.Estado AS Estado_Comite,COMITE.Fecha_Comite,  COORDINACIONT.Estado AS Estado_Coordinacion, COORDINACION.Fecha_Coordinacion, PROYECTO.Radicado
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, ESTADO_PROYECTO COORDINACION , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, TIPO_ESTADO_PROYECTO COORDINACIONT
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITE.Estado_Comite='02' AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COORDINACION.Fecha_Coordinacion ASC LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //NUEVA
    public function Consultar_Estados() {
        $sql = $this->con->query("select * 
                                  FROM TIPO_ESTADO_PROYECTO 
                                  where Id_Estado!= 00 and Id_Estado!= 01");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //NUEVA (CAROL) //14/11/2019
    public function Actualizar_Estado_Evaluador($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Evaluador=:estado_evaluador, Fecha_Evaluador=:fecha_evaluador WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_evaluador" => $estado, ":fecha_evaluador" => $fecha));
    }

    //NUEVA
    public function Actualizar_Estado_Comite($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Comite=:estado_comite, Fecha_Comite=:fecha_comite WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_comite" => $estado, ":fecha_comite" => $fecha));
    }

    //NUEVA (CAROL) 14/11/2019
    public function Guardar_Comentario_Evaluador($id_comentario, $comentario, $fecha, $Id_proyecto, $cedula) {
        $Sql = "INSERT INTO COMENTARIO_EVALUADORPF(Id_Comentario,Comentario,Fecha,Id_Proyecto,Cedula)" . "VALUES (:id_c,:comentario,:fecha,:id_p,:cedula);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_c" => $id_comentario, ":comentario" => nl2br($comentario), ":fecha" => $fecha, ":id_p" => $Id_proyecto, "cedula" => $cedula));
    }

    //NUEVA (CAROL) 14/11/2019
    public function Ultimo_Comentario_Evaluador() {
        $sql = $this->con->query("select LAST_INSERT_ID(Id_Comentario) as last from COMENTARIO_EVALUADORPF order by Id_Comentario desc limit 0,1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Actualizar_Estado_Coordinacion($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Coordinacion=:estado_coordinacion, Fecha_Coordinacion=:fecha_corrdinacion WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_coordinacion" => $estado, ":fecha_corrdinacion" => $fecha));
    }

    public function Guardar_Comentario_Comite($id_comentario, $comentario, $fecha, $Id_proyecto) {
        $Sql = "INSERT INTO COMENTARIO_COMITE(Id_Comentario,Comentario,Fecha,Id_Proyecto)" . "VALUES (:id_c,:comentario,:fecha,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_c" => $id_comentario, ":comentario" => nl2br($comentario." "), ":fecha" => $fecha, ":id_p" => $Id_proyecto));
    }

    public function ultimo_comentario() {
        $sql = $this->con->query("select LAST_INSERT_ID(Id_Comentario) as last from COMENTARIO_COMITE order by Id_Comentario desc limit 0,1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function ultimo_radicado() {
        $sql = $this->con->query("select LAST_INSERT_ID(Radicado) as last from PROYECTO order by Radicado desc limit 0,1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //FUNCION REPETIDA POSIBLEMENTE A ELIMINAR
    public function Actualiar_Radicado($id_proyecto, $radicado) {
        $fecha = date('Y-m-d');
        $consulta = "UPDATE PROYECTO SET Radicado=:radicado, Fecha_Radicado=:fecha WHERE Id_Proyecto=:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":radicado" => $radicado, ":fecha" => $fecha));
    }

    //FUNCION MODIFICADA 2019/11/20 (CAROL) Esta es para coordinacion
    public function Consultar_Anteproyecto_Titulo($nombre_proyecto) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO COORDINACION,ESTADO_PROYECTO COMITEEST,ESTADO_PROYECTO COORDINACIONEST, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COORDINACION.Id_Estado = COORDINACIONEST.Estado_Coordinacion AND 
                                  COMITEEST.Estado_Comite='02' AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITEEST.Id_Proyecto AND 
	                          PROYECTO.Id_Proyecto  = COORDINACIONEST.Id_Proyecto AND 
                                  PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%'
                                  GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

public function Consultar_Anteproyecto_Titulo_paginacion($nombre_proyecto,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO COORDINACION,ESTADO_PROYECTO COMITEEST,ESTADO_PROYECTO COORDINACIONEST, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COORDINACION.Id_Estado = COORDINACIONEST.Estado_Coordinacion AND 
                                  COMITEEST.Estado_Comite='02' AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITEEST.Id_Proyecto AND 
	                          PROYECTO.Id_Proyecto  = COORDINACIONEST.Id_Proyecto AND 
                                  PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%'
                                  GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //FUNCION NUEVA 2019/11/20 (CAROL) Esta es para comite
    public function Consultar_Anteproyecto_Titulo_Comite($nombre_proyecto) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO EVALUADORT,TIPO_ESTADO_PROYECTO COMITET, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITE.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto  = EVALUADOR.Id_Proyecto AND
                                  EVALUADOR.Estado_Evaluador= '02'  AND	
                                  PROYECTO.Nombre_Proyecto LIKE '%".$nombre_proyecto."%' GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
  public function Consultar_Anteproyecto_Titulo_Comite_paginacion($nombre_proyecto,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO EVALUADORT,TIPO_ESTADO_PROYECTO COMITET, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITE.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto  = EVALUADOR.Id_Proyecto AND
                                  EVALUADOR.Estado_Evaluador= '02'  AND	
                                  PROYECTO.Nombre_Proyecto LIKE '%".$nombre_proyecto."%' GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function Estudiante_Agrega_AnteProyecto($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $Duracion, $objetivo_general, $objetivos, $resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE PROYECTO SET Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_ante,
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha, ":Id_Linea" => $linea,
                    ":Id_Modalidad" => $modalidad, ":Duracion_Estudiante" => $Duracion,
                    ":Resumen_Ejecutivo" => nl2br($resumen), ":Objetivo_General" => nl2br($objetivo_general),
                    ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function Duracion() {
        $sql = $this->con->query("SELECT * 
                                  FROM DURACION 
                                  where Id_Duracion!= '00'");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Agregar_Espacio_Academico($id_proyecto, $espacio_academico) {
        $Sql = "INSERT INTO INSCRIPCION_ESPACIO_ACADEMICO(Id_Espacio,Id_Proyecto)" . "VALUES (:id_e,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_e" => $espacio_academico, ":id_p" => $id_proyecto));
    }

    //FUNCION QUE APARTE DEL TUTOR AGREGA A EL EVALUADOR
    public function Agregar_tutor($docente, $rol, $proyecto) {
        $estado = "00";
        $fecha= date("y-m-d");
        $Sql = "INSERT INTO PROFESOR_PROYECTO(Cedula,Id_Rol,Id_Proyecto,Estado,Fecha)" . "VALUES (:cedula,:id_r,:id_p,:estado,:fecha);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":cedula" => $docente, ":id_r" => $rol, ":id_p" => $proyecto,":estado"=>$estado,":fecha"=>$fecha));
    }

    //FUNCION MODIFICADA 04/12/2019 (CAROL)
    public function Agregar_estudiante($id_proyecto, $estudiante2) {

            $consulta = "UPDATE ESTUDIANTE SET Id_proyecto=:Id_2 WHERE Codigo=:id;";
            $resultado = $this->con->prepare($consulta);
            $resultado->execute(array(":id" => $estudiante2, ":Id_2" => $id_proyecto));
            return $id_proyecto;
    }

    public function Estudiante_Agrega_Pasantia($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $duracion, $objetivo_general, $objetivos, $resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE PROYECTO SET Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_ante,
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha,
                    ":Id_Linea" => $linea,
                    ":Id_Modalidad" => $modalidad,
                    ":Duracion_Estudiante" => $duracion, ":Resumen_Ejecutivo" => nl2br($resumen),
                    ":Objetivo_General" => nl2br($objetivo_general), ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function Documentos_modalidades($id, $nombre, $id_proyecto) {
        $Sql = "INSERT INTO ARCHIVO_PROYECTO (Id_Archivo,Nombre_Archivo,Id_Proyecto)" . "VALUES (:id_a,:nombre,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_a" => $id, ":nombre" => $nombre, ":id_p" => $id_proyecto));
    }

    public function Estudiante_Agrega_Produccion_Academica($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $duracion, $objetivo_general, $objetivos, $resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE PROYECTO SET Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_ante,
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha,
                    ":Id_Linea" => $linea, ":Id_Modalidad" => $modalidad,
                    ":Duracion_Estudiante" => $duracion, ":Resumen_Ejecutivo" => nl2br($resumen),
                    ":Objetivo_General" => nl2br($objetivo_general),
                    ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function Estudiante_Agrega_Investigacion_Innovacion($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $duracion, $objetivo_general, $objetivos, $resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE PROYECTO SET Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_ante,
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha, ":Id_Linea" => $linea,
                    ":Id_Modalidad" => $modalidad, ":Duracion_Estudiante" => $duracion,
                    ":Resumen_Ejecutivo" => nl2br($resumen),
                    ":Objetivo_General" => nl2br($objetivo_general),
                    ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function Estado() {
        $sql = $this->con->query("SELECT * 
                                  FROM TIPO_ESTADO_PROYECTO 
                                  WHERE TIPO_ESTADO_PROYECTO.id_estado!='00';");
        $listaEstado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaEstado;
    }

    //FUNCION MODIFICADA (CAROL) 23/11/2019
    public function Filtrado_Estado($estado, $modalidad, $carrera, $director, $fechad, $fechah) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  EVALUADOR.Estado_Evaluador ='02'  AND 
                                  COMITE.Estado_Comite ='" . $estado . "' AND 
                                  PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
       public function Filtrado_Estado_paginacion($estado, $modalidad, $carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  EVALUADOR.Estado_Evaluador ='02'  AND 
                                  COMITE.Estado_Comite ='" . $estado . "' AND 
                                  PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }

    //FUNCION NUEVA (CAROL) 20/11/2019
    public function Filtrado_EstadoDOS($estado, $carrera, $director, $fechad, $fechah) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  EVALUADOR.Estado_Evaluador ='02'  AND 
                                  COMITE.Estado_Comite ='".$estado."' AND 
                                  ESTUDIANTE.Id_Carrera='".$carrera."' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='".$director."'AND
                                  PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }


 public function Filtrado_EstadoDOS_paginacion($estado, $carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  EVALUADOR.Estado_Evaluador ='02'  AND 
                                  COMITE.Estado_Comite ='".$estado."' AND 
                                  ESTUDIANTE.Id_Carrera='".$carrera."' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='".$director."'AND
                                  PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    //FUNCION NUEVA (CAROL) 23/11/2019
    public function Filtrado_EstadoC($modalidad, $carrera, $director, $fechad, $fechah) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    
        public function Filtrado_EstadoC_paginacion($modalidad, $carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    
    public function Filtrado_EstadoDOSC_paginacion($carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "'AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    //FUNCION NUEVA (CAROL) 23/11/2019
    public function Filtrado_EstadoDOSC($carrera, $director, $fechad, $fechah) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "'AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }

    //FUNCION QUE ES PARA SABER CUANTO PROYECTOS HA TENIDO O TIENE UM PROFESOR COMO DIRECTOR POR HACER CAROL TODAVIA NO TENER EN CUENTA LO DEL GROUP BY
    public function Filtrado_Profesor($profesor) {
        $sql = $this->con->query("select * 
                                  from PROYECTO,PROFESOR,ESTADO,PERSONA 
                                  where PERSONA.Cedula=PROFESOR.Cedula  and PROYECTO.tutor=PROFESOR.Cedula and ESTADO.Id_Estado=PROYECTO.Estado_Comite 
                                  and ESTADO.Id_Estado='01' and PROYECTO.tutor='" . $profesor . "' GROUP BY PROYECTO.Nombre_Proyecto");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }

    //FUNCION A MODICAR O ELMINAR(CAROL Y NEIL)
    public function ListarModificarProponentes() {
        $sql = $this->con->query("SELECT PROYECTO.id_proyecto, PROYECTO.nombre_proyecto,PERSONA.nombre AS nombre_profesor, es.nombre AS nombre_es, estu.nombre AS nombre_estu, PERSONA.cedula
                                  FROM PROYECTO,PERSONA, PERSONA ES, PERSONA ESTU, PROFESOR,ROL, ESTUDIANTE, ESTUDIANTE ESS 
                                  WHERE ESS.Correo != ESTUDIANTE.Correo AND PERSONA.Correo=PROFESOR.Correo AND PERSONA.Cedula=PROFESOR.Cedula AND ROL.Id_Rol = PROFESOR.Id_Rol AND 
                                  PROFESOR.Id_Rol='03' AND PROFESOR.Cedula=PROYECTO.Tutor AND ES.Correo= ESTUDIANTE.Correo AND ESTU.Correo=ESS.Correo AND 
                                  ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto AND ESS.Id_Proyecto = PROYECTO.Id_Proyecto group by PROYECTO.Id_Proyecto;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //FUNCION A MODICAR (CAROL Y NEIL)
    public function ActualizarProponentes($idproy, $es, $estu) {
        try {
            $Query = " UPDATE ESTUDIANTE SET Id_Proyecto=:idproy WHERE Codigo=:codigoes OR Codigo=:codigoestu ;";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":idproy" => $idproy, ":codigoes" => $es, ":codigoestu" => $estu));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $idproy . "<br>" . $es . "<br>" . $estu);
        }
        return $Resp;
    }

    //FUNCION POSIBLEMENTE A ELIMINAR(CAROL Y NEIL)
    public function ActualizarTutor($idproy, $tutor) {
        try {
            $Query = "UPDATE PROYECTO SET Tutor=:tutor WHERE Id_Proyecto=:idproy;";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":idproy" => $idproy, ":tutor" => $tutor));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $idproy . "<br>" . $tutor);
        }
        return $Resp;
    }

    //FUNCION MODIFICADA (CAROL)13/11/2019
    public function estados_proyecto($id) {
        $Sql = "INSERT INTO ESTADO_PROYECTO(Id_Proyecto,Estado_Evaluador,Estado_Comite,Estado_Coordinacion,Fecha_Evaluador,Fecha_Comite,Fecha_Coordinacion)" .
                "VALUES(:Id_Proyecto,:Estado_Evaluador,:Estado_Comite,:Estado_Coordinacion,:Fecha_Evaluador,:Fecha_Comite,:Fecha_Coordinacion)";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":Id_Proyecto" => $id, ":Estado_Evaluador" => '00', ":Estado_Comite" => '00', ":Estado_Coordinacion" => '00'
            , ":Fecha_Evaluador" => NULL, ":Fecha_Comite" => NULL, ":Fecha_Coordinacion" => NULL));
    }
    
    public function Ingresar_Datos_ES($Id_Proyecto,$codigo1)
    {
        try{
        $Sql = "INSERT INTO ESTADO_SUSTENTACION (Id_Proyecto, Codigo_E, Solicitud_Primer_Estudiante, Fecha_Primer_Estudiante, Solicitar_Sustentacion, Fecha_Solicitar_Sustentacion, Estado_Final,Id_Jurado, Fecha_Limite_Aprobacion_J, Estado_Jurado, Fecha_Estado_Jurado)
                VALUES(:Id_Proyecto, :Codigo_E, :Solicitud_Primer_Estudiante, :Fecha_Primer_Estudiante, :Solicitar_Sustentacion, :Fecha_Solicitar_Sustentacion, :Estado_Final,:Id_Jurado, :Fecha_Limite_Aprobacion_J, :Estado_Jurado, :Fecha_Estado_Jurado)";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":Id_Proyecto"=>$Id_Proyecto, ":Codigo_E"=>$codigo1, ":Solicitud_Primer_Estudiante"=>'00', ":Fecha_Primer_Estudiante"=>NULL, ":Solicitar_Sustentacion"=>'00', ":Fecha_Solicitar_Sustentacion"=>NULL, ":Estado_Final"=>'00',":Id_Jurado"=>'0', ":Fecha_Limite_Aprobacion_J"=>NULL, ":Estado_Jurado"=>'00', ":Fecha_Estado_Jurado"=>NULL));

        }catch(Exception $ex){
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $Id_Proyecto . "<br>" . $codigo1);
        }
        try{
        $Sql = "INSERT INTO ESTADO_SUSTENTACION (Id_Proyecto, Codigo_E, Solicitud_Primer_Estudiante, Fecha_Primer_Estudiante, Solicitar_Sustentacion, Fecha_Solicitar_Sustentacion, Estado_Final,Id_Jurado, Fecha_Limite_Aprobacion_J, Estado_Jurado, Fecha_Estado_Jurado)
                VALUES(:Id_Proyecto, :Codigo_E, :Solicitud_Primer_Estudiante, :Fecha_Primer_Estudiante, :Solicitar_Sustentacion, :Fecha_Solicitar_Sustentacion, :Estado_Final,:Id_Jurado, :Fecha_Limite_Aprobacion_J, :Estado_Jurado, :Fecha_Estado_Jurado)";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":Id_Proyecto"=>$Id_Proyecto, ":Codigo_E"=>$codigo1, ":Solicitud_Primer_Estudiante"=>'00', ":Fecha_Primer_Estudiante"=>NULL, ":Solicitar_Sustentacion"=>'00', ":Fecha_Solicitar_Sustentacion"=>NULL, ":Estado_Final"=>'00',":Id_Jurado"=>'1', ":Fecha_Limite_Aprobacion_J"=>NULL, ":Estado_Jurado"=>'00', ":Fecha_Estado_Jurado"=>NULL));

        }catch(Exception $ex){
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $Id_Proyecto . "<br>" . $codigo1);
        }
        
    }
    
    //Uso Carlos
   public function listarProyectos($id_proyecto,$codigo) {
        if($codigo != null)
        {
        $consulta = $this->con->query("SELECT
    PROYECTO.Nombre_Proyecto,
    PROYECTO.Radicado,
    PROYECTO.Fecha_Radicado,
    PROYECTO.Fecha_Registro,
    Estado_E1.EstadoS AS E1_Estado,
    Estado_Com.Estado AS Com_Estado,
    Estado_Eva.Estado AS Eva_Estado,
    Estado_Coo.Estado AS Coo_Estado,
    E_Estado_Id.Solicitud_Primer_Estudiante AS Id_EstadoE,
    ESTADO_SUSTENTACION.Solicitar_Sustentacion,
    ESTADO_SUSTENTACION.Fecha_Solicitar_Sustentacion,
    TIPO_ESTADO_SUSTENTACION.EstadoS AS Solicitar_S
FROM
    PROYECTO,
    ESTADO_SUSTENTACION AS E_Estado_Id,
    TIPO_ESTADO_SUSTENTACION AS Estado_E1,
    TIPO_ESTADO_PROYECTO AS Estado_Com,
    TIPO_ESTADO_PROYECTO AS Estado_Eva,
    TIPO_ESTADO_PROYECTO AS Estado_Coo,
    ESTADO_PROYECTO,
    ESTADO_SUSTENTACION,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROYECTO.Id_Proyecto = '".$id_proyecto."' 
    AND E_Estado_Id.Id_Proyecto = '".$id_proyecto."'
    AND ESTADO_PROYECTO.Id_Proyecto = '".$id_proyecto."'
    AND E_Estado_Id.Solicitud_Primer_Estudiante = Estado_E1.Id_EstadoS 
    AND ESTADO_PROYECTO.Estado_Comite = Estado_Com.Id_Estado 
    AND ESTADO_PROYECTO.Estado_Evaluador = Estado_Eva.Id_Estado 
    AND ESTADO_PROYECTO.Estado_Coordinacion = Estado_Coo.Id_Estado
    AND E_Estado_Id.Codigo_E = '".$codigo."'
    AND ESTADO_SUSTENTACION.Solicitar_Sustentacion = E_Estado_Id.Solicitar_Sustentacion
    AND ESTADO_SUSTENTACION.Solicitar_Sustentacion = TIPO_ESTADO_SUSTENTACION.Id_EstadoS
    LIMIT 1" );}
 else {
     $consulta = $this->con->query("SELECT
    PROYECTO.Nombre_Proyecto,
    PROYECTO.Radicado,
    PROYECTO.Fecha_Radicado,
    PROYECTO.Fecha_Registro,
    Estado_E1.EstadoS AS E1_Estado,
    Estado_Com.Estado AS Com_Estado,
    Estado_Eva.Estado AS Eva_Estado,
    Estado_Coo.Estado AS Coo_Estado,
    E_Estado_Id.Solicitud_Primer_Estudiante AS Id_EstadoE,
    ESTADO_SUSTENTACION.Solicitar_Sustentacion,
    ESTADO_SUSTENTACION.Fecha_Solicitar_Sustentacion,
    TIPO_ESTADO_SUSTENTACION.EstadoS AS Solicitar_S
FROM
    PROYECTO,
    ESTADO_SUSTENTACION AS E_Estado_Id,
    TIPO_ESTADO_SUSTENTACION AS Estado_E1,
    TIPO_ESTADO_PROYECTO AS Estado_Com,
    TIPO_ESTADO_PROYECTO AS Estado_Eva,
    TIPO_ESTADO_PROYECTO AS Estado_Coo,
    ESTADO_PROYECTO,
    ESTADO_SUSTENTACION,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROYECTO.Id_Proyecto = '".$id_proyecto."' 
    AND E_Estado_Id.Id_Proyecto = '".$id_proyecto."'
  	AND ESTADO_PROYECTO.Id_Proyecto = '".$id_proyecto."'
  	AND E_Estado_Id.Solicitud_Primer_Estudiante = Estado_E1.Id_EstadoS 
  	AND ESTADO_PROYECTO.Estado_Comite = Estado_Com.Id_Estado 
    AND ESTADO_PROYECTO.Estado_Evaluador = Estado_Eva.Id_Estado 
    AND ESTADO_PROYECTO.Estado_Coordinacion = Estado_Coo.Id_Estado
  	AND ESTADO_SUSTENTACION.Solicitar_Sustentacion = TIPO_ESTADO_SUSTENTACION.Id_EstadoS
    LIMIT 1" );
     
 }
        $listaca = $consulta->fetch(PDO::FETCH_ASSOC);
        return $listaca;
    }
    //Uso Carlos
    public function Actualizar_Estado_J($id_proyecto,$estado,$cedula)
    {
        
        $fecha = date("y-m-d");
        $consulta =$this->con->query("UPDATE ESTADO_SUSTENTACION SET Estado_Jurado = '".$estado."', Fecha_Estado_Jurado = '".$fecha."' WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Jurado = '".$cedula."'") ;
   }
   //Uso Carlos
   public function buscar_profesor($cedula)
   {
       $consulta = $this->con->query("SELECT * FROM PERSONA WHERE PERSONA.Cedula = '".$cedula."'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
   }
    //Uso Carlos
    public function consultar_jurados($id_proyecto)
    {
    $consulta = $this->con->query("SELECT
    jurado1.Id_Jurado AS jurado01,
    profesor1.Nombre AS profesor1,
    jurado1.Estado_Jurado estado_j1,
    j1.EstadoS AS estadoj1,
    jurado2.Id_Jurado jurado02,
    profesor2.Nombre AS profesor2,
    jurado2.Estado_Jurado AS estado_j2,
    j2.EstadoS AS estadoj2
FROM
    ESTADO_SUSTENTACION AS jurado1,
    ESTADO_SUSTENTACION AS jurado2,
    TIPO_ESTADO_SUSTENTACION AS j1,
    TIPO_ESTADO_SUSTENTACION AS j2,
    PERSONA AS profesor1,
    PERSONA AS profesor2
WHERE
    jurado1.Id_Proyecto = '".$id_proyecto."'
    AND jurado2.Id_Proyecto = '".$id_proyecto."'
    AND jurado1.Id_Jurado != jurado2.Id_Jurado 
    AND jurado1.Estado_Jurado = j1.Id_EstadoS 
    AND jurado2.Estado_Jurado = j2.Id_EstadoS 
    AND jurado1.Id_Jurado = profesor1.Cedula 
    AND jurado2.Id_Jurado = profesor2.Cedula
LIMIT 1");
         $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function verificar_2estudiantes($id_proyecto)
    {
        $sql = $this->con->query("SELECT
        COUNT(*) AS numero
        FROM
        ESTADO_SUSTENTACION AS estudiante1,
        ESTADO_SUSTENTACION AS estudiante2
        WHERE
        estudiante1.Codigo_E != estudiante2.Codigo_E AND estudiante1.Id_Proyecto = '".$id_proyecto."' AND estudiante2.Id_Proyecto = '".$id_proyecto."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function consultar_1Estudiante($id_proyecto)
    {
        $consulta = $this->con->query("SELECT
        ESTADO_SUSTENTACION.*, 
        PERSONA.*,  
        TIPO_ESTADO_SUSTENTACION.*
        FROM
        ESTADO_SUSTENTACION,
        TIPO_ESTADO_SUSTENTACION,
        ESTUDIANTE,
        PERSONA
        WHERE
        ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
        AND ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante = TIPO_ESTADO_SUSTENTACION.Id_EstadoS   
        AND ESTADO_SUSTENTACION.Codigo_E = ESTUDIANTE.Codigo 
        AND ESTUDIANTE.Correo = PERSONA.Correo
        LIMIT 1");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function consultar_estudiantes($id_proyecto)
    {
        $consulta = $this->con->query("SELECT
        estudiante1.Codigo_E AS codigo_e1,
        alumno1.Nombre nombre_e1,
        alumno1.Correo correo_e1,
        estudiante1.Solicitud_Primer_Estudiante estado_e1,
        estadoe1.EstadoS estados_e1,
        estudiante2.Codigo_E AS codigo_e2,
        alumno2.Nombre AS nombre_e2,
        alumno2.Correo AS correo_e2,
        estudiante2.Solicitud_Primer_Estudiante estado_e2,
        estadoe2.EstadoS estados_e2
        FROM
        ESTADO_SUSTENTACION AS estudiante1,
        ESTADO_SUSTENTACION AS estudiante2,
        TIPO_ESTADO_SUSTENTACION AS estadoe1,
        TIPO_ESTADO_SUSTENTACION AS estadoe2,
        ESTUDIANTE AS est1,
        ESTUDIANTE AS est2,
        PERSONA AS alumno1,
        PERSONA AS alumno2
        WHERE
        estudiante1.Id_Proyecto = '".$id_proyecto."'
        AND estudiante2.Id_Proyecto = '".$id_proyecto."'
        AND estudiante1.Codigo_E != estudiante2.Codigo_E
        AND estudiante1.Codigo_E = est1.Codigo
        AND estudiante2.Codigo_E = est2.Codigo
        AND est1.Correo = alumno1.Correo
        AND est2.Correo = alumno2.Correo
        AND estudiante1.Solicitud_Primer_Estudiante = estadoe1.Id_EstadoS
        AND estudiante2.Solicitud_Primer_Estudiante = estadoe2.Id_EstadoS
        LIMIT 1");
        $resultado = $consulta ->fetch(PDO::FETCH_ASSOC);
        return $resultado;
       
    }
    //Uso Carlos
    public function listarProyectosJ($id_proyecto,$cedula)
    {
        $consulta = $this->con->query("SELECT
    ESTADO_SUSTENTACION.Id_Proyecto,
    TIPO_ESTADO_SUSTENTACION.EstadoS,
    ESTADO_SUSTENTACION.Id_Jurado,
    ESTADO_SUSTENTACION.Fecha_Limite_Aprobacion_J,
    ESTADO_SUSTENTACION.Estado_Jurado
FROM
    ESTADO_SUSTENTACION,
    TIPO_ESTADO_SUSTENTACION
WHERE
    ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
    AND ESTADO_SUSTENTACION.Estado_Jurado = TIPO_ESTADO_SUSTENTACION.Id_EstadoS 
    AND ESTADO_SUSTENTACION.Id_Jurado = '".$cedula."'
    LIMIT 1");
    $resultado = $consulta ->fetch(PDO::FETCH_ASSOC);
    return $resultado;
   
}
//Uso Carlos
    public function Informacion_estudiantes_proyecto($id_proyecto) {

        $consulta = $this->con->query("SELECT 
        ESTUDIANTE.Correo,
        PERSONA.Nombre,
        PROYECTO.Id_Proyecto
        FROM 
        ESTUDIANTE,
        PERSONA,
        PROYECTO 
        WHERE
        PROYECTO.Id_Proyecto= '" . $id_proyecto . "'
        AND PROYECTO.Id_Proyecto = ESTUDIANTE.Id_Proyecto 
        AND PERSONA.Correo = ESTUDIANTE.Correo");
        $lista = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $lista;
    }
    //Uso Carlos
    public function Informacion_Jurados_Proyecto($id_proyecto)
    {
        $consulta = $this->con->query("SELECT
        ESTADO_SUSTENTACION.Id_Proyecto, ESTADO_SUSTENTACION.Codigo_E, PERSONA.Nombre, PERSONA.Correo, PERSONA.Cedula
        FROM
        ESTADO_SUSTENTACION,
        PROFESOR_PROYECTO,
        PROFESOR, 
        PERSONA
        WHERE
        ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
        AND ESTADO_SUSTENTACION.Id_Jurado = PROFESOR_PROYECTO.Cedula 
        AND PROFESOR_PROYECTO.Id_Rol = '01' 
        AND PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula 
        AND PROFESOR_PROYECTO.Id_Rol = PROFESOR.Id_Rol 
        AND PROFESOR.Cedula = PERSONA.Cedula
        ORDER BY ESTADO_SUSTENTACION.Codigo_E LIMIT 2");
        $lista = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $lista;
    }
    //Uso Carlos
      public function Informacion_Tutor_Proyecto($id_proyecto,$cedula)
    {
        $consulta = $this->con->query("SELECT
        *
        FROM
        PROFESOR_PROYECTO,
        PROFESOR,
        PERSONA
        WHERE
        PROFESOR_PROYECTO.Cedula = '".$cedula."' 
        AND PROFESOR_PROYECTO.Cedula = PROFESOR.Cedula 
        AND PROFESOR_PROYECTO.Id_Rol = '05' 
        AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."' 
        AND PROFESOR.Id_Rol = '05'
        AND PROFESOR.Cedula = PERSONA.Cedula");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION MODIFICADA 20/11/2019
    public function datosficha2($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, ESTUDIANTE.Codigo, ESTUDIANTEUNO.Nombre AS Estudiante_Uno, ESTUDIANTEUNO.Correo, ESTUDIANTEUNO.Telefono, CARRERA.Carrera, ESTUDIANTED.Codigo AS Codigo_Dos, ESTUDIANTEDOS.Nombre AS Estudiante_Dos , ESTUDIANTEDOS.Correo AS correo_dos,  ESTUDIANTEDOS.Telefono AS Telefono_Dos, CARRERADOS.Carrera, LINEAINVESTIGACION.Linea, MODALIDAD.Modalidad, GRUPOINVESTIGACION.Grupo, 
                                 PROFESORDOS.Nombre AS Evaluador, DURACION.Duracion, PROYECTO.Resumen_Ejecutivo,PROYECTO.Objetivo_General,PROYECTO.Objetivo_Especifico 
                                 FROM PROYECTO, PROFESOR_PROYECTO PRODOS, PERSONA PROFESORDOS,PERSONA ESTUDIANTEUNO, PERSONA ESTUDIANTEDOS, PROFESOR PROFEDOS, ROL EVALUADOR, ESTUDIANTE,
                                 ESTUDIANTE ESTUDIANTED, CARRERA, CARRERA CARRERADOS, DURACION, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION
                                 WHERE ESTUDIANTE.Correo != ESTUDIANTED.Correo AND DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND PROFESORDOS.Correo=PROFEDOS.Correo 
                                 AND PROFESORDOS.Cedula=PROFEDOS.Cedula AND EVALUADOR.id_rol = PROFEDOS.Id_Rol AND DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante 
                                 AND PROFEDOS.Cedula=PRODOS.Cedula AND EVALUADOR.Id_Rol=PRODOS.Id_Rol AND PROYECTO.Id_Proyecto=PRODOS.Id_Proyecto AND PROFEDOS.Id_Rol='03' 
                                 AND ESTUDIANTEUNO.Correo= ESTUDIANTE.Correo AND ESTUDIANTEDOS.Correo=ESTUDIANTED.Correo AND ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto 
                                 AND ESTUDIANTED.Id_Proyecto = PROYECTO.Id_Proyecto AND CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera AND CARRERADOS.Id_carrera=ESTUDIANTED.Id_Carrera AND 
                                 LINEAINVESTIGACION.Id_Linea=PROYECTO.Id_Linea AND MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad AND GRUPOINVESTIGACION.Id_Grupo=PROYECTO.Id_Grupo 
                                 AND PROYECTO.Id_Proyecto ='" . $id . "' LIMIT 1;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    //FUNCION MODIFICADA 20/11/2019
    public function datosficha($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,ESTUDIANTE.Codigo,ESTUDIANTEUNO.Nombre AS Estudiante_Uno,ESTUDIANTEUNO.Correo,ESTUDIANTEUNO.Telefono,
                                  CARRERA.Carrera,LINEAINVESTIGACION.Linea,MODALIDAD.Modalidad,GRUPOINVESTIGACION.Grupo,PERSONA.nombre AS Evaluador,DURACION.Duracion,PROYECTO.Resumen_Ejecutivo,PROYECTO.Objetivo_General,PROYECTO.Objetivo_Especifico                                  
                                  FROM PROYECTO,DURACION,PERSONA, PERSONA ESTUDIANTEUNO, PERSONA ESTUDIANTEDOS, PROFESOR,ROL,PROFESOR_PROYECTO, ESTUDIANTE, CARRERA, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION                                
                                  WHERE PERSONA.Correo=PROFESOR.Correo AND PERSONA.Cedula=PROFESOR.Cedula AND DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND ROL.Id_Rol = PROFESOR.Id_Rol AND PROFESOR.Cedula=PROFESOR_PROYECTO.Cedula AND PROFESOR.Id_Rol=PROFESOR_PROYECTO.Id_Rol AND PROYECTO.Id_Proyecto=PROFESOR_PROYECTO.Id_Proyecto AND PROFESOR.Id_Rol='03' AND ESTUDIANTEUNO.Correo= ESTUDIANTE.Correo AND ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto AND CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera AND LINEAINVESTIGACION.Id_Linea=PROYECTO.Id_Linea AND MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad AND GRUPOINVESTIGACION.Id_Grupo=PROYECTO.Id_Grupo AND PROYECTO.Id_Proyecto ='" . $id . "' LIMIT 1;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    //13/11/2019 (NEIL)
    public function Actualizar_Radicado($id_proyecto, $radicado) {
           $fecha = date("y-m-d");
        $consulta = "UPDATE PROYECTO SET Radicado=:radicado, Fecha_Radicado=:fecha WHERE Id_Proyecto=:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":radicado" => $radicado, ":fecha" => $fecha));
    }

    //14/11/2019 (NEIL)
    public function Actualizar_Duracion_Proyecto($id_proyecto) {
        $tiempo = "104";
        $consulta = "UPDATE PROYECTO SET Duracion_Coordinacion=:tiempo WHERE Id_Proyecto=:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":tiempo" => $tiempo));
    }

    //13/11//2019 (NEIL)
    public function Consultar_Estados_comite() {
        $sql = $this->con->query("select * from TIPO_ESTADO_PROYECTO where Id_Estado!= 00 and Id_Estado!= 01");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //13/11/2019 (NEIL)
    public function Consultar_Estados_coordinacion() {
        $sql = $this->con->query("select * from TIPO_ESTADO_PROYECTO where Id_Estado!= 00 and Id_Estado!= 02");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT  ROL.Rol,PERSONA.Nombre,TIPO_ESTADO_PROYECTO.Estado, ESTADO_PROYECTO.Fecha_Evaluador
                                  FROM ESTADO_PROYECTO, TIPO_ESTADO_PROYECTO,PROFESOR_PROYECTO,ROL,PROYECTO,PROFESOR, PERSONA
                                  WHERE
                                  PERSONA.Cedula=PROFESOR.Cedula AND
                                  PROFESOR_PROYECTO.Cedula=PROFESOR.Cedula AND
                                  PROFESOR.Id_Rol=ROL.Id_Rol AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Evaluador AND
                                  ROL.Id_Rol = PROFESOR_PROYECTO.Id_Rol AND
                                  ESTADO_PROYECTO.Id_Proyecto= PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Proyecto =  PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='03' AND
                                  ESTADO_PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Existencia_Comentario_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_EVALUADORPF WHERE Id_Proyecto='" . $id_proyecto . "';");
        $rows = $sql->rowCount();
        return $rows;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Comentario_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_EVALUADORPF WHERE Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT PERSONA.Nombre, TIPO_ESTADO_PROYECTO.Estado ,ESTADO_PROYECTO.Fecha_Comite
                                  FROM PERSONA,COMITE,TIPO_ESTADO_PROYECTO,ESTADO_PROYECTO,PROYECTO
                                  WHERE 
                                  PERSONA.Correo=COMITE.Correo AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Comite AND 
                                  ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Existencia_Comentario_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_COMITE WHERE Id_Proyecto='" . $id_proyecto . "';");
        $rows = $sql->rowCount();
        return $rows;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Comentario_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_COMITE WHERE Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_CoordinacionE($id_proyecto) {
        $sql = $this->con->query("SELECT PERSONA.Nombre, TIPO_ESTADO_PROYECTO.Estado, ESTADO_PROYECTO.Fecha_Coordinacion, PROYECTO.Fecha_Radicado, PROYECTO.Radicado
                                  FROM PERSONA,COORDINACION,TIPO_ESTADO_PROYECTO,ESTADO_PROYECTO,PROYECTO
                                  WHERE 
                                  PERSONA.Correo=COORDINACION.Correo AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Coordinacion AND 
                                  ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //FUNCION NUEVA 20/11/2019 CAROL
    public function cantidad_de_directores($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) 
                                       FROM PROFESOR_PROYECTO 
                                       WHERE Id_Rol='05' AND Id_Proyecto='" . $id_proyecto . "';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    //FUNCION NUEVA 20/11/2019 CAROL
    public function datosfichasUnDirector($id) {
        $sql = $this->con->query("SELECT PERSONA.Nombre 
                                  FROM PERSONA,PROFESOR, PROYECTO, PROFESOR_PROYECTO 
                                  WHERE PERSONA.Cedula=PROFESOR.Cedula AND PROFESOR.Cedula=PROFESOR_PROYECTO.CEDULA AND PROFESOR.Id_Rol = PROFESOR_PROYECTO.Id_Rol AND PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFESOR.Id_Rol='05' AND PROYECTO.Id_Proyecto='" . $id . "';");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    //FUNCION NUEVA 20/11/2019 CAROL
    public function datosfichasDosDirector($id) {
        $sql = $this->con->query("SELECT PROFESORUNO.Nombre AS Evaluador_Uno, PROFESORDOS.Nombre AS Evaluador_Dos 
                                  FROM PERSONA PROFESORUNO, PERSONA PROFESORDOS, PROFESOR PROFEUNO, PROFESOR PROFEDOS, PROYECTO, PROFESOR_PROYECTO PROUNO, PROFESOR_PROYECTO PRODOS  
                                  WHERE PROFESORUNO.Cedula != PROFESORDOS.Cedula AND PROFESORUNO.Cedula=PROFEUNO.Cedula AND PROFEUNO.Cedula=PROUNO.CEDULA AND PROFEUNO.Id_Rol = PROUNO.Id_Rol AND 
                                  PROUNO.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFEUNO.Id_Rol='05' AND PROFESORDOS.Cedula=PROFEDOS.Cedula AND PROFEDOS.Cedula=PRODOS.CEDULA AND 
                                  PROFEDOS.Id_Rol = PRODOS.Id_Rol AND PRODOS.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFEDOS.Id_Rol='05' AND PROYECTO.Id_Proyecto='" . $id . "' LIMIT 1 ;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    //NUEVA (CAROL) //20/11/2019
    public function Actualizar_Id_Anteproyecto_A_Radicado($id_proyecto, $radicado) {
        $consulta = "UPDATE PROYECTO SET Id_Proyecto=:radicado WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":radicado" => $radicado));
    }

    //NUEVA (CAROL) 22/11/2019
    public function verRadicadoProy($id) {
        $sql = $this->con->query("SELECT PROYECTO.Radicado, PROYECTO.Fecha_Radicado, DURACION.Duracion
                                  FROM PROYECTO, DURACION
                                  WHERE DURACION.Id_Duracion=PROYECTO.Duracion_Coordinacion AND PROYECTO.Id_Proyecto='" . $id . "';");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    /* //FUNCION POR TERMINAR(CARLOS Y YEISON)
      public function Ver_Fecha_Sustentacion($id_proyecto){
      $consulta= $this->con->query("select sustentacion.Fecha_Sustentacion,sustentacion.Hora,lugar.Lugar from sustentacion,proyecto,lugar where proyecto.id_proyecto=sustentacion.id_proyecto and sustentacion.Id_Lugar=lugar.Id_Lugar and proyecto.Id_Proyecto='".$id_proyecto."';");
      $lista=$consulta->fetch(PDO::FETCH_ASSOC);
      return $lista;
      }
     */

    //FUNCION A MODIFICAR(CARLOS Y YEISON)
    

    //Uso Carlos
    public function Lista_Proyectos_Sustentar() {

        $sql = $this->con->query(/*"SELECT
        proy1.Id_Proyecto, proy1.Codigo_E, proy1.Id_Jurado, proy1.Estado_Jurado, proy2.Codigo_E, proy2.Id_Jurado, proy2.Estado_Jurado, PROYECTO.Nombre_Proyecto
        FROM
        ESTADO_SUSTENTACION AS proy1,
        ESTADO_SUSTENTACION AS proy2,
        PROYECTO
        WHERE
	    proy1.Id_Proyecto = PROYECTO.Id_Proyecto
        AND proy1.Id_Proyecto = proy2.Id_Proyecto
        AND proy1.Codigo_E != proy2.Codigo_E
        AND proy1.Id_Jurado != proy2.Id_Jurado
        AND proy1.Estado_Jurado = '01'
        AND proy2.Estado_Jurado = '01'"*/"SELECT
    ESTADO_SUSTENTACION.Id_Proyecto,
    PROYECTO.Nombre_Proyecto
FROM
    ESTADO_SUSTENTACION,
    PROYECTO
WHERE
    Estado_Jurado = '01' AND PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    // Uso Carlos
    public function Consultar_Proyectos_Faltantes_Fecha($id_proyecto)
    {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) AS numero FROM SUSTENTACION WHERE Id_Proyecto = '".$id_proyecto."' ");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
       
    }
    //Uso Carlos
public function Solictar_S($estado, $id_proyecto) {
    try{
        $fecha= date("y-m-d");
        $consulta = $this->con->query("UPDATE
        ESTADO_SUSTENTACION
        SET
        Solicitar_Sustentacion = '".$estado."',
        Fecha_Solicitar_Sustentacion = '".$fecha."'
        WHERE
        ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."'");
    }
    catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
    }
    }
    
    //uso carlos
    public function Cantidad_proyectos_Faltante_FechaS()
    {
        $consulta = $this -> con->query("SELECT
    COUNT(*) cantidad
FROM
    ESTADO_SUSTENTACION
WHERE
    ESTADO_SUSTENTACION.Id_Proyecto NOT IN
    (SELECT Id_Proyecto FROM SUSTENTACION) 
    AND ESTADO_SUSTENTACION.Solicitar_Sustentacion = '01' 
	AND ESTADO_SUSTENTACION.Estado_Jurado = '01'");
	$resultado = $consulta->fetch(PDO::FETCH_ASSOC);
    return $resultado;
    }
    //Uso Carlos
public function Subir_Proyecto($id_proyecto,$nombre_archivo) {
         $fecha = date('Y-m-d'); 
        $consulta = $this->con->query("UPDATE
        PROYECTO
        SET
        PROYECTO.Nombre_Archivo = '".$nombre_archivo."',
        PROYECTO.Fecha_Archivo = '".$fecha."'
        WHERE
        PROYECTO.Id_Proyecto = '".$id_proyecto."'");
    }  
    //Uso Carlos
public function buscarP_P($id_proyecto) {
        $consulta = $this->con->query("SELECT * FROM PROFESOR_PROYECTO WHERE Id_Proyecto ='".$id_proyecto."'  AND Id_Rol = '01'");
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        if($resultado)
        {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    //Uso Carlos
    public function Consultar_Proyectos_Faltantes_Jurado()
    {
        $consulta = $this ->con->query("SELECT COUNT(ESTADO_SUSTENTACION.Id_Proyecto) AS numero FROM ESTADO_SUSTENTACION WHERE Solicitar_Sustentacion = '01' AND Id_Jurado = '0' OR Id_Jurado = '1' AND Solicitar_Sustentacion = '01'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function Listar_Proyectos_Faltantes_Jurado()
    {
        $consulta = $this ->con->query("SELECT
    PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto
FROM
    ESTADO_SUSTENTACION,
    PROYECTO
WHERE
    ESTADO_SUSTENTACION.Solicitar_Sustentacion = '01' 
    AND ESTADO_SUSTENTACION.Id_Jurado = '0' 
    AND PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto 
    OR ESTADO_SUSTENTACION.Id_Jurado = '1' 
    AND ESTADO_SUSTENTACION.Solicitar_Sustentacion = '01' 
    AND PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto");
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function asignarJurado($id_proyecto, $cedula1, $cedula2) {
        $rol="01";
        $estado = "00";
        $fecha= date("y-m-d");
        $consulta  = "INSERT INTO PROFESOR_PROYECTO(Cedula,Id_Rol,Id_Proyecto,Estado,Fecha)" . "VALUES (:cedula,:id_r,:id_p,:estado,:fecha);";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":cedula" => $cedula1, ":id_r" => $rol, ":id_p" => $id_proyecto,":estado"=>$estado,":fecha"=>$fecha));
            
        $consulta2  = "INSERT INTO PROFESOR_PROYECTO(Cedula,Id_Rol,Id_Proyecto,Estado,Fecha)" . "VALUES (:cedula,:id_r,:id_p,:estado,:fecha);";
        $resultado2 = $this->con->prepare($consulta2);
        $resultado2->execute(array(":cedula" => $cedula2, ":id_r" => $rol, ":id_p" => $id_proyecto,":estado"=>$estado,":fecha"=>$fecha));
        if($resultado != NULL && $resultado2 != NULL)
        Return TRUE;
      
    }

    //////////////////////////////////////


    public function Consultar_Anteproyectos() {
        $sql = $this->con->query("SELECT Proyecto.Id_Proyecto , Proyecto.Nombre_Proyecto,Proyecto.Nombre_Archivo,  ComiteT.Estado AS Estado_Comite
        FROM Proyecto, Estado_Proyecto Evaluador, Estado_Proyecto Comite , Tipo_Estado_Proyecto EvaluadorT, Tipo_Estado_Proyecto ComiteT
        WHERE
        Proyecto.Id_Proyecto = Evaluador.Id_Proyecto AND
        Proyecto.Id_Proyecto = Comite.Id_Proyecto AND
        EvaluadorT.Id_Estado = Evaluador.Estado_Evaluador AND
        ComiteT.Id_Estado = Comite.Estado_Comite AND
        ComiteT.Id_Estado = '00' AND
        Evaluador.Estado_Evaluador ='02';
        ");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    
    
    //Uso Carlos
    public function Consultar_Proyectos_T($cedula) {
       
        $consulta = $this->con->query("SELECT
    PROYECTO.Nombre_Proyecto, PROYECTO.Radicado, PERSONA.Nombre, TIPO_ESTADO_SUSTENTACION.EstadoS, PROYECTO.Id_Proyecto
FROM
    PROYECTO,
    PROFESOR_PROYECTO,
    PERSONA,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto 
    AND PROFESOR_PROYECTO.Id_Rol = '05'
    AND PROFESOR_PROYECTO.Cedula = '".$cedula."'
    AND PROYECTO.Id_Proyecto = PROYECTO.Radicado
    AND PERSONA.Cedula = PROFESOR_PROYECTO.Cedula
    AND TIPO_ESTADO_SUSTENTACION.Id_EstadoS = PROFESOR_PROYECTO.Estado");
    $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
    return $resultado;
    }
    
    //uso carlos
    public function Consultar_Proyectos_J($cedula)
    {
        $consulta = $this -> con ->query("SELECT
    PROYECTO.Nombre_Proyecto,
    PROYECTO.Radicado,
    ESTADO_SUSTENTACION.Id_Jurado,
    PROYECTO.Id_Proyecto,
    TIPO_ESTADO_SUSTENTACION.EstadoS
FROM
    PROYECTO,
    ESTADO_SUSTENTACION,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto 
    AND TIPO_ESTADO_SUSTENTACION.Id_EstadoS = ESTADO_SUSTENTACION.Estado_Jurado
    AND ESTADO_SUSTENTACION.Id_Jurado = '".$cedula."' ");
     $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
     return $resultado;
    }


    

    //Uso Carlos
    public function Actualizar_Estado_T($id_proyecto, $estado, $cedula) {
    $fecha = date("y-m-d");
    $consulta = $this->con->query("UPDATE
    PROFESOR_PROYECTO
SET
    Estado = '".$estado."',
    Fecha = '".$fecha."'
WHERE
    Id_Proyecto = '".$id_proyecto."' AND Id_Rol = '05' AND Cedula = '".$cedula."' ");
    
    
    //$resultado = $consulta->fetch(PDO::FETCH_ASSOC);
    }

    

    public function Consultar_ProyectoT($id_proyecto){
        $sql = $this->con->query("SELECT
    PERSONA.Nombre,
    PROFESOR_PROYECTO.Estado,
    PROYECTO.Nombre_Proyecto,
    PROYECTO.Radicado,
    TIPO_ESTADO_SUSTENTACION.EstadoS
FROM
    PROFESOR_PROYECTO,
    PERSONA,
    PROYECTO,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."'
    AND PROFESOR_PROYECTO.Id_Rol = '05' 
    AND PERSONA.Cedula = PROFESOR_PROYECTO.Cedula 
    AND PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto 
    AND PROFESOR_PROYECTO.Estado = TIPO_ESTADO_SUSTENTACION.Id_EstadoS");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    
    //Nuevo
       public function Estudiante_Agrega_Proyecto($id_proyecto,$radicado,$nombre_proyecto, $nombre_archivo, $fecha, $linea, $modalidad,$duracion,$objetivo_general,$objetivos,$resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM proyecto WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE proyecto SET Radicado=:Radicado,Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_proyecto, 
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha,
                    ":Id_Linea" => $linea,
                    ":Id_Modalidad" => $modalidad,
                    ":Radicado" => $radicado,
                    ":Duracion_Estudiante" => $duracion,
                    ":Resumen_Ejecutivo" => nl2br($resumen),
                    ":Objetivo_General" =>nl2br($objetivo_general),
                    ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }
    
    //Uso Carlos
    public function consultarEstudiante2($id_proyecto,$codigo1)
    {
        $consulta = $this->con->query("SELECT ESTUDIANTE.Codigo AS estudiante2, PERSONA.*
    FROM
    ESTUDIANTE,
    PROYECTO,
    PERSONA
    WHERE
    PROYECTO.Id_Proyecto = '".$id_proyecto."' AND ESTUDIANTE.Id_Proyecto = '".$id_proyecto."' AND ESTUDIANTE.Codigo != '".$codigo1."'  AND ESTUDIANTE.Correo = PERSONA.Correo");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function buscar_Estado_Sustentacion()
    {
        $consulta = $this->con->query("SELECT * FROM TIPO_ESTADO_SUSTENTACION");
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    //Uso Carlos
    Public function Buscar_Codigo_Estudiante($id_proyecto,$codigo)
    {
        
        $consulta = $this->con->query("SELECT
    ESTADO_SUSTENTACION.Codigo_E,
    PROYECTO.Nombre_Proyecto,
    ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante,
    TIPO_ESTADO_SUSTENTACION.EstadoS,
    PERSONA.Nombre
FROM
    ESTADO_SUSTENTACION,
    PROYECTO,
    TIPO_ESTADO_SUSTENTACION,
    ESTUDIANTE,
    PERSONA
WHERE
    ESTADO_SUSTENTACION.Codigo_E = '".$codigo."'  
    AND PROYECTO.Id_Proyecto = '".$id_proyecto."'
    AND TIPO_ESTADO_SUSTENTACION.Id_EstadoS = ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante
    AND ESTADO_SUSTENTACION.Codigo_E = ESTUDIANTE.Codigo
    AND ESTUDIANTE.Correo = PERSONA.Correo
    LIMIT 1");
       $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado; 
    }
    
    //Uso Carlos 
    public function Actualizar_Estado_Sustentacion_Id_Radicado($id_proyecto,$radicado){
        $consulta = " UPDATE ESTADO_SUSTENTACION SET Id_Proyecto =:radicado WHERE Id_Proyecto =:id_proyecto" ;
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":radicado" => $radicado, ":id_proyecto" => $id_proyecto));
    }
    //Uso Carlos
    public function consultar_coordinacion()
    {
        $consulta = $this->con->query("SELECT * FROM COORDINACION");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado; 
    }
    //Uso Carlos
    public function consultar_comite()
    {
        $consulta = $this->con->query("SELECT * FROM COMITE");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado; 
    }
    //Uso Carlos
    public function Actualizar_Estado_Sustentacion_E($id_proyecto,$estado, $codigo) {
     $fecha = date("y-m-d");
        try {
            $consulta="UPDATE
                        ESTADO_SUSTENTACION
                        SET
                        Solicitud_Primer_Estudiante = :esta, Fecha_Primer_Estudiante = :fecha
                        WHERE
                        Id_Proyecto = :id_proyecto AND ESTADO_SUSTENTACION.Codigo_E = '".$codigo."'";
            $resultado = $this->con->prepare($consulta);
            
            $resultado->execute(array(":id_proyecto" => $id_proyecto, ":esta" => $estado, ":fecha"=>$fecha));   
            
         } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " lineas" . $ex->getLine() . " ".$estado );
        }
    }
    
    //Carlos
    public function Actualizar_Estado_Sustentacion($fecha, $id_proyecto, $id_jurado1,  $id_jurado2) {
        $consulta = $this->con->query("UPDATE ESTADO_SUSTENTACION
                    SET Id_Jurado = '".$id_jurado1."', Fecha_Limite_Aprobacion_J = '".$fecha."' 
                    WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Jurado = '0'");
        
        $consulta2 = $this->con->query("UPDATE ESTADO_SUSTENTACION
                    SET Id_Jurado = '".$id_jurado2."', Fecha_Limite_Aprobacion_J = '".$fecha."' 
                    WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Jurado = '1'");
    }
    //Uso Carlos
    public function Consultar_Pcb()
    {
        $consulta = $this->con->query("SELECT * FROM EVALUACION_PCB");
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    //Uso Carlos
    public function Consultar_Jurado2($id_proyecto, $id_jurado)
    {
        $consulta = $this->con->query("SELECT * FROM    ESTADO_SUSTENTACION, PERSONA WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Jurado != '".$id_jurado."' AND PERSONA.Cedula = ESTADO_SUSTENTACION.Id_Jurado
        LIMIT 1");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    // Uso Carlos
    public function Consultar_Jurado1($id_proyecto, $id_jurado)
    {
        $consulta = $this->con->query("SELECT * FROM ESTADO_SUSTENTACION WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Jurado = '". $id_jurado."' LIMIT 1");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Uso Carlos
    public function Actualizar_Estado_Jurado($id_proyecto, $id_jurado, $estado)
    {
        try{
        $consulta = "UPDATE ESTADO_SUSTENTACION
                    SET Estado_Jurado = :estado
                    WHERE Id_Proyecto = :id_proyecto AND Id_Jurado = :id_jurado";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado" => $estado, ":id_jurado"=>$id_jurado));                
        }catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " lineas" . $ex->getLine());
        }
    }
    
    //Uso Carlos
    public function Actualizar_Pcb($id_proyecto,$estado) {
        $fecha = date("Y-m-d");
        $consulta = $this->con->query("UPDATE
        PROYECTO
        SET
        Evaluado_Pcb = '".$estado."'
        WHERE
        PROYECTO.Id_Proyecto = '".$id_proyecto."'");
    }
    
    public function Buscar_E($id_proyecto) {
        $consulta = $this->con->query("SELECT
    PROYECTO.Id_Proyecto,
    e1.Solicitud_Primer_Estudiante AS EstadoE1,
    e2.Solicitud_Primer_Estudiante AS EstadoE2,
    e1_codigo.Codigo AS codigoE1,
    e2_codigo.Codigo AS codigoE2,
    Nombre_E1.EstadoS AS Estadoe1,
    Nombre_E2.EstadoS AS Estadoe2
    
FROM
    PROYECTO, 
    ESTADO_SUSTENTACION AS e1,
    ESTADO_SUSTENTACION AS e2,
    ESTUDIANTE AS e1_codigo,
    ESTUDIANTE AS e2_codigo,
    TIPO_ESTADO_SUSTENTACION AS Nombre_E1,
    TIPO_ESTADO_SUSTENTACION AS Nombre_E2
WHERE
PROYECTO.Id_Proyecto = '".$id_proyecto."'
AND PROYECTO.Id_Proyecto = e1.Id_Proyecto
AND PROYECTO.Id_Proyecto = e2.Id_Proyecto
AND e1_codigo.Id_Proyecto = PROYECTO.Id_Proyecto 
AND e2_codigo.Id_Proyecto = PROYECTO.Id_Proyecto 
AND e1.Codigo_E != e2.Codigo_E
AND e1_codigo.Codigo = e1.Codigo_E
AND e2_codigo.Codigo = e2.Codigo_E
AND e1.Solicitud_Primer_Estudiante = Nombre_E1.Id_EstadoS
AND e2.Solicitud_Primer_Estudiante = Nombre_E2.Id_EstadoS
LIMIT 1");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

//FUNCION Neil 15/12/2019 
   
public function Anteproyectodevuelto($codigo,$id_proyecto,$cedula,$correo,$nombre,$telefono,$carrera){
    $Sql = "INSERT INTO ANTEPROYECTO_DEVUELTO_DATOS (Codigo,Id_Proyecto,Cedula,Correo,Nombre,Telefono,Carrera) VALUES (:codigo,:id_proyecto,:cedula,:correo,:nombre,:telefono,:carrera);";
    $resultado = $this->con->prepare($Sql);
    $resultado->execute(array(":codigo" => $codigo, ":id_proyecto" => $id_proyecto, ":cedula" => $cedula, ":correo" => $correo,":nombre"=>$nombre,":telefono"=>$telefono,":carrera"=>$carrera));
    }
//FUNCION Neil 15/12/2019 
   public function buscar_proyectos_para_cancelar($nombre_proyecto){
        $sql= $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, GROUP_CONCAT(PERSONA.Nombre) AS Nombres FROM PERSONA INNER JOIN ESTUDIANTE ON PERSONA.Correo=ESTUDIANTE.Correo INNER JOIN PROYECTO ON PROYECTO.Id_Proyecto = ESTUDIANTE.Id_Proyecto WHERE PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%' GROUP BY PROYECTO.Id_Proyecto");
        $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function validarjurado($id_proyecto, $cedula)
    {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) cantidad FROM PROFESOR_PROYECTO WHERE Id_Proyecto = '".$id_proyecto."' AND Cedula = '".$cedula."' AND Id_Rol = '05'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
        //return $consulta;
       
    }
    
    public function confirmarsus($id_proyecto)
    {
        $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM ESTADO_SUSTENTACION WHERE Id_Proyecto = '".$id_proyecto."'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        if($resultado["cantidad"]==2)
        {
            $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM ESTADO_SUSTENTACION WHERE Solicitar_Sustentacion = '01' AND Id_Proyecto ='".$id_proyecto."'");
            $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
            if($resultado["cantidad"]==2){
                return true;
            }else{
                return false;
            }
        }elseif($resultado["cantidad"]==4)
        {
            $consulta = $this->con->query("SELECT COUNT(*)
            FROM
            ESTADO_SUSTENTACION e1,
            ESTADO_SUSTENTACION e2
            WHERE
            e1.Id_Proyecto = '".$id_proyecto."' AND e2.Id_Proyecto = '".$id_proyecto."'
            AND e1.Codigo_E != e2.Codigo_E AND e1.Solicitar_Sustentacion = '01' 
            AND e2.Solicitar_Sustentacion = '01'");
            $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
            if($resultado["cantidad"]==4){
                return true;
            }else{
                return false;
        }}
    }
    
    public function confirmarsus2($id_proyecto)
    {
       $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM ESTADO_SUSTENTACION WHERE Id_Proyecto = '".$id_proyecto."'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        if($resultado["cantidad"]==2)
        {
            $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM PROFESOR_PROYECTO WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Rol = '05'");
            $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
            if($resultado["cantidad"]==1)
            {
                $consulta = $this->con->query("SELECT
                COUNT(*) cantidad
                FROM
                ESTADO_SUSTENTACION,
                PROFESOR_PROYECTO
                WHERE
                ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
                AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."'
                AND ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante = '01'
                AND PROFESOR_PROYECTO.Id_Rol = '05' 
                AND PROFESOR_PROYECTO.Estado = '01'");
                $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                if($resultado["cantidad"]==2)
                    {return 1;}else{return 2;}
            }elseif($resultado["cantidad"]==2) {
                $consulta = $this->con->query("SELECT
                COUNT(*) cantidad
                FROM
                ESTADO_SUSTENTACION,
                PROFESOR_PROYECTO
                WHERE
                ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
                AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."'
                AND ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante = '01'
                AND PROFESOR_PROYECTO.Id_Rol = '05' 
                AND PROFESOR_PROYECTO.Estado = '01'");
                $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                if($resultado["cantidad"]==4)
                    {return 1;}else{return 2;}
            }
        }elseif($resultado["cantidad"]==4)
        {   
            $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM PROFESOR_PROYECTO WHERE Id_Proyecto = '".$id_proyecto."' AND Id_Rol = '05'");
            $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
            if($resultado["cantidad"]==1)
            {
                $consulta = $this->con->query("SELECT
                COUNT(*) cantidad
                FROM
                ESTADO_SUSTENTACION,
                PROFESOR_PROYECTO
                WHERE
                ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
                AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."'
                AND ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante = '01'
                AND PROFESOR_PROYECTO.Id_Rol = '05' 
                AND PROFESOR_PROYECTO.Estado = '01'");
                $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                if($resultado["cantidad"]==4)
                    {return 1;}else{return 2;}
            }elseif($resultado["cantidad"]==2) {
                $consulta = $this->con->query("SELECT
                COUNT(*) cantidad
                FROM
                ESTADO_SUSTENTACION,
                PROFESOR_PROYECTO
                WHERE
                ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
                AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."'
                AND ESTADO_SUSTENTACION.Solicitud_Primer_Estudiante = '01'
                AND PROFESOR_PROYECTO.Id_Rol = '05' 
                AND PROFESOR_PROYECTO.Estado = '01'");
                $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                if($resultado["cantidad"]==8)
                    {return 1;}else{return 2;}
            }}
            return 0;
        
    }
    
    //Uso Carlos
    public function proyecto_aplazado ($id_proyecto, $estado)
    {
        $fecha = date("y-m-d");
        $consulta = "UPDATE ESTADO_SUSTENTACION e SET e.Solicitar_Sustentacion = :estado, e.Fecha_Solicitar_Sustentacion = :fecha WHERE e.Id_Proyecto = :id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado" => $estado, ":fecha" => $fecha));
    }
    public function consulta_concepto ($concepto)
    {
        $consulta = $this->con->query("SELECT * FROM TIPO_ESTADO_SUSTENTACION WHERE Id_EstadoS ='".$concepto."'");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function validar_proyecto($id_proyecto)
    {
        $consulta = $this->con->query("SELECT COUNT(*) cantidad FROM ESTADO_SUSTENTACION WHERE Id_Proyecto = '".$id_proyecto."' AND Solicitar_Sustentacion ='00' ");
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }


}
