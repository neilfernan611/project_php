<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Coordinacion extends Conexion implements DAO_Contraseña{

    private $con;

    public function DAO_Coordinacion() {

        $this->con = parent::__construct();
    }

    public function InformacionCoordinacion($Correo) {

        $sql = $this->con->query("SELECT * 
                                  FROM COORDINACION 
                                  WHERE lower(Correo)='" . $Correo . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function SessionCoordinacion($usuario) {

        $sql = $this->con->query("SELECT * 
                                  FROM COORDINACION 
                                  WHERE lower(Correo)='" . $usuario . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function crearUsuarioCoordinacion($codigo_c, $correo, $pass) {

        $id = "";

        $Sql = "INSERT INTO COORDINACION(Codigo,Correo,Pass_C) VALUES (:codigo,:correo,:pass);";

        $SQLID = $this->con->query("SELECT * FROM COORDINACION WHERE Codigo = '" . $codigo_c . "' OR Correo='" . $correo . "';");
        $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

        if ($verificar['Codigo'] == $codigo_c OR $verificar['Correo'] == $correo) {
            return;
        } else {
            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array(":codigo" => $codigo_c, ":correo" => $correo, ":pass" => $pass));

            $SQLID = $this->con->query("SELECT * FROM COORDINACION WHERE Codigo = '" . $codigo_c . "' AND Correo='" . $correo . "';");
            $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
            $id = $verificar['Codigo'];
        }
        return $id;
    }
    
    public function listarTablaCoordinacion($consulta) {
        $query = $this->con->query($consulta);
        $listaC = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaC;
    }

    public function correoCoordinacion($cod) {
        $sql = $this->con->query("SELECT COORDINACION.Correo 
                                  FROM COORDINACION 
                                  WHERE Codigo='" . $cod . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function Correo_Masivo_Estudiantes(){
       $sql= $this->con->query("SELECT PERSONA.Correo FROM PERSONA,ESTUDIANTE WHERE PERSONA.Correo=ESTUDIANTE.Correo;");
       $respuesta=$sql->fetchAll(PDO::FETCH_ASSOC);
       return $respuesta;
    }
    public function Correo_Masivo_Profesor(){
       $sql= $this->con->query("SELECT PERSONA.Correo FROM PERSONA,PROFESOR WHERE PERSONA.Cedula = PROFESOR.Cedula AND PROFESOR.Correo= PERSONA.Correo AND PERSONA.Correo != '1' AND PERSONA.Correo !='0' GROUP BY PERSONA.Correo;");
       $respuesta=$sql->fetchAll(PDO::FETCH_ASSOC);
       return $respuesta;
    }

    public function cambiar_pass($correo, $contra_a, $contra_n) {
        try {

            $Sql = $this->con->query("SELECT * FROM COORDINACION WHERE  Correo='" . $correo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ($ver['Correo'] == $correo && password_verify($contra_a, $ver['Pass_C'])) {
                $Query = "UPDATE COORDINACION SET Pass_C=:contra_n WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $encriptado = password_hash($contra_n, PASSWORD_DEFAULT, ['cost' => 5]);
                $resultado->execute(array(":correo" => $correo, ":contra_n" => $encriptado));
                $Resp = TRUE;
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }
    public function reset_pass($correo, $contraseña) {}

}


