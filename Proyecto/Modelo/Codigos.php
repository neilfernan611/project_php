<?php

class Codigos {

    public function getPINCOM($length) {
        $r = "";

        for ($i = 0; $i < $length; $i++) {
            $r .= mt_rand(1, 30);
        }
        return "COM" . $r;
    }

    public function getPINCRO($length) {
        $r = "";

        for ($i = 0; $i < $length; $i++) {
            $r .= mt_rand(1, 30);
        }
        return "CRO" . $r;
    }

    public function getPINCOOR($length) {
        $r = "";

        for ($i = 0; $i < $length; $i++) {
            $r .= mt_rand(1, 30);
        }
        return "COOR" . $r;
    }

    public function getPINSoli($length) {
        $r = "";

        for ($i = 0; $i < $length; $i++) {
            $r .= mt_rand(1, 30);
        }
        return "" . $r;
    }

}
