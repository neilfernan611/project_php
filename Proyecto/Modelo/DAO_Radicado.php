<?php

require_once 'Conexion.php';

class DAO_Radicado extends Conexion {

    private $con;

    function DAO_Radicado() {
        $this->con = parent::__construct();
    }

    //13/11/2019 (NEIL)
    public function Actualizar_Radicado($id_proyecto, $radicado) {
        setlocale(LC_ALL, "es_CO");
        date_default_timezone_set('America/Bogota');
        $fecha= date("Y-m-d G:i:s");
        $consulta = "UPDATE PROYECTO SET Radicado=:radicado, Fecha_Radicado=:fecha WHERE Id_Proyecto=:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":radicado" => $radicado, ":fecha" => $fecha));
       
    }

    //14/11/2019 (NEIL)
    public function Actualizar_Duracion_Proyecto($id_proyecto) {
        $tiempo = "104";
        $consulta = "UPDATE PROYECTO SET Duracion_Coordinacion=:tiempo WHERE Id_Proyecto=:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":tiempo" => $tiempo));
    }

    //NUEVA (CAROL) //20/11/2019
    public function Actualizar_Id_Anteproyecto_A_Radicado($id_proyecto, $radicado) {
        $consulta = "UPDATE PROYECTO SET Id_Proyecto=:radicado WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":radicado" => $radicado));
    }

    //NUEVA (CAROL) 22/11/2019 
    public function verRadicadoProy($id) {
        $sql = $this->con->query("SELECT PROYECTO.Radicado, PROYECTO.Fecha_Radicado, DURACION.Duracion
                                  FROM PROYECTO, DURACION
                                  WHERE DURACION.Id_Duracion=PROYECTO.Duracion_Coordinacion AND PROYECTO.Id_Proyecto='" . $id . "';");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    public function ultimo_radicado() {
        $sql = $this->con->query("SELECT PROYECTO.Radicado AS last FROM PROYECTO ORDER BY Fecha_Radicado DESC LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
     public function Reset_consecutivo_Radicado() {
        $consulta = "UPDATE PARAM_GENERAL SET Valor=:valor WHERE Codigo=:cod";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":valor" => 1, ":cod" => 1001));
    }

  public function Activar_consecutivo_Radicado() {
        $consulta = "UPDATE PARAM_GENERAL SET Valor=:valor WHERE Codigo=:cod";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":valor" => 0, ":cod" => 1001));
    }
     public function Consultar_consecutivo_Radicado() {
        $sql = $this->con->query("SELECT Valor FROM PARAM_GENERAL WHERE Codigo=1001");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

}
