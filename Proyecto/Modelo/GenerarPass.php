<?php

class GenerarPass {

    private $NUMEROS = "";
    private $MAYUSCULAS = "";
    private $MINUSCULAS = "";
    private $ESPECIALES = "";

    public function __construct() {
        $this->NUMEROS = "0123456789";
        $this->MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $this->MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
    }

    function randomPass($length) {
        $key = "";
        for ($i = 0; $i < $length; $i++) {

            $key .= $this->NUMEROS{rand(0, 9)};
            $key .= $this->MAYUSCULAS{rand(0, 25)};
            $key .= $this->MINUSCULAS{rand(0, 25)};
        }

        return $key;
    }

}
