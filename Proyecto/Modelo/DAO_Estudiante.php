<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Estudiante extends Conexion implements DAO_Contraseña{

    private $con;

    public function DAO_Estudiante() {

        $this->con = parent::__construct();
    }

    public function Verificacion_de_carrera($Codigo){
         $sql = $this->con->query("SELECT Id_Carrera 
                                   FROM ESTUDIANTE
                                   WHERE Codigo='" . $Codigo . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    public function InformacionEstudiante($Correo) {

        $sql = $this->con->query("SELECT * 
                                  FROM ESTUDIANTE
                                  WHERE lower(Correo)='" . $Correo . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function SessionEstudiante($usuario) {

        $sql = $this->con->query("SELECT * 
                                  FROM ESTUDIANTE
                                  WHERE lower(Correo)='" . $usuario . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function crearUSuarioEst($Codigo, $Id_Proyecto, $Id_Carrera, $Correo, $Pass_E) {

        $id = "";

        $Sql = "INSERT INTO ESTUDIANTE (Codigo,Id_Proyecto,Id_Carrera,Correo,Pass_E) "
                . "VALUES (:codigo,:id_proyecto,:id_carrera,:correo,:pass);";

        $SQLID = $this->con->query("SELECT * FROM ESTUDIANTE WHERE Codigo = '" . $Codigo . "';");
        $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

        if ($verificar['Codigo'] == $Codigo) {
            
        } else {

            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array(":codigo" => $Codigo, ":id_proyecto" => $Id_Proyecto, ":id_carrera" => $Id_Carrera
                , ":correo" => $Correo, ":pass" => $Pass_E));

            $SQLID = $this->con->query("SELECT * FROM ESTUDIANTE WHERE Codigo = '" . $Codigo . "';");
            $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
            $id = $verificar['Codigo'];
        }
        return $id;
    }

    public function Estudiante_Codigo($codigo) {
        try {

            $consulta = $this->con->query("SELECT Codigo FROM ESTUDIANTE WHERE Codigo='" . $codigo . "'");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if ($verificarConsulta['Codigo'] == $codigo) {
                $Respuesta = TRUE;
            } else {
                $Respuesta = FALSE;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }

    //  MODIFICADA CAROL 25/01/2020
    public function Ingresar_Proyecto($id_proyecto) {

        $fecha = date('Y-m-d');
        $id = "";

        $Sql = "INSERT INTO PROYECTO (Id_Proyecto,Nombre_Proyecto,Nombre_Archivo,Fecha_Registro,Fecha_Archivo,Id_Linea,Id_Modalidad,Id_Grupo,Id_Tipo,Duracion_Estudiante,Resumen_Ejecutivo,Objetivo_General,Objetivo_Especifico,Radicado,Fecha_Radicado,Duracion_Coordinacion,Evaluado_Pcb)"
                . "VALUES (:Id_Proyecto,:Nombre_Proyecto,:Nombre_Archivo,:Fecha_Registro,:Fecha_Archivo,:Id_Linea,:Id_Modalidad,:Id_Grupo,:Id_Tipo,:Duracion_Estudiante,:Resumen_Ejecutivo,:Objetivo_General,:Objetivo_Especifico,:Radicado,:Fecha_Radicado,:Duracion_Coordinacion,:Evaluado_Pcb);";

        $SQLID = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto = '" . $id_proyecto . "';");
        $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

        if ($verificar['Id_Proyecto'] == $id_proyecto) {
            
        } else {
            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array(":Id_Proyecto" => $id_proyecto, ":Nombre_Proyecto" => NULL, ":Nombre_Archivo" => NULL
                , ":Fecha_Registro" => $fecha, ":Fecha_Archivo" => NULL, ":Id_Linea" => NULL, ":Id_Modalidad" => NULL
                , ":Id_Grupo" => NULL, ":Id_Grupo" => NULL, ":Id_Tipo" => NULL
                , ":Duracion_Estudiante" => NULL, ":Resumen_Ejecutivo" => NULL, ":Objetivo_General" => NULL
                , ":Objetivo_Especifico" => NULL, ":Radicado" => NULL, ":Fecha_Radicado" => NULL
                , ":Duracion_Coordinacion" => NULL, ":Evaluado_Pcb" => NULL));

            $SQLID = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto = '" . $id_proyecto . "';");
            $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
            $id = $verificar['Id_Proyecto'];
        }
        return $id;
    }

    public function ListaCarrera() {
        $query = $this->con->query("SELECT * FROM CARRERA");
        $list_Carrera = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Carrera;
    }

    public function ListaTutor() {
        $query = $this->con->query("SELECT PERSONA.* 
                                    FROM PERSONA, PROFESOR,ROL 
                                    WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula != 2 AND PERSONA.Cedula != 0 AND ROL.Id_Rol=PROFESOR.Id_Rol AND ROL.rol='Director de proyecto' ");
        $list_Carrera = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Carrera;
    }

    public function ListaTutor2() {
        $query = $this->con->query("SELECT PERSONA.* 
                                    FROM PERSONA, PROFESOR,ROL 
                                    WHERE PERSONA.Cedula= PROFESOR.Cedula  AND ROL.Id_Rol=PROFESOR.Id_Rol AND ROL.rol='Director de proyecto' ");
        $list_Carrera = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Carrera;
    }

    public function ListaEstudiantes($consulta) {
        $query = $this->con->query($consulta);
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }

    public function ListarEstudiantes() {
        $query = $this->con->query("SELECT ESTUDIANTE.Codigo,PERSONA.Nombre,ESTUDIANTE.Correo,PERSONA.Telefono 
                                    FROM ESTUDIANTE,PERSONA 
                                    WHERE ESTUDIANTE.Correo=PERSONA.Correo;");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    //NO BORRAR CONSULTAS PARA PAGINACION
    public function ListarEstudiantes_paginacion($iniciar, $articulosx) {
        $query = $this->con->query("SELECT ESTUDIANTE.Codigo,PERSONA.Nombre,ESTUDIANTE.Correo,PERSONA.Telefono 
                                    FROM ESTUDIANTE,PERSONA 
                                    WHERE ESTUDIANTE.Correo=PERSONA.Correo LIMIT " . $iniciar . "," . $articulosx . ";");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    //FUNCION NUEVA(CAROL) 14/11/2019
    public function ListarEstudiantesCodigo($codigo) {
        $query = $this->con->query("SELECT ESTUDIANTE.Codigo,PERSONA.Nombre,ESTUDIANTE.Correo,PERSONA.Telefono 
                                    FROM ESTUDIANTE,PERSONA 
                                    WHERE ESTUDIANTE.Correo=PERSONA.Correo AND ESTUDIANTE.Codigo LIKE '%" . $codigo . "%';");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    //NO BORRAR CONSULTAS PARA PAGINACION
    public function ListarEstudiantesCodigo_paginaciono($codigo, $iniciar, $articulosx) {
        $query = $this->con->query("SELECT ESTUDIANTE.Codigo,PERSONA.Nombre,ESTUDIANTE.Correo,PERSONA.Telefono 
                                    FROM ESTUDIANTE,PERSONA 
                                    WHERE ESTUDIANTE.Correo=PERSONA.Correo AND ESTUDIANTE.Codigo LIKE '%" . $codigo . "%' LIMIT " . $iniciar . "," . $articulosx . ";");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    //FUNCION MODIFICADA (CAROL) 20/11/2019 para coordinacion
    public function BuscarEstudiante($codigo) {
        $query = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COORDINACIONT.Estado AS Estado_Coordinacion
                                    FROM PROYECTO, ESTADO_PROYECTO COORDINACION, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO COORDINACIONT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE,PERSONA
                                    WHERE PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACION.Estado_Comite ='02' AND ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto AND ESTUDIANTE.Correo=PERSONA.Correo AND ESTUDIANTE.Codigo LIKE  '%" . $codigo . "%'  AND PROYECTO.Id_Proyecto !='00' GROUP BY PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    //FUNCION NUEVA (CAROL) 20/11/2019 para comite
    public function BuscarEstudianteComite($codigo) {
        $query = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado AS Estado_Comite
                                    FROM  ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET,PROYECTO
                                    INNER JOIN ESTUDIANTE ON ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto  AND ESTUDIANTE.Codigo LIKE '%".$codigo."%'  
                                    WHERE 
                                    PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND 
                                    PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND 
                                    EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                    COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                    EVALUADOR.Estado_Evaluador ='02' 
                                    GROUP BY PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                    
                                    UNION ALL
                                    
                                    SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado AS Estado_Comite
                                    FROM  ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET,PROYECTO
                                    INNER JOIN ANTEPROYECTO_DEVUELTO_DATOS ON ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto= PROYECTO.Id_Proyecto  AND ANTEPROYECTO_DEVUELTO_DATOS.Codigo LIKE '%".$codigo."%'  
                                    WHERE 
                                    PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND 
                                    PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND 
                                    EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                    COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                    EVALUADOR.Estado_Evaluador ='02' 
                                    GROUP BY PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado");
        $list_Estudiantes = $query->fetchAll(PDO::FETCH_ASSOC);
        return $list_Estudiantes;
    }
    
    public function cantidad_de_proponentes($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) 
                                       FROM ESTUDIANTE 
                                       WHERE Id_Proyecto='" . $id_proyecto . "';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);

        return $lista;
    }

    public function Codigo_Carrera($id_proyecto) {
        $consulta = $this->con->query("SELECT Id_Carrera 
                                       FROM ESTUDIANTE  
                                       WHERE Id_Proyecto='" . $id_proyecto . "';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 14/11/2019 CAROL
    public function Correo_Estudiante($codigo) {
        $query = $this->con->query("SELECT ESTUDIANTE.Correo 
                                    FROM ESTUDIANTE
                                    WHERE  ESTUDIANTE.Codigo=" . $codigo . ";");
        $list_Profe = $query->fetch(PDO::FETCH_ASSOC);
        return $list_Profe;
    }
    //FUNCION NUEVA 4/12/2019 CAROL
    public function eliminarProyecto($codigo) {
        try {

            $Sql = $this->con->query("SELECT * FROM ESTUDIANTE WHERE Codigo='" . $codigo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ($ver['Codigo'] == $codigo) {

                $sq = $this->con->query("SELECT PROYECTO.* FROM PROYECTO,ESTUDIANTE WHERE PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND ESTUDIANTE.Codigo='" . $codigo . "';");
                $resu = $sq->fetch(PDO::FETCH_ASSOC);
                $id_a = $resu['Id_Proyecto'];

                $consulta = "UPDATE ESTUDIANTE SET Id_Proyecto=:id_proy WHERE Codigo =:cod";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id_proy" => NULL, ":cod" => $codigo));

                $Query = "DELETE FROM PROYECTO WHERE Id_Proyecto=:id_proy";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":id_proy" => $id_a));

                $Resp = TRUE;
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }
    //FUNCION NUEVA 4/12/2019 CAROL
    public function estudianteExiste($codigo) {
        try {
            $sql = $this->con->query("SELECT * 
                                      FROM ESTUDIANTE
                                      WHERE Codigo='" . $codigo . "';");
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
            if ($resultado['Codigo'] == $codigo) {
                $resp = TRUE;
            } else {
                $resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $resp;
    }
    //Neil 15/12/2019 Dejamos el id del proyecto en null para poder asignar un nuevo id de proyecto
    public function anteproyecto_devuelto_estudiante($codigo, $id_proyecto) {
        $consulta = "UPDATE ESTUDIANTE SET Id_Proyecto=:id_proy WHERE Codigo =:cod";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proy" => NULL, ":cod" => $codigo));

        $consulta = "UPDATE ESTUDIANTE SET Id_Proyecto=:id_proy WHERE Codigo =:cod";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proy" => $id_proyecto, ":cod" => $codigo));
    }

    public function cambiar_pass($correo, $contra_a, $contra_nu) {
        try {

            $Sql = $this->con->query("SELECT * FROM ESTUDIANTE WHERE Correo='" . $correo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ((hash_equals($ver['Correo'], $correo)) && (password_verify($contra_a, $ver['Pass_E']))) {
                $Query = "UPDATE ESTUDIANTE SET Pass_E=:contra_nu WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $encriptado = password_hash($contra_nu, PASSWORD_DEFAULT, ['cost' => 5]);
                $resultado->execute(array(":correo" => $correo, ":contra_nu" => $encriptado));
                $Resp = TRUE;
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }

    public function reset_pass($correo, $contraseña){}
    
    //FUNCION NUEVA 29/01/2020 CAROL
    public function proyectoRelacionado($codigo) {

            $sql = $this->con->query("SELECT PROYECTO.Nombre_Proyecto, PROYECTO.Id_Modalidad 
                                      FROM PROYECTO, ESTUDIANTE 
                                      WHERE 
                                      PROYECTO.Id_Proyecto = ESTUDIANTE.Id_Proyecto AND 
                                      ESTUDIANTE.Codigo = '".$codigo."';");
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
            return $resultado;
    }
    //FUNCION NUEVA 29/01/2020 CAROL
    public function proyectodevueltomodalidad($codigo) {
            $sql = $this->con->query("SELECT PROYECTO.Id_Modalidad 
                                      FROM PROYECTO, ANTEPROYECTO_DEVUELTO_EVALUADOR 
                                      WHERE PROYECTO.Id_Proyecto = ANTEPROYECTO_DEVUELTO_EVALUADOR.Id_Proyecto AND 
                                      ANTEPROYECTO_DEVUELTO_EVALUADOR.Codigo = '".$codigo."';");
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
            return $resultado;
    }
    //FUNCION NUEVA CAROL 25/01/2020
    public function cantidad_de_proponentes_devueltoC($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) 
                                       FROM  ANTEPROYECTO_DEVUELTO_DATOS
                                       WHERE Id_Proyecto='" . $id_proyecto . "';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 29/01/2020 CAROL
    public function proyectoDevuelto($codigo) {

            $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto 
                                      FROM PROYECTO, ANTEPROYECTO_DEVUELTO_EVALUADOR 
                                      WHERE PROYECTO.Id_Proyecto = ANTEPROYECTO_DEVUELTO_EVALUADOR.Id_Proyecto AND 
                                      ANTEPROYECTO_DEVUELTO_EVALUADOR.Codigo = '".$codigo."'");
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
            return $resultado;
    }

}
