<?php

interface DAO_Estado {
    
    public function Actualizar_Estado_Evaluador($id_proyecto, $estado, $fecha);
    public function Guardar_Comentario_Evaluador($id_comentario, $comentario, $fecha, $Id_proyecto, $cedula);
    public function Consultar_Estado_EvaluadorE($id_proyecto);
    public function Ultimo_Comentario_Evaluador();
    public function Consultar_Existencia_Comentario_EvaluadorE($id_proyecto);
    public function Consultar_Comentario_EvaluadorE($id_proyecto);

    public function Actualizar_Estado_Comite($id_proyecto, $estado, $fecha);
    public function Guardar_Comentario_Comite($id_comentario, $comentario, $fecha, $Id_proyecto);
    public function ultimo_comentario();
    public function Consultar_Estado_ComiteE($id_proyecto);
    public function Consultar_Existencia_Comentario_ComiteE($id_proyecto);
    public function Consultar_Comentario_ComiteE($id_proyecto);
     
    public function Actualizar_Estado_Coordinacion($id_proyecto, $estado, $fecha);
    public function Consultar_Estado_CoordinacionE($id_proyecto);
    public function estados_proyecto($id);
    
    public function Consultar_Estados_comite();
    public function Consultar_Estados_coordinacion();
    
}
