<?php

require_once 'Conexion.php';

class DAO_Modalidad extends Conexion {

    private $con;

    public function DAO_Modalidad() {
        $this->con = parent::__construct();
    }

    public function listarTablaModalidad($consulta) {
        $query = $this->con->query($consulta);
        $listaRol = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaRol;
    }

    public function listaModalidad() {
        $sql = $this->con->query("SELECT * 
                                  FROM MODALIDAD 
                                  WHERE Id_Modalidad != 00;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
     //FUNCION NUEVA 14/11/2019
    public function listaModalidadMoE() {
        $sql = $this->con->query("SELECT * 
                                  FROM MODALIDAD 
                                  WHERE Id_Modalidad != 00 AND Id_Modalidad='207' OR Id_Modalidad='202';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }



}


