<?php

require_once 'Conexion.php';

class DAO_Rol extends Conexion {

    private $con;

    public function DAO_Rol() {
        $this->con = parent::__construct();
    }

    public function listarTabla($consulta) {
        $query = $this->con->query($consulta);
        $listaRol = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaRol;
    }

    public function listaRol() {
        $sql = $this->con->query("SELECT * FROM ROL where Id_Rol!=00;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
//MIRAR DE AQUI PARA ABAJO SI ESTAMOS USANDO ESAS FUNCIONES O SI NO A ELIMINAR
    public function listarRolDirector() {
        $sql = $this->con->query("SELECT Id_Rol FROM ROL WHERE ROL='Director de proyecto';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function listarRolJurado() {
        $sql = $this->con->query("SELECT Id_Rol FROM ROL WHERE ROL='Jurado';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function listarRolEvaluador() {
        $sql = $this->con->query("SELECT Id_Rol FROM ROL WHERE ROL='Evaluador';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

}
