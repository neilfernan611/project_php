<?php

require_once 'Conexion.php';

class DAO_Notas extends Conexion {

    private $con;

    public function DAO_Notas() {

        $this->con = parent::__construct();
    }

    function Ingresar_notas_director($id,$id_proy,$id_director,$nota1, $nota2, $nota3, $nota4, $nota5, $nota6, $radio, $final) {
        TRY {
            $insercion = "INSERT INTO NOTA_DIRECTOR (Id_Nota,Id_Proyecto,Id_Director,Nota_1, Nota_2, Nota_3, Nota_4, Nota_5, Nota_6, Merito,Final) 
               VALUES (:id,:id_proy,:id_director,:nota1,:nota2,:nota3,:nota4,:nota5,:nota6,:radio,:final);";
            $resultado = $this->con->prepare($insercion);
            $resultado->execute(array(":id" => $id,":id_proy" => $id_proy,":id_director" => $id_director,":nota1" => $nota1, ":nota2" => $nota2, ":nota3" => $nota3, ":nota4" => $nota4, ":nota5" => $nota5, ":nota6" => $nota6, ":radio" => $radio, ":final" => $final));
        } catch (Exception $ex) {
            echo "linea de error" . $ex->getLine();
        }
    }

    function Ingresar_notas_jurado($id,$id_proy,$id_director, $nota1, $nota2, $nota3, $nota4, $nota5, $nota6, $nota7, $nota8, $nota9, $nota10, $nota11, $nota12, $radio,$final) {
        TRY {
            $insercion = "INSERT INTO NOTA_JURADO (Id_Nota, Id_Proyecto,Id_Director, Nota_1, Nota_2, Nota_3, Nota_4, Nota_5, Nota_6, Nota_7, Nota_8, Nota_9, Nota_10, Nota_11, Nota_12, Merito,Final) 
               VALUES (:id,:id_proy,:id_director,:nota1,:nota2,:nota3,:nota4,:nota5,:nota6,:nota7,:nota8,:nota9,:nota10,:nota11,:nota12,:radio,:final);";
            $resultado = $this->con->prepare($insercion);
            $resultado->execute(array(":id" => $id,":id_proy" => $id_proy,":id_director" => $id_director, ":nota1" => $nota1, ":nota2" => $nota2, ":nota3" => $nota3, ":nota4" => $nota4, ":nota5" => $nota5, ":nota6" => $nota6, ":nota7" => $nota7, ":nota8" => $nota8, ":nota9" => $nota9, ":nota10" => $nota10, ":nota11" => $nota11, ":nota12" => $nota12, ":radio" => $radio,":final" => $final));
        } catch (Exception $ex) {
            echo "linea de error" . $ex->getLine();
        }
    }

   public function generar_id_calificacion() {
       
    $sql = $this->con->query("SELECT count(*) as id from NOTA_DIRECTOR");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
              
        return $resultado;
    }
    public function generar_id_calificacion_jurado() {
       
    $sql = $this->con->query("SELECT count(*) as id from NOTA_JURADO");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
              
        return $resultado;
    }
    public function tabla_calificar_director($cedula){
        $sql = $this->con->query("SELECT
    PROYECTO.Id_Proyecto,
    PROYECTO.Nombre_Proyecto
FROM
    PROFESOR_PROYECTO,
    PROYECTO,
    ESTADO_SUSTENTACION,
    SUSTENTACION
WHERE
    PROFESOR_PROYECTO.Cedula ='".$cedula."' 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = SUSTENTACION.Id_Proyecto 
    AND
    PROYECTO.Id_Proyecto = PROYECTO.Radicado
    AND
    PROFESOR_PROYECTO.Id_Rol = 05 ");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
              
        return $resultado;
    }
    public function tabla_calificar_jurado($cedula){
        $sql = $this->con->query("SELECT
    PROYECTO.Id_Proyecto,
    PROYECTO.Nombre_Proyecto
FROM
    PROFESOR_PROYECTO,
    PROYECTO,
    ESTADO_SUSTENTACION,
    SUSTENTACION
WHERE
    PROFESOR_PROYECTO.Cedula ='".$cedula."' 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = ESTADO_SUSTENTACION.Id_Proyecto 
    AND
    PROFESOR_PROYECTO.Id_Proyecto = SUSTENTACION.Id_Proyecto 
    AND
    PROYECTO.Id_Proyecto = PROYECTO.Radicado
    AND
    PROFESOR_PROYECTO.Id_Rol = 01 ");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
              
        return $resultado;
    }
    public function verificar_calificacion($id_proyecto,$id_profesor){
         $sql = $this->con->query("SELECT COUNT(*) AS conteo 
         FROM NOTA_DIRECTOR 
         WHERE Id_Proyecto = '" . $id_proyecto . "' 
         AND Id_Director = '" . $id_profesor . "' ");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function verificar_calificacion_jurado($id_proyecto,$id_profesor){
         $sql = $this->con->query("SELECT COUNT(*) AS conteo 
         FROM NOTA_JURADO 
         WHERE Id_Proyecto = '" . $id_proyecto . "' 
         AND Id_Director = '" . $id_profesor . "' ");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function tabla_acta(){
        $sql = $this->con->query("SELECT DISTINCT PROYECTO.Nombre_Proyecto, PROFESOR_PROYECTO.Id_Proyecto 
                                    FROM PROYECTO,PROFESOR_PROYECTO 
                                    WHERE PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function verificar_calificado($id){
            $jurado = $this -> con ->query ("SELECT COUNT(*) AS conteo FROM NOTA_JURADO WHERE Id_Proyecto = '".$id."'");
            $countjurado = $jurado->fetch(PDO::FETCH_ASSOC);
            $director =$this -> con ->query ("SELECT COUNT(*) AS conteo FROM NOTA_DIRECTOR WHERE Id_Proyecto = '".$id."'");
            $countdirector = $director->fetch(PDO::FETCH_ASSOC);
            $calificado =$this -> con ->query ("SELECT COUNT(*) AS conteo FROM PROFESOR_PROYECTO WHERE Id_Proyecto = '".$id."' AND (Id_Rol = 01 OR Id_Rol = 05)");
            $countcalificado = $calificado->fetch(PDO::FETCH_ASSOC);
            $resultado =($countdirector["conteo"]+$countjurado["conteo"])-$countcalificado["conteo"];
           return $resultado;
        }
        
    public function get_titulo($id){
        $sql = $this->con->query("SELECT Nombre_Proyecto AS titulo FROM PROYECTO WHERE Id_Proyecto = '".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }    
        
    public function get_modalidad($id){
        $sql = $this->con->query("SELECT 
                                    MODALIDAD AS modalidad
                                    FROM
                                    MODALIDAD,PROYECTO
                                    WHERE
                                    PROYECTO.Id_Proyecto = '".$id."' AND PROYECTO.Id_Modalidad = MODALIDAD.Id_Modalidad");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }     
      
    public function get_proponentes($id){
        $sql = $this->con->query("SELECT COUNT(*) AS proponentes FROM ESTUDIANTE WHERE Id_Proyecto = '".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    } 
    
    public function get_directores($id){
        $sql = $this->con->query("SELECT COUNT(*) AS directores FROM NOTA_DIRECTOR WHERE Id_Proyecto = '".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }     
        
    public function get_jurados($id){
        $sql = $this->con->query("SELECT COUNT(*) AS jurados FROM NOTA_JURADO WHERE Id_Proyecto  = '".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }    
        
    public function get_estudiante1($id){
        $sql = $this->con->query("SELECT codigo AS codigo, nombre AS nombre, cedula AS cedula 
                                    FROM
                                    PERSONA,
                                    ESTUDIANTE
                                    WHERE
                                    PERSONA.Correo = ESTUDIANTE.Correo AND ESTUDIANTE.Id_Proyecto = '".$id."'
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }     
  
    public function get_estudiante2($id,$cedula){
        $sql = $this->con->query("SELECT codigo AS codigo, nombre AS nombre, cedula AS cedula 
                                    FROM
                                    PERSONA,
                                    ESTUDIANTE
                                    WHERE
                                    PERSONA.Correo = ESTUDIANTE.Correo AND ESTUDIANTE.Id_Proyecto = '".$id."' AND  PERSONA.Cedula != '".$cedula."'
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function get_director1($id){
        $sql = $this->con->query("SELECT
                                    PERSONA.nombre AS nombre,
                                    PERSONA.Cedula AS cedula
                                    FROM
                                    PERSONA,
                                    PROFESOR_PROYECTO
                                    WHERE
                                    PERSONA.Cedula = PROFESOR_PROYECTO.Cedula AND PROFESOR_PROYECTO.Id_Proyecto = '".$id."' AND PROFESOR_PROYECTO.Id_Rol = 05
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
     
    public function get_director2($id,$cedula){
        $sql = $this->con->query("SELECT
                                    PERSONA.nombre AS nombre,
                                    PERSONA.Cedula AS cedula
                                    FROM
                                    PERSONA,
                                    PROFESOR_PROYECTO
                                    WHERE
                                    PERSONA.Cedula = PROFESOR_PROYECTO.Cedula AND PROFESOR_PROYECTO.Id_Proyecto = '".$id."' AND PROFESOR_PROYECTO.Id_Rol = 05 AND PERSONA.Cedula != '".$cedula."'
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function get_notas_director($id,$cedula){
        $sql = $this->con->query("SELECT
                                    Nota_1 AS n1,
                                    Nota_2 AS n2,
                                    Nota_3 AS n3,
                                    Nota_4 AS n4,
                                    Nota_5 AS n5,
                                    Nota_6 AS n6,
                                    Final AS final
                                    FROM
                                    NOTA_DIRECTOR
                                    WHERE
                                    Id_Proyecto = '".$id."' AND Id_Director = '".$cedula."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function get_jurado1($id){
        $sql = $this->con->query("SELECT
                                    PERSONA.nombre AS nombre,
                                    PERSONA.Cedula AS cedula
                                    FROM
                                    PERSONA,
                                    PROFESOR_PROYECTO
                                    WHERE
                                    PROFESOR_PROYECTO.Cedula = PERSONA.Cedula AND PROFESOR_PROYECTO.Id_Proyecto = '".$id."' AND PROFESOR_PROYECTO.Id_Rol = 01
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
     
    public function get_jurado2($id,$cedula){
        $sql = $this->con->query("SELECT
                                    PERSONA.nombre AS nombre,
                                    PERSONA.Cedula AS cedula
                                    FROM
                                    PERSONA,
                                    PROFESOR_PROYECTO
                                    WHERE
                                    PROFESOR_PROYECTO.Cedula = PERSONA.Cedula AND PROFESOR_PROYECTO.Id_Proyecto = '".$id."' AND PROFESOR_PROYECTO.Id_Rol = 01 AND PERSONA.Cedula != '".$cedula."'
                                    LIMIT 1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function get_notas_jurado($id,$cedula){
        $sql = $this->con->query("SELECT
                                    Nota_1 AS n1,
                                    Nota_2 AS n2,
                                    Nota_3 AS n3,
                                    Nota_4 AS n4,
                                    Nota_5 AS n5,
                                    Nota_6 AS n6,
                                    Nota_7 AS n7,
                                    Nota_8 AS n8,
                                    Nota_9 AS n9,
                                    Nota_10 AS n10,
                                    Nota_11 AS n11,
                                    Nota_12 AS n12,
                                    Final AS final
                                    FROM
                                    NOTA_JURADO
                                    WHERE
                                    Id_Proyecto = '".$id."' AND Id_Director = '".$cedula."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function get_carrera($id){
        $sql = $this->con->query("SELECT Carrera 
                                FROM CARRERA,ESTUDIANTE 
                                where CARRERA.Id_Carrera = ESTUDIANTE.Id_Carrera 
                                AND ESTUDIANTE.Id_Proyecto = '".$id."' LIMIT 1 ");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    function Ingresar_finalizado_calificacion($id,$nota) {
        TRY {
            $insercion = "UPDATE SUSTENTACION SET Calificacion = :nota ,Finalizado = 'si' WHERE Id_Proyecto= :id";
            $resultado = $this->con->prepare($insercion);
            $resultado->execute(array(":nota" => $nota,":id" => $id));
        } catch (Exception $ex) {
            echo "linea de error" . $ex->getLine();
        }
    }
    
    }


