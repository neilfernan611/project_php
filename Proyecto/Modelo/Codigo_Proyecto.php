<?php

class Codigo_Proyecto {

    public function getPIN($length) {

        $r = "";

        for ($i = 0; $i < $length; $i++) {
            $r .= mt_rand(1, 30);
        }
        return "Proy" . $r;
    }

    function generate_numbers($start, $count, $digits) {
        $result = array();
        for ($n = $start; $n < $start + $count; $n++) {
            $result[] = str_pad($n, $digits, "0", STR_PAD_LEFT);
        }
        return $result;
    }

}
