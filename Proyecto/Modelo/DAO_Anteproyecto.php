<?php

require_once 'Conexion.php';
require_once 'DAO_Estado.php';

class DAO_Anteproyecto extends Conexion implements DAO_Estado{

    private $con;

    public function DAO_Anteproyecto() {
        $this->con = parent::__construct();
    }
    
    public function InformacionProyecto($id_proyecto) {

        $sql = $this->con->query("SELECT * 
                                  FROM PROYECTO 
                                  WHERE Id_Proyecto='" . $id_proyecto . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function verPDF() {
        $sql = $this->con->query("SELECT * FROM PROYECTO");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION MODIFICADA (CAROL) 17/11/2019
    public function verPDF2($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Nombre_Proyecto,TIPO_ESTADO_PROYECTO.Estado ,PERSONA.Nombre, PROYECTO.Fecha_Registro 
                                  FROM PROYECTO, PERSONA, PROFESOR_PROYECTO,ESTADO_PROYECTO, TIPO_ESTADO_PROYECTO,PROFESOR
                                  WHERE
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Evaluador AND 
                                  PERSONA.Cedula = PROFESOR.Cedula AND
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROYECTO.Id_Proyecto='" . $id . "' AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = ESTADO_PROYECTO.Id_Proyecto
                                  GROUP BY PROYECTO.Id_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Nombre_Proyecto,TIPO_ESTADO_PROYECTO.Estado ,PERSONA.Nombre ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA 14/11/2019
    public function Consultar_Anteproyectos_Evaluador($cedula) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado 
                                  FROM TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO 
                                  WHERE 
                                  TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
                                  PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND 
                                  ESTADO_PROYECTO.Estado_Evaluador != '02' AND 
                                  ESTADO_PROYECTO.Estado_Evaluador!='03' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Id_Rol='03' AND 
                                  PROFESOR_PROYECTO.Cedula='".$cedula."';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
     
    
     public function Consultar_Anteproyectos_Evaluador_paginacion($cedula,$iniciar,$ariculosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado 
                                  FROM  TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO
                                  WHERE 
                                  TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
                                  PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND 
                                  ESTADO_PROYECTO.Estado_Evaluador != '02' AND 
                                  ESTADO_PROYECTO.Estado_Evaluador!='03' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Id_Rol='03' AND 
                                  PROFESOR_PROYECTO.Cedula='" . $cedula . "' LIMIT " . $iniciar . "," . $ariculosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA (CAROL) 24/01/2020
    public function Consultar_Anteproyectos_Evaluador_Devuelto($cedula) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado 
                                  FROM TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO 
                                  WHERE 
                                  TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
                                  PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND  
                                  ESTADO_PROYECTO.Estado_Evaluador='03' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Id_Rol='03' AND 
                                  PROFESOR_PROYECTO.Cedula='".$cedula."';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
     public function Consultar_Anteproyectos_Evaluador_Devuelto_paginacion($cedula,$iniciar,$ariculosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro,TIPO_ESTADO_PROYECTO.Estado 
                                  FROM  TIPO_ESTADO_PROYECTO, ESTADO_PROYECTO,PROYECTO,PROFESOR_PROYECTO
                                  WHERE 
                                  TIPO_ESTADO_PROYECTO.Id_Estado=ESTADO_PROYECTO.Estado_Evaluador AND 
                                  PROYECTO.Id_Proyecto=ESTADO_PROYECTO.Id_Proyecto AND 
                                  ESTADO_PROYECTO.Estado_Evaluador='03' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Id_Rol='03' AND 
                                  PROFESOR_PROYECTO.Cedula='" . $cedula . "' LIMIT " . $iniciar . "," . $ariculosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION MODIFICADA (CAROL) 20/11/2019 
    public function Consultar_Anteproyectos_Comite() {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo,  COMITET.Estado AS Estado_Comite, COMITE.Fecha_Comite
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COMITE.Fecha_Comite ASC;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
      public function Consultar_Anteproyectos_Comite_paginacion($iniciar, $articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo,  COMITET.Estado AS Estado_Comite, COMITE.Fecha_Comite
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COMITE.Fecha_Comite ASC LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION MODIFICADA 20/11/2019     
    public function Consultar_Anteproyectos_Coordinacion() {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Fecha_Registro ,COMITET.Estado AS Estado_Comite,COMITE.Fecha_Comite,  COORDINACIONT.Estado AS Estado_Coordinacion, COORDINACION.Fecha_Coordinacion, PROYECTO.Radicado
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, ESTADO_PROYECTO COORDINACION , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, TIPO_ESTADO_PROYECTO COORDINACIONT
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITE.Estado_Comite='02' AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COORDINACION.Fecha_Coordinacion ASC ;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    
     public function Cantidad_Anteproyectos_Coordinacion() {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Fecha_Registro ,COMITET.Estado AS Estado_Comite,COMITE.Fecha_Comite,  COORDINACIONT.Estado AS Estado_Coordinacion, COORDINACION.Fecha_Coordinacion, PROYECTO.Radicado
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, ESTADO_PROYECTO COORDINACION , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, TIPO_ESTADO_PROYECTO COORDINACIONT
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITE.Estado_Comite='02' AND COORDINACION.Estado_Coordinacion='00'  AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COORDINACION.Fecha_Coordinacion ASC");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
       public function Cantidad_Anteproyectos_Comite() {
        $sql = $this->con->query("
SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo,  COMITET.Estado AS Estado_Comite, COMITE.Fecha_Comite
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND
COMITET.Id_Estado ='00' AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COMITE.Fecha_Comite ASC;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Consultar_Anteproyectos_Coordinacion_paginacion($iniciar, $articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Nombre_Archivo, PROYECTO.Fecha_Registro ,COMITET.Estado AS Estado_Comite,COMITE.Fecha_Comite,  COORDINACIONT.Estado AS Estado_Coordinacion, COORDINACION.Fecha_Coordinacion, PROYECTO.Radicado
                                  FROM PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, ESTADO_PROYECTO COORDINACION , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, TIPO_ESTADO_PROYECTO COORDINACIONT
                                  WHERE PROYECTO.Id_Proyecto = EVALUADOR.Id_Proyecto AND PROYECTO.Id_Proyecto = COMITE.Id_Proyecto AND PROYECTO.Id_Proyecto = COORDINACION.Id_Proyecto AND EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND COMITET.Id_Estado = COMITE.Estado_Comite AND COORDINACIONT.Id_Estado = COORDINACION.Estado_Coordinacion AND COMITE.Estado_Comite='02' AND EVALUADOR.Estado_Evaluador ='02' ORDER BY PROYECTO.Fecha_Registro DESC, COORDINACION.Fecha_Coordinacion ASC LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //NUEVA
    public function Consultar_Estados() {
        $sql = $this->con->query("select * 
                                  FROM TIPO_ESTADO_PROYECTO 
                                  where Id_Estado!= 00 and Id_Estado!= 01");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //NUEVA (CAROL) //14/11/2019
    public function Actualizar_Estado_Evaluador($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Evaluador=:estado_evaluador, Fecha_Evaluador=:fecha_evaluador WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_evaluador" => $estado, ":fecha_evaluador" => $fecha));
    }
    //NUEVA
    public function Actualizar_Estado_Comite($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Comite=:estado_comite, Fecha_Comite=:fecha_comite WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_comite" => $estado, ":fecha_comite" => $fecha));
    }
    //NUEVA (CAROL) 14/11/2019
    public function Guardar_Comentario_Evaluador($id_comentario, $comentario, $fecha, $Id_proyecto, $cedula) {
        $Sql = "INSERT INTO COMENTARIO_EVALUADORPF(Id_Comentario,Comentario,Fecha,Id_Proyecto,Cedula)" . "VALUES (:id_c,:comentario,:fecha,:id_p,:cedula);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_c" => $id_comentario, ":comentario" => nl2br($comentario), ":fecha" => $fecha, ":id_p" => $Id_proyecto, ":cedula" => $cedula));
    }
    //NUEVA (CAROL) 14/11/2019
    public function Ultimo_Comentario_Evaluador() {
        $sql = $this->con->query("SELECT LAST_INSERT_ID(Id_Comentario) AS LAST FROM COMENTARIO_EVALUADORPF ORDER BY Id_Comentario DESC LIMIT 0,1;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Actualizar_Estado_Coordinacion($id_proyecto, $estado, $fecha) {
        $consulta = "UPDATE ESTADO_PROYECTO SET Estado_Coordinacion=:estado_coordinacion, Fecha_Coordinacion=:fecha_corrdinacion WHERE Id_Proyecto =:id_proyecto";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":estado_coordinacion" => $estado, ":fecha_corrdinacion" => $fecha));
    }

    public function Guardar_Comentario_Comite($id_comentario, $comentario, $fecha, $Id_proyecto) {
        $Sql = "INSERT INTO COMENTARIO_COMITE(Id_Comentario,Comentario,Fecha,Id_Proyecto)" . "VALUES (:id_c,:comentario,:fecha,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_c" => $id_comentario, ":comentario" => nl2br($comentario." "), ":fecha" => $fecha, ":id_p" => $Id_proyecto));
    }

    public function ultimo_comentario() {
        $sql = $this->con->query("select LAST_INSERT_ID(Id_Comentario) as last from COMENTARIO_COMITE order by Id_Comentario desc limit 0,1");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION MODIFICADA 2019/11/20 (CAROL) Esta es para coordinacion
    public function Consultar_Anteproyecto_Titulo($nombre_proyecto) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO COORDINACION,ESTADO_PROYECTO COMITEEST,ESTADO_PROYECTO COORDINACIONEST, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COORDINACION.Id_Estado = COORDINACIONEST.Estado_Coordinacion AND 
                                  COMITEEST.Estado_Comite='02' AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITEEST.Id_Proyecto AND 
	                          PROYECTO.Id_Proyecto  = COORDINACIONEST.Id_Proyecto AND 
                                  PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%'
                                  GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Consultar_Anteproyecto_Titulo_paginacion($nombre_proyecto,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO COORDINACION,ESTADO_PROYECTO COMITEEST,ESTADO_PROYECTO COORDINACIONEST, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COORDINACION.Id_Estado = COORDINACIONEST.Estado_Coordinacion AND 
                                  COMITEEST.Estado_Comite='02' AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITEEST.Id_Proyecto AND 
	                              PROYECTO.Id_Proyecto  = COORDINACIONEST.Id_Proyecto AND 
                                  PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%'
                                  GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COORDINACION.Estado, PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA 2019/11/20 (CAROL) Esta es para comite
    public function Consultar_Anteproyecto_Titulo_Comite($nombre_proyecto) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO EVALUADORT,TIPO_ESTADO_PROYECTO COMITET, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITE.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto  = EVALUADOR.Id_Proyecto AND
                                  EVALUADOR.Estado_Evaluador= '02'  AND	
                                  PROYECTO.Nombre_Proyecto LIKE '%".$nombre_proyecto."%' GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function Consultar_Anteproyecto_Titulo_Comite_paginacion($nombre_proyecto,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro
                                  FROM PROYECTO, PERSONA, TIPO_ESTADO_PROYECTO EVALUADORT,TIPO_ESTADO_PROYECTO COMITET, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE, PROFESOR_PROYECTO,PROFESOR
                                  WHERE 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  PROFESOR.Cedula = PROFESOR_PROYECTO.Cedula AND 
                                  PROYECTO.Id_Proyecto = PROFESOR_PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='05' AND
                                  PROYECTO.Id_Proyecto  = COMITE.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto  = EVALUADOR.Id_Proyecto AND
                                  EVALUADOR.Estado_Evaluador= '02'  AND	
                                  PROYECTO.Nombre_Proyecto LIKE '%".$nombre_proyecto."%' GROUP BY  PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, PROYECTO.Nombre_Archivo, COMITET.Estado, PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //SUBIDA DE DATOS ANTEPROYECTOS
    public function Estudiante_Agrega_AnteProyecto($id_proyecto, $nombre_ante, $nombre_archivo, $fecha, $linea, $modalidad, $grupo, $Duracion, $objetivo_general, $objetivos, $resumen) {

        try {
            $sql = $this->con->query("SELECT * FROM PROYECTO WHERE Id_Proyecto='" . $id_proyecto . "'");
            $verificar = $sql->fetch();
            if ($verificar['Id_Proyecto'] == $id_proyecto) {
                $consulta = "UPDATE PROYECTO SET Nombre_Proyecto=:Nombre_Proyecto,Nombre_Archivo=:Nombre_Archivo,Fecha_Registro=:Fecha_Registro,Id_Linea=:Id_Linea,Id_Modalidad=:Id_Modalidad,Id_Grupo=:Id_Grupo,Duracion_Estudiante=:Duracion_Estudiante,Resumen_Ejecutivo=:Resumen_Ejecutivo,Objetivo_General=:Objetivo_General,Objetivo_Especifico=:Objetivo_Especifico WHERE Id_Proyecto =:id;";
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id" => $id_proyecto,
                    ":Nombre_Proyecto" => $nombre_ante,
                    ":Nombre_Archivo" => $nombre_archivo,
                    ":Fecha_Registro" => $fecha, 
                    ":Id_Linea" => $linea,
                    ":Id_Modalidad" => $modalidad,
                    ":Id_Grupo" => $grupo,
                    ":Duracion_Estudiante" => $Duracion,
                    ":Resumen_Ejecutivo" => nl2br($resumen), 
                    ":Objetivo_General" => nl2br($objetivo_general),
                    ":Objetivo_Especifico" => nl2br($objetivos)));
            } else {
                
            }
        } catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
        }
    }

    public function Duracion() {
        $sql = $this->con->query("SELECT * 
                                  FROM DURACION 
                                  where Id_Duracion!= '00'");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function Agregar_Espacio_Academico($id_proyecto, $espacio_academico) {
        $Sql = "INSERT INTO INSCRIPCION_ESPACIO_ACADEMICO(Id_Espacio,Id_Proyecto)" . "VALUES (:id_e,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_e" => $espacio_academico, ":id_p" => $id_proyecto));
    }
    //FUNCION QUE APARTE DEL TUTOR AGREGA A EL EVALUADOR
   public function Agregar_tutor($docente, $rol, $proyecto) {
        $estado = "00";
        $fecha= date("y-m-d");
        try{
        $Sql = "INSERT INTO PROFESOR_PROYECTO(Cedula,Id_Rol,Id_Proyecto,Estado,Fecha)" . "VALUES (:cedula,:id_r,:id_p,:estado,:fecha);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":cedula" => $docente, ":id_r" => $rol, ":id_p" => $proyecto,":estado"=>$estado,":fecha"=>$fecha));
        }catch (Exception $ex) {
            echo 'mensaje ' . $ex->getMessage() . "<br>";
            echo 'linea' . $ex->getLine();
            echo $proyecto . " - " . $rol . " - " . $docente."";
        }
   }
    //FUNCION MODIFICADA 04/12/2019 (CAROL)
    public function Agregar_estudiante($id_proyecto, $estudiante2) {

        $consulta = "UPDATE ESTUDIANTE SET Id_proyecto=:Id_2 WHERE Codigo=:id;";
        $resultado = $this->con->prepare($consulta);
        $resultado->execute(array(":id" => $estudiante2, ":Id_2" => $id_proyecto));
        return $id_proyecto;
    }

    public function Documentos_modalidades($id, $nombre, $id_proyecto) {
        $Sql = "INSERT INTO ARCHIVO_PROYECTO (Id_Archivo,Nombre_Archivo,Id_Proyecto)" . "VALUES (:id_a,:nombre,:id_p);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_a" => $id, ":nombre" => $nombre, ":id_p" => $id_proyecto));
    }

    public function Estado() {
        $sql = $this->con->query("SELECT * 
                                  FROM TIPO_ESTADO_PROYECTO 
                                  WHERE TIPO_ESTADO_PROYECTO.id_estado!='00';");
        $listaEstado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaEstado;
    }
    //FUNCION MODIFICADA (CAROL) 23/11/2019
/*    public function Filtrado_Estado($estado, $modalidad, $carrera, $director, $fechad, $fechah) {
        
        if($estado=='02'){
            $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                      FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                      WHERE 
                                      CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                      PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                      COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                      EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                      PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                      EVALUADOR.Estado_Evaluador ='02'  AND 
                                      COMITE.Estado_Comite ='" . $estado . "' AND 
                                      PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                      ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                      PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                      PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                      PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                      GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
            $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyecto;
        }else if($estado=='03'){
            $sqld = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                       FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                                       WHERE 
                                       CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='".$estado."' AND 
                                       PROYECTO.Id_Modalidad='".$modalidad."'  AND 
                                       ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='".$carrera."' AND 
                                       PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                       PROFESOR_PROYECTO.Cedula='".$director."' AND
                                       PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
            $listaProyectod = $sqld->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyectod;
        }
    }
    
    public function Filtrado_Estado_paginacion($estado, $modalidad, $carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        
        if($estado=='02'){
            $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                      FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                      WHERE 
                                      CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                      PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                      COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                      EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                      PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                      EVALUADOR.Estado_Evaluador ='02'  AND 
                                      COMITE.Estado_Comite ='" . $estado . "' AND 
                                      PROYECTO.Id_Modalidad='" . $modalidad . "'  AND 
                                      ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                      PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                      PROFESOR_PROYECTO.Cedula='" . $director . "' AND
                                      PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                      GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
            $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyecto;
        }else if($estado=='03'){
            $sqld = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                       FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                                       WHERE 
                                       CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='".$estado."' AND 
                                       PROYECTO.Id_Modalidad='".$modalidad."'  AND 
                                       ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='".$carrera."' AND 
                                       PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                       PROFESOR_PROYECTO.Cedula='".$director."' AND
                                       PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
            $listaProyectod = $sqld->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyectod;
        }
    }*/
    //FUNCION NUEVA (CAROL) 20/11/2019
    /*
  public function Filtrado_EstadoDOS($estado, $carrera, $director, $fechad, $fechah) {
        
        if($estado=='02'){
            $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                      FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                      WHERE 
                                      CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                      PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                      COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                      EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                      PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                      EVALUADOR.Estado_Evaluador ='02'  AND 
                                      COMITE.Estado_Comite ='".$estado."' AND 
                                      ESTUDIANTE.Id_Carrera='".$carrera."' AND 
                                      PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                      PROFESOR_PROYECTO.Cedula='".$director."'AND
                                      PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                      GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro");
            $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyecto;
        }else if($estado== '03'){
            $sqld = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                       FROM PROYECTO, PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                                       WHERE 
                                       CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='".$estado."' AND 
                                       ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='".$carrera."' AND 
                                       PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                       PROFESOR_PROYECTO.Cedula='".$director."' AND
                                       PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro");
            $listaProyectod = $sqld->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyectod;
        }
    }

    public function Filtrado_EstadoDOS_paginacion($estado, $carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        
        if($estado=='02'){
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  EVALUADOR.Estado_Evaluador ='02'  AND 
                                  COMITE.Estado_Comite ='".$estado."' AND 
                                  ESTUDIANTE.Id_Carrera='".$carrera."' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='".$director."'AND
                                  PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
        }else if($estado== '03'){
            $sqld = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado
                                       FROM PROYECTO, PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                                       WHERE 
                                       CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='".$estado."' AND 
                                       ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='".$carrera."' AND 
                                       PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                       PROFESOR_PROYECTO.Cedula='".$director."' AND
                                       PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechah."'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
            $listaProyectod = $sqld->fetchAll(PDO::FETCH_ASSOC);
            return $listaProyectod;
        }
    }
  */
      public function Filtrado_EstadoDOS($estado, $carrera, $director, $fechad, $fechah, $modalidad) {
          $sentencia="";
        $sentencia .= "SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado";
        if ($estado == '02') {
            $sentencia .= " FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , 
            TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                WHERE 
                                      CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                      PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                      COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                      EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                      PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                      EVALUADOR.Estado_Evaluador ='02'  AND 
                                      COMITE.Estado_Comite ='" . $estado . "' AND 
                                      ";
                                      if($modalidad=='00'){
                                          
                                      }else{
                                           $sentencia.="PROYECTO.Id_Modalidad='".$modalidad."'  AND"; 
                                      }
            if ($carrera == '00') {
               
            }else{
                 $sentencia .= " ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                             PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND ";
            }
        } else if($estado=='03'){
            $sentencia .= " FROM PROYECTO, PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , 
                    TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                    WHERE
                    CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='" . $estado . "' AND 
                                       ";
                if($modalidad=="00"){
                                          
                }else{
                $sentencia.=" PROYECTO.Id_Modalidad='".$modalidad."'  AND "; 
                }
            if ($carrera=='00') {
                
            }else{
              $sentencia .= " ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='" . $carrera . "' AND 
                            PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND";  
            }
        }
        if($director==''){
            
        }else{
             $sentencia .= " PROFESOR_PROYECTO.Cedula='" . $director . "' AND";
        }
        $sentencia .= " PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro";
        $sql = $this->con->query($sentencia);
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }

    public function Filtrado_EstadoDOS_paginacion($estado, $carrera, $director, $fechad, $fechah, $iniciar, $articulosx,$modalidad) {


            $sentencia="";
        $sentencia .= "SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro, COMITET.Estado";
        if ($estado == '02') {
            $sentencia .= " FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , 
            TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                WHERE 
                                      CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                      PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                      COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                      EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                      PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                      EVALUADOR.Estado_Evaluador ='02'  AND 
                                      COMITE.Estado_Comite ='" . $estado . "' AND ";
                                      
                                       if($modalidad=='00'){
                                          
                                      }else{
                                           $sentencia.=" PROYECTO.Id_Modalidad='".$modalidad."'  AND "; 
                                      }
            if ($carrera == '00') {
               
            }else{
                 $sentencia .= " ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                             PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND ";
            }
        } else if($estado=='03'){
            $sentencia .= " FROM PROYECTO, PROFESOR_PROYECTO, ESTADO_PROYECTO EVALUADOR, ESTADO_PROYECTO COMITE , 
                    TIPO_ESTADO_PROYECTO EVALUADORT, TIPO_ESTADO_PROYECTO COMITET, ANTEPROYECTO_DEVUELTO_DATOS, CARRERA
                    WHERE
                    CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera  AND 
                                       PROYECTO.Id_Proyecto=ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto AND 
                                       COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                       EVALUADORT.Id_Estado = EVALUADOR.Estado_Evaluador AND
                                       PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                       EVALUADOR.Estado_Evaluador ='02'  AND 
                                       COMITE.Estado_Comite ='" . $estado . "' AND ";
            if($modalidad=='00'){
                                          
             }else{
              $sentencia.=" PROYECTO.Id_Modalidad='".$modalidad."'  AND "; 
              }
            if ($carrera=='00') {
                
            }else{
              $sentencia .= " ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='" . $carrera . "' 
              AND PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND ";  
            }
        }
        if($director==''){
            
        }else{
             $sentencia .= " PROFESOR_PROYECTO.Cedula='" . $director . "' AND ";
        }
        $sentencia .= " PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                       GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";";
        $sql = $this->con->query($sentencia);
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;

                }

    //FUNCION NUEVA (CAROL) 23/11/2019
    public function Filtrado_EstadoC($carrera, $director, $fechad, $fechah,$modalidad) {
        $sentencia="";
        $sentencia.="SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'";
                if($modalidad=='00'){
                
                }else{
                    $sentencia.=" AND PROYECTO.Id_Modalidad='" . $modalidad . "' ";
                }
                if($carrera=="00"){
                    
                }else{
                    $sentencia.=" AND ESTUDIANTE.Id_Carrera='" . $carrera . "'";
                }
                $sentencia.=" AND PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto";
                if($director==""){
                    
                }else{
                    $sentencia.=" AND PROFESOR_PROYECTO.Cedula='" . $director . "'";
                }
                $sentencia.=" AND PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;";
       $sql = $this->con->query($sentencia);
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    
    public function Filtrado_EstadoC_paginacion($carrera, $director, $fechad, $fechah,$iniciar,$articulosx,$modalidad) {
        
          $sentencia="";
        $sentencia.="SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'";
                if($modalidad=='00'){
                
                }else{
                    $sentencia.=" AND PROYECTO.Id_Modalidad='" . $modalidad . "' ";
                }
                if($carrera=="00"){
                    
                }else{
                    $sentencia.=" AND ESTUDIANTE.Id_Carrera='" . $carrera . "'";
                }
                $sentencia.=" AND PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto";
                if($director==""){
                    
                }else{
                    $sentencia.=" AND PROFESOR_PROYECTO.Cedula='" . $director . "'";
                }
                $sentencia.=" AND PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";";
       $sql = $this->con->query($sentencia);
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
                
    }
    /*
    public function Filtrado_EstadoDOSC_paginacion($carrera, $director, $fechad, $fechah,$iniciar,$articulosx) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "'AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }
    //FUNCION NUEVA (CAROL) 23/11/2019
    public function Filtrado_EstadoDOSC($carrera, $director, $fechad, $fechah) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro
                                  FROM PROYECTO,PROFESOR_PROYECTO, ESTADO_PROYECTO COMITE, TIPO_ESTADO_PROYECTO COMITET, ESTUDIANTE, CARRERA
                                  WHERE 
                                  CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera  AND 
                                  PROYECTO.Id_Proyecto=ESTUDIANTE.Id_Proyecto AND 
                                  COMITET.Id_Estado = COMITE.Estado_Comite AND 
                                  PROYECTO.Id_Proyecto= COMITE.Id_Proyecto AND 
                                  COMITE.Estado_Comite ='02'  AND 
                                  ESTUDIANTE.Id_Carrera='" . $carrera . "' AND 
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROFESOR_PROYECTO.Cedula='" . $director . "'AND
                                  PROYECTO.Fecha_Registro BETWEEN '" . $fechad . "' AND '" . $fechah . "'
                                  GROUP BY PROYECTO.Id_Proyecto , PROYECTO.Nombre_Proyecto,PROYECTO.Fecha_Registro;");
        $listaProyecto = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listaProyecto;
    }*/
    //FUNCION MODIFICADA (CAROL)13/11/2019
    public function estados_proyecto($id) {
        $Sql = "INSERT INTO ESTADO_PROYECTO(Id_Proyecto,Estado_Evaluador,Estado_Comite,Estado_Coordinacion,Fecha_Evaluador,Fecha_Comite,Fecha_Coordinacion)" .
                "VALUES(:Id_Proyecto,:Estado_Evaluador,:Estado_Comite,:Estado_Coordinacion,:Fecha_Evaluador,:Fecha_Comite,:Fecha_Coordinacion)";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":Id_Proyecto" => $id, ":Estado_Evaluador" => '00', ":Estado_Comite" => '00', ":Estado_Coordinacion" => '00'
            , ":Fecha_Evaluador" => NULL, ":Fecha_Comite" => NULL, ":Fecha_Coordinacion" => NULL));
    }
    //FUNCION MODIFICADA 20/11/2019
    public function datosficha2($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, ESTUDIANTE.Codigo, ESTUDIANTEUNO.Nombre AS Estudiante_Uno, ESTUDIANTEUNO.Correo, ESTUDIANTEUNO.Telefono, CARRERA.Carrera, ESTUDIANTED.Codigo AS Codigo_Dos, ESTUDIANTEDOS.Nombre AS Estudiante_Dos , ESTUDIANTEDOS.Correo AS correo_dos,  ESTUDIANTEDOS.Telefono AS Telefono_Dos, CARRERADOS.Carrera, LINEAINVESTIGACION.Linea, MODALIDAD.Modalidad, GRUPOINVESTIGACION.Grupo, 
                                 PROFESORDOS.Nombre AS Evaluador, DURACION.Duracion, PROYECTO.Resumen_Ejecutivo,PROYECTO.Objetivo_General,PROYECTO.Objetivo_Especifico 
                                 FROM PROYECTO, PROFESOR_PROYECTO PRODOS, PERSONA PROFESORDOS,PERSONA ESTUDIANTEUNO, PERSONA ESTUDIANTEDOS, PROFESOR PROFEDOS, ROL EVALUADOR, ESTUDIANTE,
                                 ESTUDIANTE ESTUDIANTED, CARRERA, CARRERA CARRERADOS, DURACION, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION
                                 WHERE ESTUDIANTE.Correo != ESTUDIANTED.Correo AND DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND PROFESORDOS.Correo=PROFEDOS.Correo 
                                 AND PROFESORDOS.Cedula=PROFEDOS.Cedula AND EVALUADOR.id_rol = PROFEDOS.Id_Rol AND DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante 
                                 AND PROFEDOS.Cedula=PRODOS.Cedula AND EVALUADOR.Id_Rol=PRODOS.Id_Rol AND PROYECTO.Id_Proyecto=PRODOS.Id_Proyecto AND PROFEDOS.Id_Rol='03' 
                                 AND ESTUDIANTEUNO.Correo= ESTUDIANTE.Correo AND ESTUDIANTEDOS.Correo=ESTUDIANTED.Correo AND ESTUDIANTE.Id_Proyecto= PROYECTO.Id_Proyecto 
                                 AND ESTUDIANTED.Id_Proyecto = PROYECTO.Id_Proyecto AND CARRERA.Id_Carrera=ESTUDIANTE.Id_Carrera AND CARRERADOS.Id_carrera=ESTUDIANTED.Id_Carrera AND 
                                 LINEAINVESTIGACION.Id_Linea=PROYECTO.Id_Linea AND MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad AND GRUPOINVESTIGACION.Id_Grupo=PROYECTO.Id_Grupo 
                                 AND PROYECTO.Id_Proyecto ='" . $id . "' LIMIT 1;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION MODIFICADA 20/11/2019
    public function datosficha($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto,PROYECTO.Nombre_Proyecto, ESTUDIANTE.Codigo, ESTUDIANTEP.Nombre AS Estudiante_Uno, ESTUDIANTEP.Correo, ESTUDIANTEP.Telefono, CARRERA.Carrera, LINEAINVESTIGACION.Linea, MODALIDAD.Modalidad, GRUPOINVESTIGACION.Grupo, PERSONA.Nombre AS Evaluador, DURACION.Duracion,PROYECTO.Resumen_Ejecutivo,PROYECTO.Objetivo_General,PROYECTO.Objetivo_Especifico
                                  FROM PROYECTO, ESTUDIANTE, PERSONA ESTUDIANTEP, CARRERA, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION, PERSONA, PROFESOR, PROFESOR_PROYECTO, DURACION
                                  WHERE
                                  CARRERA.Id_Carrera = ESTUDIANTE.Id_Carrera AND
                                  ESTUDIANTEP.Correo = ESTUDIANTE.Correo AND
                                  ESTUDIANTE.Id_Proyecto=PROYECTO.Id_Proyecto AND
                                  PROYECTO.Id_Linea = LINEAINVESTIGACION.Id_Linea AND
                                  PROYECTO.Id_Modalidad = MODALIDAD.Id_Modalidad AND
                                  PROYECTO.Id_Grupo = GRUPOINVESTIGACION.Id_Grupo AND
                                  PERSONA.Cedula = PROFESOR.Cedula AND
                                  PERSONA.Correo = PROFESOR.Correo AND
                                  PROFESOR_PROYECTO.Cedula = PROFESOR.Cedula AND
                                  PROFESOR_PROYECTO.Id_Rol = PROFESOR.Id_Rol AND
                                  PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='03' AND
                                  DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND
                                  PROYECTO.Id_Proyecto='".$id."';");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //13/11//2019 (NEIL)
    public function Consultar_Estados_comite() {
        $sql = $this->con->query("select * from TIPO_ESTADO_PROYECTO where Id_Estado!= 00 and Id_Estado!= 01");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //13/11/2019 (NEIL)
    public function Consultar_Estados_coordinacion() {
        $sql = $this->con->query("select * from TIPO_ESTADO_PROYECTO where Id_Estado!= 00 and Id_Estado!= 02");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT  ROL.Rol,PERSONA.Nombre,TIPO_ESTADO_PROYECTO.Estado, ESTADO_PROYECTO.Fecha_Evaluador
                                  FROM ESTADO_PROYECTO, TIPO_ESTADO_PROYECTO,PROFESOR_PROYECTO,ROL,PROYECTO,PROFESOR, PERSONA
                                  WHERE
                                  PERSONA.Cedula=PROFESOR.Cedula AND
                                  PROFESOR_PROYECTO.Cedula=PROFESOR.Cedula AND
                                  PROFESOR.Id_Rol=ROL.Id_Rol AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Evaluador AND
                                  ROL.Id_Rol = PROFESOR_PROYECTO.Id_Rol AND
                                  ESTADO_PROYECTO.Id_Proyecto= PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Proyecto =  PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='03' AND
                                  ESTADO_PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Existencia_Comentario_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_EVALUADORPF WHERE Id_Proyecto='" . $id_proyecto . "';");
        $rows = $sql->rowCount();
        return $rows;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Comentario_EvaluadorE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_EVALUADORPF WHERE Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT PERSONA.Nombre, TIPO_ESTADO_PROYECTO.Estado ,ESTADO_PROYECTO.Fecha_Comite
                                  FROM PERSONA,COMITE,TIPO_ESTADO_PROYECTO,ESTADO_PROYECTO,PROYECTO
                                  WHERE 
                                  PERSONA.Correo=COMITE.Correo AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Comite AND 
                                  ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Existencia_Comentario_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_COMITE WHERE Id_Proyecto='" . $id_proyecto . "';");
        $rows = $sql->rowCount();
        return $rows;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Comentario_ComiteE($id_proyecto) {
        $sql = $this->con->query("SELECT * FROM COMENTARIO_COMITE WHERE Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //17/11/2019 (CAROL) FUNCION NUEVA
    public function Consultar_Estado_CoordinacionE($id_proyecto) {
        $sql = $this->con->query("SELECT PERSONA.Nombre, TIPO_ESTADO_PROYECTO.Estado, ESTADO_PROYECTO.Fecha_Coordinacion, PROYECTO.Fecha_Radicado, PROYECTO.Radicado
                                  FROM PERSONA,COORDINACION,TIPO_ESTADO_PROYECTO,ESTADO_PROYECTO,PROYECTO
                                  WHERE 
                                  PERSONA.Correo=COORDINACION.Correo AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Coordinacion AND 
                                  ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  PROYECTO.Id_Proyecto='" . $id_proyecto . "';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA 20/11/2019 CAROL
    public function cantidad_de_directores($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) 
                                       FROM PROFESOR_PROYECTO 
                                       WHERE Id_Rol='05' AND Id_Proyecto='" . $id_proyecto . "';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 20/11/2019 CAROL
    public function datosfichasUnDirector($id) {
        $sql = $this->con->query("SELECT PERSONA.Nombre 
                                  FROM PERSONA,PROFESOR, PROYECTO, PROFESOR_PROYECTO 
                                  WHERE PERSONA.Cedula=PROFESOR.Cedula AND PROFESOR.Cedula=PROFESOR_PROYECTO.CEDULA AND PROFESOR.Id_Rol = PROFESOR_PROYECTO.Id_Rol AND PROFESOR_PROYECTO.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFESOR.Id_Rol='05' AND PROYECTO.Id_Proyecto='" . $id . "';");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 20/11/2019 CAROL
    public function datosfichasDosDirector($id) {
        $sql = $this->con->query("SELECT PROFESORUNO.Nombre AS Evaluador_Uno, PROFESORDOS.Nombre AS Evaluador_Dos 
                                  FROM PERSONA PROFESORUNO, PERSONA PROFESORDOS, PROFESOR PROFEUNO, PROFESOR PROFEDOS, PROYECTO, PROFESOR_PROYECTO PROUNO, PROFESOR_PROYECTO PRODOS  
                                  WHERE PROFESORUNO.Cedula != PROFESORDOS.Cedula AND PROFESORUNO.Cedula=PROFEUNO.Cedula AND PROFEUNO.Cedula=PROUNO.CEDULA AND PROFEUNO.Id_Rol = PROUNO.Id_Rol AND 
                                  PROUNO.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFEUNO.Id_Rol='05' AND PROFESORDOS.Cedula=PROFEDOS.Cedula AND PROFEDOS.Cedula=PRODOS.CEDULA AND 
                                  PROFEDOS.Id_Rol = PRODOS.Id_Rol AND PRODOS.Id_Proyecto=PROYECTO.Id_Proyecto AND PROFEDOS.Id_Rol='05' AND PROYECTO.Id_Proyecto='" . $id . "' LIMIT 1 ;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }

    public function Consultar_Anteproyectos() {
        $sql = $this->con->query("SELECT Proyecto.Id_Proyecto , Proyecto.Nombre_Proyecto,Proyecto.Nombre_Archivo,  ComiteT.Estado AS Estado_Comite
                                  FROM Proyecto, Estado_Proyecto Evaluador, Estado_Proyecto Comite , Tipo_Estado_Proyecto EvaluadorT, Tipo_Estado_Proyecto ComiteT
                                  WHERE Proyecto.Id_Proyecto = Evaluador.Id_Proyecto AND Proyecto.Id_Proyecto = Comite.Id_Proyecto AND EvaluadorT.Id_Estado = Evaluador.Estado_Evaluador AND
                                  ComiteT.Id_Estado = Comite.Estado_Comite AND ComiteT.Id_Estado = '00' AND Evaluador.Estado_Evaluador ='02';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION Estudiante_Agrega_Proyecto Borrada es de Carlos

    //FUNCION MODIFICADA CAROL 24/01/2020
    public function Anteproyectodevuelto($codigo,$id_proyecto,$cedula,$correo,$nombre,$telefono,$carrera,$fecha){
        $Sql = "INSERT INTO ANTEPROYECTO_DEVUELTO_DATOS (Codigo,Id_Proyecto,Cedula,Correo,Nombre,Telefono,Id_Carrera,Fecha_Devolucion) VALUES (:codigo,:id_proyecto,:cedula,:correo,:nombre,:telefono,:carrera,:fecha);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":codigo" => $codigo, ":id_proyecto" => $id_proyecto, ":cedula" => $cedula, ":correo" => $correo,":nombre"=>$nombre,":telefono"=>$telefono,":carrera"=>$carrera, ":fecha"=>$fecha));
        }
    //FUNCION Neil 15/12/2019 
    public function buscar_proyectos_para_cancelar($nombre_proyecto){
        $sql= $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, GROUP_CONCAT(PERSONA.Nombre) AS Nombres 
                                 FROM PERSONA INNER JOIN ESTUDIANTE ON PERSONA.Correo=ESTUDIANTE.Correo INNER JOIN PROYECTO ON PROYECTO.Id_Proyecto = ESTUDIANTE.Id_Proyecto 
                                 WHERE PROYECTO.Nombre_Proyecto LIKE '%" . $nombre_proyecto . "%' GROUP BY PROYECTO.Id_Proyecto");
        $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function Archivo_proyecto($id){
        $sql= $this->con->query("SELECT * 
                                 FROM ARCHIVO_PROYECTO 
                                 WHERE Id_Proyecto='".$id."';");
        $resultado=$sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function estado_proyecto($id){
        $sql= $this->con->query("SELECT * FROM ESTADO_PROYECTO where Id_Proyecto='".$id."';");
        $resultado=$sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA CAROL 25/01/2020
    public function Anteproyectodevueltoevaluador($codigo,$id_proyecto,$fecha,$cedulap){
        $Sql = "INSERT INTO ANTEPROYECTO_DEVUELTO_EVALUADOR (Codigo,Id_Proyecto,Fecha_Devolucion,Cedula) VALUES (:codigo,:id_proyecto,:fecha,:cedulap);";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":codigo" => $codigo, ":id_proyecto" => $id_proyecto, ":fecha"=>$fecha, ":cedulap" => $cedulap));
    }
    //FUNCION NUEVA
    public function Anteproyectodevueltoevaluadorcodigo($codigo){
        $sql= $this->con->query("SELECT * FROM ANTEPROYECTO_DEVUELTO_EVALUADOR WHERE Codigo='".$codigo."';");
        $resultado=$sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //FUNCION NUEVA CAROL cuando el estudiante tiene más de un proyecto devuelto por el evaluador
    public function Anteproyectodevueltoevaluadorupdate($codigo,$id_proyecto,$fecha,$cedulap){
        $Sql = "UPDATE ANTEPROYECTO_DEVUELTO_EVALUADOR SET Id_Proyecto=:id_proyecto, Fecha_Devolucion=:fecha, Cedula=:cedulap WHERE Codigo=:codigo;";
        $resultado = $this->con->prepare($Sql);
        $resultado->execute(array(":id_proyecto" => $id_proyecto, ":fecha"=>$fecha, ":cedulap" => $cedulap,":codigo" => $codigo));
    }
    //FUNCION NUEVA se elimina cuando el estudiante tiene más de un proyecto devuelto por el evaluador
    public function Anteproyectodevueltoevaluadoreliminar($id_proyecto){
        $Query = "DELETE FROM PROYECTO WHERE Id_Proyecto=:id;";
        $resultado = $this->con->prepare($Query);
        $resultado->execute(array(":id" => $id_proyecto));
    }
    //FUNCION NUEVA CAROL 03/02/2020 
    public function datosfichadevueltoC($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto, ANTEPROYECTO_DEVUELTO_DATOS.Codigo, ANTEPROYECTO_DEVUELTO_DATOS.Nombre AS Estudiante_Uno, ANTEPROYECTO_DEVUELTO_DATOS.Correo, ANTEPROYECTO_DEVUELTO_DATOS.Telefono, CARRERA.Carrera, LINEAINVESTIGACION.Linea, MODALIDAD.Modalidad, GRUPOINVESTIGACION.Grupo, PERSONA.nombre AS Evaluador, DURACION.Duracion, PROYECTO.Resumen_Ejecutivo, PROYECTO.Objetivo_General, PROYECTO.Objetivo_Especifico                                  
                                  FROM  PROYECTO, DURACION, PERSONA, ANTEPROYECTO_DEVUELTO_DATOS,PROFESOR,ROL,PROFESOR_PROYECTO, ESTUDIANTE, CARRERA, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION                                
                                  WHERE 
                                  PERSONA.Correo=PROFESOR.Correo AND 
                                  PERSONA.Cedula=PROFESOR.Cedula AND 
                                  DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND 
                                  ROL.Id_Rol = PROFESOR.Id_Rol AND 
                                  PROFESOR.Cedula=PROFESOR_PROYECTO.Cedula AND 
                                  PROFESOR.Id_Rol=PROFESOR_PROYECTO.Id_Rol AND 
                                  PROYECTO.Id_Proyecto=PROFESOR_PROYECTO.Id_Proyecto AND 
                                  PROFESOR.Id_Rol='03' AND 
                                  ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto= PROYECTO.Id_Proyecto AND 
                                  CARRERA.Id_Carrera=ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera AND 
                                  LINEAINVESTIGACION.Id_Linea=PROYECTO.Id_Linea AND 
                                  MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad AND 
                                  GRUPOINVESTIGACION.Id_Grupo=PROYECTO.Id_Grupo AND 
                                  PROYECTO.Id_Proyecto ='".$id."' LIMIT 1;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA CAROL 03/02/2020
    public function datosfichadevueltoC2($id) {
        $sql = $this->con->query("SELECT PROYECTO.Id_Proyecto, PROYECTO.Nombre_Proyecto,  ESTU.Codigo,  ESTU.Nombre AS Estudiante_Uno,  ESTU.Correo,  ESTU.Telefono, CARRERA.Carrera, ESTUDIANTED.Codigo AS Codigo_Dos, ESTUDIANTED.Nombre AS Estudiante_Dos , ESTUDIANTED.Correo AS Correo_Dos,  ESTUDIANTED.Telefono AS Telefono_Dos, CARRERADOS.Carrera AS Carrera_Dos, LINEAINVESTIGACION.Linea, MODALIDAD.Modalidad, GRUPOINVESTIGACION.Grupo, PROFESORDOS.Nombre AS Evaluador, DURACION.Duracion, PROYECTO.Resumen_Ejecutivo, PROYECTO.Objetivo_General, PROYECTO.Objetivo_Especifico 
                                  FROM PROYECTO, PROFESOR_PROYECTO PRODOS, PERSONA PROFESORDOS,  PROFESOR PROFEDOS, ROL EVALUADOR, ANTEPROYECTO_DEVUELTO_DATOS ESTU, ANTEPROYECTO_DEVUELTO_DATOS ESTUDIANTED, CARRERA, CARRERA CARRERADOS, DURACION, LINEAINVESTIGACION, MODALIDAD, GRUPOINVESTIGACION
                                  WHERE 
                                  ESTU.Correo != ESTUDIANTED.Correo AND 
                                  DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND 
                                  PROFESORDOS.Correo=PROFEDOS.Correo AND 
                                  PROFESORDOS.Cedula=PROFEDOS.Cedula AND 
                                  EVALUADOR.id_rol = PROFEDOS.Id_Rol AND 
                                  DURACION.Id_Duracion = PROYECTO.Duracion_Estudiante AND 
                                  PROFEDOS.Cedula=PRODOS.Cedula AND 
                                  EVALUADOR.Id_Rol=PRODOS.Id_Rol AND 
                                  PROYECTO.Id_Proyecto=PRODOS.Id_Proyecto AND 
                                  PROFEDOS.Id_Rol='03' AND 
                                  ESTU.Id_Proyecto= PROYECTO.Id_Proyecto AND 
                                  ESTUDIANTED.Id_Proyecto = PROYECTO.Id_Proyecto AND 
                                  LINEAINVESTIGACION.Id_Linea=PROYECTO.Id_Linea AND 
                                  MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad AND 
                                  GRUPOINVESTIGACION.Id_Grupo=PROYECTO.Id_Grupo AND 
                                  CARRERADOS.Id_Carrera = ESTUDIANTED.Id_Carrera AND
                                  CARRERA.Id_Carrera = ESTU.Id_Carrera AND
                                  PROYECTO.Id_Proyecto ='".$id."' LIMIT 1;");
        $lista = $sql->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //25/01/2020 (CAROL) FUNCION NUEVA 
    public function Consultar_Estado_EvaluadorE_Devuelto($codigo) {
        $sql = $this->con->query("SELECT  ROL.Rol,PERSONA.Nombre,TIPO_ESTADO_PROYECTO.Estado, ESTADO_PROYECTO.Fecha_Evaluador, ANTEPROYECTO_DEVUELTO_EVALUADOR.Id_Proyecto
                                  FROM ESTADO_PROYECTO, TIPO_ESTADO_PROYECTO,PROFESOR_PROYECTO,ROL,PROYECTO,PROFESOR, PERSONA, ANTEPROYECTO_DEVUELTO_EVALUADOR
                                  WHERE
                                  PERSONA.Cedula=PROFESOR.Cedula AND
                                  PROFESOR_PROYECTO.Cedula=PROFESOR.Cedula AND
                                  PROFESOR.Id_Rol=ROL.Id_Rol AND
                                  TIPO_ESTADO_PROYECTO.Id_Estado = ESTADO_PROYECTO.Estado_Evaluador AND
                                  ROL.Id_Rol = PROFESOR_PROYECTO.Id_Rol AND
                                  ESTADO_PROYECTO.Id_Proyecto= PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Proyecto =  PROYECTO.Id_Proyecto AND
                                  PROFESOR_PROYECTO.Id_Rol='03' AND
	                              ANTEPROYECTO_DEVUELTO_EVALUADOR.Id_Proyecto =PROYECTO.Id_Proyecto AND
	                              ANTEPROYECTO_DEVUELTO_EVALUADOR.Codigo='".$codigo."';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    //25/01/2020 (CAROL) FUNCION NUEVA
    public function Consultar_Existencia_Estudiante_Devuelto($codigo) {
        $sql = $this->con->query("SELECT Codigo FROM ANTEPROYECTO_DEVUELTO_EVALUADOR WHERE Codigo='".$codigo."';");
        $rows = $sql->rowCount();
        return $rows;
    }
    //FUNCION NUEVA 31/01/2020
    public function estudiante_cantidad_devuelto($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) AS Cantidad
                                       FROM ANTEPROYECTO_DEVUELTO_EVALUADOR 
                                       WHERE Id_Proyecto='".$id_proyecto."';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 30/01/2020 CAROL
    public function cantidad_de_materias($id_proyecto) {
        $consulta = $this->con->query("SELECT COUNT(Id_Proyecto) 
                                       FROM INSCRIPCION_ESPACIO_ACADEMICO 
                                       WHERE Id_Proyecto='".$id_proyecto."'");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //FUNCION NUEVA 30/01/2020 CAROL
    public function datosfichasMaterias($id) {
        $sql = $this->con->query("SELECT ESPACIO_ACADEMICO.Espacio 
                                  FROM ESPACIO_ACADEMICO,INSCRIPCION_ESPACIO_ACADEMICO 
                                  WHERE ESPACIO_ACADEMICO.Id_Espacio= INSCRIPCION_ESPACIO_ACADEMICO.Id_Espacio AND INSCRIPCION_ESPACIO_ACADEMICO.Id_Proyecto='".$id."';");
        $lista = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $lista;
    }
    
   
    //25/01/2020 (CAROL) FUNCION NUEVA NO RECUERDO DONDE LA USO BUSCAR
    public function Consultar_Existencia_Estados_Ante($id_proyecto) {
        $sql = $this->con->query("SELECT Estado_Evaluador FROM ESTADO_PROYECTO WHERE Id_Proyecto='".$id_proyecto."';");
        $rows = $sql->rowCount();
        return $rows;
    }
    //FUNCION NUEVA CAROL 25/01/2020
    public function nombre_modalidad($id) {
        $consulta = $this->con->query("SELECT MODALIDAD.Modalidad 
                                       FROM MODALIDAD 
                                       WHERE MODALIDAD.Id_Modalidad='".$id."';");
        $lista = $consulta->fetch(PDO::FETCH_ASSOC);
        return $lista;
    }
    //NEIL
    public function prueba_grafico($carrera,$fechad,$fechaa,$modalidad){
        $sql= $this->con->query("SELECT COUNT(DISTINCT ESTUDIANTE.Id_Proyecto) as Cantidad,  MODALIDAD.Modalidad
	FROM  ESTUDIANTE 
	INNER JOIN PROYECTO ON PROYECTO.Id_Proyecto = ESTUDIANTE.Id_Proyecto 
	INNER JOIN ESTADO_PROYECTO ON ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto 
	INNER JOIN MODALIDAD ON MODALIDAD.Id_Modalidad = PROYECTO.Id_Modalidad  
	INNER JOIN CARRERA ON CARRERA.Id_Carrera =ESTUDIANTE.Id_Carrera 
	WHERE 
    ESTADO_PROYECTO.Estado_Comite='02' 
    AND
	ESTUDIANTE.Id_Carrera='".$carrera."' AND 
    PROYECTO.Id_Modalidad='".$modalidad."' AND
    PROYECTO.Id_Modalidad!='00' AND
	PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechaa."'");
        $resultado=$sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
     public function prueba_grafico_devueltos($carrera,$fechad,$fechaa,$modalidad){
        $sql= $this->con->query("SELECT COUNT(PROYECTO.Id_Proyecto) as Cantidad, MODALIDAD.Modalidad FROM ANTEPROYECTO_DEVUELTO_DATOS
        INNER JOIN PROYECTO ON PROYECTO.Id_Proyecto = ANTEPROYECTO_DEVUELTO_DATOS.Id_Proyecto 
        INNER JOIN ESTADO_PROYECTO ON ESTADO_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto INNER JOIN 
        MODALIDAD ON MODALIDAD.Id_Modalidad = PROYECTO.Id_Modalidad INNER JOIN CARRERA ON CARRERA.Id_Carrera =ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera 
        WHERE ESTADO_PROYECTO.Estado_Comite='03' AND ANTEPROYECTO_DEVUELTO_DATOS.Id_Carrera='".$carrera."' AND PROYECTO.Id_Modalidad='".$modalidad."' AND
        PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechaa."'");
        $resultado=$sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    
    public function grafico_director($fechad, $fechaa,$tutor){
        $sql= $this->con->query("SELECT COUNT(PROFESOR_PROYECTO.Id_Proyecto) AS Total, MODALIDAD.Modalidad 
        FROM PROFESOR_PROYECTO,PROYECTO,MODALIDAD 
        WHERE MODALIDAD.Id_Modalidad=PROYECTO.Id_Modalidad 
        AND PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Id_Proyecto 
        AND PROFESOR_PROYECTO.Cedula='".$tutor."' AND Id_Rol='05' 
        AND PROYECTO.Fecha_Registro BETWEEN '".$fechad."' AND '".$fechaa."' GROUP BY MODALIDAD.Modalidad");
         $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
        
    public function modalidades(){
        $sql=$this->con->query("select m.Id_Modalidad from MODALIDAD m where m.Id_Modalidad!=00");
        $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }
    

}
