<?php

require_once ('Conexion.php');

class DAO_Cronograma extends Conexion {

    private $con;

    public function DAO_Cronograma() {
        $this->con = parent::__construct();
    }

    public function Crear_Cronograma($codigo, $fecha_i, $fecha_f) {
        $sql = $this->con->query("SELECT * FROM CRONOGRAMA WHERE Codigo_Cronograma='" . $codigo . "'");
        $verificar = $sql->fetch(PDO::FETCH_ASSOC);
        if ($verificar['Codigo_Cronograma'] == $codigo) {
            
        } else {
            $SQL = "INSERT INTO CRONOGRAMA (Codigo_Cronograma,Fecha_Inicio,Fecha_Fin) "
                    . "VALUES (:Codigo_Cronograma,:Fecha_Inicio,:Fecha_Fin);";
            $resultado = $this->con->prepare($SQL);
            $resultado->execute(array(":Codigo_Cronograma" => $codigo, ":Fecha_Inicio" => $fecha_i, ":Fecha_Fin" => $fecha_f
            ));
        }
    }

    public function Ver_Cronograma() {
        $sql = $this->con->query("SELECT * FROM CRONOGRAMA");
        $listacronograma = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $listacronograma;
    }

    public function Eliminar_Cronograma($id) {
        try {
            $Query = "DELETE FROM CRONOGRAMA WHERE Codigo_Cronograma=:id";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":id" => $id));
            $Respuesta = TRUE;
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }

    public function Lista_Lugar() {
        $sql = $this->con->query("SELECT * FROM LUGAR");
        $lista = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $lista;
    }

    public function Asignar_Fecha_Sustentacion($id_proyecto, $fecha, $hora, $lugar) {
        $sql = $this->con->query("SELECT * FROM SUSTENTACION");
        $verificar = $sql->fetch(PDO::FETCH_ASSOC);
        if ($verificar['Id_Proyecto'] == $id_proyecto) {
            
        } else {
            $SQL = "INSERT INTO SUSTENTACION (Fecha_Sustentacion,Hora,Id_Proyecto,Id_Lugar,Codigo_Cronograma) "
                    . "VALUES (:Fecha_Sustentacion,:Hora,:Id_Proyecto,:Id_Lugar,:Codigo_Cronograma);";
            $resultado = $this->con->prepare($SQL);
            $resultado->execute(array(":Fecha_Sustentacion" => $fecha, ":Hora" => $hora, ":Id_Proyecto" => $id_proyecto
                , ":Id_Lugar" => $lugar, ":Codigo_Cronograma" => NULL));
                
        }
    }

}


