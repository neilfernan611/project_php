<?php

require_once 'Conexion.php';

class DAO_LineaInvestigacion extends Conexion {

    private $con;

    public function DAO_LineaInvestigacion() {
        $this->con = parent::__construct();
    }

    public function insertarLinea($id_linea, $linea,$esta) {
        try {
            $id = "";
            
            $consulta = "INSERT INTO LINEAINVESTIGACION VALUES(:id_linea,:linea,:estadoi);";
            
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id_linea" => $id_linea, ":linea" => $linea,":estadoi"=>$esta));
            
        } catch (Exception $e) {
            die("Error " . $e->getMessage() . " en la linea" . $e->getLine());
        }
        return $id;
    }

    public function eliminarLinea($id_linea) {
        try {

            $consulta = $this->con->query("SELECT * FROM LINEAINVESTIGACION WHERE Id_Linea='" . $id_linea . "'");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if ($verificarConsulta['Id_Linea'] == $id_linea) {
                $Query = "DELETE FROM LINEAINVESTIGACION WHERE Id_Linea=:id;";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":id" => $id_linea));
                $Respuesta = TRUE;
            } else {

                $Respuesta = false;
                return $Respuesta;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }

    public function actualizarLinea($id, $linea, $estado) {
        try {
            $Query = "UPDATE LINEAINVESTIGACION SET Linea=:linea, Id_EstadoI=:estado WHERE Id_Linea=:idl";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":idl" => $id, ":linea" => $linea, ":estado" => $estado));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }

    //NO BORRAR CONSULTAS PARA PAGINACION
    public function listarTablaLinea() {

        $sql = $this->con->query("SELECT LINEAINVESTIGACION.Id_Linea,LINEAINVESTIGACION.Linea,LINEAINVESTIGACION.Id_Estadoi,ESTADOI.Estado 
                                  FROM LINEAINVESTIGACION, ESTADOI
                                  WHERE LINEAINVESTIGACION.Linea!= 'Ninguno' AND ESTADOI.Id_Estadoi=LINEAINVESTIGACION.Id_Estadoi ORDER BY LINEAINVESTIGACION.Id_Linea DESC;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    //NO BORRAR CONSULTAS PARA PAGINACION
    public function listarTablaLinea2($iniciar, $articulosx) {

        $sql = $this->con->query("SELECT LINEAINVESTIGACION.Id_Linea,LINEAINVESTIGACION.Linea,LINEAINVESTIGACION.Id_Estadoi,ESTADOI.Estado FROM LINEAINVESTIGACION, ESTADOI WHERE LINEAINVESTIGACION.Linea!= 'Ninguno' AND ESTADOI.Id_Estadoi=LINEAINVESTIGACION.Id_Estadoi ORDER BY LINEAINVESTIGACION.Id_Linea,LINEAINVESTIGACION.Id_Estadoi, ESTADOI.Estado LIMIT " . $iniciar . "," . $articulosx . ";");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function listaLinea() {
        $sql = $this->con->query("SELECT * 
                                  FROM LINEAINVESTIGACION 
                                  WHERE Id_Linea != 00;");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    //FUNCION NUEVA 20/11/2019
     public function listaLineaHabilitada() {
        $sql = $this->con->query("SELECT * 
                                  FROM LINEAINVESTIGACION 
                                  WHERE Id_Linea != 00 AND Id_EstadoI !='05';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    
    public function verificar_inves($id_linea,$linea,$formulario,$estado){
        
        $resul=0;
        
        if($formulario==0){
            $sql = $this->con->query("SELECT * FROM LINEAINVESTIGACION WHERE Id_Linea='".$id_linea."' OR Linea='".$linea."' ;");
            $resultado=$sql->fetch(PDO::FETCH_ASSOC);
            if($resultado['Id_Linea']==$id_linea || $resultado['Linea']==$linea|| strcasecmp($resultado['Linea'],$linea) == 0 ){
                $resul=1;
            }else{
                $resul=0;
            }
        }else if(formulario == 1){
            $sql = $this->con->query("SELECT * FROM LINEAINVESTIGACION WHERE Id_Linea='".$id_linea."' OR Linea='".$linea."' ;");
            $resultado=$sql->fetch(PDO::FETCH_ASSOC);
            if(($resultado['Linea']==$linea|| strcasecmp($resultado['Linea'],$linea) == 0)  && $estado == $resultado['Id_EstadoI'] ){
                $resul=1;
            }else{
                $resul=0;
            }
        }
        
        
       
        
        return $resul;
    }
    
    




}


