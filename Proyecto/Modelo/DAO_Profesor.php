<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Profesor extends Conexion implements DAO_Contraseña {

    public function DAO_Profesor() {
        $this->con = parent::__construct();
    }

    public function SessionProfesor($usuario) {

        $sql = $this->con->query("SELECT * 
                                  FROM PROFESOR 
                                  WHERE lower(Correo)='" . $usuario . "'  LIMIT 1;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function correoProfesor($cedula) {
        $query = $this->con->query("SELECT PROFESOR.Correo 
                                    FROM PROFESOR 
                                    WHERE  PROFESOR.Cedula=" . $cedula . ";");
        $list_Profe = $query->fetch(PDO::FETCH_ASSOC);
        return $list_Profe;
    }

    public function InformacionProfesor($Cedula) {

        $sql = $this->con->query("SELECT * 
                                  FROM PROFESOR 
                                  WHERE Cedula='" . $Cedula . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function crearUSuarioPro($correo, $pass_P, $cedula, $pertenecer,$externo) {
          try{
        if($externo=='01'){
            $rol=['03','01'];
            for($i=0;i<=1;$i++){
             $Sql = "INSERT INTO PROFESOR (Id_Rol,Correo,Cedula,Pass_P,Id_Pertenecer,Id_Externo) "
                . "VALUES (:id_rol,:correo,:cedula,:pass,:pertenecer,:externo);";

            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array("id_rol" =>$rol[$i], ":correo" => $correo, ":cedula" => $cedula, ":pass" => $pass_P, ":pertenecer" => $pertenecer,":externo"=>$externo));
                
                 if($i==1){
                break;
                          }
            }
        
        }else{
            $rol=['01','03','05'];
        for($i=0;i<=2;$i++){
        $Sql = "INSERT INTO PROFESOR (Id_Rol,Correo,Cedula,Pass_P,Id_Pertenecer,Id_Externo) VALUES (:id_rol,:correo,:cedula,:pass,:pertenecer,:externo)";
            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array("id_rol" =>$rol[$i], ":correo" => $correo, ":cedula" => $cedula, ":pass" => $pass_P, ":pertenecer" => $pertenecer,":externo"=>$externo));
            if($i==2){
                break;
                     }
        }
        }
          }catch(Exception $e){
              echo $e->getMessage()."<br>".$e->getLine();
          }
    }

    public function crearUsuarioProfe($id_rol, $correo, $cedula, $pass_P) {
        try {
            $id = "";

            $SQLID = $this->con->query("SELECT * FROM PROFESOR  WHERE Cedula = '" . $cedula . "' AND Id_Rol='" . $id_rol . "';");
            $Sql = "INSERT INTO PROFESOR (Id_Rol,Correo,Cedula,Pass_P) VALUES (:id,:correo,:cedula,:pass);";
            $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

            if ($verificar['Cedula'] == $cedula and $verificar['Id_Rol'] == $id_rol) {
                return;
            } else {
                $resultado = $this->con->prepare($Sql);
                $resultado->execute(array(":id" => $id_rol, ":correo" => $correo, ":cedula" => $cedula, ":pass" => $pass_P));
                $SQLID = $this->con->query("SELECT * FROM PROFESOR  WHERE Cedula = '" . $cedula . "' AND Id_Rol='" . $id_rol . "';");
                $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
                $id = $verificar['Cedula'];
            }
        } catch (Exception $e) {
            die("Error " . $e->getMessage() . " en la linea" . $e->getLine());
        }
        return $id;
    }
    
    //FUNCION MODIFICADA (CAROL) 20/11/2019
    public function listarProfesor() {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono, CIENCIAS_BASICAS.Pertenecer 
                                       FROM PERSONA, PROFESOR , CIENCIAS_BASICAS,ROL 
                                       WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula != 0 AND PERSONA.Cedula != 1 AND PERSONA.Cedula != 2 AND PROFESOR.Id_Pertenecer = CIENCIAS_BASICAS.Id_Pertenecer AND ROL.Id_Rol=PROFESOR.Id_Rol GROUP BY PERSONA.Cedula, PERSONA.Nombre, PERSONA.Correo, CIENCIAS_BASICAS.Pertenecer;");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }

    public function listarProfesoresRoles() {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono, CIENCIAS_BASICAS.Pertenecer, PROFESOR.Id_Rol,ROL.Rol
                                       FROM PERSONA, PROFESOR , CIENCIAS_BASICAS,ROL
                                       WHERE 
                                       PERSONA.Cedula= PROFESOR.Cedula AND 
                                       PERSONA.Cedula != 0 AND
                                       PERSONA.Cedula != 1 AND
                                       PERSONA.Cedula != 2 AND
                                       PROFESOR.Id_Pertenecer = CIENCIAS_BASICAS.Id_Pertenecer AND
                                       ROL.Id_Rol=PROFESOR.Id_Rol;");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }

    public function listarProfesorNombre($nombre,$externo) {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono,CIENCIAS_BASICAS.Pertenecer 
                                       FROM PERSONA, PROFESOR , CIENCIAS_BASICAS,EXTERNO 
                                       WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula != 0 AND PERSONA.Cedula !=1 AND PERSONA.Cedula != 2 AND
                                       PROFESOR.Id_Externo=$externo AND EXTERNO.Id_Externo=PROFESOR.Id_Externo AND
                                       PROFESOR.Id_Pertenecer = CIENCIAS_BASICAS.Id_Pertenecer AND PERSONA.Nombre LIKE '%" . $nombre . "%' GROUP BY PERSONA.Cedula,PERSONA.Nombre,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono,CIENCIAS_BASICAS.Pertenecer;");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);

        return $listap;
    }

    //NO BORRAR CONSULTAS PARA PAGINACION
    public function listarProfesorNombre_paginacion($nombre, $iniciar, $ariculosx) {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono, CIENCIAS_BASICAS.Pertenecer
                                       FROM PERSONA, PROFESOR , CIENCIAS_BASICAS
                                       WHERE 
                                       PERSONA.Cedula= PROFESOR.Cedula AND 
                                       PERSONA.Cedula != 0 AND 
                                       PERSONA.Cedula != 1 AND
                                       PERSONA.Cedula != 2 AND
                                       PROFESOR.Id_Pertenecer = CIENCIAS_BASICAS.Id_Pertenecer AND
                                       PERSONA.Nombre LIKE '%" . $nombre . "%'
                                       GROUP BY PERSONA.Cedula,PERSONA.Nombre,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono,CIENCIAS_BASICAS.Pertenecer LIMIT " . $iniciar . "," . $ariculosx . ";");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);

        return $listap;
    }

    //NO BORRAR CONSULTAS PARA PAGINACION
    public function listarProfesor_paginacion($iniciar, $articulosx) {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Correo,PERSONA.Telefono, CIENCIAS_BASICAS.Pertenecer
                                       FROM PERSONA, PROFESOR , CIENCIAS_BASICAS
                                       WHERE 
                                       PERSONA.Cedula= PROFESOR.Cedula AND 
                                       PERSONA.Cedula != 0 AND 
                                       PERSONA.Cedula != 1 AND
                                       PERSONA.Cedula != 2 AND
                                       PROFESOR.Id_Pertenecer = CIENCIAS_BASICAS.Id_Pertenecer
                                       GROUP BY PERSONA.Cedula, PERSONA.Nombre, PERSONA.Correo,CIENCIAS_BASICAS.Pertenecer LIMIT " . $iniciar . "," . $articulosx . ";");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);

        return $listap;
    }

    public function listarProfesor2($consulta) {

        $query = $this->con->query($consulta);
        $listaprof = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaprof;
    }

    public function listarProfesorJurado(){
        $consulta = $this->con->query("SELECT PERSONA.* FROM PERSONA, PROFESOR WHERE PERSONA.Cedula = PROFESOR.Cedula AND PERSONA.Cedula !='0' AND PROFESOR.Id_Rol='01' AND PROFESOR.Id_Pertenecer != '01'");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }
    //CARLOS
    public function listarProfesorJuradoPCB(){
        $consulta = $this->con->query("SELECT PERSONA.* FROM PERSONA, PROFESOR WHERE PERSONA.Cedula = PROFESOR.Cedula AND PERSONA.Cedula !='0' AND PROFESOR.Id_Rol='01' AND PROFESOR.Id_Pertenecer = '01'");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }
    public function listartutor() {
        $consulta = $this->con->query("SELECT PERSONA.* 
                                       FROM PERSONA, PROFESOR 
                                       WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula != 0 AND PERSONA.Cedula !=1 AND PROFESOR.Id_Rol='05' GROUP BY PERSONA.Cedula, PERSONA.Correo;");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }

    public function listarProfesorRoles($id_rol) {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre 
                                       FROM PERSONA, PROFESOR, ROL 
                                       WHERE PERSONA.Cedula=PROFESOR.Cedula AND PERSONA.Cedula != '0' AND PERSONA.Cedula !=1 AND PERSONA.Cedula !=2 AND PROFESOR.Id_Rol  = ROL.Id_Rol AND ROL.Id_Rol ='" . $id_rol . "';");
        $listar = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listar;
    }
    public function listarProfesorRoles_paginacion($id_rol,$iniciar,$final) {
        $consulta = $this->con->query("SELECT PERSONA.Cedula,PERSONA.Nombre,PERSONA.Telefono,PERSONA.Correo 
                                       FROM PERSONA, PROFESOR, ROL 
                                       WHERE PERSONA.Cedula=PROFESOR.Cedula AND PERSONA.Cedula != '0' AND PERSONA.Cedula !=1 
                                       AND PERSONA.Cedula !=2 AND PROFESOR.Id_Rol  = ROL.Id_Rol AND ROL.Id_Rol ='" . $id_rol . "'" . " LIMIT " . $iniciar . "," . $final . ";");
        $listar = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listar;
    }


    public function listarEvaluador() {
        $consulta = $this->con->query("SELECT PERSONA.* 
                                       FROM PERSONA, PROFESOR,ROL 
                                       WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula != 0 AND PERSONA.Cedula !=1 AND PERSONA.Cedula !=2 AND ROL.Id_Rol=PROFESOR.Id_Rol AND ROL.Rol='Evaluador' ");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }
    
        public function listarEvaluadorProduccion() {
        $consulta = $this->con->query("SELECT PERSONA.* 
                                       FROM PERSONA, PROFESOR,ROL 
                                       WHERE PERSONA.Cedula= PROFESOR.Cedula AND PERSONA.Cedula ='2' AND ROL.Id_Rol=PROFESOR.Id_Rol");
        $listap = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $listap;
    }
    
    public function editarCienciasBasicas($cedula, $id_pertenecer) {
        try {

            $Query = "UPDATE PROFESOR SET Id_Pertenecer=:pertenece WHERE Cedula=:cedula";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":cedula" => $cedula, ":pertenece" => $id_pertenecer));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . " " . $ex->getLine() . "<br>" . $cedula . "<br>" . $id_pertenecer);
        }
        return $Resp;
    }
    
    public function verificarRoles($cedula){
        $consulta=$this->con->query("SELECT PROFESOR.Id_Rol FROM PROFESOR WHERE PROFESOR.Cedula='".$cedula."';");
        $lista=$consulta->fetchAll(PDO::FETCH_ASSOC);
        return $lista;
    }

    public function cambiar_pass($correo, $contra_a, $contra_n) {
        try {

            $Sql = $this->con->query("SELECT * FROM PROFESOR WHERE Correo='" . $correo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ($ver['Correo'] == $correo && password_verify($contra_a, $ver['Pass_P'])) {
                $Query = "UPDATE PROFESOR SET Pass_P=:contra_n WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $encriptado = password_hash($contra_n, PASSWORD_DEFAULT, ['cost' => 5]);
                $resultado->execute(array(":correo" => $correo, ":contra_n" => $encriptado));
                $Resp = TRUE;
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }
    
    public function reset_pass($correo, $contraseña) {}
    
    //Carlos
    public function cantidad_tut($id_proyecto)
    {
        $consulta = $this->con->query ("SELECT COUNT(Id_Proyecto) cantidad FROM PROFESOR_PROYECTO WHERE Id_Rol = '05'  AND Id_Proyecto = '" . $id_proyecto . "' ");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function uno_tutor($id_proyecto)
    {
        $consulta =$this->con->query ("SELECT
    PERSONA.Cedula,
    ESTADO_SUSTENTACION.Id_Proyecto,
    PROFESOR_PROYECTO.Estado,
    PROFESOR_PROYECTO.Fecha,
    PERSONA.Nombre,
    TIPO_ESTADO_SUSTENTACION.EstadoS,
    PROFESOR_PROYECTO.Fecha
FROM
    PROFESOR_PROYECTO,
    ESTADO_SUSTENTACION,
    PERSONA,
    TIPO_ESTADO_SUSTENTACION
WHERE
    PROFESOR_PROYECTO.Id_Rol = '05' 
    AND PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."' 
    AND ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."'
    AND PERSONA.Cedula = PROFESOR_PROYECTO.Cedula 
    AND TIPO_ESTADO_SUSTENTACION.Id_EstadoS = PROFESOR_PROYECTO.Estado
LIMIT 1");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function dos_tutor($id_proyecto)
    {
        $consulta =$this->con->query ("SELECT
    t1.cedula tutor1,
    t2.cedula tutor2,
    t1.id_proyecto,
    t1.estado estadot1,
    t2.estado estadot2,
    t1.fecha fechat1,
    t2.fecha fechat2,
    pt1.Nombre Nombret1,
    pt2.Nombre Nombret2,
    estadot1.EstadoS nestado1,
    estadot2.EstadoS nestado2
FROM
    PERSONA pt1,
    PERSONA pt2,
    PROFESOR_PROYECTO AS t1,
    PROFESOR_PROYECTO AS t2,
    ESTADO_SUSTENTACION,
    TIPO_ESTADO_SUSTENTACION AS estadot1,
    TIPO_ESTADO_SUSTENTACION AS estadot2
WHERE
    t1.Id_Rol = '05' 
    AND t1.Id_Proyecto = '".$id_proyecto."' 
    AND ESTADO_SUSTENTACION.Id_Proyecto = '".$id_proyecto."' 
    AND t2.Id_Rol = '05' 
    AND t2.Id_Proyecto = '".$id_proyecto."' 
    AND t1.cedula != t2.cedula
    AND t1.Cedula = pt1.Cedula
    AND t2.Cedula = pt2.Cedula
    AND t1.Estado = estadot1.Id_EstadoS
    AND t2.Estado = estadot2.Id_EstadoS
LIMIT 1");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function consultaT2($id_proyecto){
        $consulta = $this ->con->query("SELECT
    PERSONA.Nombre,
    PROFESOR_PROYECTO.Cedula,
    PROFESOR_PROYECTO.Id_Proyecto,
    PROFESOR_PROYECTO.Fecha,
    PROFESOR_PROYECTO.Estado,
    TIPO_ESTADO_SUSTENTACION.EstadoS
FROM
    PROFESOR_PROYECTO,
    TIPO_ESTADO_SUSTENTACION,
    PERSONA
WHERE
    PROFESOR_PROYECTO.Id_Proyecto = '".$id_proyecto."' 
    AND PROFESOR_PROYECTO.Id_Rol = '05' 
    AND PROFESOR_PROYECTO.Estado = TIPO_ESTADO_SUSTENTACION.Id_EstadoS 
    AND PROFESOR_PROYECTO.Cedula = PERSONA.Cedula");
    $resultado=$consulta->fetchAll(PDO::FETCH_ASSOC);
    return $resultado;
    }
    public function Cantidad_P_Tutor($cedula)    
    {
        $consulta = $this ->con->query("SELECT
    COUNT(PROYECTO.Id_Proyecto) cantidad
FROM
    PROFESOR_PROYECTO,
    PROYECTO
WHERE
    PROFESOR_PROYECTO.Cedula = '".$cedula."' 
    AND PROFESOR_PROYECTO.Id_Rol = '05' 
    AND PROFESOR_PROYECTO.Id_Proyecto = PROYECTO.Radicado");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function Cantidad_P_Jurado($cedula)    
    {
        $consulta = $this ->con->query("SELECT
    COUNT(PROYECTO.Id_Proyecto) cantidad
FROM
    ESTADO_SUSTENTACION,
    PROYECTO
WHERE
    ESTADO_SUSTENTACION.Id_Jurado = '".$cedula."'
    AND PROYECTO.Radicado = ESTADO_SUSTENTACION.Id_Proyecto");
        $resultado=$consulta->fetch(PDO::FETCH_ASSOC);
        return $resultado;
    }

public function estado_externo(){
  $consulta=$this->con->query("SELECT * FROM EXTERNO");
        $lista=$consulta->fetchAll(PDO::FETCH_ASSOC);
        return $lista;  
}

}
