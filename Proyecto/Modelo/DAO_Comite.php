<?php

require_once 'Conexion.php';
require_once 'DAO_Contraseña.php';

class DAO_Comite extends Conexion implements DAO_Contraseña {

    private $con;

    public function DAO_Comite() {

        $this->con = parent::__construct();
    }

    public function InformacionComite($Correo) {

        $sql = $this->con->query("SELECT * 
                                  FROM COMITE 
                                  WHERE lower(Correo)='" . $Correo . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function SessionComite($usuario) {

        $sql = $this->con->query("SELECT * 
                                  FROM COMITE 
                                  WHERE lower(Correo)='" . $usuario . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function crearUsuarioComite($codigo_c, $correo, $pass) {

        $id = "";

        $Sql = "INSERT INTO COMITE(Codigo_C,Correo,Pass)VALUES (:codigo,:correo,:pass);";

        $SQLID = $this->con->query("SELECT * FROM COMITE WHERE Codigo_C = '" . $codigo_c . "' AND Correo='" . $correo . "';");
        $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);

        if ($verificar['Codigo_C'] == $codigo_c and $verificar['Correo'] == $correo) {
            return;
        } else {
            $resultado = $this->con->prepare($Sql);
            $resultado->execute(array(":codigo" => $codigo_c, ":correo" => $correo, ":pass" => $pass));

            $SQLID = $this->con->query("SELECT * FROM COMITE WHERE Codigo_C = '" . $codigo_c . "' AND Correo='" . $correo . "';");
            $verificar = $SQLID->fetch(PDO::FETCH_ASSOC);
            $id = $verificar['Codigo_C'];
        }
        return $id;
    }

    public function correoComite($cod) {
        $sql = $this->con->query("SELECT COMITE.Correo 
                                  FROM COMITE 
                                  WHERE Codigo_C='" . $cod . "' ;");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }

    public function director($id) {
        $sql = $this->con->query("SELECT PERSONA.Nombre,PERSONA.Correo FROM PROYECTO,PROFESOR_PROYECTO,PERSONA WHERE 
        PERSONA.Cedula=PROFESOR_PROYECTO.Cedula and PROYECTO.Id_Proyecto=PROFESOR_PROYECTO.Id_Proyecto AND PROFESOR_PROYECTO.Id_Rol='05' AND PROYECTO.Id_Proyecto='".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
     public function director2($id) {
        $sql = $this->con->query("SELECT PERSONA.Nombre,PERSONA.Correo FROM PROYECTO,PROFESOR_PROYECTO,PERSONA WHERE 
        PERSONA.Cedula=PROFESOR_PROYECTO.Cedula and PROYECTO.Id_Proyecto=PROFESOR_PROYECTO.Id_Proyecto AND PROFESOR_PROYECTO.Id_Rol='05' AND PROYECTO.Id_Proyecto='".$id."'");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
     public function cantidad_directores($id) {
        $sql = $this->con->query("SELECT count(*) as cantidad FROM PROYECTO,PROFESOR_PROYECTO,PERSONA WHERE 
        PERSONA.Cedula=PROFESOR_PROYECTO.Cedula and PROYECTO.Id_Proyecto=PROFESOR_PROYECTO.Id_Proyecto AND PROFESOR_PROYECTO.Id_Rol='05' AND PROYECTO.Id_Proyecto='".$id."'");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    public function listarTablaComite($consulta) {
        $query = $this->con->query($consulta);
        $listaC = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaC;
    }
    
    public function cambiar_pass($correo, $contra_a, $contra_n) {
        try {

            $Sql = $this->con->query("SELECT * FROM COMITE WHERE  Correo='" . $correo . "';");
            $ver = $Sql->fetch(PDO::FETCH_ASSOC);
            if ($ver['Correo'] == $correo && password_verify($contra_a, $ver['Pass'])) {
                $Query = "UPDATE COMITE SET Pass=:contra_n WHERE Correo=:correo";
                $resultado = $this->con->prepare($Query);
                $encriptado = password_hash($contra_n, PASSWORD_DEFAULT, ['cost' => 5]);
                $resultado->execute(array(":correo" => $correo, ":contra_n" => $encriptado));
                $Resp = TRUE;
            } else {
                $Resp = FALSE;
            }
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }

    public function reset_pass($correo, $contraseña) {}

}
