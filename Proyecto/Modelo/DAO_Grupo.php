<?php

require_once 'Conexion.php';

class DAO_Grupo extends Conexion {

    private $con;

    public function DAO_Grupo() {
        $this->con = parent::__construct();
    }

    public function insertarGrupo($id_grupo, $grupo, $estado) {
        try {
            $id = "";
            $verificacion = $this->con->query("SELECT * FROM GRUPOINVESTIGACION WHERE Id_Grupo='" . $id_grupo . "'");
            $consulta = "INSERT INTO GRUPOINVESTIGACION VALUES(:id_grupo,:grupo,:estado);";
            $consultaverificada = $verificacion->fetch(PDO::FETCH_ASSOC);

            if ($consultaverificada['Id_Grupo'] == $id_grupo) {
                $id = $consultaverificada['Id_Grupo'];
            } else {
                $resultado = $this->con->prepare($consulta);
                $resultado->execute(array(":id_grupo" => $id_grupo, ":grupo" => $grupo, ":estado" => $estado));
            }
        } catch (Exception $e) {
            die("Error " . $e->getMessage() . " en la linea" . $e->getLine());
        }
        return $id;
    }

    public function eliminarGrupo($id_grupo) {
        try {

            $consulta = $this->con->query("SELECT * FROM GRUPOINVESTIGACION WHERE Id_Grupo='" . $id_grupo . "'");
            $verificarConsulta = $consulta->fetch(PDO::FETCH_ASSOC);

            if ($verificarConsulta['Id_Grupo'] == $id_grupo) {
                $Query = "DELETE FROM GRUPOINVESTIGACION WHERE Id_Grupo=:id;";
                $resultado = $this->con->prepare($Query);
                $resultado->execute(array(":id" => $id_grupo));
                $Respuesta = TRUE;
            } else {

                $Respuesta = false;
                return $Respuesta;
            }
        } catch (Exception $ex) {
            die("Error: " . $ex->getMessage() . " en la linea " . $ex->getLine());
        }
        return $Respuesta;
    }

    public function actualizarGrupo($id, $grupo, $estado) {
        try {

            $Query = "UPDATE GRUPOINVESTIGACION SET Grupo=:grupo, Id_EstadoI=:estado WHERE Id_Grupo=:idg";
            $resultado = $this->con->prepare($Query);
            $resultado->execute(array(":idg" => $id, ":grupo" => $grupo, ":estado" => $estado));
            $Resp = TRUE;
        } catch (Exception $ex) {
            die('Error: ' . $ex->getMessage() . $ex->getLine());
        }
        return $Resp;
    }

    public function listarTablaGrupo() {
        $query = $this->con->query("SELECT GRUPOINVESTIGACION.Id_Grupo,GRUPOINVESTIGACION.Grupo,GRUPOINVESTIGACION.Id_EstadoI,ESTADOI.Estado 
                                    FROM GRUPOINVESTIGACION,ESTADOI 
                                    WHERE GRUPOINVESTIGACION.Id_EstadoI=ESTADOI.Id_EstadoI AND
                                    GRUPOINVESTIGACION.Id_Grupo != '00';");
        $listaRol = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaRol;
    }
    /*Paginaciòn*/
     public function listarTablaGrupo_paginacion($iniciar,$articulosx) {
        $query = $this->con->query("SELECT GRUPOINVESTIGACION.Id_Grupo,GRUPOINVESTIGACION.Grupo,GRUPOINVESTIGACION.Id_EstadoI,ESTADOI.Estado 
                                    FROM GRUPOINVESTIGACION,ESTADOI 
                                    WHERE GRUPOINVESTIGACION.Id_EstadoI=ESTADOI.Id_EstadoI AND
                                    GRUPOINVESTIGACION.Id_Grupo != '00' LIMIT " . $iniciar . "," . $articulosx . ";");
        $listaRol = $query->fetchAll(PDO::FETCH_ASSOC);

        return $listaRol;
    }
   
    public function listarGrupo() {
        $sql = $this->con->query("SELECT * 
                                  FROM GRUPO 
                                  WHERE Estado='Habilitado';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }
    
    public function verificar_grupo($id_grupo,$grupo,$formulario,$estado) {
    $resul=false;
    
    if($formulario == 0){
        $sql = $this->con->query("SELECT * FROM GRUPOINVESTIGACION WHERE Id_Grupo='".$id_grupo."' OR Grupo='".$grupo."';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        if($resultado['Id_Grupo']==$id_grupo || $resultado['Grupo']==$grupo || strcasecmp($resultado['Grupo'],$grupo)==0 ){
            $resul=false;
        }else{
            $resul=true;
        }
    }else if($formulario == 1){
        $sql = $this->con->query("SELECT * FROM GRUPOINVESTIGACION WHERE Id_Grupo='".$id_grupo."' OR Grupo='".$grupo."';");
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        if(($resultado['Grupo']==$grupo || strcasecmp($resultado['Grupo'],$grupo)==0) && $estado == $resultado['Id_EstadoI']){
            $resul=false;
        }else{
            $resul=true;
        }
    }
       

        return $resul;
    }
        
    //FUNCION NUEVA 20/11/2019
    public function listaGrupohabilitados() {
        $sql = $this->con->query("SELECT * 
                                  FROM GRUPOINVESTIGACION WHERE Id_EstadoI!='05';");
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $resultado;
    }


}


