<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Profesor.php';
$info=$_SESSION['informacion2'];
$id_proyecto=$info['Id_Proyecto'];
$id_proy=$id_proyecto;
$codigo1=$info['Codigo'];
?>

<?php

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!--<link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
        <link rel="stylesheet" href="css/bootstrap.min.css">-->
        
        <script>
            function confirmar()
            {
                var c1 = document.getElementById('Estado_Sustentacion').checked;
                var c2 = document.getElementById('Estado_Sustentacion1').checked;
                if(c1 && c2 || c1 != true && c2 != true )
                {
                    confirm("Seleccione solo un concepto")
                    return false;
                }else{
	            if(confirm("¿Desea continuar?")){
	                return true;
	            }else{
	                return false;
                }}
            }
            function showContent() {
        element = document.getElementById("content");
        check = document.getElementById("Estado_Sustentacion1");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }
</script>
    </head>
    <body>
        
            
    <?php
    $bandera1 = htmlentities(addslashes(filter_input(INPUT_GET, "bandera")));
    $bandera2 = htmlentities(addslashes(filter_input(INPUT_GET, "bandera2")));
    $codigoo='';
    $obj_D_P = new DAO_Proyecto();
    $obj_D_Pro = new DAO_Profesor();
    $codigo2 = $obj_D_P->consultarEstudiante2($id_proyecto,$codigo1);
    $listar = $obj_D_P->listarProyectos($id_proyecto,$codigo1);
    $info_proy = $obj_D_P->InformacionProyecto($id_proyecto);
    $infoJ = $obj_D_P -> consultar_jurados($id_proyecto);
    $cantT = $obj_D_Pro -> cantidad_tut($id_proyecto);
    $confsus = $obj_D_P -> confirmarsus2($id_proyecto);
    
    $codigoo = $codigo2["estudiante2"];
    $buscarE2 = $obj_D_P->Buscar_Codigo_Estudiante($id_proyecto,$codigoo);
    if($estudiante!=null){ ?>
    <div class="container well">
        <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
            <div class="card-header color_cabecera">
                <b><b><center>Información Del Proyecto</center></b></b>
            </div>
                <div class="card-body ">
                    <b><label style="color: black">Título del Proyecto </label></b>
                    <label style="color: black"><?php echo ($listar["Nombre_Proyecto"]);?></label>
                    <b><label style="color: black">Número de radicado </label></b>                    
                    <label style="color: black"><?php echo ($listar["Radicado"]); ?></label>
                    <b><label style="color: black">Fecha de Radicado </label></b>
                    <label style="color: black"><?php echo($listar["Fecha_Radicado"]);?></label>
                    <b><label style="color: black">Fecha de Registro </label></b>
                    <label style="color: black"><?php echo ($listar["Fecha_Registro"]);?></label>
                    
                </div>
                <hr>
                <div class="card-body ">
                    <b><label style="color: black">Comite </label></b>
                  <label style="color:#20A300"><?php echo ($listar["Com_Estado"]);?></label>
                  <?php if($infoJ["jurado01"]== '0' || $infoJ["jurado01"]== '1') {?>
           
                    <?php }else{ ?>
                    <b><label style="color: black">Concepto Jurado <?php echo ($infoJ["profesor1"]); ?></label></b>
                    <?php if(($infoJ["estadoj1"]) == 'Listo para sustentar'){?>
                     <b><i><label style="color:#20A300"><label style="color: black"><?php echo ($infoJ["estadoj1"]);?></label></b></i>
                    <?php }else{?>
                      <b><i><label style="color:#C10000"><label style="color: black"><?php echo ($infoJ["estadoj1"]);?></label></b></i>
                    <?php }}?>
                    <?php if($infoJ["jurado02"]== '0' || $infoJ["jurado02"]== '1') {?>
                
                    <?php }else{ ?>
                    <b><label style="color: black">Concepto Jurado <?php echo ($infoJ["profesor2"]); ?> </label></b>
                    <?php if(($infoJ["estadoj2"]) == 'Listo para sustentar'){?>
                     <b><i><label style="color:#20A300"><label style="color: black"><?php echo ($infoJ["estadoj2"]);?></label></b></i>
                    <?php }else{?>
                      <b><i><label style="color:#C10000"><label style="color: black"><?php echo ($infoJ["estadoj2"]);?></label></b></i>
                    <?php }}?>
                    
                    
                    <b><label style="color: black">Coordinacion </label></b>
                    <label style="color:#20A300"><?php echo ($listar["Coo_Estado"]);?> </label>
                    
                    <!-- Tutor-->
                     <?php if($cantT["cantidad"] == '1')
                            {
                                $listaTut = $obj_D_Pro -> uno_tutor($id_proyecto);
                                
                    ?>
                              <b><label style="color: black">Concepto Tutor <?php echo $listaTut["Nombre"] ;?> </label></b>
                                <?php if($listaTut["EstadoS"] == 'Listo para sustentar') {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["EstadoS"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["EstadoS"]);?> </label></b></i>
                                 <?php }?>
                    <?php
                            }elseif ($cantT["cantidad"] == '2')
                            {
                                $listaTut = $obj_D_Pro -> dos_tutor($id_proyecto);
                    ?>          
                                <b><label style="color: black">Concepto Tutor <?php echo ($listaTut["Nombret1"]);?> </label></b>
                                <?php if(($listaTut["nestado1"]) == "Listo para sustentar") {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["nestado1"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["nestado1"]);?> </label></b></i>
                                 <?php }?>
                                 
                                 <b><label style="color: black">Concepto Tutor <?php echo ($listaTut["Nombret2"]);?> </label></b>
                                 <?php if(($listaTut["nestado2"]) == "Listo para sustentar") {?>
                                <b><i><label style="color:#20A300"><?php echo ($listaTut["nestado2"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($listaTut["nestado2"]);?> </label></b></i>
                                 <?php }?>
                    <?php
                            }
                    
                    ?>    
                    
            
                    <?php
                        if($codigoo == NULL){
                            
                        }else{ ?>
                            <b><label style="color: black">Concepto Estudiante <?php echo ($buscarE2["Nombre"]);?> </label></b>
                            <?php if($buscarE2["EstadoS"] == "Listo para sustentar") {?>
                                <b><i><label style="color:#20A300"><?php echo ($buscarE2["EstadoS"]);?> </label></b></i>
                                <?php }else{?>
                                 <b><i><label style="color:#C10000"><?php echo ($buscarE2["EstadoS"]);?> </label></b></i>
                                 <?php }?>
                        <?php } ?>
                </div>
                <hr>
                <!-- Listo para sustentar-->
               <div class="card-body ">
                   
                    <?php if($listar["Id_EstadoE"] != "01" && $listar["Id_EstadoE"] != "02"){?>
                    <form action="Control/Actualizar_Estado_Sustentacion.php" method="POST" id="form-registro">
                      <div id="content" style="display: none;" > 
                            <b> <label style="color: black" value=NULL > Motivo del aplazamiento</label> </b>
                            <input type ="text" name="comentario">
                        </div>
                    <?php if($bandera1 == 2 )
                    { ?>
                         <b><i><label style="color: black" > El motivo del aplazamiento es obligatorio </label> </i></b>
                    <?php } ?>    
                       <b><label  style="color: black" >Concepto</label></b>
                    <div class="col-md-12 mb-2">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Estado_Sustentacion" name="Estado_Sustentacion" value="01">
                                <label class="custom-control-label" for="Estado_Sustentacion">Listo para sustentar</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="Estado_Sustentacion1" name="Estado_Sustentacion"  onchange="javascript:showContent()" value="02">
                                <label class="custom-control-label" for="Estado_Sustentacion1">Aplazado</label>
                            </div>
                    </div>
                       <input type="submit" onclick="return confirmar()" style="background:#A20C0C" name="Guardar"class="btn btn-primary btn-lg"  value="Guardar Información" >
                        <?php if ($bandera==1){ ?> &nbsp;&nbsp;
                                    <label style="color: black">Información Guardad</label>
                        <?php } ?>
                          <input type="text" name="codigo" placeholder="" value="<?php echo ($codigo1);?>" style="display: none">
                          <input type="text"  id="id_proyecto" name="id_proyecto" placeholder="" value="<?php echo ($id_proyecto);?>" style="display: none">
                    </form>
                    <?php }else if ($listar["Id_EstadoE"] == "02") {?>
                         <b><i><label style="color: black" > Usted ha solicitado aplazar el proyecto, hable con el administrador de la página para habilitar nuevamente el concepto. </label> </i></b>
                </div>
                <div class="card-body ">
                    
                    <?php }
                    $nombre_archivo = $id_proyecto . "R.pdf";
                    if ($info_proy["Nombre_Archivo"] == $nombre_archivo){?>
                        <b><label  style="color: black">El proyecto ya ha sido subido</label></b>
                        <b><label  style="color: black">Fecha </label> </b>
                        <label> <?php echo $info_proy["Fecha_Archivo"];?> </label>    
                        <b><label style="color: black">Ver Documento 
                        <a href="PDF.php?id=<?php echo($id_proy); ?>" target="_blank"><img src="Img/doc.png"></a> </label></b>
                        <?php
                        }else{?>
                            <form action="Control/Subir_Proyecto_Monografia.php" method="POST" id="form-registro" enctype="multipart/form-data" >
                                <b><label  style="color: black">Subir Proyecto:</label><b>
                                <input type="file" name="archivo" aria-describedby="inputGroupFileAddon01" required value="Subir Proyecto" > &nbsp;
                                <?php 
                                if ($bandera2!=NULL){
                                    if($bandera2==1){
                                    ?>
                                        <b><label  style="color: black">Proyecto Subido</label><b>
                                        <?php 
                                        }elseif($bandera2==2){ ?>
                                            <b><label  style="color: black">Error. Intente de nuevo</label></b>
                                        <?php 
                                        }elseif($bandera2==3){ ?>
                                            <b><label  style="color: black">El documento excede el tamaño permitido</label></b>
                                <?php }} ?>
                                <input type="submit" name="Guardar" id="btn" class="btn btn-outline-success" onclick="return confirmar();" value="Subir" >
                                
                                </form>
                        <?php } ?>
                        
                        <?php if($codigoo != NULL){
                                if ($confsus==1){ 
                                    if($listar["Solicitar_Sustentacion"]=="01") {?>
                                    <b><label style="color: black">El (Los) estudiante(s) ya han solicitado sustentación</label></b>
                                    <b><label style="color: black">Fecha de solicitud </label></b>
                                    <labelstyle="color: black"><?php echo $listar["Fecha_Solicitar_Sustentacion"];?></label>
                                <?php
                                }else{ ?>
                                <label style="color: black">El proyecto  <b><i> <font color="#C10000">NO TIENE </font> </b></i> el concepto <font color="#C10000"> <b><i> listo para sustentar</b></i> </font> de todas las partes.</label>
                                
                            <?php 
                            }}}else{  
                                    if ($confsus == 1){ 
                                      if($listar["Solicitar_Sustentacion"]!="01"){?>
                                    
                                        <form action="Control/Solicitar_Sustentacion.php" method="POST" id="form-registro">
                                            <input type="submit" name="Guardar" id="btn" class="btn btn-outline-success" onclick="return confirmar();" value="Solicitar Sustentación" >
                                            <input type="text"  id="id_proyecto" name="Id_Proyecto" placeholder="" value="<?php echo ($id_proyecto);?>" style="display: none">
                                            <input type="text"  id="id_proyecto" name="Estudiante1" placeholder="" value="<?php echo ($codigo1);?>" style="display: none">
                                            <input type="text"  id="id_proyecto" name="Estudiante2" placeholder="" value="<?php echo ($codigoo);?>" style="display: none">
                                            <input type="text"  id="id_proyecto" name="Solicitar_Sustentacion" placeholder="" value="01" style="visibility:hidden">
                                        </form>
                                        <?php
                                    }elseif($listar["Solicitar_Sustentacion"]=="01"){ ?>
                                        <b><label style="color: black">El (Los) estudiante(s) ya han solicitado sustentación</label></b>
                                        <b><label style="color: black">Fecha de solicitud </label></b>
                                        <label> <?php echo $listar["Fecha_Solicitar_Sustentacion"];?></label>
                                    <?php
                                    }else{ ?>
                                        <label style="color: black">El proyecto no ha sido aprobado por todas las partes, no es posiblie solicitar sustentación </label>
                            <?php }}} ?>
                        
                </div>
        </div>
    </div>
        <?php
    }else
    {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }    
    
?>
<br/>
    </body>
</html>
