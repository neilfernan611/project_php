<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
session_start();

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    $datos = $_SESSION["informacion2"]; //Nuevo
    $cedula = $datos['Cedula']; //Nuevo
    $rol = $datos['Id_Rol']; //Nuevo
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
$estado = htmlentities(addslashes(filter_input(INPUT_GET, "estado")));
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Información Anteproyecto/Proyecto</title>
        <link rel="stylesheet" href="css/estilos_ficha.css">

        <?php   
        require_once './Modelo/DAO_Anteproyecto.php';
        require_once './Modelo/DAO_Estudiante.php';
        require_once './Modelo/DAO_Radicado.php';
        ?>
    </head>
    <body>
        
        <?php
        if ($coordinacion != null || $comite != null || $profesor != null) {
            $id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));

            $objProyecto = new DAO_Anteproyecto();
            $objestudiante = new DAO_Estudiante();
            $cantidad = $objestudiante->cantidad_de_proponentes_devueltoC($id_proyecto);
    
            $cant = intval(implode($cantidad));
            ?>
            <div class="container">
                <?php
                if ($cant > 1) {
                    $proyecto = $objProyecto->datosfichadevueltoC2($id_proyecto);
                    ?>
                    
                    <div class="card">
                        <div>
                            <h2><?php echo ($proyecto['Nombre_Proyecto']); ?></h2>
                            <p><b>Proponentes</b></p>
                            <p><?php echo ($proyecto['Codigo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Estudiante_Uno'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Correo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono']); ?></p>
                            <p><?php echo ($proyecto['Codigo_Dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Estudiante_Dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Correo_Dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono_Dos']); ?></p><br>
                            <p><b>Carrera: </b><?php echo($proyecto['Carrera']); ?> </p>
                            <p><b>Modalidad: </b><?php echo($proyecto['Modalidad']); ?> </p>
                            <p><b>Línea de investigación: </b><?php echo($proyecto['Linea']); ?> </p>
                            <p><b>Grupo de investigación: </b><?php echo($proyecto['Grupo']); ?> </p>
                            <p><b>Duración: </b><?php echo($proyecto['Duracion']); ?> </p><br>
                            <p><b>Espacios Académicos:</b><br></p>
                            <?php
                            $cantMateria = $objProyecto->cantidad_de_materias($id_proyecto);
                            $cantM = intval(implode($cantMateria));
                            $materias =  $objProyecto->datosfichasMaterias($id_proyecto);
                            if($cantM>1){
                                foreach($materias as $mate){
                            ?>  
                                    <p><?php echo($mate['Espacio']);?></p>
                            <?php
                                }
                            }else{
                                foreach($materias as $mat){
                            ?>
                                    <p><?php echo($mat['Espacio']); ?></p>
                            <?php        
                                }
                            } ?>
                            <br>
                            <p><b>Evaluador: </b><?php echo($proyecto['Evaluador']); ?> </p><br>
                            
                            <?php
                                $cantDirector = $objProyecto->cantidad_de_directores($id_proyecto);
                                $cantD = intval(implode($cantDirector));
                                if ($cantD > 1) {
                                    $directores = $objProyecto->datosfichasDosDirector($id_proyecto);
                            ?>                  
                                    <p><b>Primer director de proyecto: </b><?php echo($directores['Evaluador_Uno']); ?> </p>
                                    <p><b>Segundo director de proyecto: </b><?php echo($directores['Evaluador_Dos']); ?> </p><br>
                                    <?php
                                } else {
                                    $undirector = $objProyecto->datosfichasUnDirector($id_proyecto);
                                    ?>                   
                                    <p><b>Director de proyecto: </b><?php echo($undirector['Nombre']); ?> </p><br>
                                <?php } ?>
                            
                            <p><b>Resumen ejecutivo:</b></p>
                            <p style="font-size: 19px"><?php echo ($proyecto['Resumen_Ejecutivo']); ?></p><br>
                            
                            <p><b>Objetivo general:</b></p>
                            <p style="font-size: 19px"><?php echo ($proyecto['Objetivo_General']); ?></p><br>
                            
                            <p><b>Objetivos específicos:</b></p>
                            <p style="font-size: 19px"><?php echo ($proyecto['Objetivo_Especifico']); ?></p>
                            
                            <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Documento de anteproyecto</a><br>
                            <hr class="col-md-8 mb-1">
                            <?php
                            if($proyecto['Modalidad']=='Producción académica' /*|| $proyecto['Modalidad']=='Espacios académicos de postgrado' || $proyecto['Modalidad']=='Espacios académicos profundización'*/ || $proyecto['Modalidad']=='Investigación/Innovación'){ ?>
                               
                                <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>&correo=<?phpecho($proyecto["Id_Proyecto"]);?>" target="_blank">Anexo de documentos</a> 
                            <?php } ?>
                        </div>   
                    </div>
                <?php
                } else {
                    $proyecto = $objProyecto->datosfichadevueltoC($id_proyecto);
                ?>
                    <div class="card">
                        <div>
                            <h2><?php echo ($proyecto['Nombre_Proyecto']); ?></h2>
                            <p><b>Proponentes</b></p>
                            <p><?php echo ($proyecto['Codigo']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$proyecto['Estudiante_Uno'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Correo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono']); ?></p><br>
                            <p><b>Carrera: </b><?php echo($proyecto['Carrera']); ?></p>
                            <p><b>Modalidad: </b><?php echo($proyecto['Modalidad']); ?></p>
                            <p><b>Línea de investigación: </b><?php echo($proyecto['Linea']); ?></p>
                            <p><b>Grupo de investigación: </b><?php echo($proyecto['Grupo']); ?></p>
                            <p><b>Duración: </b><?php echo($proyecto['Duracion']); ?></p><br>
                            
                            <p><b>Espacios Académicos:</b><br></p>
                            <?php
                            $cantMateria = $objProyecto->cantidad_de_materias($id_proyecto);
                            $cantM = intval(implode($cantMateria));
                            $materias =  $objProyecto->datosfichasMaterias($id_proyecto);
                            if($cantM>1){
                                foreach($materias as $mate){
                            ?>  
                                    <p><?php echo($mate['Espacio']);?></p>
                            <?php
                                }
                            }else{
                                foreach($materias as $mat){
                            ?>
                                    <p><?php echo($mat['Espacio']); ?></p>
                            <?php        
                                }
                            } ?>
                            <br>
                            <p><b>Evaluador: </b><?php echo($proyecto['Evaluador']); ?></p><br>
                            
                            <?php
                                $cantidadDirector = $objProyecto->cantidad_de_directores($id_proyecto);
                                $cantDi = intval(implode($cantidadDirector));
                                    if ($cantDi > 1) {
                                        $directores = $objProyecto->datosfichasDosDirector($id_proyecto);
                            ?>          
                                        <p><b>Directores de proyecto </b></p>
                                        <p><?php echo($directores['Evaluador_Uno']); ?></p>
                                        <p><?php echo($directores['Evaluador_Dos']); ?></p><br>
                                    <?php
                                    } else {
                                        $undirector = $objProyecto->datosfichasUnDirector($id_proyecto);
                                    ?>
                                        <p><b>Director de proyecto </b></p>
                                        <p><?php echo($undirector['Nombre']); ?></p><br>
                                    <?php } ?>
                            
                            <p><b>Resumen ejecutivo:</b></p>
                            <p style="font-size:19px"><?php echo ($proyecto['Resumen_Ejecutivo']); ?></p><br>
                            
                            <p><b>Objetivo general:</b></p>
                            <p style="font-size:19px"><?php echo ($proyecto['Objetivo_General']); ?></p><br>
                            
                            <p><b>Objetivos Especificos:</b></p>
                            <p style="font-size:19px"><?php echo ($proyecto['Objetivo_Especifico']); ?></p>
                            
                            <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Documento del anteproyecto</a><br>
                                <hr class="col-md-8 mb-1">
                            <?php
                                if($proyecto['Modalidad']=='Producción académica' /*|| $proyecto['Modalidad']=='Espacios académicos de postgrado' || $proyecto['Modalidad']=='Espacios académicos profundización'*/ || $proyecto['Modalidad']=='Investigación/Innovación'){ ?>
                               
                                    <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>&correo=<?php echo($proyecto["Id_Proyecto"])?>" target="_blank">Anexo de documentos</a> 
                            <?php } ?>
                        </div>   
                    </div>
                <?php }
                ?>
            </div>
        <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>


