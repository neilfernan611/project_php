<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (!filter_input(INPUT_POST, "bot_grupo")) {

    $id = filter_input(INPUT_GET, "idg");
    $grupo = filter_input(INPUT_GET, "grupo");
    $estado = filter_input(INPUT_GET, "estado");
}


if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta charset="gb18030">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <br>
        <?php
        if ($administrador != null) {
            ?>

            <form action="Control/Editar_Grupo.php" method="POST">
                <table class="table" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">
                        <tr>
                            <th scope="col">Código</th>
                            <th scope="col">Grupo</th>
                            <th scope="col">Estado</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tr>


                        <th scope="col"><?php echo $id ?> </th>
                        <td  hidden>><input type="hidden" name="id" value="<?php echo $id ?>"></td>


                        <td>
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <input type="text" class="form-control"  id="linea" name="gru" placeholder="" value="<?php echo $grupo; ?>" required value="<?php echo $grupo ?>"> 
                                    <div class="invalid-feedback">
                                        Completa este campo
                                    </div>

                                </div>

                            </div>
                        </td>

                        <td>
                            <div class="row">



                                <div class="col-md-5 mb-3">

                                    <select id="selectTipo" class="custom-select"  value="<?php echo $estado; ?>" required name="estado" size='1' class='centrado' required>
                                        <?php if ($estado == "Deshabilitado") { ?>


                                            <option required value="05"><?php echo('Deshabilitado'); ?></option>
                                            <option required value="04"><?php echo('Habilitado'); ?></option>
                                        <?php } else {
                                            ?>

                                            <option required value="04"><?php echo('Habilitado'); ?></option>
                                            <option required value="05"><?php echo('Deshabilitado'); ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                    <div class="invalid-feedback">
                                        Campo obligatorio.
                                    </div>
                                </div>

                            </div>
                        </td>


                        <td colspan="2"><input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR" name="bot_grupo" value="Actualizar"></td>
                    </tr>

                </table>
            </form>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>


