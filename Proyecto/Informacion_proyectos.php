<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
session_start();

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
    $datos = $_SESSION["informacion2"]; //Nuevo
    $cedula = $datos['Cedula']; //Nuevo
    $rol = $datos['Id_Rol']; //Nuevo
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
$estado = htmlentities(addslashes(filter_input(INPUT_GET, "estado")));
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Información Anteproyecto/Proyecto</title>
        <link rel="stylesheet" href="css/estilos_ficha.css">

        <?php   
            require_once './Modelo/DAO_Anteproyecto.php';
            require_once './Modelo/DAO_Estudiante.php';
            require_once './Modelo/DAO_Radicado.php';
        ?>
    </head>
    <body>
        <?php
        if ($coordinacion != null || $comite != null || $profesor != null) {
           
            $id_proyecto = htmlentities(addslashes(filter_input(INPUT_GET, "id")));
            $proy = new DAO_Anteproyecto();
            $estado2=$proy->estado_proyecto($id_proyecto);
                     
            $objProyecto = new DAO_Anteproyecto();
            $objestudiante = new DAO_Estudiante();
            $cantidad = $objestudiante->cantidad_de_proponentes($id_proyecto);
            $objetoradicado= new DAO_Radicado();
    
            $cant = intval(implode($cantidad));

            if ($cant > 1) {
                $proyecto = $objProyecto->datosficha2($id_proyecto);
            ?>
                <div class="container">
                    <div class="card">
                        <div>
                            <h2><?php echo ($proyecto['Nombre_Proyecto']); ?></h2>
                            <p><b>Proponentes</b></p>
                            <p><?php echo ($proyecto['Codigo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Estudiante_Uno'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Correo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono']); ?></p>
                            <p><?php echo ($proyecto['Codigo_Dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Estudiante_Dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['correo_dos'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono_Dos']); ?></p><br>
                                
                            <p><b>Carrera: </b><?php echo($proyecto['Carrera']); ?> </p>
                            <p><b>Modalidad: </b><?php echo($proyecto['Modalidad']); ?></p>
                            <p><b>Línea de investigación: </b><?php echo($proyecto['Linea']); ?> </p>
                            <p><b>Grupo de investigación: </b><?php echo($proyecto['Grupo']); ?> </p>
                            <p><b>Duración: </b><?php echo($proyecto['Duracion']); ?> </p><br>
                                
                            <p><b>Espacios académicos</b></p>
                            <?php
                                $cantMateria = $objProyecto->cantidad_de_materias($id_proyecto);
                                $cantM = intval(implode($cantMateria));
                                $materias =  $objProyecto->datosfichasMaterias($id_proyecto);
                                if($cantM>1){
                                    foreach($materias as $mate){
                            ?>  
                                        <p><?php echo($mate['Espacio']); ?></p>
                                <?php }
                                }else{
                                    foreach($materias as $mat){
                                ?>        
                                        <p><?php echo($mat['Espacio']); ?></p>
                                <?php }
                                } 
                                ?>
                            <br>
                            <p><b>Evaluador: </b><?php echo($proyecto['Evaluador']); ?> </p><br>
                                
                            <?php
                                $cantDirector = $objProyecto->cantidad_de_directores($id_proyecto);
                                $cantD = intval(implode($cantDirector));
                                if ($cantD > 1) {
                                    $directores = $objProyecto->datosfichasDosDirector($id_proyecto);
                            ?>      
                                    <p><b>Directores de proyecto </b></p>
                                    <p><?php echo($directores['Evaluador_Uno']); ?></p>
                                    <p><?php echo($directores['Evaluador_Dos']); ?></p><br>
                            <?php 
                                } else {
                                    $undirector = $objProyecto->datosfichasUnDirector($id_proyecto);
                            ?>      
                                    <p><b>Director de proyecto</b></p>
                                    <p><?php echo($undirector['Nombre']); ?></p><br>
                            <?php  } 
                            ?>
                            <?php
                                $radicadov = $objetoradicado->verRadicadoProy($id_proyecto);
                                if ($radicadov['Radicado'] != NULL) {
                            ?>
                                    <p><b>Radicado: </b><?php echo($radicadov['Radicado']); ?> </p>
                                    <p><b>Fecha del radicado: </b><?php echo($radicadov['Fecha_Radicado']); ?> </p>
                                    <p><b>Duración asignada: </b><?php echo($radicadov['Duracion']); ?> </p><br>
                            <?php } ?>
                            
                            <p><b>Resumen ejecutivo:</b></p>
                                <p style="font-size: 19px"><?php echo ($proyecto['Resumen_Ejecutivo']); ?></p><br>
                            <p><b>Objetivo general:</b></p>
                                <p style="font-size: 19px"><?php echo ($proyecto['Objetivo_General']); ?></p><br> 
                            <p><b>Objetivos específicos:</b></p>
                                <p style="font-size: 19px"><?php echo ($proyecto['Objetivo_Especifico']); ?></p>
                                
                            <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Documento de Anteproyecto</a><br>
                                <hr class="col-md-8 mb-1">
                            <?php
                                if($proyecto['Modalidad']=='Producción académica' /*|| $proyecto['Modalidad']=='Espacios académicos de postgrado' || $proyecto['Modalidad']=='Espacios académicos profundización'*/ || $proyecto['Modalidad']=='Investigación/Innovación'){ ?>
                                    <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>&correo=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Anexo Documentos</a> 
                            <?php }
                            ?>
                        </div>   
                    </div>
                </div>
                <!--Formulario-->
                <?php
                    if ($profesor != null || $comite != null && ($estado2['Estado_Evaluador']=='00' || $estado2['Estado_Comite']=='00')) {
                        if ($estado == "calificar") {
                            require_once './Modelo/DAO_Anteproyecto.php';
                            $pdf = new DAO_Anteproyecto();
                            $objp = new DAO_Anteproyecto();
                            $proy = new DAO_Anteproyecto();
                ?>
                            <div class="container-aval">    
                                <div class="card">
                                    <div>
                                        <h2>AVAL DEL ANTEPROYECTO</h2>
                                            <form action="Control/Actualizar_Anteproyecto.php" method="post"  enctype="multipart/form-data" >
                                                <input type="text" value="<?php echo ($id_proyecto); ?>" name="id_proyecto" id="id_proyecto"  hidden>
                                                    <div class="row">
                                                        <?php
                                                            if ($profesor != null && ($estado2['Estado_Evaluador']=='00')) {
                                                        ?>
                                                                <label for="concepto">Concepto</label>
                                                                <select id="selectTipo" class="selector" required name="estado">
                                                                    <option selected required> </option>
                                                                    <?php
                                                                        $listaest = $objp->Consultar_Estados_comite();
                                                                        foreach ($listaest as $list) {
                                                                    ?>
                                                                            <option required value="<?php echo($list['Id_Estado']); ?>"><?php echo($list['Estado']); ?></option>
                                                                    <?php   }
                                                                    ?>
                                                                </select>

                                                                <label for="comentario" >Comentario</label> <br>
                                                                <textarea class="comentario" id="comentario" name="comentario" placeholder="Deposite aquí su comentario" cols="20" rows="6" value="" ></textarea>
                                                        <?php 
                                                            } else if ($comite != NULL && ($estado2['Estado_Evaluador']=='00' || $estado2['Estado_Comite']=='00' || $estado2['Estado_Coordinacion']=='00' )) {
                                                        ?>
                                                                <label for="concepto"><p>Concepto</p></label>
                                                                <select id="selectTipo" class="selector" required name="estado">
                                                                    <option selected required> </option>
                                                                    <?php
                                                                        $listaest = $objp->Consultar_Estados_comite();
                                                                        foreach ($listaest as $list) {
                                                                    ?>
                                                                            <option required value="<?php echo($list['Id_Estado']); ?>"><?php echo($list['Estado']); ?></option>
                                                                        <?php }
                                                                        ?>
                                                                </select>
                                                                        
                                                                <label for="comentario" ><p>Comentario</p></label>
                                                                <textarea class="comentario" id="comentario" name="comentario" placeholder="Deposite aquí su comentario" cols="20" rows="6" value=""></textarea>
                                                                        
                                                                <label for="fecha" ><p>Fecha de sesión del comité</p></label>
                                                                <input type="date" id="fecha" name="fech">
                                                        <?php  }
                                                                //Boton
                                                            if( ($profesor!=null && $estado2['Estado_Evaluador']=='00') || ($comite!=null && $estado2['Estado_Comite']=='00') || ($coordinacion!=null && $estado2['Estado_Coordinacion']=='00')){
                                                        ?>
                                                                <center><button type="submit" class='boton' id="btn-modificar">Enviar</button></center>
                                                        <?php }
                                                        ?>
                                                    </div>
                                            </form>
                                    </div>   
                                </div>
                            </div>    
            <?php       } 
                    }
            } else {
                $proyecto = $objProyecto->datosficha($id_proyecto);
            ?>
                <div class="container">
                    <div class="card">
                        <div>
                            <h2><?php echo ($proyecto['Nombre_Proyecto']); ?></h2>
                            <p><b>Proponentes</b></p>
                            <p><?php echo ($proyecto['Codigo']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$proyecto['Estudiante_Uno'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Correo'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " . $proyecto['Telefono']); ?><br></p><br>
                            <p><b>Carrera: </b><?php echo($proyecto['Carrera']); ?></p>
                            <p><b>Modalidad: </b><?php echo($proyecto['Modalidad']); ?></p>
                            <p><b>Línea de investigación: </b><?php echo($proyecto['Linea']); ?></p>
                            <p><b>Grupo de investigación: </b><?php echo($proyecto['Grupo']); ?></p>
                            <p><b>Duración: </b><?php echo($proyecto['Duracion']); ?></p><br>
                                
                            <p><b>Espacios Académicos</b></p>
                            <?php
                                $cantMateria = $objProyecto->cantidad_de_materias($id_proyecto);
                                $cantM = intval(implode($cantMateria));
                                $materias =  $objProyecto->datosfichasMaterias($id_proyecto);
                                if($cantM>1){
                                    foreach($materias as $mate){
                            ?>  
                                        <p><?php echo($mate['Espacio']);?></p>
                                <?php }
                                }else{
                                    foreach($materias as $mat){
                                ?>        
                                        <p><?php echo($mat['Espacio']); ?></p>
                                <?php  }
                                } 
                                ?>
                            <br>
                            <p><b>Evaluador: </b><?php echo($proyecto['Evaluador']); ?> </p><br>
                                
                            <?php
                                $cantidadDirector = $objProyecto->cantidad_de_directores($id_proyecto);
                                $cantDi = intval(implode($cantidadDirector));
                                if ($cantDi > 1) {
                                    $directores = $objProyecto->datosfichasDosDirector($id_proyecto);
                            ?>   
                                    <p><b>Directores de proyecto </b></p>
                                    <p><?php echo($directores['Evaluador_Uno']); ?></p>
                                    <p><?php echo($directores['Evaluador_Dos']); ?></p><br>
                                <?php
                                } else {
                                    $undirector = $objProyecto->datosfichasUnDirector($id_proyecto);
                                ?>
                                    <p><b>Director de proyecto </b></p>
                                    <p><?php echo($undirector['Nombre']); ?></p><br>
                                <?php } 
                                ?>
                            <?php
                                $radicadov = $objetoradicado->verRadicadoProy($id_proyecto);
                                if ($radicadov['Radicado'] != NULL) {
                            ?>
                                    <p><b>Radicado: </b><?php echo($radicadov['Radicado']); ?> </p>
                                    <p><b>Fecha del radicado: </b><?php echo($radicadov['Fecha_Radicado']); ?></p>
                                    <p><b>Duración asignada: </b><?php echo($radicadov['Duracion']); ?></p><br>
                            <?php } ?>
                                    
                            <p><b>Resumen ejecutivo:</b></p>
                                <p style="font-size:19px"><?php echo ($proyecto['Resumen_Ejecutivo']); ?></p><br>
                            <p><b>Objetivo general:</b></p>
                                <p style="font-size:19px"><?php echo ($proyecto['Objetivo_General']); ?></p><br>
                            <p><b>Objetivos específicos:</b></p>
                                <p style="font-size:19px"><?php echo ($proyecto['Objetivo_Especifico']); ?></p>
                                    
                            <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Documento de Anteproyecto</a><br>
                                <hr class="col-md-8 mb-1">
                            <?php
                                if($proyecto['Modalidad']=='Producción académica' /*|| $proyecto['Modalidad']=='Espacios académicos de postgrado' || $proyecto['Modalidad']=='Espacios académicos profundización'*/ || $proyecto['Modalidad']=='Investigación/Innovación'){ ?>
                                    <a href="PDF.php?id=<?php echo($proyecto["Id_Proyecto"]); ?>&correo=<?php echo($proyecto["Id_Proyecto"]); ?>" target="_blank">Anexo Documentos</a> 
                            <?php } 
                            ?>    
                        </div>   
                    </div>
                </div>    
                <!--Formulario -->
                <?php
                    if ($profesor != null || $comite != null && ($estado2['Estado_Evaluador']=='00' || $estado2['Estado_Comite']=='00')) {
                        if ($estado == "calificar") {
                            require_once './Modelo/DAO_Anteproyecto.php';
                            $pdf = new DAO_Anteproyecto();
                            $objp = new DAO_Anteproyecto();
                            $proy = new DAO_Anteproyecto();
                ?>
                            <div class="container-aval">    
                                <div class="card">
                                    <div>
                                        <h2>AVAL DEL ANTEPROYECTO</h2>
                                            <form action="Control/Actualizar_Anteproyecto.php" method="post"  enctype="multipart/form-data" >
                                                <input type="text" value="<?php echo ($id_proyecto); ?>" name="id_proyecto" id="id_proyecto"  hidden>
                                                    <div class="row">
                                                        <?php
                                                            if ($profesor != null && ($estado2['Estado_Evaluador']=='00')) {
                                                        ?>
                                                                <label for="concepto">Concepto</label>
                                                                <select id="selectTipo" class="selector" required name="estado">
                                                                    <option selected required> </option>
                                                                    <?php
                                                                        $listaest = $objp->Consultar_Estados_comite();
                                                                        foreach ($listaest as $list) {
                                                                    ?>
                                                                            <option required value="<?php echo($list['Id_Estado']); ?>"><?php echo($list['Estado']); ?></option>
                                                                        <?php }
                                                                        ?>
                                                                </select>

                                                                <label for="comentario" >Comentario</label> <br>
                                                                <textarea class="comentario" id="comentario" name="comentario" placeholder="Deposite aquí su comentario" cols="20" rows="6" value="" ></textarea>
                                                        <?php   
                                                            } else if ($comite != NULL && ($estado2['Estado_Evaluador']=='00' || $estado2['Estado_Comite']=='00' || $estado2['Estado_Coordinacion']=='00' )) {
                                                        ?>
                                                                <label for="concepto"><p>Concepto</p></label>
                                                                <select id="selectTipo" class="selector" required name="estado">
                                                                    <option selected required> </option>
                                                                        <?php
                                                                            $listaest = $objp->Consultar_Estados_comite();
                                                                            foreach ($listaest as $list) {
                                                                        ?>
                                                                                <option required value="<?php echo($list['Id_Estado']); ?>"><?php echo($list['Estado']); ?></option>
                                                                            <?php }
                                                                            ?>
                                                                </select>
                                                                        
                                                                <label for="comentario" ><p>Comentario</p></label>
                                                                <textarea class="comentario" id="comentario" name="comentario" placeholder="Deposite aquí su comentario" cols="20" rows="6" value=""></textarea>
                                                                        
                                                                <label for="fecha" ><p>Fecha de sesión del comité</p></label>
                                                                <input type="date" id="fecha" name="fech">
                                                        <?php   }
                                                            //Boton
                                                            if( ($profesor!=null && $estado2['Estado_Evaluador']=='00') || ($comite!=null && $estado2['Estado_Comite']=='00') || ($coordinacion!=null && $estado2['Estado_Coordinacion']=='00')){
                                                        ?>
                                                                <center><button type="submit" class='boton' id="btn-modificar">Enviar</button></center>
                                                        <?php }
                                                        ?>
                                                    </div>
                                            </form>
                                    </div>   
                                </div>
                            </div>    
        <?php           }
                    }
            }
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>


