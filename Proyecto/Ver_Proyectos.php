<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
require_once './home.php';
require_once './Modelo/DAO_Proyecto.php';
require_once './Modelo/DAO_Profesor.php';
$info=$_SESSION['informacion2'];
$cedula=$info['Cedula'];
$idrol = htmlentities(addslashes(filter_input(INPUT_GET, "idrol")));
?>

<?php

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">

    </head>
    <body>
       
<?php
       
if($profesor!=null){
    $proy = new DAO_Proyecto();
    $Obj_D_P = new DAO_Profesor();
    $cantidadT = $Obj_D_P ->Cantidad_P_Tutor($cedula);
    $cantidadJ = $Obj_D_P ->Cantidad_P_Jurado($cedula);
?>
<div class="container">
   <br>
   <?php
        if($idrol =='05'){
            if($cantidadT["cantidad"] != 0 ){?>
            <table class="table table-bordered" >
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><center>Título del Proyecto</th>
                    <th scope="col"><center>Radicado</th>
                    <th scope="col"><center>Estado</th>
                    <th scope="col"><center>Ver Información</th>
                </tr>
                </thead>
            <?php
            $lista = $proy->Consultar_Proyectos_T($cedula);
            foreach ($lista as $list){?>
                <tbody>
                    <tr>
                        <td><center> <?php echo($list["Nombre_Proyecto"]); ?> </td>
                        <td><center><?php echo($list["Radicado"]); ?> </td>
                        <td><center><?php echo($list["EstadoS"]); ?> </td>
                        <td class="bot"><center><a href="Ver_Todo_T.php?&id=<?php echo($list["Id_Proyecto"]);?>"><img src="Img/ver.png"></a></td>
                    </tr> 
                </tbody>
            
        <?php }}else{ ?>
        </table>
                <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
                    <div class="card-body ">
                        <p><center><h6>NO TIENE PROYECTOS ASIGNADOS COMO TUTOR</h6></center></p><hr>
                    </div>
                </div>
        <?php }}elseif($idrol =='01'){
            if($cantidadJ["cantidad"] != 0 ){?>
            <table class="table table-bordered" >
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><center>Título del Proyecto</th>
                    <th scope="col"><center>Radicado</th>
                    <th scope="col"><center>Estado</th>
                    <th scope="col"><center>Ver Información</th>
                </tr>
                </thead>
        <?php
        $lista = $proy->Consultar_Proyectos_J($cedula);
        $temporal = '0';
            foreach ($lista as $list){ 
                if($temporal != $list["Radicado"]){?>
                    <tbody>
                        <tr>
                            <td><center><?php echo($list["Nombre_Proyecto"]); ?> </td>
                            <td><center><?php echo($list["Radicado"]); ?> </td>
                            <td><center><?php echo($list["EstadoS"]); ?> </td>
                            <td class="bot"><center><a href="Ver_Todo_J.php?&id=<?php echo($list["Radicado"]);?>" > <img src="Img/ver.png"></a></td>
                        </tr>                 
                    </tbody>
            
          <?php } $temporal = $list["Radicado"]; 
        }}else{ ?>
        </table>
                <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
                    <div class="card-body ">
                        <p><center><h6>NO TIENE PROYECTOS ASIGNADOS COMO JURADO</h6></center></p><hr>
                    </div>
                </div>
                    
                    
<?php }}?>
    
    
     
</div>
<?php }else{
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }?>
    </body>
</html>
