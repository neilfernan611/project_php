
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
require_once './Modelo/DAO_Modalidad.php';
require_once './Modelo/DAO_LineaInvestigacion.php';
require_once './Modelo/DAO_Estudiante.php';
require_once './Modelo/DAO_Grupo.php';
require_once './Modelo/DAO_Profesor.php';
require_once './Modelo/DAO_Anteproyecto.php';

$informacion = $_SESSION['informacion2'];
$carrera = $informacion['Id_Carrera'];
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

        <script>
            $(document).ready(function () {
                $("input[type=radio]").click(function (event) {
                    var valor = $(event.target).val();
                    if (valor == "no") {
                        $("#div1").show();
                        $("#div2").hide();
                    } else if (valor == "si") {
                        $("#div1").hide();
                        $("#div2").show();
                    } else {
                        // Otra cosa
                    }
                });
            });
        </script>
        <script>
            function confirmar() {
                if (!confirm('¿Está seguro de enviar el anteproyecto? Verifique si su archivo está completo de lo contrario no podra realizar modificaciones en la inscripción'))
                    return false;
            }
        </script>
        
    </head>
    <body>
        <?php
        if ($estudiante != null) {
        ?>
            <div class="py-5 text-center">
                <h5>REGISTRO DEL ANTEPROYECTO EN PRODUCCIÓN ACADÉMICA</h5> 
                <p><center>Ingrese la información en todos los campos</center></p>
            </div>
            <div class="col-md-8 order-md-1 container">
                <form action="Control/Agregar_Produccion_Academica.php" method="POST" id="form-registro" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="firstName" style="color: black">Título del anteProyecto</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" required>
                            <div class="invalid-feedback">
                                El título es obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectTipo" >Grupo De Investigación</label>
                            <select id="selectTipo" class="custom-select" required name="grupo">
                                <option selected required></option>
                                <?php
                                $obj_gr = new DAO_Grupo();
                                $listagr = $obj_gr->listaGrupohabilitados();
                                foreach ($listagr as $list) {
                                ?>
                                    <option required value="<?php echo($list['Id_Grupo']); ?>"><?php echo($list['Grupo']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectTipo" >Línea De Investigacón</label>
                            <select id="selectTipo" class="custom-select"  required name="linea">
                            <option selected required></option>
                            <?php
                                $obj_li = new DAO_LineaInvestigacion();
                                $lista_li = $obj_li->listaLineaHabilitada();
                                foreach ($lista_li as $list2) {
                                ?>
                                    <option value="<?php echo($list2['Id_Linea']); ?>" required><?php echo($list2['Linea']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectTipo" >Director de proyecto</label>
                            <select id="selectTipo" class="custom-select" required name="tutor">
                                <option selected></option>
                                <?php
                                $obj_tu = new DAO_Estudiante();
                                $lista_tu = $obj_tu->ListaTutor();
                                foreach ($lista_tu as $list) {
                                ?>
                                    <option value="<?php echo($list['Cedula']); ?>" required><?php echo($list['Nombre']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectTipo" >Evaluador</label>
                            <select id="selectTipo" class="custom-select" required name="evaluador">
                                <option selected></option>
                                <?php
                                $obj_e = new DAO_Profesor();
                                $lista_e = $obj_e->listarEvaluadorProduccion();
                                foreach ($lista_e as $lista) {
                                ?>
                                    <option value="<?php echo($lista['Cedula']); ?>" required><?php echo($lista['Nombre']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectTipo" >Duración</label>
                            <select id="selectTipo" class="custom-select" required name="duracion">
                                <option selected></option>
                                <?php
                                $obj_d = new DAO_Anteproyecto();
                                $lista_d = $obj_d->Duracion();
                                foreach ($lista_d as $list) {
                                ?>
                                    <option value="<?php echo($list['Id_Duracion']); ?>" required><?php echo($list['Duracion']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <input type="hidden" class="form-control" id="nombre" name="modalidad" value="208" min="1" maxlength="11" pattern="^[0-9]+" required>                       
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="firstName" style="color: black">Resumen Ejecutivo</label>
                            <textarea class="form-control" id="nombre" name="resumen" rows="5" placeholder="" value="" required></textarea>
                            <div class="invalid-feedback">
                                Campo requerido.
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="firstName" style="color: black">Objetivo General</label>
                            <textarea class="form-control" id="nombre" rows="2" name="objetivo_general" placeholder="" value="" required></textarea>
                            <div class="invalid-feedback">
                                Campo requerido.
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="firstName" style="color: black">Objetivos Específicos</label>
                            <textarea class="form-control" id="nombre" rows="5" name="objetivos" placeholder="" value="" required></textarea>
                            <div class="invalid-feedback">
                                Campo requerido.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="input-group-prepend">
                                <label for="firstName" style="color: black">Documento PDF</label>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="archivo" id="real-file" hidden="hidden" required />
                                <button type="button" id="custom-button">Subir Documento</button>
                                <span id="custom-text">Ningún archivo elegido, todavía.</span>
                            </div>
                        </div>    
                        <div class="col-md-6 mb-3">
                            <div class="input-group-prepend">
                                <label for="firstName" style="color: black">Carta a Consejo PDF</label>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="carta_c" id="real-filed" hidden="hidden" required />
                                <button type="button" id="custom-buttond">Subir Documento</button>
                                <span id="custom-textd">Ningún archivo elegido, todavía.</span>
                            </div>
                        </div>
                    <?php
                    if ($carrera == '4828') {
                    } else {
                    ?>
                        <div class="col-md-6 mb-3">
                            <label for="firstName" style="color: black">Espacios Académicos</label>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="Trabajo1" value="02" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Trabajo de grado 1</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="Trabajo2" value="03" class="custom-control-input" id="customCheck2">
                                <label class="custom-control-label" for="customCheck2">Trabajo de grado 2</label>
                            </div>
                        </div>
                    <?php } ?>
                    
                        <div class="col-md-6 mb-3">
                            <label for="firstName" style="color: black">¿Trabaja el anteproyecto con otro estudiante?</label>
                            <br>
                            <div class="custom-control custom-radio">
                                <input name="radioc" type="radio" value="si"/>
                                <label for="firstName" style="color: black">Sí</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input checked="checked" name="radioc" type="radio" value="no"/>
                                <label for="firstName" style="color: black">No</label>
                            </div>
                            <div id="div2" style="display:none;">
                                <label for="firstName" style="color: black">Código del segundo estudiante</label>
                                <input type="number" class="form-control" id="nombre" name="codigo2" min="1" maxlength="11" pattern="^[0-9]+">
                            </div>
                        </div>
                    </div>
                <hr class="col-md-8 mb-1">
                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit" onclick="return confirmar()">Enviar Información</button>
            </form>
        </div>
    <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>
    <script>
        const realFileBtn = document.getElementById("real-file");
        const customBtn = document.getElementById("custom-button");
        const customTxt = document.getElementById("custom-text");

        customBtn.addEventListener("click", function () {
            realFileBtn.click();
        });

        realFileBtn.addEventListener("change", function () {
            if (realFileBtn.value) {
                customTxt.innerHTML = realFileBtn.value.match(
                        /[\/\\]([\w\d\s\.\-\(\)]+)$/
                        )[1];
            } else {
                customTxt.innerHTML = "No file chosen, yet.";
            }
        });
    </script>
    
    <script>
        const realFileBtn2 = document.getElementById("real-filed");
        const customBtn2 = document.getElementById("custom-buttond");
        const customTxt2 = document.getElementById("custom-textd");

        customBtn2.addEventListener("click", function () {
            realFileBtn2.click();
        });

        realFileBtn2.addEventListener("change", function () {
            if (realFileBtn2.value) {
                customTxt2.innerHTML = realFileBtn2.value.match(
                        /[\/\\]([\w\d\s\.\-\(\)]+)$/
                        )[1];
            } else {
                customTxt2.innerHTML = "No file chosen, yet.";
            }
        });
    </script>
    </body>
</html>
