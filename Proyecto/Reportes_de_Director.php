<!DOCTYPE html>
<?php
require_once'./home.php';
require_once './Modelo/DAO_Estudiante.php';
?>
<html>
    <head><meta charset="gb18030">
        
        <title></title>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/plotly-latest.min.js"></script>
         <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
  <?php
       if($coordinacion=!null || $comite=!null){
       ?>
       <br>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">Ingrese la información</div><br>
                        <div class="panel panel-body">
                            <form method="POST" action="Control/exportar_director.php">
                            <div class="row">
                                <div class="col-sm-6 mb-6">
                                    <label for="selectTipo" >Fecha desde</label>
                                    <input type="date" class="custom-select" id="fechadesde"  name="fechadesde" required="">
                                </div>
                                <div class="col-sm-6 mb-6">
                                    <label for="selectTipo" >Fecha hasta</label>
                                    <input type="date" class="custom-select" id="fechahasta"  name="fechahasta" required="">
                                </div>
                                 <div class="col-md-6 mb-6">
                            <label for="selectTipo" >Docente</label>
                            <select  class="custom-select" id="tutor" name="tutor" required>
                                <option selected required ></option>
                                <?php
                                $obj_tu = new DAO_Estudiante();
                                $lista_tu = $obj_tu->ListaTutor();
                                foreach ($lista_tu as $list) {
                                ?>
                                    <option value="<?php echo($list['Cedula']); ?>" required><?php echo($list['Nombre']); ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">
                                Campo obligatorio.
                            </div>
                        </div>
                                <br>
                                <div><br><input type="submit" class="btn btn-primary mx-sm-6 mb-6" id="btn-aceptarR" onclick="ajax_post();" value="buscar"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">Grafica</div>
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div id="cargar_barras"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div id="cargar_circular">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <script type="text/javascript">


              function ajax_post() {
                var ajax;
                if (window.XMLHttpRequest) {
                    ajax = new XMLHttpRequest();
                } else {
                    ajax = new ActiveXObject("Microsoft.XMLHTTP");

                }
                var fechad = document.getElementById("fechadesde").value;
                var fechaa = document.getElementById("fechahasta").value;
                var tutor  = document.getElementById("tutor").value;
                ajax.onreadystatechange = function () {
                    if (ajax.readyState === 4 && ajax.status === 200) {
                     
                    }
                };

                $('#cargar_barras').load("Control/grafico_Director.php?fechadesde="+fechad+"&fechahasta="+fechaa+"&tutor="+tutor);    
                $('#cargar_circular').load("Control/grafico_circular_Director.php?fechadesde="+fechad+"&fechahasta="+fechaa+"&tutor="+tutor);    

            }

        </script>

<?php
}else{
    session_destroy();
            echo "<script>location.href='index.php';</script>";
}

?>
    </body>
</html>
