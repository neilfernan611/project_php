<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
require_once './Modelo/DAO_Cronograma.php';
require_once './Modelo/DAO_Proyecto.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}



?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
        <link rel="stylesheet" href="css/estilos_ficha.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
         <script>
              function confirmar()
            {
                if(confirm("¿Desea continuar?")){
	                return true;
	            }else{
	                return false;
                }
            }
                   
        </script>
    </head>
    <body>
    <?php
    $bandera = htmlentities(addslashes(filter_input(INPUT_GET, "bandera")));
    $obj_ca = new DAO_Proyecto();
    if($comite!=null || $coordinacion!=null){
    $cantidad = $obj_ca ->Cantidad_proyectos_Faltante_FechaS();
        if($cantidad["cantidad"] != 0){?>
    
        <div class="container well" >
            <b><p><center><h4>Ingrese todos los datos solicitados</h4></center></p></b>
            <table class="table table-bordered" >
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"><center>Nombre Proyecto</center></th>
                            <th scope="col"><center>Fecha Sustentación</center></th>
                            <th scope="col"><center>Lugar</center></th>
                            <th scope="col"><center>Hora</center></th>
                            <th scope="col"><center>Asignar Fecha</th>
                        </tr>
                    </thead>
                <?php
                    $lista = $obj_ca->Lista_Proyectos_Sustentar();
                    $temporal =NULL;
                    foreach ($lista as $list){ 
                        $id = $list['Id_Proyecto'];
                        $infoFaltantesP = $obj_ca -> Consultar_Proyectos_Faltantes_Fecha($id);
                        if($list['Id_Proyecto'] != $temporal && $infoFaltantesP['numero'] == 0){?>
                            <form action="Control/Asignar_Fecha_Sustentacion.php" method="POST" id="Asignar_Jurado" >
                                <tr>
                                    <td><center><?php echo ($list['Nombre_Proyecto']); ?>
                                        <?php $temporal = $list['Id_Proyecto'];?>
                                        </center>
                                    </td>
                                    <td style="display: none">
                                        <input type="text"  id="proyecto" name="proyecto"  value="<?php echo ($list['Id_Proyecto']); ?>" style="display: none">
                                    </td>
                                    <td><center>
                                        <input type="date" class="form-control" name="fecha" required >
                                        <div class="invalid-feedback">
                                            Por favor, introduzca una fecha valida.
                                        </div>
                                        </center>
                                    </td>
                                    <td>
                                        <select id="selectTipo"  required class="custom-select" name="lugar">
                                            <option selected value="" required>Seleccionar</option>
                                            <?php
                                            $obj = new DAO_Cronograma();
                                            $lista_a = $obj->Lista_Lugar();
                                            foreach ($lista_a as $list) { ?>
                                                <option value="<?php echo($list['Id_Lugar']); ?>"><?php echo($list['Lugar']); ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            Campo obligatorio.
                                        </div>
                                    </td>
                                    <td>
                                        <input type="time" class="form-control" name="hora" id="telefono" value="00:00" required>
                                        <div class="invalid-feedback" style="width: 100%;">
                                            La hora es necesaria.
                                        </div>
                                    </td>   
                                    <td><center>
                                        <input type="submit" name="Guardar" id="btn" class="btn btn-outline-danger" value="Guardar" onclick="return confirmar();"> 
                                    </td></center>
                                </tr>
                            </form>
            <?php   }}?>
                </table>       
            </div>
<?php }else{?>
        <div class ="container">
                <div class="card text-dark mb-1 bg-light color_cabecera mb-1" style="max-width: 50rem;">
                    <div class="card-body ">
                        <p><center><h6>NO HAY PROYECTOS PENDIENTES POR ASIGNACIÓN DE FECHA PARA SUSTENTAR</h6></center></p><hr>
                    </div>
                </div>
            </div>
    <?php }}else{
                session_destroy();
                echo "<script>location.href='index.php';</script>";
                } ?>
  
        
    </body>
</html>
