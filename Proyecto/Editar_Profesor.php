<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (!filter_input(INPUT_POST, "bot_pro")) {


    $cedula = htmlentities(addslashes(filter_input(INPUT_GET, "idp")));
    $nombre = htmlentities(addslashes(filter_input(INPUT_GET, "nombre")));
    $correo = htmlentities(addslashes(filter_input(INPUT_GET, "correo")));
    $tel = htmlentities(addslashes(filter_input(INPUT_GET, "tel")));
    $pertenece= htmlentities(addslashes(filter_input(INPUT_GET, "pertenece")));
}

if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <br>
        <?php
        if ($administrador != null) {
            ?>

            <!--Fin Navbar-->        

            <form action="Control/Editar_ProfesorA.php" method="POST">
                <table class="table" id="table">
                    <thead class="thead-dark">
                        <tr  align="center" valign="middle">
                           <!-- <th colspan="16"><h2><p style="color:white"><center>Actualizar Datos Profesor</center></p></h2></th></tr>-->
                        <tr>
                            <th scope="col">Cedula</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Ciencias Basicas</th>
                            <th scope="col"></th>

                        </tr>
                    </thead>

                    <tr>


                        <th ><?php echo $cedula ?> </th>
                        <td  hidden><input type="hidden" name="cedula" value="<?php echo $cedula ?>"></td>
                        <td>
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <input type="email" class="form-control"  id="correo" name="correo" placeholder="" value="<?php echo $correo; ?>" required value="<?php echo $correo ?>"> 
                                    <div class="invalid-feedback">
                                        Por favor, introduzca una dirección de correo electrónico válida.
                                    </div>

                                </div>

                            </div>
                        </td>
                        <th ><?php echo $nombre ?> </th>
                        <td  hidden><input type="hidden" name="nombre" pattern="[A-Z a-z]{8,50}" value="<?php echo $nombre ?>"></td>

                        <td>
                            <div class="row">
                                
                                
                               <div class="col-md-6 mb-3">
                            
                            <input type="text" pattern="[0-9]{10,10}" title="Formato 10 nùmeros" class="form-control" name="tel" id="telefono" placeholder="3221112222" maxlength="10" minlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" 
                              required value="<?php echo $tel?>" >
                            
                        </div>

                            </div>
                        </td>

                        <td>
                           <div class="row">
                                <div class="col-md-6 mb-3">


                                    <select id="selectTipo" class="custom-select" required name="pertene" style="width:120px" class='centrado' required>
                                        <option required value="00"><?php echo('No Aplica'); ?></option>
                                        <option required value="01"><?php echo('Aplica'); ?></option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Campo obligatorio.
                                    </div>
                                </div>


                            </div>
                        </td>

                        <td>
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <input type="submit" class="btn btn-primary mb-2" id="btn-aceptarR" name="bot_pro" value="Actualizar">
                                </div>
                            </div>
                        </td>
                    </tr>

                </table>
            </form>
            <?php
        } else {
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
        ?>
    </body>
</html>

