<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
if (isset($_SESSION['administrador'])) {
    $administrador = $_SESSION['administrador'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['comite'])) {
    $comite = $_SESSION['comite'];
    $coordinacion = NULL;
    $estudiante = NULL;
    $administrador = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['coordinacion'])) {
    $coordinacion = $_SESSION['coordinacion'];
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['estudiante'])) {
    $estudiante = $_SESSION['estudiante'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $profesor = NULL;
} else if (isset($_SESSION['profesor'])) {
    $profesor = $_SESSION['profesor'];
    $coordinacion = NULL;
    $administrador = NULL;
    $comite = NULL;
    $estudiante = NULL;
} else {
    $administrador = NULL;
    $estudiante = NULL;
    $comite = NULL;
    $coordinacion = NULL;
    $profesor = NULL;
}
?>

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Agregar Coordinación</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 

    </head>
    <body>
        <?php
        if ($administrador != null) {
            ?>

        <div class="py-5 text-center" id="centrar">
                <h3><center>AGREGAR COORDINACIÓN</center></h3>
        </div>
        <div class="col-md-8 order-md-1 container">
            <form action="Control/Agregar_Persona.php" method="POST" id="form-registro" >
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName" style="color: black">Nombre Completo</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" pattern="[A-Z a-záéíóú]{8,50}" required>
                        <div class="invalid-feedback">
                            Los nombres son obligatorios.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="email" style="color: black">Email <span class="text-muted"></span></label>
                        <input type="email" class="form-control" name="email" required placeholder="you@example.com">
                        <div class="invalid-feedback">
                            Por favor, introduzca una dirección de correo electrónico válida.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="cedula" style="color: black">Cédula</label>
                        <input type="number" class="form-control" id="cedula "name="cedula" title="un número de cedula correcto No tiene puntos,espacios ni letras" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            El número de documento es necesario
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="telefono" style="color: black">Número celular</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" pattern="[0-9]{10,10}" title="Formato 10 nùmeros" class="form-control" name="telefono" id="telefono" placeholder="3221112222" maxlength="10" minlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" 
                              required>
                            
                            <input type="text" value="COOR" class="form-control" name="iden" hidden>
                            <div class="invalid-feedback" style="width: 100%;">
                                El usuario es obligatorio.
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="col-md-8 mb-1">
                <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Información</button>
            </form>
        </div>
        <!--
        </div>
    </div>
        -->
        <?php
    } else {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
    ?>
</body>
</html>
