<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once './home.php';
require_once './Modelo/DAO_Proyecto.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css" >

        <link rel="stylesheet" href="css/estilos.css">
        <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet"> 
    </head>
    <body><br>
        <?php
        if($administrador!=null){
        
        ?>


       
            <div class="py-5 text-center">

                <p><center>Ingrese la información en todos los campos</center></p>
            </div>

            
                <div class="col-md-8 order-md-1 container">

                    <!-- class="needs-validation" novalidate-->
                    <form action="Control/Agregar_Cronograma.php" method="POST" id="form-registro" >
                        <div class="row">
                           
                            <div class="col-md-6 mb-3">
                                <label for="email" style="color: black">Fecha de Inicio <span class="text-muted"></span></label>
                                <input type="date" class="form-control" name="fechai" required placeholder="you@example.com">
                                <div class="invalid-feedback">
                                    Por favor, introduzca una fecha valida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="docuemnto" style="color: black">Fecha Final</label>
                                <input type="date" class="form-control" name="fechaf" placeholder="" value="" required>
                                <div class="invalid-feedback">
                                      Por favor, introduzca una fecha valida.
                                </div>
                            </div>

                        </div>
                        <hr class="col-md-8 mb-1">
                        <button class="btn btn-primary btn-lg btn-block" id="btn-aceptarR" type="submit">Enviar Informacion</button>

                        <div class="modal-body">
                            <p></p>
                            <hr>
                            <p></p>
                        </div>
                    </form>
                </div>
        <?php
        }else{
            session_destroy();
            echo "<script>location.href='index.php';</script>";
        }
?>
            
    </body>
</html>
