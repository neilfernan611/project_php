<!DOCTYPE html>
<?php
require_once './home.php';
?>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <title></title>
    </head>
    <body>
       <?php if ($comite != null) { ?>
       
       <?php
            if (!($_GET)) {

                echo "<script>location.href='tabla_comite.php?pagina=1';</script>";
            }
            ?>
            <div class="container-fluid">
                <?php
                require_once './Modelo/DAO_Anteproyecto.php';
                $pdf = new DAO_Anteproyecto();
                ?>
                    <br>
                    <table class="table" id="table">
                        <thead class="thead-dark">
                            <tr  align="center" valign="middle">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Titulo del Anteproyecto</th>
                                <th scope="col">Estado del Comite</th>
                                <th scope="col">Ver</th>
                            </tr>
                        </thead>
                        <?php
                        $proy = new DAO_Anteproyecto();
                        $lista1 = $proy->Consultar_Anteproyectos_Comite();
                        $id_2 = "calificar";
                        
                    $numero_de_registros = count($lista1);
                    $registros_x_pagina = 10;
                    $paginas = ceil($numero_de_registros / $registros_x_pagina);
                    if (filter_input(INPUT_GET, 'pagina') > $paginas || filter_input(INPUT_GET, 'pagina') <= 0) {
                        echo "<script>location.href='tabla_comite.php?pagina=1';</script>";
                    }
                    $iniciar = ((filter_input(INPUT_GET, 'pagina')) - 1) * $registros_x_pagina;
                    $lista2 = $proy->Consultar_Anteproyectos_Comite_paginacion($iniciar, $registros_x_pagina);
                    
                        foreach ($lista2 as $lista) {
                            if ($lista['Estado_Comite'] == "Por Evaluar") {
                        ?>

                                <tr style="background-color: #cccccc">
                                    <th scope="col"> <?php echo($lista["Id_Proyecto"]); ?></th>
                                    <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                    <th scope="col"><?php echo($lista["Estado_Comite"]); ?>  </th>
                                    <th class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>&estado=<?php echo($id_2); ?>">Ver</a></th>
                                </tr>
                            <?php
                            } else {
                            ?>
                                <tr style="background-color: #FBFBE6">
                                    <th scope="col"> <?php echo($lista["Id_Proyecto"]); ?></th>
                                    <th scope="col"> <?php echo($lista["Nombre_Proyecto"]); ?> </th>
                                    <th scope="col"><?php echo($lista["Estado_Comite"]); ?>  </th>
                                    
                                    <?php 
                                    if($lista['Estado_Comite'] == "Avalado"){
                                    ?>
                                    <th class="bot"><a href="Informacion_proyectos.php?id=<?php echo($lista["Id_Proyecto"]); ?>&estado=<?php echo($id_2); ?>">Ver</a></th>
                                    <?php 
                                    }else{
                                    ?>
                                    <th class="bot"><a href="Informacion_anteproyectos_devueltos.php?id=<?php echo($lista["Id_Proyecto"]); ?>">Ver</a></th>
                                    <?php } 
                                    ?>
                                </tr>
                            <?php
                            }
                        }
                            ?>
                    </table>
                        <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') <= 1 ? 'disabled' : ''); ?>"><a class="page-link" href="tabla_comite.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') - 1); ?>">Anterior</a></li>
                    <?php for ($i = 0; $i < $paginas; $i++): ?>
                        <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') == $i + 1) ? 'active' : '' ?>"><a class="page-link" href="tabla_comite.php?pagina=<?php echo($i + 1); ?>"><?php echo($i + 1); ?></a></li>
                        <?php
                    endfor;
                    ?>

                    <li class="page-item <?php echo (filter_input(INPUT_GET, 'pagina') >= $paginas ? 'disabled' : '') ?>"><a class="page-link" href="tabla_comite.php?pagina=<?php echo(filter_input(INPUT_GET, 'pagina') + 1); ?>">Siguiente</a></li>
                </ul>
            </nav>
                </div>
        <?php
        }
        ?>
    </body>
</html>
